$(document).ready(function () {
    if ([1, 5, 6].includes(masterCategoryId)) {
        var rules = {
            sub_category: {
                required: true,
            },
        };
        var messages = {
            sub_category: {
                required: label + " is required",
            },
        };
    } else {
        var rules = {};
        var messages = {};
        const languages = JSON.parse(allLanguages);

        languages.forEach((language) => {
            let inputName = "sub_category_" + language.id;

            rules[inputName] = {
                required: true,
            };
            messages[inputName] = {
                required: label + " (" + language.title + ")" + " is required",
            };
        });
    }

    $("#regForm").validate({
        rules: rules,
        messages: messages,
    });
});
