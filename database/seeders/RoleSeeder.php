<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles =
            [
                [
                    "name" => "Super Admin",
                    "created_at" => now()
                ],
                [
                    "name" => "Doctor",
                    "created_at" => now()
                ],
                [
                    "name" => "Patient",
                    "created_at" => now()
                ],
                [
                    "name" => "Doctor Admin",
                    "created_at" => now()
                ],
                [
                    "name" => "Patient Admin",
                    "created_at" => now()
                ]
            ];
        Role::insert($roles);
    }
}
