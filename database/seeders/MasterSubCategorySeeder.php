<?php

namespace Database\Seeders;

use App\Models\MasterCategory;
use App\Models\MasterSubCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterSubCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $certificateJson = public_path('json/certificate.json');
        $certificates = json_decode(file_get_contents($certificateJson), true);

        foreach ($certificates as $key => $value) {
            $masterCategory = new MasterSubCategory();
            $masterCategory->master_category_id = 1;
            $masterCategory->label = $value['certification'];
            $masterCategory->is_other = 0;
            $masterCategory->is_approved = 1;
            $masterCategory->order_no = $key + 1;
            $masterCategory->save();
        }

        $specialtyJson = public_path('json/specialty.json');
        $specialties = json_decode(file_get_contents($specialtyJson), true);

        foreach ($specialties as $key => $value) {
            $masterCategory = new MasterSubCategory();
            $masterCategory->master_category_id = 3;
            $masterCategory->label = $value['specialty'];
            $masterCategory->is_other = 0;
            $masterCategory->is_approved = 1;
            $masterCategory->order_no = $key + 1;
            $masterCategory->save();
        }

        $symptomJson = public_path('json/symptom.json');
        $symptoms = json_decode(file_get_contents($symptomJson), true);

        foreach ($symptoms as $key => $value) {
            $masterCategory = new MasterSubCategory();
            $masterCategory->master_category_id = 4;
            $masterCategory->label = $value['symptom'];
            $masterCategory->is_other = 0;
            $masterCategory->is_approved = 1;
            $masterCategory->order_no = $key + 1;
            $masterCategory->save();
        }

        $languageJson = public_path('json/language.json');
        $languages = json_decode(file_get_contents($languageJson), true);

        foreach ($languages as $key => $value) {
            $masterCategory = new MasterSubCategory();
            $masterCategory->master_category_id = 5;
            $masterCategory->label = $value['name'];
            $masterCategory->value = $value['code'];
            $masterCategory->is_other = 0;
            $masterCategory->is_approved = 1;
            $masterCategory->order_no = $key + 1;
            $masterCategory->save();
        }

        $procedureJson = public_path('json/procedure.json');
        $procedures = json_decode(file_get_contents($procedureJson), true);

        foreach ($procedures as $key => $value) {
            $masterCategory = new MasterSubCategory();
            $masterCategory->master_category_id = 6;
            $masterCategory->label = $value['procedure'];
            $masterCategory->is_other = 0;
            $masterCategory->is_approved = 1;
            $masterCategory->order_no = $key + 1;
            $masterCategory->save();
        }
    }
}
