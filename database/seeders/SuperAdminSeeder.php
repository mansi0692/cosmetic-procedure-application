<?php

namespace Database\Seeders;

use App\Models\ModelHasRole;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class SuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdmin =
            [
                [
                    "email" => "superadmin@yumme.com",
                    'country_code' => "+91",
                    "password" => Hash::make("Yume@2022"),
                    "phone_number" => NULL,
                    "role_id" => 1,
                    "status" => 1,
                    'is_verified' => 1,
                    "social_account_type" => NULL,
                    "social_account_token" => NULL,
                    "email_verified_at" => "2022-04-25T05:58:14.000000Z",
                    "otp_count" => 0,
                    "created_by" => NULL,
                    "created_at" => "2022-04-25T05:58:14.000000Z",
                    "updated_by" => NULL,
                    "updated_at" => "2022-04-25T05:58:14.000000Z",
                    "deleted_by" => NULL,
                    "deleted_at" => NULL
                ],
                [
                    "email" => "doctoradmin@yumme.com",
                    'country_code' => "+91",
                    "password" => Hash::make("Yume@2022"),
                    "phone_number" => NULL,
                    "role_id" => 1,
                    "status" => 1,
                    'is_verified' => 1,
                    "social_account_type" => NULL,
                    "social_account_token" => NULL,
                    "email_verified_at" => "2022-04-25T05:58:14.000000Z",
                    "otp_count" => 0,
                    "created_by" => NULL,
                    "created_at" => "2022-04-25T05:58:14.000000Z",
                    "updated_by" => NULL,
                    "updated_at" => "2022-04-25T05:58:14.000000Z",
                    "deleted_by" => NULL,
                    "deleted_at" => NULL
                ],
                [
                    "email" => "patientadmin@yume.com",
                    'country_code' => "+91",
                    "password" => Hash::make("Yume@2022"),
                    "phone_number" => NULL,
                    "role_id" => 1,
                    "status" => 1,
                    'is_verified' => 1,
                    "social_account_type" => NULL,
                    "social_account_token" => NULL,
                    "email_verified_at" => "2022-04-25T05:58:14.000000Z",
                    "otp_count" => 0,
                    "created_by" => NULL,
                    "created_at" => "2022-04-25T05:58:14.000000Z",
                    "updated_by" => NULL,
                    "updated_at" => "2022-04-25T05:58:14.000000Z",
                    "deleted_by" => NULL,
                    "deleted_at" => NULL
                ]
            ];
        User::insert($superAdmin);

        $modelRole = [
            [
                'role_id' => 1,
                'model_type' => 'App\Models\User',
                'model_id' => 2
            ],
            [
                'role_id' => 4,
                'model_type' => 'App\Models\User',
                'model_id' => 3
            ],
            [
                'role_id' => 5,
                'model_type' => 'App\Models\User',
                'model_id' => 4
            ]
        ];
        ModelHasRole::insert($modelRole);
    }
}
