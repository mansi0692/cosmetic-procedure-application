<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\NotificationTemplate;

class NotificationTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $english = [
            [
                'template_name' => 'appointment_reminder_title',
                'language_id' => 1,
                'message' => 'Appointment Reminder',
            ],
            [
                'template_name' => 'appointment_update_title',
                'language_id' => 1,
                'message' => 'Appointment Update',
            ],
            [
                'template_name' => 'appointment_booking_title',
                'language_id' => 1,
                'message' => 'Appointment Booking',
            ],
            [
                'template_name' => 'appointment_cancelled_title',
                'language_id' => 1,
                'message' => 'Appointment Cancelled',
            ],
            [
                'template_name' => 'appointment_update_doctor',
                'language_id' => 1,
                'message' => 'Your appointment with {{patientName}} was {{appointmentStatus}} for {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_doctor_video',
                'language_id' => 1,
                'message' => 'Your video visit with {{patientName}} was {{appointmentStatus}} for {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_patient',
                'language_id' => 1,
                'message' => 'Your appointment with Dr {{doctorName}} was {{appointmentStatus}} for {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_patient_video',
                'language_id' => 1,
                'message' => 'Your video visit with Dr {{doctorName}} was {{appointmentStatus}} for {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_doctor',
                'language_id' => 1,
                'message' => 'You have received an appointment request by {{patientName}} for {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_doctor_video',
                'language_id' => 1,
                'message' => 'You have received a video visit request by {{patientName}} for {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_patient',
                'language_id' => 1,
                'message' => 'Your appointment is being reviewed by the office of Dr {{doctorName}} for {{appointmentTiming}}',
            ],            
            [
                'template_name' => 'appointment_booking_patient_video',
                'language_id' => 1,
                'message' => 'Your video visit is being reviewed by the office of Dr {{doctorName}} for {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_reschedule_request_doctor',
                'language_id' => 1,
                'message' => 'Your appointment with {{patientName}} has been requested to be rescheduled for {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_reschedule_request_patient',
                'language_id' => 1,
                'message' => 'Your appointment with Dr {{doctorName}} has been requested to be rescheduled for {{appointmentTiming}}',
            ],
        ];

        $arabic = [
            [
                'template_name' => 'appointment_reminder_title',
                'language_id' => 2,
                'message' => 'منبه مواعيد',
            ],
            [
                'template_name' => 'appointment_update_title',
                'language_id' => 2,
                'message' => 'تحديث الموعد',
            ],
            [
                'template_name' => 'appointment_booking_title',
                'language_id' => 2,
                'message' => 'حجز موعد',
            ],
            [
                'template_name' => 'appointment_cancelled_title',
                'language_id' => 2,
                'message' => 'تم إلغاء الموعد',
            ],
            [
                'template_name' => 'appointment_update_doctor',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} لمدة {{appointmentStatus}} كان {{patientName}} موعدك مع',
            ],
            [
                'template_name' => 'appointment_update_doctor_video',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} لمدة {{appointmentStatus}} كان {{patientName}} زيارة الفيديو الخاص بك مع',
            ],
            [
                'template_name' => 'appointment_update_patient',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} لمدة {{appointmentStatus}} كان {{doctorName}} موعدك مع د',
            ],
            [
                'template_name' => 'appointment_update_patient_video',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} لمدة {{appointmentStatus}} كان {{doctorName}} زيارتك بالفيديو مع د',
            ],
            [
                'template_name' => 'appointment_booking_doctor',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} لمدة {{patientName}} لقد تلقيت طلب موعد من',
            ],
            [
                'template_name' => 'appointment_booking_doctor_video',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} لمدة {{patientName}} لقد تلقيت طلب زيارة بالفيديو عن طريق',
            ],
            [
                'template_name' => 'appointment_booking_patient',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} لمدة {{doctorName}} يتم مراجعة موعدك من قبل مكتب د',
            ],            
            [
                'template_name' => 'appointment_booking_patient_video',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} لمدة {{doctorName}} تتم مراجعة زيارتك بالفيديو من قبل مكتب د',
            ],
            [
                'template_name' => 'appointment_reschedule_request_doctor',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} في {{patientName}} تم طلب إعادة تحديد موعدك مع',
            ],
            [
                'template_name' => 'appointment_reschedule_request_patient',
                'language_id' => 2,
                'message' => '{{appointmentTiming}} ليوم {{doctorName}} تم طلب إعادة تحديد موعدك مع دكتور',
            ],
        ];

        $french = [
            [
                'template_name' => 'appointment_reminder_title',
                'language_id' => 3,
                'message' => 'Rappel de rendez-vous',
            ],
            [
                'template_name' => 'appointment_update_title',
                'language_id' => 3,
                'message' => 'Mise à jour du rendez-vous',
            ],
            [
                'template_name' => 'appointment_booking_title',
                'language_id' => 3,
                'message' => 'Prise de rendez-vous',
            ],
            [
                'template_name' => 'appointment_cancelled_title',
                'language_id' => 3,
                'message' => 'Rendez-vous annulé',
            ],
            [
                'template_name' => 'appointment_update_doctor',
                'language_id' => 3,
                'message' => 'Votre rendez-vous avec {{patientName}} a été {{appointmentStatus}} pour {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_doctor_video',
                'language_id' => 3,
                'message' => 'Votre visite en vidéo avec {{patientName}} a été {{appointmentStatus}} pour {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_patient',
                'language_id' => 3,
                'message' => 'Votre rendez-vous avec le Dr {{doctorName}} a été {{appointmentStatus}} pour {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_patient_video',
                'language_id' => 3,
                'message' => 'Votre visite vidéo avec le Dr {{doctorName}} a été {{appointmentStatus}} pour {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_doctor',
                'language_id' => 3,
                'message' => 'Vous avez reçu une demande de rendez-vous par {{patientName}} pour {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_doctor_video',
                'language_id' => 3,
                'message' => 'Vous avez reçu une demande de visite vidéo par {{patientName}} pour {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_patient',
                'language_id' => 3,
                'message' => 'Votre rendez-vous est en cours d\'examen par le bureau du Dr {{doctorName}} pour {{appointmentTiming}}',
            ],            
            [
                'template_name' => 'appointment_booking_patient_video',
                'language_id' => 3,
                'message' => 'Votre visite vidéo est en cours d\'examen par le bureau du Dr {{doctorName}} pour {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_reschedule_request_doctor',
                'language_id' => 3,
                'message' => 'Votre rendez-vous avec {{patientName}} a été demandé de reporter à {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_reschedule_request_patient',
                'language_id' => 3,
                'message' => 'Votre rendez-vous avec le Dr {{doctorName}} a été demandé de reporter à {{appointmentTiming}}',
            ],
        ];

        $spanish = [
            [
                'template_name' => 'appointment_reminder_title',
                'language_id' => 4,
                'message' => 'Recordatorio de cita',
            ],
            [
                'template_name' => 'appointment_update_title',
                'language_id' => 4,
                'message' => 'Actualización de cita',
            ],
            [
                'template_name' => 'appointment_booking_title',
                'language_id' => 4,
                'message' => 'Reserva de cita',
            ],
            [
                'template_name' => 'appointment_cancelled_title',
                'language_id' => 4,
                'message' => 'Cita cancelada',
            ],
            [
                'template_name' => 'appointment_update_doctor',
                'language_id' => 4,
                'message' => 'Tu cita con {{patientName}} fue {{appointmentStatus}} para el {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_doctor_video',
                'language_id' => 4,
                'message' => 'Tu video visita con {{patientName}} fue {{appointmentStatus}} para el {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_patient',
                'language_id' => 4,
                'message' => 'Su cita con el Dr. {{doctorName}} fue {{appointmentStatus}} para el {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_patient_video',
                'language_id' => 4,
                'message' => 'Tu video visita con el Dr. {{doctorName}} fue {{appointmentStatus}} para el {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_doctor',
                'language_id' => 4,
                'message' => 'Ha recibido una solicitud de cita por {{patientName}} para el {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_doctor_video',
                'language_id' => 4,
                'message' => 'Ha recibido una solicitud de visita por vídeo de {{patientName}} para el {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_patient',
                'language_id' => 4,
                'message' => 'Su cita está siendo revisada por la oficina del Dr. {{doctorName}} para el {{appointmentTiming}}',
            ],            
            [
                'template_name' => 'appointment_booking_patient_video',
                'language_id' => 4,
                'message' => 'Su visita por video está siendo revisada por la oficina del Dr. {{doctorName}} para el {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_reschedule_request_doctor',
                'language_id' => 4,
                'message' => 'Tu cita con {{patientName}} se ha solicitado su reprogramación para {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_reschedule_request_patient',
                'language_id' => 4,
                'message' => 'Su cita con el P. {{doctorName}} se ha solicitado su reprogramación para {{appointmentTiming}}',
            ],
        ];

        $russian = [
            [
                'template_name' => 'appointment_reminder_title',
                'language_id' => 5,
                'message' => 'Напоминание о встрече',
            ],
            [
                'template_name' => 'appointment_update_title',
                'language_id' => 5,
                'message' => 'Обновление назначения',
            ],
            [
                'template_name' => 'appointment_booking_title',
                'language_id' => 5,
                'message' => 'Запись на прием',
            ],
            [
                'template_name' => 'appointment_cancelled_title',
                'language_id' => 5,
                'message' => 'Назначение отменено',
            ],
            [
                'template_name' => 'appointment_update_doctor',
                'language_id' => 5,
                'message' => 'Ваша встреча с {{patientName}} была {{appointmentStatus}} на {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_doctor_video',
                'language_id' => 5,
                'message' => 'Ваш видео визит с {{patientName}} была {{appointmentStatus}} на {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_patient',
                'language_id' => 5,
                'message' => 'Ваша встреча с доктором {{doctorName}} была {{appointmentStatus}} на {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_update_patient_video',
                'language_id' => 5,
                'message' => 'Ваш видеовизит с доктором {{doctorName}} была {{appointmentStatus}} на {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_doctor',
                'language_id' => 5,
                'message' => 'Вы получили запрос на встречу от {{patientName}} на {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_doctor_video',
                'language_id' => 5,
                'message' => 'Вы получили запрос видеопосещения от {{patientName}} на {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_booking_patient',
                'language_id' => 5,
                'message' => 'Ваше назначение рассматривается офисом доктора {{doctorName}} на {{appointmentTiming}}',
            ],            
            [
                'template_name' => 'appointment_booking_patient_video',
                'language_id' => 5,
                'message' => 'Ваше видео-посещение рассматривается офисом доктора {{doctorName}} на {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_reschedule_request_doctor',
                'language_id' => 5,
                'message' => 'Ваша встреча с {{patientName}} была запрошена перепланировка на {{appointmentTiming}}',
            ],
            [
                'template_name' => 'appointment_reschedule_request_patient',
                'language_id' => 5,
                'message' => 'Ваша встреча с доктором {{doctorName}} была запрошена перепланировка на {{appointmentTiming}}',
            ],
        ];

        foreach (array_merge($english, $arabic, $french, $spanish, $russian) as $row) {
            $newRow = new NotificationTemplate();
            $newRow->language_id = $row['language_id'];
            $newRow->template_name = $row['template_name'];
            $newRow->message = $row['message'];
            $newRow->save();
        }
    }
}
