<?php

namespace Database\Seeders;

use App\Models\EmailTemplate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EmailTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $template = [
            [
                'id' => 1,
                'template_name' => 'document_upload_notification',
                'template_subject' => 'Document Upload Notification',
                'template_body' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                <tbody>
                    <tr>
                        <td style="background-color:#ffffff; border-width:0px; vertical-align:top">
                        <table border="0" cellpadding="0" cellspacing="2" style="width:100%">
                            <tbody>
                                <tr>
                                    <td colspan="2"><strong>Hello Admin</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">[doctor_name] has uploaded a proof document [document_name]. Kindly login to admin platform and verify it..</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Thanks</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Yume</td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            ',
                'status' => 1,
                'created_by' => 0,
            ],
            [
                'id' => 2,
                'template_name' => 'account_status_change_notification',
                'template_subject' => 'Account Status Change Notification',
                'template_body' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                <tbody>
                    <tr>
                        <td style="background-color:#ffffff; border-width:0px; vertical-align:top">
                        <table border="0" cellpadding="0" cellspacing="2" style="width:100%">
                            <tbody>
                                <tr>
                                    <td colspan="2"><strong>Hello [first_name]</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2"><p> This is to inform you that your profile has been <strong> [status] </strong> by the admin.</p></td>
                                </tr>
                                <tr>
                                <td colspan="2">If you have any questions, feel free to Contact Us.</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Thanks</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Yume</td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>',
                'status' => 1,
                'created_by' => 0,
            ],
            [
                'id' => 3,
                'template_name' => 'admin_create_notification',
                'template_subject' => 'Admin Create Notification',
                'template_body' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                <tbody>
                    <tr>
                        <td style="background-color:#ffffff; border-width:0px; vertical-align:top">
                        <table border="0" cellpadding="0" cellspacing="2" style="width:100%">
                            <tbody>
                                <tr>
                                    <td colspan="2"><strong>Hello,</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">You have been invited as administrator for Yume platform.</td>
                                </tr>
                                <tr>
                                     <td colspan="2">Login to the <a href="[adminLoginUrl]">admin panel</a> and check out the features.</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Thanks</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Yume</td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
             ',
                'status' => 1,
                'created_by' => 0,
            ],
            [
                'id' => 4,
                'template_name' => 'cancel_request_approve_notification',
                'template_subject' => 'Cancel Request Approve Notification',
                'template_body' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                <tbody>
                    <tr>
                        <td style="background-color:#ffffff; border-width:0px; vertical-align:top">
                        <table border="0" cellpadding="0" cellspacing="2" style="width:100%">
                            <tbody>
                                <tr>
                                    <td colspan="2"><strong>Hello [first_name],</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Your cancel request has been approved by admin. Your account is suspended now and all your details have been removed from the system.</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                     <td colspan="2">You can no longer now access the platform.</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                     <td colspan="2">It was a pleasure to serve you for the time you have been in the platform.</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                     <td colspan="2">Hope to see you again in future !!</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Thanks</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Yume</td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
             ',
                'status' => 1,
                'created_by' => 0,
            ],
            [
                'id' => 5,
                'template_name' => 'contact_us_reply_notification',
                'template_subject' => 'Contact Us Reply Notification',
                'template_body' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                <tbody>
                    <tr>
                        <td style="background-color:#ffffff; border-width:0px; vertical-align:top">
                        <table border="0" cellpadding="0" cellspacing="2" style="width:100%">
                            <tbody>
                                <tr>
                                    <td colspan="2"><strong>Hello [first_name],</strong></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Admin has sent below reply for your message :-</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                     <td colspan="2"><strong> Message : </strong> [message]</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                     <td colspan="2"><strong> Reply : </strong> [content]</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Thanks</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Yume</td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
             ',
                'status' => 1,
                'created_by' => 0,
            ],
            [
                'id' => 7,
                'template_name' => 'doctor_approve_notification',
                'template_subject' => 'Doctor Approve Notification',
                'template_body' => '<table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%">
                <tbody>
                    <tr>
                        <td style="background-color:#ffffff; border-width:0px; vertical-align:top">
                        <table border="0" cellpadding="0" cellspacing="2" style="width:100%">
                            <tbody>
                                <tr>
                                    <td colspan="2">Your application has been verified successfully.</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                    <tr>
                                    <td colspan="2">You can now login to the platform and start exploring it. Use the below credentials to <a href="[doctorLoginUrl]" type="submit" class="btn add-btn">login</a> :-</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Email address :- [email]</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Thanks</td>
                                </tr>
                                <tr>
                                    <td colspan="2">Yume</td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
             ',
                'status' => 1,
                'created_by' => 0,
            ],
            [
                'id' => 9,
                'template_name' => 'appointment_pending',
                'template_subject' => 'Appointment Pending',
                'template_body' => '<table align="center" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td height="15px"></td>
    </tr>
    <tr>
        <td>
            <table align="center" cellspacing="0" cellpadding="0" border="0" width="600" bgcolor="#ffffff" style="width: 600px; margin: 0 auto;">
                <tbody>
                <tr>
                    <td width="46"></td>
                    <td valign="top" style="padding:1em 0;padding-top:66px">
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                            <tr>
                                <td width="40%" style="text-align:left;vertical-align:top">
                                    <a href="[siteurl]" target="_blank">
                                        <img src="logo1.png" height="40" alt="">
                                    </a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="46"></td>
                </tr>
                <tr>
                    <td width="46"></td>
                    <td style="padding-top:0px;padding-bottom:40px">
                        <table role="presentation" cellspacing="0" cellpadding="0" border="0" width="100%">
                            <tbody>
                            <tr>
                                <td>
                                    <table role="presentation" border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                        <tr>
                                            <td style="padding-bottom:10px">
                                                <div style="font-family:\'Heldane Display\';font-size:36px;font-weight:normal;font-stretch:normal;font-style:normal;line-height:1.83;letter-spacing:normal;color:#013648">
                                                    Your Appointment is Pending
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-bottom:solid 1px rgba(32,58,112,0.08);padding-bottom:17px">
                                                <table>
                                                    <tbody>
                                                    <tr style="vertical-align:top">
                                                        <td width="156" height="156" style="width: 156px; vertical-align: middle; text-align: center;height:156px;border-radius:5px;border: solid 2px #6c919d;color: #013648;">
                                                            <div style="font-size:16px;font-family:\'Open Sans\',sans-serif;font-size:24px;display: block !important;">
                                                                <div style="font-family: \'Open Sans\', sans-serif; font-weight: 600;  font-size: 20px;">[appointmentDayName]</div>
                                                                <div style="font-size: 20px;">[appointmentMonthDate]</div>
                                                                <div style="font-size: 20px;">[appointmentTime]</div>
                                                            </div>
                                                        </td>
                                                        <td style="padding-left:19px">
                                                            <table>
                                                                <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div style="padding-left:5px;font-family:\'Open Sans\',sans-serif;font-size:15px;font-weight:normal;font-stretch:normal;font-style:normal;line-height:1.5;letter-spacing:normal;color:#013648">
                                                                            Hi [patientFirstName]. Your appointment will be confirmed by the office of
                                                                            <a href="{{providerSeoUrl}}" style="font-family:\'Open Sans\',sans-serif;font-size:15px;letter-spacing:normal;color:#3dbce7;text-decoration:none" target="_blank">
                                                                                [providerFirstName] [providerLastName][providerSuffix]
                                                                            </a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding-top: 6px;">
                                                                        <div style="padding-left:5px;font-family:\'Open Sans\',sans-serif;font-size:15px;font-weight:600;font-stretch:normal;font-style:normal;line-height:1.5;letter-spacing:normal;color:#013648;">
                                                                            You will receive an email with your confirmation.
                                                                        </div>
                                                                        <div style="padding-left:5px;font-family:\'Open Sans\',sans-serif;font-size:15px;font-weight:normal;font-stretch:normal;font-style:normal;line-height:1.5;letter-spacing:normal;color:#013648;">
                                                                            <a href="[patientSignInUrl]" style="font-family:\'Open Sans\',sans-serif;font-size:15px;letter-spacing:normal;color:#3dbce7;text-decoration:none" target="_blank">
                                                                                Sign in
                                                                            </a>
                                                                            to modify or cancel your appointment.
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-bottom:solid 1px rgba(32,58,112,0.08);padding-top:28px">
                                                <table>
                                                    <tbody>
                                                    <tr style="vertical-align:top">
                                                        <td width="120" height="179px">
                                                            <img src="[providerProfilePhoto]" alt="Doctor Image"  width="100%" style="width:100%;border:solid 1px rgba(1,54,72,0.2);border-radius:4px;overflow:hidden">
                                                        </td>
                                                        <td style="padding-left:20px">
                                                            <table>
                                                                <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div style="font-family:\'Open Sans\',sans-serif;font-size:22px;padding-bottom:5px;line-height:1.5;color:#013648">
                                                                            [providerFirstName] [providerLastName] [providerSuffix]
                                                                        </div>
                                                                        <div style="font-family:\'Open Sans\',sans-serif;font-size:16px;line-height:1.4;letter-spacing:-0.24px;color:#013648">
                                                                            [providerSpecialty]
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="padding-top:28px;vertical-align:top">
                                                                        <table>
                                                                            <tbody>
                                                                            <tr style="vertical-align:top">
                                                                                <td style="padding-top:5px">
                                                                                    <img src="{{imgUrl}}maps-and-location.png" alt="location"/>
                                                                                </td>
                                                                                <td style="padding-left:15px;padding-bottom:18px">
                                                                                    <div style="font-family:\'Open Sans\',sans-serif;font-size:15px;line-height:1.5;letter-spacing:-0.23px;color:#72787d">
                                                                                        [providerLocationName],<br>
                                                                                        [providerAddress]<br>
                                                                                        <div>[providerPhoneNumberFormatted]</div>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>

                                                                            <tr style="vertical-align:top">
                                                                                <td style="padding-top:5px">
                                                                                    <img src="{{imgUrl}}user-icon.png" alt="user"/>
                                                                                </td>
                                                                                <td style="padding-left:15px;padding-bottom:22px">
                                                                                    <div style="font-family:\'Open Sans\',sans-serif;font-size:16px;line-height:1.17;letter-spacing:-0.23px;color:#72787d">
                                                                                        [patientFirstName]
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center;padding-top:39px;font-family:\'Open Sans\',sans-serif;font-size:16px;line-height:1.4;letter-spacing:-0.24px;color:#013648">
                                                <div> Need help with your booking?
                                                    <a href="{{contactUsUrl}}" style="font-size:16px;font-family:\'Open Sans\',sans-serif;line-height:1.25;letter-spacing:normal;color:#3dbce7;text-decoration:none" target="_blank">
                                                        Contact Us
                                                    </a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="padding-top:40px;font-family:\'Open Sans\',sans-serif;font-size:14px;line-height:1.5;text-align:center;color:#979ea6">
                                                <div>©<?php echo date("Y"); ?> Yume, All rights reserved.</div>
                                                <div style="font-family:\'Open Sans\',sans-serif;font-weight:600">
                                                    <a href="{{privacyPolicyUrl}}" style="color: #979ea6;text-decoration: none;">Privacy Policy</a>
                                                    |
                                                    <a href="{{termsUrl}}" style="color: #979ea6;text-decoration: none;">Terms of Use</a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="46"></td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td height="15px"></td>
    </tr>
</table>',
                'status' => 1,
                'created_by' => 0,
            ],
        ];
        EmailTemplate::insert($template);
    }
}
