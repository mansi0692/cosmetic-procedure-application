<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages=[
            [
            'id'=>1,
            'title'=>'English',
            'code'=>'en',
            'status'=>1
            ],
            [
            'id'=>2,
            'title'=>'Arabic',
            'code'=>'ar',
            'status'=>1
            ],
            [
            'id'=>3,
            'title'=>'French',
            'code'=>'fr',
            'status'=>1
            ],
            [
            'id'=>4,
            'title'=>'Spanish',
            'code'=>'sp',
            'status'=>1
            ],
            [
            'id'=>5,
            'title'=>'Russian',
            'code'=>'ru',
            'status'=>1
            ],
        ];

        Language::insert($languages);
    }
}
