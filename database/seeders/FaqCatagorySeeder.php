<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\FaqCategory;

class FaqCatagorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languages=[
            'English'=>'About,Data & Privacy,Appointments,Account,Review',
            'Arabic'=>'حول,خصوصية,البيانات,المواعيد,الحساب,المراجعة',
            'French'=>'À propos,Confidentialité des données,Rendez-vous,Compte,Révision',
            'Spanish'=>'Acerca de, Privacidad de datos, Citas, Cuenta, Revisión',
            'Russian'=>'О нас,Конфиденциальность данных,Встречи,Аккаунт,Обзор',
        ];

        $i=1;
        
        foreach($languages as $language){
            $temp=explode(',',$language);
            foreach ($temp as $key => $value) {
               $faqCatagory = new FaqCategory;
               $faqCatagory->name = $value;
               $faqCatagory->language_id = $i;
               $faqCatagory->save();
            }
            $i++;
            $temp=[];
        }
    }
}
