<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $states =
            [
                [
                    "id" => 1,
                    "country_id" => 1,
                    "name" => "New York",
                    "code" => "NY"
                ],
                [
                    "id" => 2,
                    "country_id" => 1,
                    "name" => "New Jersey",
                    "code" => "NJ"
                ],
                [
                    "id" => 3,
                    "country_id" => 1,
                    "name" => "Connecticut",
                    "code" => "CT"
                ],
                [
                    "id" => 4,
                    "country_id" => 1,
                    "name" => "Delaware",
                    "code" => "DE"
                ],
                [
                    "id" => 5,
                    "country_id" => 1,
                    "name" => "Florida",
                    "code" => "FL"
                ],
                [
                    "id" => 6,
                    "country_id" => 1,
                    "name" => "Georgia",
                    "code" => "GA"
                ],
                [
                    "id" => 7,
                    "country_id" => 1,
                    "name" => "Indiana",
                    "code" => "IN"
                ],
                [
                    "id" => 8,
                    "country_id" => 1,
                    "name" => "Kentucky",
                    "code" => "KY"
                ],
                [
                    "id" => 9,
                    "country_id" => 1,
                    "name" => "Maine",
                    "code" => "ME"
                ],
                [
                    "id" => 10,
                    "country_id" => 1,
                    "name" => "Maryland",
                    "code" => "MD"
                ],
                [
                    "id" => 11,
                    "country_id" => 1,
                    "name" => "Masachusettes",
                    "code" => "MA"
                ],
                [
                    "id" => 12,
                    "country_id" => 1,
                    "name" => "Michigan",
                    "code" => "MI"
                ],
                [
                    "id" => 13,
                    "country_id" => 1,
                    "name" => "New Hampshire",
                    "code" => "NH"
                ],
                [
                    "id" => 14,
                    "country_id" => 1,
                    "name" => "North Carolina",
                    "code" => "NC"
                ],
                [
                    "id" => 15,
                    "country_id" => 1,
                    "name" => "Ohio",
                    "code" => "OH"
                ],
                [
                    "id" => 16,
                    "country_id" => 1,
                    "name" => "Pennsylvania",
                    "code" => "PA"
                ],
                [
                    "id" => 17,
                    "country_id" => 1,
                    "name" => "Rhode Island",
                    "code" => "RI"
                ],
                [
                    "id" => 18,
                    "country_id" => 1,
                    "name" => "South Carolina",
                    "code" => "SC"
                ],
                [
                    "id" => 19,
                    "country_id" => 1,
                    "name" => "Vermont",
                    "code" => "VT"
                ],
                [
                    "id" => 20,
                    "country_id" => 1,
                    "name" => "Virginia",
                    "code" => "VA"
                ],
                [
                    "id" => 21,
                    "country_id" => 1,
                    "name" => "West Virginia",
                    "code" => "WV"
                ],
                [
                    "id" => 22,
                    "country_id" => 1,
                    "name" => "Alabama",
                    "code" => "AL"
                ],
                [
                    "id" => 23,
                    "country_id" => 1,
                    "name" => "Arkansas",
                    "code" => "AR"
                ],
                [
                    "id" => 24,
                    "country_id" => 1,
                    "name" => "Illionis",
                    "code" => "IL"
                ],
                [
                    "id" => 25,
                    "country_id" => 1,
                    "name" => "Iowa",
                    "code" => "IA"
                ],
                [
                    "id" => 26,
                    "country_id" => 1,
                    "name" => "Kansas",
                    "code" => "KA"
                ],
                [
                    "id" => 27,
                    "country_id" => 1,
                    "name" => "Louisiana",
                    "code" => "LA"
                ],
                [
                    "id" => 28,
                    "country_id" => 1,
                    "name" => "Minnesota",
                    "code" => "MN"
                ],
                [
                    "id" => 29,
                    "country_id" => 1,
                    "name" => "Mississippi",
                    "code" => "MS"
                ],
                [
                    "id" => 30,
                    "country_id" => 1,
                    "name" => "Missouri",
                    "code" => "MO"
                ],
                [
                    "id" => 31,
                    "country_id" => 1,
                    "name" => "Nebraska",
                    "code" => "NE"
                ],
                [
                    "id" => 32,
                    "country_id" => 1,
                    "name" => "North Dakota",
                    "code" => "ND"
                ],
                [
                    "id" => 33,
                    "country_id" => 1,
                    "name" => "Oklahoma",
                    "code" => "OK"
                ],
                [
                    "id" => 34,
                    "country_id" => 1,
                    "name" => "South Dakota",
                    "code" => "SD"
                ],
                [
                    "id" => 35,
                    "country_id" => 1,
                    "name" => "Tennesse",
                    "code" => "TN"
                ],
                [
                    "id" => 36,
                    "country_id" => 1,
                    "name" => "Texas",
                    "code" => "TA"
                ],
                [
                    "id" => 37,
                    "country_id" => 1,
                    "name" => "Wisconsin",
                    "code" => "WI"
                ],
                [
                    "id" => 38,
                    "country_id" => 1,
                    "name" => "Caifornia",
                    "code" => "CA"
                ],
                [
                    "id" => 39,
                    "country_id" => 1,
                    "name" => "Nevada",
                    "code" => "NV"
                ],
                [
                    "id" => 40,
                    "country_id" => 1,
                    "name" => "Oregon",
                    "code" => "OR"
                ],
                [
                    "id" => 41,
                    "country_id" => 1,
                    "name" => "Washington",
                    "code" => "WA"
                ],
                [
                    "id" => 42,
                    "country_id" => 1,
                    "name" => "Alaska",
                    "code" => "AK"
                ],
                [
                    "id" => 43,
                    "country_id" => 1,
                    "name" => "Arizona",
                    "code" => "AZ"
                ],
                [
                    "id" => 44,
                    "country_id" => 1,
                    "name" => "Colorado",
                    "code" => "CO"
                ],
                [
                    "id" => 45,
                    "country_id" => 1,
                    "name" => "Idaho",
                    "code" => "ID"
                ],
                [
                    "id" => 46,
                    "country_id" => 1,
                    "name" => "Hawaii",
                    "code" => "HI"
                ],
                [
                    "id" => 47,
                    "country_id" => 1,
                    "name" => "Montana",
                    "code" => "MT"
                ],
                [
                    "id" => 48,
                    "country_id" => 1,
                    "name" => "New Mexico",
                    "code" => "NM"
                ],
                [
                    "id" => 49,
                    "country_id" => 1,
                    "name" => "Utah",
                    "code" => "UT"
                ],
                [
                    "id" => 50,
                    "country_id" => 1,
                    "name" => "Wyoming",
                    "code" => "WY"
                ]
            ];

        State::insert($states);
    }
}
