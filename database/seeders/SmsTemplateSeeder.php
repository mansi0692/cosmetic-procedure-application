<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\SmsTemplate;

class SmsTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $english = [
            [
                'template_name' => 'appointment_pending',
                'language_id' => 1,
                'message' => 'Your appointment is being reviewed by the office of Dr {{doctorName}} for {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_cancelled_by_provider',
                'language_id' => 1,
                'message' => 'Your appointment with Dr {{doctorName}} is cancelled for {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_reschedule',
                'language_id' => 1,
                'message' => 'Your appointment with Dr {{doctorName}} is rescheduled for {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_confirmed',
                'language_id' => 1,
                'message' => 'Your appointment with Dr {{doctorName}} is confirmed for {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_reminder',
                'language_id' => 1,
                'message' => 'You have an upcoming appointment with Dr {{doctorName}} on {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'leave_review_patient',
                'language_id' => 1,
                'message' => 'We hope your visit went well! Please leave a review for the Dr {{doctorName}}.',
            ],
            [
                'template_name' => 'appointment_wait_list',
                'language_id' => 1,
                'message' => 'Your wait list request is being reviewed by the office of Dr {{doctorName}} for {{appointmentTiming}}.',
            ],
        ];

        $arabic = [
            [
                'template_name' => 'appointment_pending',
                'language_id' => 2,
                'message' => '.{{appointmentTiming}} ليوم {{doctorName}} تتم مراجعة موعدك من قبل مكتب الطبيب',
            ],
            [
                'template_name' => 'appointment_cancelled_by_provider',
                'language_id' => 2,
                'message' => '.{{appointmentTiming}} ليوم {{doctorName}} تم إلغاء موعدك مع دكتور',
            ],
            [
                'template_name' => 'appointment_reschedule',
                'language_id' => 2,
                'message' => '.{{appointmentTiming}} ليوم {{doctorName}} تم تغيير موعدك مع دكتور',
            ],
            [
                'template_name' => 'appointment_confirmed',
                'language_id' => 2,
                'message' => '.{{appointmentTiming}} في {{doctorName}} تم تأكيد موعدك مع دكتور',
            ],
            [
                'template_name' => 'appointment_reminder',
                'language_id' => 2,
                'message' => '.{{appointmentTiming}} في {{doctorName}} لديك موعد قادم مع دكتور',
            ],
            [
                'template_name' => 'leave_review_patient',
                'language_id' => 2,
                'message' => '.{{doctorName}} نأمل أن تمت زيارتك بشكل جيد! الرجاء ترك مراجعة للدكتور',
            ],
            [
                'template_name' => 'appointment_wait_list',
                'language_id' => 2,
                'message' => '.{{appointmentTiming}} ليوم {{doctorName}} طلب قائمة الانتظار الخاص بك قيد المراجعة من قبل مكتب الدكتور',
            ],
        ];

        $french = [
            [
                'template_name' => 'appointment_pending',
                'language_id' => 3,
                'message' => 'Votre rendez-vous est en cours d\'examen par le cabinet du Dr {{doctorName}} pour {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_cancelled_by_provider',
                'language_id' => 3,
                'message' => 'Votre rendez-vous avec le Dr {{doctorName}} est annulé pour le {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_reschedule',
                'language_id' => 3,
                'message' => 'Votre rendez-vous avec le Dr {{doctorName}} est reporté au {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_confirmed',
                'language_id' => 3,
                'message' => 'Votre rendez-vous avec le Dr {{doctorName}} est confirmé pour le {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_reminder',
                'language_id' => 3,
                'message' => 'Vous avez un prochain rendez-vous avec le Dr {{doctorName}} le {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'leave_review_patient',
                'language_id' => 3,
                'message' => 'Nous espérons que votre visite s\'est bien passée ! S\'il vous plaît laissez un commentaire pour le Dr {{doctorName}}.',
            ],
            [
                'template_name' => 'appointment_wait_list',
                'language_id' => 3,
                'message' => 'Votre demande de liste d\'attente est en cours d\'examen par le cabinet du Dr {{doctorName}} pour le {{appointmentTiming}}.',
            ],
        ];

        $spanish = [
            [
                'template_name' => 'appointment_pending',
                'language_id' => 4,
                'message' => 'Su cita está siendo revisada por la oficina del Dr. {{doctorName}} para el {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_cancelled_by_provider',
                'language_id' => 4,
                'message' => 'Se cancela su cita con el Dr. {{doctorName}} para el {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_reschedule',
                'language_id' => 4,
                'message' => 'Su cita con el Dr. {{doctorName}} es reprogramada para el {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_confirmed',
                'language_id' => 4,
                'message' => 'Su cita con el Dr. {{doctorName}} es confirmada para el {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_reminder',
                'language_id' => 4,
                'message' => 'Tiene una próxima cita con el Dr. {{doctorName}} el {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'leave_review_patient',
                'language_id' => 4,
                'message' => '¡Esperamos que su visita haya ido bien! Por favor, deje un comentario para el Dr. {{doctorName}}.',
            ],
            [
                'template_name' => 'appointment_wait_list',
                'language_id' => 4,
                'message' => 'Su solicitud de lista de espera está siendo revisada por la oficina del Dr. {{doctorName}} para el {{appointmentTiming}}.',
            ],
        ];

        $russian = [
            [
                'template_name' => 'appointment_pending',
                'language_id' => 5,
                'message' => 'Ваше назначение рассматривается офисом доктора {{doctorName}} на {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_cancelled_by_provider',
                'language_id' => 5,
                'message' => 'Ваша встреча с доктором {{doctorName}} ис отменена на {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_reschedule',
                'language_id' => 5,
                'message' => 'Ваша встреча с доктором {{doctorName}} ис перенесена на {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_confirmed',
                'language_id' => 5,
                'message' => 'Ваша встреча с доктором {{doctorName}} ис подтверждена на {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'appointment_reminder',
                'language_id' => 5,
                'message' => 'У вас назначена встреча с доктором {{doctorName}} на {{appointmentTiming}}.',
            ],
            [
                'template_name' => 'leave_review_patient',
                'language_id' => 5,
                'message' => 'Надеемся, ваш визит прошел хорошо! Пожалуйста, оставьте отзыв о Dr {{doctorName}}.',
            ],
            [
                'template_name' => 'appointment_wait_list',
                'language_id' => 5,
                'message' => 'Ваш запрос в лист ожидания рассматривается офисом доктора {{doctorName}} {{appointmentTiming}}.',
            ],
        ];
        
        foreach (array_merge($english, $arabic, $french, $spanish, $russian) as $row) {
            $newRow = new SmsTemplate();
            $newRow->language_id = $row['language_id'];
            $newRow->template_name = $row['template_name'];
            $newRow->message = $row['message'];
            $newRow->created_by = 0;
            $newRow->save();
        }
    }
}
