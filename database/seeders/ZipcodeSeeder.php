<?php

namespace Database\Seeders;

use App\Models\Zipcode;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ZipcodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $path = public_path('json/zipcodes.json');

        $zipcodes = json_decode(file_get_contents($path), true);

        foreach (array_chunk($zipcodes, 500) as $x) {
            // array_chunk() will divide $arr_a into smaller array as [[1, 2, 3..., 500],[501, 502, .... , 1000] and so one till n]
            //do some stuff with $x;
            Zipcode::insert($x);
        }
    }
}
