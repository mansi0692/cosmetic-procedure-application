<?php

namespace Database\Seeders;

use App\Models\MasterCategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $masterCategories = [
            'certification',
            'occupation',
            'specialty',
            'symptom',
            'language',
            'procedure'
        ];
        foreach ($masterCategories as $key => $value) {
            $masterCategory = new MasterCategory();
            $masterCategory->name = $value;
            $masterCategory->save();
        }
    }
}
