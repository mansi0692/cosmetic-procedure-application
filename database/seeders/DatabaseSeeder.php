<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(1)->create();
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CountrySeeder::class);
        $this->call(StateSeeder::class);
        $this->call(CitySeeder::class);
        $this->call(MasterCategorySeeder::class);
        $this->call(ZipcodeSeeder::class);
        $this->call(MasterSubCategorySeeder::class);
        $this->call(MasterSubCategoryChildSeeder::class);
        $this->call(FAQCatagorySeeder::class);
        $this->call(LanguageSeeder::class);
    }
}
