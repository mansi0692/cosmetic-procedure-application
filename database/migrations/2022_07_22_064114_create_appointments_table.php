<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('appointment_patient_id')->comment('id of appointment_patient_details');
            $table->foreign('appointment_patient_id')->references('id')->on('appointment_patient_details')->onDelete('cascade');
            $table->integer('reschedule_id')->nullable();
            $table->integer('doctor_id')->nullable();
            $table->integer('location_id')->nullable();
            $table->string('booking_id')->nullable();
            $table->dateTime('appointment_date')->nullable();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->enum('consultant_type', ['video', 'in person']);
            $table->string('consultation_url')->nullable();
            $table->text('reason');
            $table->string('registration_form')->nullable();
            $table->text('additional_note')->nullable();
            $table->enum('status', ['confirmation_pending', 'confirmed', 'completed', 'cancelled', 'reschedule_requested', 'waiting']);
            $table->tinyInteger('allow_text_messages')->default(0);
            $table->text('declined_reason')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
};
