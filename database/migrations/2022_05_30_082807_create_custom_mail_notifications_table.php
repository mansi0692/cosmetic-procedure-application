<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_mail_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('send_to', ['all', 'specific_user'])->nullable();
            $table->json('send_to_id')->nullable();
            $table->string('subject')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->longText('content')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_mail_notifications');
    }
};
