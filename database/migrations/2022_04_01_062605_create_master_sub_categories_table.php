<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_sub_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('master_category_id')->comment('id of master_categories');
            $table->foreign('master_category_id')->references('id')->on('master_categories');
            $table->integer('parent_id')->default(0);
            $table->integer('language_id')->default(1);
            $table->string('label')->nullable();
            $table->string('value')->nullable();
            $table->tinyInteger('is_other')->default(0)->nullable();
            $table->tinyInteger('is_approved')->default(1)->nullable();
            $table->integer('order_no')->nullable();
            $table->tinyInteger('status')->default(1)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_sub_categories');
    }
};
