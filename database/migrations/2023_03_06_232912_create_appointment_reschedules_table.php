<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_reschedules', function (Blueprint $table) {
            $table->id();
            $table->integer('appointment_id');
            $table->integer('location_id')->nullable();
            $table->dateTime('appointment_date');
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->enum('status', ['accepted', 'rejected', 'pending']);
            $table->enum('consultant_type', ['video_consultancy', 'in_person']);
            $table->text('reason');
            $table->text('declined_reason');
            $table->enum('requested_by', ['patient', 'doctor'])->nullable();
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_reschedules');
    }
};
