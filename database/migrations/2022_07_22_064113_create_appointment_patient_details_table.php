<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_patient_details', function (Blueprint $table) {
            $table->id();
            $table->integer('reschedule_id')->nullable();
            $table->integer('patient_id')->unsigned();
            $table->enum('appointment_for', ['self', 'someone else']);
            $table->string('patient_name')->nullable();
            $table->integer('patient_age')->nullable();
            $table->string('patient_phone_number')->nullable();
            $table->string('patient_address')->nullable();
            $table->bigInteger('patient_state_id')->unsigned()->nullable();
            $table->integer('patient_city_id')->nullable();
            $table->integer('patient_country_id')->nullable();
            $table->tinyInteger('is_offline')->default(0);
            $table->tinyInteger('joint_by_doctor')->default(0);
            $table->tinyInteger('joint_by_patient')->default(0);
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_patient_details');
    }
};
