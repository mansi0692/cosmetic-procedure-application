<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_block_slots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_id')->unsigned()->comment('id of users');
            $table->foreign('doctor_id')->references('id')->on('users');
            $table->integer('location_id')->nullable();
            $table->date('date');
            $table->time('start');
            $table->time('end');
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_block_slots');
    }
};
