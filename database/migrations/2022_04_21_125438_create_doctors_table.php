<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('npi_number');
            $table->string('name_prefix');
            $table->string('first_name');
            $table->string('last_name');
            $table->char('gender');
            $table->string('profile_image')->nullable();
            $table->string('suffix');
            $table->text('specialties');
            $table->text('specialties_copy');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('zipcode');
            $table->string('sole_proprietor');
            $table->tinyInteger('is_registered')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->tinyInteger('has_deleted')->default(0);
            $table->tinyInteger('has_updated')->default(0);
            $table->tinyInteger('is_updated')->default(0);
            $table->tinyInteger('is_top_doctor')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
};
