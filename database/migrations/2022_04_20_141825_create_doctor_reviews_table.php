<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('patient_id')->unsigned()->comment('id of users');
            $table->foreign('patient_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('doctor_id')->unsigned()->comment('id of users');
            $table->foreign('doctor_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('booking_id')->nullable();
            $table->float('rating')->default(0);
            $table->float('bedside_manner')->default(0);
            $table->float('answered_questions')->default(0);
            $table->float('after_care')->default(0);
            $table->float('time_spent')->default(0);
            $table->float('responsiveness')->default(0);
            $table->float('courtesy')->default(0);
            $table->float('payment_process')->default(0);
            $table->float('wait_times')->default(0);
            $table->text('review')->nullable();
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_reviews');
    }
};
