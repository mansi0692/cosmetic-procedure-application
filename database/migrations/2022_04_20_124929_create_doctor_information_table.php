<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_information', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->comment('id of users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->string('first_name');
            $table->string('last_name');
            $table->integer('npi_number')->nullable();
            $table->tinyInteger('gender')->default(1);
            $table->string('profile_image')->nullable();
            $table->unsignedBigInteger('occupation_id')->comment('id of master_sub_categories');
            $table->foreign('occupation_id')->references('id')->on('master_sub_categories');
            $table->unsignedBigInteger('specialty_id')->comment('id of master_sub_categories');
            $table->foreign('specialty_id')->references('id')->on('master_sub_categories');
            $table->string('symptom_id')->nullable();
            $table->string('procedure_id')->nullable();
            $table->tinyInteger('is_document_verified')->default(0)->nullable();
            $table->string('office_phone_number')->nullable();
            $table->string('ext')->nullable();
            $table->string('office_fax')->nullable();
            $table->string('medical_school')->nullable();
            $table->year('grad_year')->nullable();
            $table->date('date_of_birth')->nullable();
            $table->unsignedBigInteger('state_id')->comment('id of states');
            $table->foreign('state_id')->nullable()->references('id')->on('states');
            $table->foreignId('city_id')->nullable();
            $table->integer('state_of_office')->nullable();
            $table->integer('city_of_profession')->nullable();
            $table->longText('about')->nullable();
            $table->tinyInteger('in_person')->default(0);
            $table->tinyInteger('video_consultancy')->default(0);
            $table->time('appointment_duration')->nullable();
            $table->tinyInteger('is_featured')->default(0);
            $table->string('registration_form')->nullable();
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_information');
    }
};
