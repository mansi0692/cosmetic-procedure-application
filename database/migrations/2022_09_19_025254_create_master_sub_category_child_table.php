<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_sub_category_child', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('master_sub_category_id')->comment('id of master_sub_categories');
            $table->unsignedInteger('language_id')->default(1)->comment('id of languages');
            $table->string('label')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_sub_category_child');
    }
};
