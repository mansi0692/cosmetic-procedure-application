<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('doctor_id')->unsigned()->comment('id of users');
            $table->foreign('doctor_id')->references('id')->on('users');
            $table->string('appointment_duration')->nullable();
            $table->string('location_email')->nullable();
            $table->string('office_phone_number')->nullable();
            $table->string('office_fax')->nullable();
            $table->string('ext')->nullable();
            $table->string('address');
            $table->unsignedBigInteger('city_id')->nullable()->unsigned();
            $table->string('suite')->nullable();
            $table->unsignedBigInteger('state_id')->nullable()->unsigned();
            $table->string('zipcode');
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->integer('created_by');
            $table->timestamp('created_at')->useCurrent();
            $table->integer('updated_by')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_locations');
    }
};
