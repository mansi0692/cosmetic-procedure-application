<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_likes', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('doctor_id')->comment('id of user');
            $table->foreign('doctor_id')->references('id')->on('users');
            $table->unsignedInteger('liked_by_id')->comment('id of user');
            $table->foreign('liked_by_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_likes');
    }
};
