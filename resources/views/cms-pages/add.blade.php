@extends('master')

@section('title', 'CMS Pages')

@section('styles')
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.min.css') }}">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.min.css') }}">
    <!-- CodeMirror -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/codemirror/codemirror.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/codemirror/theme/monokai.css') }}">
    <style>
        label.error {
            color: #dc3545;
            font-size: 16px;
            font-weight: normal !important;
        }

    </style>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <form id="regForm" action="{{ route('save-cms') }}" method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Language -->
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="language" class="required">Language</label>
                                                <select name="language" id="language" class="form-control">
                                                    @foreach ($languages as $language)
                                                       <option value="{{ $language->id }}" @if ($language->id == 1) selected @endif>{{ $language->title }}</option>
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('language'))
                                                    <span class="text-danger">{{ $errors->first('language') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <!-- Title -->
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="title"class="required">Title</label>
                                                <input name="title" type="text" class="form-control" id="title"
                                                    placeholder="Title">
                                                @if ($errors->has('title'))
                                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <!-- Description -->
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="description">Description</label>
                                                <textarea id="summernote" name="description"
                                                    style="display: none;"> </textarea>
                                                <span id="custom-error-description-span" class="text-danger"></span>
                                                @if ($errors->has('title'))
                                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('redirect-cms-list') }}" type="submit"
                                    class="btn btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->


                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')
    <!-- jQuery -->
    <script src="{{ asset('assets//jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{ asset('assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
    <!-- Page specific script -->
    <!-- Summernote -->
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <!-- CodeMirror -->
    <script src="{{ asset('assets/plugins/codemirror/codemirror.js') }}"></script>
    <script src="{{ asset('assets/plugins/codemirror/mode/css/css.js') }}"></script>
    <script src="{{ asset('assets/plugins/codemirror/mode/xml/xml.js') }}"></script>
    <script src="{{ asset('assets/plugins/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script>
        var descriptionContent = '';
        $(document).ready(function() {

            // Summernote
            $("#summernote").summernote({
                height: 300,
                callbacks: {
                    onChange: function(contents, $editable) {
                        descriptionContent = contents;
                        if (descriptionContent != '') {
                            $("#custom-error-description-span").text("");
                        }
                    }
                },
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ol', 'ul', 'paragraph']],
                    ['insert', ['picture']],
                    ['picture', ['link']]
                ]
            });

            // CodeMirror
            CodeMirror.fromTextArea(document.getElementById("codeMirrorDemo"), {
                mode: "htmlmixed",
                theme: "monokai"
            });
        });
        $("#regForm").submit(function(e) {
            if (descriptionContent === '') {
                $("#custom-error-description-span").text("Description required");
                return false;
            } else {
                $("#custom-error-description-span").text("");
            }
        });
        $(function() {
            bsCustomFileInput.init();
            $("#regForm").validate({
                rules: {
                    title: {
                        required: true,
                    },
                },
                messages: {
                    title: {
                        required: "Title required",
                    },
                }
            });
        });

    </script>

    </html>
@stop
