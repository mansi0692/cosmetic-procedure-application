@extends('master')

@section('title', 'Top Specialities')

@section('styles')
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
        href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('update-top-speciality', ['id' => Request::segment(2)]) }}" method="post"
                            id="regForm">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- First name -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="speciality" class="required">Speciality</label>
                                                <input value="{{ $topSpeciality->specialityData->label ?? '' }}" name="speciality"
                                                    type="text" class="form-control" id="speciality"
                                                    disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="doctor_id" class="required">Doctors</label>
                                                <select class="form-control" name='doctor_id' id='doctor_id'>
                                                    <option value = "">Select Doctor</option> 
                                                    @foreach ($doctors as $data)
                                                        @if (isset($data->user_id))
                                                            @if ($data->user_id == $topSpeciality->doctor_id)
                                                                <option value="{{ $data->user_id }}" selected>{{ $data->first_name.' '.$data->last_name }}</option>
                                                            @else
                                                                <option value="{{ $data->user_id }}">{{ $data->first_name.' '.$data->last_name }}</option>
                                                            @endif
                                                        @else
                                                            @if ($data->id == $topSpeciality->unreg_doctor_id)
                                                                <option value="{{ $data->id }}Z" selected>{{ $data->first_name.' '.$data->last_name }}</option>
                                                            @else
                                                                <option value="{{ $data->id }}Z">{{ $data->first_name.' '.$data->last_name }}</option>
                                                            @endif
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('doctor_id'))
                                                    <span class="text-danger">{{ $errors->first('doctor_id') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="preview" style="display:none">
                                            <label>Preview</label>
                                            @foreach ($doctors as $data)
                                                @if (isset($data->user_id))
                                                    @if (!empty($data->profile_image))
                                                        @if (file_exists(public_path().'/uploads/'.$data->profile_image))
                                                            <img src="{{ URL::asset('/uploads/'.$data->profile_image) }}" style="display:none;width:150px;height:100px" id="{{ $data->user_id }}">
                                                        @elseif(stripos(get_headers(config('constants.get_file_path') . $data->user_id . '/' . $data->profile_image)[0],"200 OK"))
                                                            <img src="{{ config('constants.get_file_path') . $data->user_id . '/' . $data->profile_image}}" style="display:none;width:150px;height:100px" id="{{ $data->user_id }}">
                                                        @else
                                                            <img src="{{URL::asset('/assets/images/no-profile.jpg')}}" style="display:none;width:150px;height:100px" id="{{ $data->user_id }}">
                                                        @endif
                                                    @else
                                                        <img src="{{URL::asset('/assets/images/no-profile.jpg')}}" style="display:none;width:150px;height:100px" id="{{ $data->user_id }}">
                                                    @endif
                                                @else
                                                    @if (!empty($data->profile_image))
                                                        @if(stripos(get_headers(config('constants.get_file_path') . 'top-doctors/' . $data->profile_image)[0],"200 OK"))
                                                            <img src="{{ config('constants.get_file_path') . 'top-doctors/' . $data->profile_image }}" style="display:none;width:150px;height:100px" id="{{ $data->id }}Z">
                                                        @else
                                                            <img src="{{URL::asset('/assets/images/no-profile.jpg')}}" style="display:none;width:150px;height:100px" id="{{ $data->id }}Z">
                                                        @endif
                                                    @else
                                                        <img src="{{URL::asset('/assets/images/no-profile.jpg')}}" style="display:none;width:150px;height:100px" id="{{ $data->id }}Z">
                                                    @endif
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('list-top-speciality') }}"
                                    class="btn btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div>
                <!-- /.card -->
                </div>
                <!-- /.row -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
@stop
@section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
<script>
    $(function() {
        $("#regForm").validate({
            rules: {
                doctor_id: {
                    required: true
                }
            },
            messages: {
                doctor_id: {
                    required: "Doctor required"
                }
            },
            errorPlacement: function(error, element) {
                var placement = $(element).data('error');
                if (placement) {
                    $(placement).append(error)
                } else {
                    error.insertAfter(element);
                }
            }
        });
    });
        $('#doctor_id').on('change', function() {
            if($(this).val() != '')
            {console.log($(this).val());
                $('.preview').css('display','block');
                $('.preview img').css('display','none');
                $('#'+$(this).val()).css('display','block');
            }else {console.log('else');
                $('.preview').css('display','none');
                $('.preview img').css('display','none');
            }
        });
    </script>
@endsection