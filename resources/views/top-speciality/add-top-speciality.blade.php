@extends('master')

@section('title', 'Top Specialities')

@section('styles')
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('save-top-speciality') }}" method="post" id="topSpecialityForm">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="speciality" class="required">Speciality</label>
                                                <select name="speciality" id="speciality" class="form-control">
                                                    <option value="" disabled>Select Speciality</option>
                                                    @foreach ($specialities as $speciality)
                                                        <option value="{{ $speciality->id }}">{{ $speciality->label }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn add-btn" style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('list-top-speciality') }}" class="btn btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.row -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
@stop
@section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script>
        $(function() {
            $("#topSpecialityForm").validate({
                rules: {
                    speciality: {
                        required: true
                    }
                },
                messages: {
                    speciality: {
                        required: "Speciality required"
                    }
                },
                errorPlacement: function(error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
    </script>
@endsection
