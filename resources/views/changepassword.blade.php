@extends('master')

@section('title', 'Change password')

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    <style>
        label.error {
            color: #dc3545;
            font-size: 16px;
            font-weight: normal !important;
        }
    </style>
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-6">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success')['msg'] }}
                        </div>
                    @endif
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ url('changepassword') }}" method="post" id="regForm">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="current_password" class="required"><?php echo trans('messages.label.CURRENT_PASSWORD'); ?></label>
                                                <input type="password" name="current_password" class="form-control"
                                                    id="current_password">
                                                <a onclick="changeIconCurrentPassword(this, 'current_password')"
                                                    class="errspan">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                @if ($errors->has('current_password'))
                                                    <span
                                                        class="text-danger">{{ $errors->first('current_password') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="new_password" class="required"><?php echo trans('messages.label.NEW_PASSWORD'); ?></label>
                                                <input type="password" name="new_password" class="form-control"
                                                    id="new_password">
                                                <a onclick="changeIconCurrentPassword(this, 'new_password')"
                                                    class="errspan">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                @if ($errors->has('new_password'))
                                                    <span class="text-danger">{{ $errors->first('new_password') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="confirm_password" class="required"><?php echo trans('messages.label.CONFIRM_PASSWORD'); ?></label>
                                                <input type="password" name="confirm_password" class="form-control"
                                                    id="confirm_password">
                                                <a onclick="changeIconCurrentPassword(this, 'confirm_password')"
                                                    class="errspan">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                                @if ($errors->has('confirm_password'))
                                                    <span
                                                        class="text-danger">{{ $errors->first('confirm_password') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->


                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
    </section>

@stop

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#regForm").validate({
                rules: {
                    current_password: {
                        required: true,
                        minlength: 6,
                    },
                    new_password: {
                        required: true,
                        minlength: 6,
                    },
                    confirm_password: {
                        required: true,
                        equalTo: "#new_password"
                    },
                },
                messages: {
                    current_password: {
                        required: "Current password is required",
                        minlength: "Current password must be at least 6 characters long"
                    },
                    new_password: {
                        required: "Password is required",
                        minlength: "New password must be at least 6 characters"
                    },
                    confirm_password: {
                        required: "Confirm password is required",
                        equalTo: "Password and confirm password should same"
                    },
                }
            });

        });

        function changeIconCurrentPassword(anchor, from) {
            let icon = anchor.querySelector("i");
            icon.classList.toggle('fa-eye');
            icon.classList.toggle('fa-eye-slash');
            let iconClass = icon.className;
            switch (from) {
                case 'current_password':
                    if (iconClass == "fa fa-eye") {
                        $('#current_password').get(0).type = 'password';
                    } else {
                        $('#current_password').get(0).type = 'text';
                    }
                    break;
                case 'new_password':
                    if (iconClass == "fa fa-eye") {
                        $('#new_password').get(0).type = 'password';
                    } else {
                        $('#new_password').get(0).type = 'text';
                    }
                    break;
                case 'confirm_password':
                    if (iconClass == "fa fa-eye") {
                        $('#confirm_password').get(0).type = 'password';
                    } else {
                        $('#confirm_password').get(0).type = 'text';
                    }
                    break;
                default:
            }
        }
        setTimeout(() => {
            $('.alert').hide()
        }, 2000);
    </script>
@endsection
