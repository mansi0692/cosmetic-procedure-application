@extends('master')

@section('title', 'SMS Template')

@section('styles')

    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.min.css') }}">

@endsection

@section('content')
    @php
        $smsTemplateId = Request::segment(2);
        $languageId = Request::segment(3);
    @endphp
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="regForm"
                            action="{{ route('update-sms-template', ['smsTemplateId' => $smsTemplateId, 'languageId' => $languageId]) }}"
                            method="post">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Message -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="message">Message</label>
                                                <input value="{{ $smsTemplate->message }}" name="message"
                                                    type="text" class="form-control" id="message"
                                                    placeholder="essage">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('list-sms-template', ['languageId' => $languageId]) }}" type="submit"
                                    class="btn btn-secondary">Cancel</a>
                            </div>

                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@stop

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(document).ready(function() {

            // Summernote
            $("#summernote").summernote({
                height: 300,
                toolbar: [
                    ['style', ['style', 'code']],
                    ['font', ['bold', 'italic', 'underline']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ol', 'ul', 'paragraph']],
                    ['insert', ['picture', 'codeview']],
                ]
            });


        });
    </script>
@endsection
