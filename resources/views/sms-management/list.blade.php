@extends('master')

@section('title', 'SMS Template')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

@endsection

@section('content')
    @php
        $value = '';
        $languageId = Request::segment(2);
        foreach ($dataArray['language'] as $key => $data) {
            $selected = $languageId == $data->id ? 'selected' : '';
            $value .= '<option ' . $selected . ' value="' . $data->id . '">' . $data['title'] . '</option>';
        }
    @endphp
    <div class="category-content-wrapper">
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    {{ $errors->first() }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    <table id="templateList" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>Template name</th>
                                                <th>Message</th>
                                                <th>Status</th>
                                                <th class="no-sort">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($dataArray['smsTemplate'] as $data)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $data->template_name }}</td>
                                                    <td>{{ $data->message }}</td>
                                                    @if ($data->status)
                                                        <td>Active</td>
                                                    @else
                                                        <td>Inactive</td>
                                                    @endif
                                                    <td>
                                                        <a href="{{ route('edit-sms-template', ['smsTemplateId' => $data->id, 'languageId' => $languageId]) }}"
                                                            title="Edit">
                                                            <i class="fa fa-edit" aria-hidden="true"></i></a>
                                                        @if ($data->status)
                                                            <a href="{{ route('change-status-sms-template', ['status' => 'active', 'smsTemplateId' => $data->id]) }}"
                                                                title="Inactive">
                                                                <i class="fa fa-ban" aria-hidden="true"></i>
                                                            </a>
                                                        @else
                                                            <a href="{{ route('change-status-sms-template', ['status' => 'inactive', 'smsTemplateId' => $data->id]) }}"
                                                                title="Active">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </a>
                                                        @endif
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#templateList").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [4]
                }]
            });
            $("#templateList_wrapper .row:first-child> :first-child").append(
                '<select class="form-control" onchange="location = this.value" style="width:19%"><?php echo $value; ?></select>'
            );
        });

        var addSerialNumber = function() {
            var i = 1
            $('table tbody tr').each(function(index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };

        addSerialNumber();
    </script>
@endsection
