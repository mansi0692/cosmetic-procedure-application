@extends('master')

@section('title', 'Invite Doctor')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="category-content-wrapper">
        <a href="{{ route('doctor-invite-send-invite') }}" class="btn add-btn float-right"
            style="background-color: #FF008A; margin: 15px; color: white; width: 80px">Add</a>
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    <div class="navigation-tab" style="margin-bottom: 10px;">
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <a class="{{ Request::segment(2) == 0 ? 'nav-link active' : 'nav-link' }}"
                                                    id="certification" aria-current="page"
                                                    href="{{ route('doctor-invite-list', ['tabNumber' => 0]) }}">Invited via Admin
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="{{ Request::segment(2) == 1 ? 'nav-link active' : 'nav-link' }}"
                                                    href="{{ route('doctor-invite-list', ['tabNumber' => 1]) }}"
                                                    id="other">Invited via Yume Doctors</a>
                                            </li>
                                        </ul>
                                    </div>

                                    @if (Request::segment(2) == 0)                                        
                                        <table id="inviteList" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>Email</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($dataArray as $data)
                                                    <tr>
                                                        <td></td>
                                                        <td>{{ $data->email }} </td>
                                                        @if ($data->is_registered)
                                                            <td> Registered </td>
                                                        @else
                                                            <td> Not registered </td>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif

                                    @if (Request::segment(2) == 1)                                        
                                        <table id="inviteList" class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Sr No.</th>
                                                    <th>NPI Number</th>
                                                    <th>Firstname</th>
                                                    <th>Lastname</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($dataArray as $data)
                                                    <tr>
                                                        <td></td>
                                                        <td>{{ $data->npi_number }}</td>
                                                        <td>{{ $data->first_name }}</td>
                                                        <td>{{ $data->last_name }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    @endif

                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#inviteList").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": []
                }]
            });

        });

        var addSerialNumber = function() {
            var i = 1
            $('table tbody tr').each(function(index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };

        addSerialNumber();
    </script>
@endsection
