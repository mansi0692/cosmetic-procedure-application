@extends('master')

@section('title', 'Dashboard')

@section('styles')


@endsection
@section('content')
    <div class="wrapper">
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div><!-- /.col -->

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        @if ($role == 'Doctor Admin' || $role == 'Super Admin')
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-info">
                                    <div class="inner">
                                        <h3>{{ $dashboardData['doctorCount'] }}</h3>
                                        <p>Doctors</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-person"></i>
                                    </div>
                                    <a href="{{ route('list-doctor') }}" class="small-box-footer">More info <i
                                            class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        @endif
                        <!-- ./col -->
                        @if ($role == 'Doctor Admin' || $role == 'Super Admin')
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-success">
                                    <div class="inner">
                                        <h3>{{ $dashboardData['patientCount'] }}</h3>
                                        <p>Patients</p>
                                    </div>
                                    <div class="icon">
                                        <i class="ion ion-person-add"></i>
                                    </div>
                                    <a href="{{ route('list-patient') }}" class="small-box-footer">More info <i
                                            class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        @endif
                        <!-- ./col -->
                        @if ($role == 'Patient Admin' || $role == 'Super Admin')
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-danger">
                                    <div class="inner">
                                        <h3>{{ $dashboardData['patientReviewCount'] }}</h3>
                                        <p>Patient Reviews</p>
                                    </div>
                                    <div class="icon">
                                        <i class="nav-icon fa fa-star"></i>
                                    </div>
                                    <a href="{{ route('list-patient-review') }}" class="small-box-footer">More info <i
                                            class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        @endif
                        <!-- ./col -->
                        @if ($role == 'Doctor Admin' || $role == 'Super Admin')
                            <div class="col-lg-3 col-6">
                                <!-- small box -->
                                <div class="small-box bg-teal">
                                    <div class="inner">
                                        <h3>{{ $dashboardData['doctorReviewCount'] }}</h3>
                                        <p>Doctor Reviews</p>
                                    </div>
                                    <div class="icon">
                                        <i class="nav-icon fa fa-star"></i>
                                    </div>
                                    <a href="{{ route('list-doctor-review') }}" class="small-box-footer">More info <i
                                            class="fas fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- /.row -->

                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
			
        </div>
    </div>

@stop
@section('scripts')

@endsection
