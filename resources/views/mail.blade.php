<!DOCTYPE html
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equi(v="X-UA-Compatible" content="IE=edge" />
    <title>{{ config('app.name') }}</title>
    <style type="text/css">
        body {
            margin: 0;
            padding: 0;
            font-family: \'verdana\';
        }

        a {
            text-decoration: none;
        }

        a:hover,
        a:focus {
            color: #82c55c !important;
        }
    </style>
</head>

<body style="font-family:verdana'; font-size:13px; color:#656b6f; margin:0;">

    <table width="600" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td align="center" valign="center" style="background:#7F18E5 none repeat scroll 0 0;height: 123px;left: 0;">
                <table width="500" align="center">
                    <tr>
                        <td align="center" valign="center">
                            <img src="{{ asset("assets/images/$logoURL") }}" alt="" srcset="">
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" style="border:1px solid #cecece;">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <table align="center" cellpadding="0" cellspacing="0" border="0" width="80%">
                                <tr>
                                    <td align="left" valign="top"
                                        style="font-family:'verdana'; font-size:13px; color:#656b6f;">
                                        {!! $content !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left" height="50" valign="center" bgcolor="#7F18E5"
                style="font-size:12px; color:#ffffff; font-family:'verdana';text-align:center;background-color:#7F18E5">
                Copyright {{ date('Y') }}. All Rights Reserved
            </td>
        </tr>
    </table>
</body>

</html>
