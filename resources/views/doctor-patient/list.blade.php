@extends('master')

@section('title', 'Admin')

@section('styles')
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.min.css') }}">
@endsection

@section('content')
    <div class="category-content-wrapper">
        <a href="{{ route('add-doctor-patient-admin') }}" class="btn add-btn float-right"
            style="background-color: #FF008A; margin: 15px; color: white; width: 80px">Add</a>

        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if (session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    <div class="navigation-tab" style="margin-bottom: 10px;">
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <a class="{{ Request::segment(1) == 'list-doctor-admin' ? 'nav-link active' : 'nav-link' }}"
                                                    id="certification" aria-current="page"
                                                    href="{{ route('list-doctor-admin') }}">Doctors</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="{{ Request::segment(1) == 'list-patient-admin' ? 'nav-link active' : 'nav-link' }}"
                                                    href="{{ route('list-patient-admin') }}" id="other">Patients</a>
                                            </li>
                                        </ul>
                                    </div>

                                    <table id="doctorAdminList" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>Email</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($adminData as $data)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $data->email }}</td>
                                                    <td>
                                                        <a href="{{ route('edit-doctor-patient-admin', ['adminId' => $data->id]) }}"
                                                            title="Edit">
                                                            <i class="fa fa-edit" aria-hidden="true"></i></a>
                                                        @if ($data->status == 1)
                                                            <a href="{{ route('change-status-doctor-patient-admin', ['status' => 'active', 'adminId' => $data->id]) }}"
                                                                title="Inactive">
                                                                <i class="fa fa-ban" aria-hidden="true"></i>
                                                            </a>
                                                        @else
                                                            <a href="{{ route('change-status-doctor-patient-admin', ['status' => 'inactive', 'adminId' => $data->id]) }}"
                                                                title="Active">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </a>
                                                        @endif
                                                        <a onclick="return confirm('<?php echo trans('messages.label.ARE_YOU_SURE_TO_DELETE_THIS_RECORD'); ?>')"
                                                            href="{{ route('delete-doctor-patient-admin', ['adminId' => $data->id]) }}"
                                                            title="Delete">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#doctorAdminList").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [2]
                }]
            });
        });


        var addSerialNumber = function() {
            var i = 1
            $('table tbody tr').each(function(index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };

        addSerialNumber();
    </script>
@endsection
