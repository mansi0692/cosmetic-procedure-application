@extends('master')

@section('title', 'Admin')

@section('styles')

@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('save-doctor-patient-admin') }}" method="post" id="regForm">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Admin -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="admin">Select admin</label>
                                                <select class="form-control" name='admin'>
                                                    <option value="doctor">Doctor</option>
                                                    <option value="patient">Patient</option>
                                                </select>
                                            </div>
                                            @if ($errors->has('admin'))
                                                <span class="text-danger">{{ $errors->first('admin') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-md-6">

                                        </div>
                                        <!-- Email -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email"class="required">Email address</label>
                                                <input name="email" type="email" class="form-control" id="email"
                                                    placeholder="Email address">
                                            </div>
                                            @if ($errors->has('email'))
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                        {{-- Password --}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="password"class="required">Password</label>
                                                <input name="password" type="password" class="form-control" id="password"
                                                    placeholder="Password">
                                                <a onclick="changeIconPassword(this)" class="errspan">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="text-danger">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('redirect-admin-list') }}" type="submit"
                                    class="btn btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script>
        function changeIconPassword(anchor) {
            let icon = anchor.querySelector("i");
            icon.classList.toggle('fa-eye');
            icon.classList.toggle('fa-eye-slash');
            let iconClass = icon.className;
            if (iconClass == "fa fa-eye") {
                $('#password').get(0).type = 'password';
            } else {
                $('#password').get(0).type = 'text';
            }
        }
        $(document).ready(function() {
            jQuery.validator.addMethod("noSpace", function(value, element) {
                return value.indexOf(" ") < 0 && value != "";
            }, "White space not allowed");
            jQuery.validator.addMethod("email", function(value, element) {
                return /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
            });
            $("#regForm").validate({
                rules: {
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        required: true,
                        minlength: 6,
                        noSpace: true
                    },
                },
                messages: {
                    email: {
                        required: "Email is required",
                        email: "Please enter a valid email"
                    },
                    password: {
                        required: "Password is required",
                        minlength: "Password must be at least 6 characters long"
                    },
                }
            });
        });
    </script>
@endsection
