@extends('master')

@section('title', 'Admin')

@section('styles')

@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('update-doctor-patient-admin', ['adminId' => Request::segment(2)]) }}"
                            method="post" id="regForm">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Admin -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="admin">Select admin</label>
                                                <select class="form-control" name='admin' disabled>
                                                    @if ($adminData->role_id == config('constants.roles.doctor_admin'))
                                                        <option value="doctor" selected>Doctor</option>
                                                    @elseif ($adminData->role_id == config('constants.roles.patient_admin'))
                                                        <option value="patient" selected>Patient</option>
                                                    @endif
                                                </select>
                                            </div>
                                            @if ($errors->has('admin'))
                                                <span class="text-danger">{{ $errors->first('admin') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-md-6">

                                        </div>
                                        <!-- Email -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email"class="required">Email address</label>
                                                <input name="email" value="{{ $adminData->email }}" type="email"
                                                    class="form-control" id="email" placeholder="Email address"
                                                    disabled>
                                            </div>
                                            @if ($errors->has('email'))
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                        {{-- Password --}}
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="password"class="required">Password</label>
                                                <input name="password" type="password" class="form-control" id="password"
                                                    placeholder="Password">
                                                <a onclick="changeIconPassword(this)" class="errspan">
                                                    <i class="fa fa-eye"></i>
                                                </a>
                                            </div>
                                            @if ($errors->has('password'))
                                                <span class="text-danger">{{ $errors->first('password') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('redirect-admin-list') }}" type="submit"
                                    class="btn btn-secondary">Cancel</a>
                            </div>
                    </div>
                    <!-- /.card-body -->


                    </form>
                </div>
                <!-- /.card -->


            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>

    <script>
        function changeIconPassword(anchor) {
            let icon = anchor.querySelector("i");
            icon.classList.toggle('fa-eye');
            icon.classList.toggle('fa-eye-slash');
            let iconClass = icon.className;
            if (iconClass == "fa fa-eye") {
                $('#password').get(0).type = 'password';
            } else {
                $('#password').get(0).type = 'text';
            }
        }
        $(function() {
            $("#regForm").validate({
                rules: {
                    password: {
                        required: true,
                        minlength: 6,
                    },
                },
                messages: {
                    password: {
                        required: "Password is required",
                        minlength: "Password must be at least 6 characters long"
                    },
                }
            });
        });
    </script>
@endsection
