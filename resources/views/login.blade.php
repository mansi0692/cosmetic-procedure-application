<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Yume</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.min.css') }}">

    <style>
        label.error {
            color: #dc3545;
            font-size: 16px;
            font-weight: normal !important;
        }

    </style>
</head>

<body class="hold-transition login-page" style="">
    <div class="login-box">
        <div class="login-logo">
            <a><img src="{{ asset('assets/images/site-logo.svg') }}" alt="Site logo"></a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg"></p>

                <form action="{{ route('loginuser') }}" method="post" id="loginForm">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <input name="email" id="email" type="email" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <span id="custom-error-email-span" class="text-danger"></span>

                    <div class="input-group" style="margin-top: 10px">
                        <input id="password" name="password" type="password" class="form-control"
                            placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <span id="custom-error-password-span" class="text-danger"></span>
                    <div class="row">

                        <!-- /.col -->
                        <div class="col-12" style="margin-top: 20px">
                            <button type="submit" class="btn btn-primary btn-block"
                                style="background: #FF008A; border-color: #FF008A">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    @if ($errors->any())
                        <span class="text-danger">{{ $errors->first() }}</span>
                    @endif
                </form>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <!-- jQuery -->
    <script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('assets/js/adminlte.min.js') }}"></script>

    <script>
        var formSubmit = false;
        var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i;

        $("#loginForm").submit(function(e) {
            var emailValue = $('#email').val();
            if (emailValue == '' || emailValue === null) {
                $("#custom-error-email-span").text("Email required");
                formSubmit = false;
            } else if (!pattern.test(emailValue)) {
                $("#custom-error-email-span").text("Please enter a valid email");
                formSubmit = false;
            } else {
                $("#custom-error-email-span").text("");
                formSubmit = true;
            }
            var passwordValue = $('#email').val();
            if (passwordValue == '' || passwordValue === null) {
                $("#custom-error-password-span").text("Password required");
                formSubmit = false;
            } else {
                $("#custom-error-password-span").text("");
                formSubmit = true;
            }
            if (formSubmit) {
                return true;
            } else {
                return false;
            }
        });
        $(document).ready(function() {
            $('input[name=email]').change(function() {
                if (this.value == '' || this.value === null) {
                    $("#custom-error-email-span").text("Email required");
                    formSubmit = false;
                } else if (!pattern.test(this.value)) {
                    $("#custom-error-email-span").text("Please enter a valid email");
                    formSubmit = false;
                } else {
                    $("#custom-error-email-span").text("");
                    formSubmit = true;
                }
            });
            $('input[name=password]').change(function() {
                if (this.value == '' || this.value === null) {
                    $("#custom-error-password-span").text("Password required");
                    formSubmit = false;
                } else {
                    $("#custom-error-password-span").text("");
                    formSubmit = true;
                }
            });
        });

    </script>
</body>

</html>
