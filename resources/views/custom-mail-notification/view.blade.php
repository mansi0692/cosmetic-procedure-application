@extends('master')

@section('title', 'Custom Mail')

@section('styles')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.min.css') }}">

@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <!-- Personal information -->
                        <div class="card-body">
                            <div class="form-group">
                                <div class="row">
                                    <!-- Send to -->
                                    <!-- Subject -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="subject">Sent to</label>
                                            <input value="{{ ucfirst(str_replace('_', ' ', $dataArray->send_to)) }}"
                                                name="subject" type="text" class="form-control" id="subject"
                                                placeholder="Subject" disabled>
                                        </div>
                                    </div>

                                    <!-- Subject -->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="subject">Subject</label>
                                            <input value="{{ $dataArray->subject }}" name="subject" type="text"
                                                class="form-control" id="subject" placeholder="Subject" disabled>
                                        </div>
                                    </div>

                                    <!-- Send to -->
                                    @if ($dataArray->send_to == 'specific_user')
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="subject">Sent to</label>
                                                <select multiple="" class="custom-select" disabled="">
                                                    @if ($emailArray != null)
                                                        @foreach ($emailArray as $key => $item)
                                                            <option> {{ $key + 1 }}. {{ $item }}</option>
                                                        @endforeach
                                                    @else
                                                        <option> Mailer deleted from the system </option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                    @endif

                                    <!-- Emai Content -->
                                    <div class="col-md-12">
                                        <label for="summernote1">Content</label>
                                        <div class="form-group">
                                            <textarea id="summernote" class="form-control" rows="3" placeholder="Enter ...">{!! $dataArray->content !!} </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                        <div class="card-footer">
                            <a href="{{ route('redirect-custom-mail-list') }}" type="submit"
                                class="btn btn-secondary">Back</a>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@stop

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>

    <!-- Page specific script -->
    <script>
        $(function() {
            // Summernote
            $("#summernote").summernote({
                height: 300,
                toolbar: [
                    ['style', []],
                    ['font', ['']],
                    ['fontname', ['']],
                    ['fontsize', ['']],
                    ['para', ['']],
                    ['insert', ['', '']],
                    ['picture', ['']]
                ],
            });
            $('#summernote').summernote('disable');
        })
    </script>
@endsection
