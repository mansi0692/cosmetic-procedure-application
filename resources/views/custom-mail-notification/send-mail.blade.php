@extends('master')

@section('title', 'Admin')

@section('styles')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/select2/css/select2.min.css') }}">
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <!-- form start -->
                        <form id="regForm" action="{{ route('send-custom-mail') }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Send to -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="send_to" class="required">Send to</label>
                                                <select class="form-control" name="send_to" id="send_to"
                                                    onchange="leaveChange(this.value)">
                                                    <option value="all"> All </option>
                                                    <option value="specific_user"> Specific user </option>
                                                </select>
                                            </div>
                                            @if ($errors->has('send_to'))
                                                <span class="text-danger">{{ $errors->first('send_to') }}</span>
                                            @endif
                                        </div>

                                        <!-- Email -->
                                        <div class="col-md-6 email-div">
                                            <div class="form-group">
                                                <label for="full_name"class="required">Email</label>
                                                <select class="js-example-basic-multiple" name="email[]"
                                                    id="selectMultiEmail" multiple="multiple">
                                                </select>
                                                <span id="custom-error-multipleemail-span" class="text-danger"></span>
                                            </div>
                                            @if ($errors->has('email'))
                                                <span class="text-danger">{{ $errors->first('email') }}</span>
                                            @endif
                                        </div>
                                        <!-- Subject -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="subject"class="required">Subject</label>
                                                <input name="subject" type="text" class="form-control" id="subject"
                                                    placeholder="Subject">
                                            </div>
                                            @if ($errors->has('subject'))
                                                <span class="text-danger">{{ $errors->first('subject') }}</span>
                                            @endif
                                        </div>
                                        <!-- Emai Content -->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <textarea id="summernote" name="emailcontent" style="display: block;"> </textarea>
                                                <span id="custom-error-content-span" class="text-danger"></span>
                                            </div>
                                            @if ($errors->has('emailcontent'))
                                                <span class="text-danger">{{ $errors->first('emailcontent') }}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('redirect-custom-mail-list') }}" type="submit"
                                    class="btn btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div> <!-- /.card -->
                </div>
            </div> <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Summernote -->
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/select2/js/select2.min.js') }}"></script>

    <!-- Page specific script -->
    <script>
        // Give select2 data from controller
        var dataArray = {!! json_encode($dataArray) !!};
        var descriptionContent = '';
        var formSubmit = false;

        function leaveChange(value) {
            if (value === "all") {
                $('.email-div').hide();
                $("#selectMultiEmail").val('').trigger('change')
                $("#selectMultiEmail").empty().trigger('change')
                $("#custom-error-multipleemail-span").text("");
                setTimeout(() => {
                    $(".js-example-basic-multiple").select2({
                        data: dataArray,
                        width: '100%'
                    });
                }, 200);
            } else if (value === "specific_user") {
                $('.email-div').show();
                $("#custom-error-multipleemail-span").text("");
            }
        }
        $(document).ready(function() {
            $(".js-example-basic-multiple").select2({
                data: dataArray,
                width: '100%'
            });
            $('#selectMultiEmail').change(function(e) {
                $("#custom-error-multipleemail-span").text("");
            });
            var e = document.getElementById("send_to");
            var emailValue = e.value;
            leaveChange(emailValue);

            $("#regForm").validate({
                rules: {
                    send_to: {
                        required: true,
                    },
                    email: {
                        required: true,
                    },
                    subject: {
                        required: true,
                    },
                    summernote: {
                        required: true,
                    },
                },
                messages: {
                    send_to: {
                        required: "Please select type",
                    },
                    subject: {
                        required: "Subject is required",
                    },
                    summernote: {
                        required: "Email content is required",
                    },
                }
            });
        });
        $(function() {

            // Summernote
            $("#summernote").summernote({
                height: 300,
                callbacks: {
                    onChange: function(contents, $editable) {
                        descriptionContent = contents;
                        if (descriptionContent != '') {
                            $("#custom-error-content-span").text("");
                            formSubmit = false;
                        }
                    }
                },
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ol', 'ul', 'paragraph']],
                    ['insert', ['link']]
                ]
            });

        })
        $("#regForm").submit(function(e) {
            var selectMultiEmail = $('#selectMultiEmail').val();
            var e = document.getElementById("send_to");
            var emailValue = e.value;
            if (emailValue == 'specific_user') {
                if (selectMultiEmail == '' || selectMultiEmail == null) {
                    $("#custom-error-multipleemail-span").text("Please select email");
                    formSubmit = false;
                } else {
                    $("#custom-error-multipleemail-span").text("");
                    formSubmit = true;
                }
            }

            if (descriptionContent === '') {
                $("#custom-error-content-span").text("Content is required");
                formSubmit = false;
            } else {
                $("#custom-error-content-span").text("");
                formSubmit = true;
            }
            if (formSubmit) {
                return true;
            } else {
                return false;
            }
        });
    </script>
@endsection
