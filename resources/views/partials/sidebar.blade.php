<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4" style="background-color: #7F18E5">
    <!-- Brand Logo -->
    <a class="brand-link" href="{{ route('dashboard') }}">
        <img src="{{ asset('assets/images/site-logo.svg') }}" alt="Site logo"
            style="width: 50%; margin-left: 40px;margin-top: 10px;">
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('dashboard') }}"
                        class="{{ Request::segment(1) == 'dashboard' ? 'nav-link active' : 'nav-link' }}"
                        onMouseOver="this.style.color='#0F0'">
                        <i class="nav-icon fas fa-tachometer-alt" style="color: white"></i>
                        <p style="color: white">
                            Dashboard
                        </p>
                    </a>

                    <a href="{{ route('list-master-category') }}"
                        class="{{ Request::segment(1) == 'list-master-category' || Request::segment(1) == 'list-master-sub-category' || Request::segment(1) == 'add-master-sub-category' || Request::segment(1) == 'edit-master-sub-category' ? 'nav-link active' : 'nav-link' }}"
                        style=":hover {background: yellow}">
                        <i class="nav-icon fa fa-list-alt" style="color: white"></i>
                        <p style="color: white">
                            Category
                        </p>
                    </a>
                    @if ($role == 'Super Admin')
                        <a href="{{ route('list-doctor-admin') }}"
                            class="{{ Request::segment(1) == 'list-doctor-admin' ||
                            Request::segment(1) == 'list-patient-admin' ||
                            Request::segment(1) == 'add-doctor-patient-admin' ||
                            Request::segment(1) == 'edit-doctor-patient-admin'
                                ? 'nav-link active'
                                : 'nav-link' }}"
                            style=":hover {background: yellow}">
                            <i class="nav-icon fa fa-user" style="color: white"></i>
                            <p style="color: white">
                                Admin
                            </p>
                        </a>
                    @endif
                    @if ($role == 'Doctor Admin' || $role == 'Super Admin')
                        <a href="{{ route('list-doctor') }}"
                            class="{{ Request::segment(1) == 'list-doctor' ||
                            Request::segment(1) == 'document-doctor' ||
                            Request::segment(1) == 'add-doctor' ||
                            Request::segment(1) == 'edit-doctor' ||
                            Request::segment(1) == 'view-doctor'
                                ? 'nav-link active'
                                : 'nav-link' }}"
                            style=":hover {background: yellow}">
                            <i class="nav-icon fa fa-hospital-user" style="color: white"></i>
                            <p style="color: white">
                                Doctor
                            </p>
                        </a>
                    @endif
                    @if ($role == 'Patient Admin' || $role == 'Super Admin')
                        <a href="{{ route('list-patient') }}"
                            class="{{ Request::segment(1) == 'list-patient' ||
                            Request::segment(1) == 'view-patient' ||
                            Request::segment(1) == 'edit-patient'
                                ? 'nav-link active'
                                : 'nav-link' }}"
                            style=":hover {background: yellow}">
                            <i class="nav-icon fas fa-wheelchair" style="color: white"></i>
                            <p style="color: white">
                                Patient
                            </p>
                        </a>
                    @endif
                    <a href="{{ route('custom-mail-notification-list') }}"
                        class="{{ Request::segment(1) == 'custom-mail-notification-list' ||
                        Request::segment(1) == 'custom-mail-notification-add' ||
                        Request::segment(1) == 'view-custom-mail-notification'
                            ? 'nav-link active'
                            : 'nav-link' }}"
                        style=":hover {background: yellow}">
                        <i class="nav-icon fa fa-envelope" style="color: white"></i>
                        <p style="color: white">
                            Custom Mail
                        </p>
                    </a>
                    @if ($role == 'Doctor Admin' || $role == 'Super Admin')
                        <a href="{{ route('doctor-invite-list', ['tabNumber' => 0]) }}"
                            class="{{ Request::segment(1) == 'doctor-invite-list' || Request::segment(1) == 'doctor-invite-send-invite' ? 'nav-link active' : 'nav-link' }}"
                            style=":hover {background: yellow}">
                            <i class="nav-icon fa fa-calendar" style="color: white"></i>
                            <p style="color: white">
                                Invite Doctor
                            </p>
                        </a>
                    @endif
                    @if ($role == 'Patient Admin' || $role == 'Super Admin')
                        <a href="{{ route('patient-cancel-request-list-pending') }}"
                            class="{{ Request::segment(1) == 'patient-cancel-request-list-pending' || Request::segment(1) == 'view-patient-cancel-request' || Request::segment(1) == 'patient-cancel-request-list-approved' ? 'nav-link active' : 'nav-link' }}"
                            style=":hover {background: yellow}">
                            <i class="nav-icon fa fa-ban" style="color: white"></i>
                            <p style="color: white">
                                Patient Cancel Request
                            </p>
                        </a>
                    @endif

                    <a href="{{ route('contact-us-list') }}"
                        class="{{ Request::segment(1) == 'contact-us-list' || Request::segment(1) == 'contact-us-reply' ? 'nav-link active' : 'nav-link' }}"
                        style=":hover {background: yellow}">
                        <i class="nav-icon fa fa-address-book" style="color: white"></i>
                        <p style="color: white">
                            Contact Us
                        </p>
                    </a>
                    @if ($role == 'Super Admin')
                        <a href="{{ route('cms-list', ['languageId' => 1]) }}"
                            class="{{ Request::segment(1) == 'cms-list' || Request::segment(1) == 'add-cms' || Request::segment(1) == 'edit-cms' ? 'nav-link active' : 'nav-link' }}"
                            style=":hover {background: yellow}">
                            <i class="nav-icon fa fa-file" style="color: white"></i>
                            <p style="color: white">
                                CMS Pages
                            </p>
                        </a>
                    @endif
                    @if ($role == 'Doctor Admin' || $role == 'Super Admin')
                        <a href="{{ route('list-doctor-review') }}"
                            class="{{ Request::segment(1) == 'list-doctor-review' || Request::segment(1) == 'doctor-rating' ? 'nav-link active' : 'nav-link' }}"
                            style=":hover {background: yellow}">
                            <i class="nav-icon fa fa-star" style="color: white"></i>
                            <p style="color: white">
                                Doctor Review
                            </p>
                        </a>
                    @endif
                    @if ($role == 'Patient Admin' || $role == 'Super Admin')
                        <a href="{{ route('list-patient-review') }}"
                            class="{{ Request::segment(1) == 'list-patient-review' || Request::segment(1) == 'patient-rating' ? 'nav-link active' : 'nav-link' }}"
                            style=":hover {background: yellow}">
                            <i class="nav-icon fa fa-star" style="color: white"></i>
                            <p style="color: white">
                                Patient Review
                            </p>
                        </a>
                    @endif

                    <a href="{{ route('list-email-template', ['languageId' => 1]) }}"
                        class="{{ Request::segment(1) == 'list-email-template' || Request::segment(1) == 'edit-email-template' ? 'nav-link active' : 'nav-link' }}"
                        style=":hover {background: yellow}">
                        <i class="nav-icon fa fa-envelope" style="color: white"></i>
                        <p style="color: white">
                            Email Template
                        </p>
                    </a>

                    <a href="{{ route('list-sms-template', ['languageId' => 1]) }}"
                        class="{{ Request::segment(1) == 'list-sms-template' || Request::segment(1) == 'edit-sms-template' ? 'nav-link active' : 'nav-link' }}"
                        style=":hover {background: yellow}">
                        <i class="nav-icon fa fa-sms" style="color: white"></i>
                        <p style="color: white">
                            SMS Template
                        </p>
                    </a>

                    @if ($role == 'Super Admin' || $role == 'Patient Admin' || $role == 'Doctor Admin')
                        <a href="{{ route('list-faq-category', ['languageId' => 1]) }}"
                            class="{{ Request::segment(1) == 'list-faq-category' || Request::segment(1) == 'edit-faq-category' || Request::segment(1) == 'list-faqs' || Request::segment(1) == 'add-faqs' || Request::segment(1) == 'edit-faqs' ? 'nav-link active' : 'nav-link' }}"
                            style=":hover {background: yellow}">
                            <i class="nav-icon fa fa-question" style="color: white"></i>
                            <p style="color: white">
                                FAQ
                            </p>
                        </a>
                    @endif

                    <a href="{{ route('list-top-speciality') }}"
                        class="{{ Request::segment(1) == 'list-top-speciality' || Request::segment(1) == 'edit-top-speciality' ? 'nav-link active' : 'nav-link' }}"
                        style=":hover {background: yellow}">
                        <i class="nav-icon fa fa-hospital-user" style="color: white"></i>
                        <p style="color: white">
                            Top Specialities
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
