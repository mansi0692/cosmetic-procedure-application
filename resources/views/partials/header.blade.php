<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <h1>@yield('title')</h1>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" id="nav-link-dropdown" data-toggle="dropdown" data-slide="true" href="#" role="button">
                <i class="fas fa-th-large"></i>
                <span>{{ $role }}</span>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="{{ route('redirectToChangePassword') }}" class="dropdown-item">
                        Change password
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item">
                        Logout
                    </a>
            </a>
            </a>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
<style>
    #nav-link-dropdown {
        height: 0;
    }

</style>
