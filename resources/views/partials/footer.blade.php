<footer class="main-footer" style="display: none">
    <strong>Copyright &copy; {{ date('Y') }} <a href="{{ route('dashboard') }}">Yu+Me</a>.</strong> All rights
    reserved.
</footer>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<script>
    $(document).ready(function() {
        setTimeout(function() {
            $('.alert').fadeOut('fast');
        }, 2000);
    })
</script>
@yield('scripts')
