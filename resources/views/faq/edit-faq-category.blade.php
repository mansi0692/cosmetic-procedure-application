@extends('master')

@section('title', 'FAQ')

@section('styles')

@endsection

@section('content')
    <?php
    $languageId = Request::segment(3);
    ?>
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <form id="regForm"
                            action="{{ route('save-faq-category', ['faqCategoryId' => Request::segment(2), 'languageId' => $languageId]) }}"
                            method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="title"
                                                    class="required">Title({{ $data[0]->language->title }})</label>
                                                <input name="title" type="text" value="{{ $data[0]->name }}"
                                                    class="form-control" id="sub_category" placeholder="Title">
                                                <input type="hidden" value="" name="category-value">
                                            </div>
                                            <span id="custom-error-title-span" class="text-danger"></span>
                                            @if ($errors->all())
                                                <span class="text-danger">{{ $errors->first() }}</span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('list-faq-category', ['languageId' => Request::segment(3)]) }}"
                                    type="submit" class="btn btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#regForm").validate({
                rules: {
                    title: {
                        required: true,
                    }
                },
                messages: {
                    title: {
                        required: "Title is required",
                    },
                }
            });
        });
    </script>
@stop
