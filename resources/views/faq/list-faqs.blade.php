@extends('master')

@section('title', 'FAQ - ' . $faqs['categories'][0]['name'])

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

@endsection

@section('content')
    <?php
    $languageId = Request::segment(3);
    ?>

    <div class="category-content-wrapper">

        <a href="{{ route('add-faqs', ['faqsId' => Request::segment(2), 'languageId' => $languageId]) }}"
            class="btn add-btn float-right" style="background-color: #FF008A; margin: 15px; color: white; width: 80px">Add</a>

        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if (session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    <table id="faqs" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Title</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (!empty($faqs['faqs']))
                                                @foreach ($faqs['faqs'] as $data)
                                                    <tr>
                                                        <td>{{ $data->title }}</td>
                                                        <td class="description">{!! $data->description !!}</td>
                                                        <td>
                                                            <a href="{{ route('edit-faqs', ['faqsId' => $data->id, 'faqCategoryId' => Request::segment(2), 'languageId' => $languageId]) }}"
                                                                title="Edit">
                                                                <i class="fa fa-edit" aria-hidden="true"></i></a>

                                                            @if ($data->status == 1)
                                                                <a href="{{ route('change-faqs-status', ['status' => 'inactive', 'faqsId' => $data->id]) }}"
                                                                    title="Inactive">
                                                                    <i class="fa fa-ban" aria-hidden="true"></i>
                                                                </a>
                                                            @else
                                                                <a href="{{ route('change-faqs-status', ['status' => 'active', 'faqsId' => $data->id]) }}"
                                                                    title="Active">
                                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                                </a>
                                                            @endif

                                                            <a onclick="return confirm('<?php echo trans('messages.label.ARE_YOU_SURE_TO_DELETE_THIS_RECORD'); ?>')"
                                                                href="{{ route('delete-faqs', ['faqsId' => $data->id]) }}"
                                                                title="Delete">
                                                                <i class="fa fa-trash" aria-hidden="true"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach

                                            @endif

                                        </tbody>
                                    </table>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.12.1/dataRender/ellipsis.js"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#faqs").DataTable({
                "bSort": false,
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [2]
                }],
                "columnDefs": [{
                    "targets": [0],
                    "render": $.fn.dataTable.render.ellipsis(70, true),
                    "targets": [1],
                    "render": $.fn.dataTable.render.ellipsis(100, true)
                }]
            });
        });
    </script>
@endsection
