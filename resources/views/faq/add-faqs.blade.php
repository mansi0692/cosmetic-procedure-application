@extends('master')

@section('title', 'FAQ')

@section('styles')
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.min.css') }}">

@endsection

@section('content')
    <?php
    $languageId = Request::segment(1) == 'edit-faqs' ? Request::segment(4) : Request::segment(3);
    $faqCategoryId = Request::segment(1) == 'edit-faqs' ? Request::segment(3) : Request::segment(2);
    ?>
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <form id="regForm"
                            action="{{ Request::segment(1) == 'edit-faqs'
                                ? route('update-faqs', [
                                    'faqsId' => Request::segment(2),
                                    'faqCategoryId' => Request::segment(3),
                                    'languageId' => $languageId,
                                ])
                                : route('save-faqs', ['faqsId' => Request::segment(2), 'languageId' => $languageId]) }}"
                            method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Title -->
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="title"class="required">Title</label>
                                                <input name="title" type="text" class="form-control" id="title"
                                                    value="{{ Request::segment(1) == 'edit-faqs' && !empty($faqData) ? $faqData->title : '' }}"
                                                    placeholder="Title">
                                                @if ($errors->has('title'))
                                                    <span class="text-danger">{{ $errors->first('title') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <!-- Title -->
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="description"class="required">Description</label>
                                                <textarea id="summernote" name="description" style="display: none;">{{ Request::segment(1) == 'edit-faqs' && !empty($faqData) ? $faqData->description : '' }}</textarea>
                                                <span id="custom-error-description-span" class="text-danger"></span>
                                                @if ($errors->has('description'))
                                                    <span class="text-danger">{{ $errors->first('description') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">
                                    {{ Request::segment(1) == 'edit-faqs' ? 'Update' : 'Submit' }}
                                </button>
                                <a href="{{ route('list-faqs', ['faqCategoryId' => (int) $faqCategoryId, 'languageId' => $languageId]) }}"
                                    type="submit" class="btn btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->


                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')
    <!-- Page specific script -->
    <!-- Summernote -->
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script>
        var descriptionContent = $("#summernote").text();
        $(document).ready(function() {

            // Summernote
            $("#summernote").summernote({
                height: 300,
                callbacks: {
                    onChange: function(contents, $editable) {
                        descriptionContent = contents;
                        if (descriptionContent != '') {
                            $("#custom-error-description-span").text("");
                        }
                    }
                },
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ol', 'ul', 'paragraph']],
                    ['insert', ['picture']],
                    ['picture', ['link']]
                ]
            });


        });
        var formSubmit = false;
        $("#regForm").submit(function(e) {
            if (descriptionContent === '') {
                $("#custom-error-description-span").text("Description required");
                formSubmit = false;
                return false;
            } else {
                $("#custom-error-description-span").text("");
            }
        });
        $(function() {
            $("#regForm").validate({
                rules: {
                    title: {
                        required: true,
                    },
                },
                messages: {
                    title: {
                        required: "Title required",
                    },
                }
            });
        });
    </script>

    </html>
@stop
