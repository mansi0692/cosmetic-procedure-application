@extends('master')

@section('title', 'FAQ')
@section('content')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
    @php
        $languageId = Request::segment(2);
        $value = '';
        foreach ($faqCategoryData['language'] as $key => $data) {
            $selected = Request::segment(2) == $data->id ? 'selected' : '';
            $value .= '<option ' . $selected . ' value="' . $data->id . '">' . $data['title'] . '</option>';
        }
    @endphp
    <div class="category-content-wrapper">

        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    <table id="categoryList" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>Name</th>
                                                <th class="no-sort">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($faqCategoryData['category'] as $data)
                                                <tr>
                                                    <td></td>
                                                    <td><?php echo str_replace('_', ' ', ucwords($data->name, '_')); ?>
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('edit-faq-category', ['categoryId' => $data->id, 'languageId' => $languageId]) }}"
                                                            title="Edit">
                                                            <i class="fa fa-edit" aria-hidden="true"></i></a>
                                                        <a
                                                            href="{{ route('list-faqs', ['faqCategoryId' => $data->id, 'languageId' => $languageId]) }}">
                                                            <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#categoryList").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "searching": false,
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [2]
                }]
            })
            $("#categoryList_wrapper .row:first-child> :first-child").append(
                '<select class="form-control" onchange="location = this.value" style="width:20%"><?php echo $value; ?></select>'
            );
        });


        var addSerialNumber = function() {
            var i = 1
            $('table tbody tr').each(function(index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };

        addSerialNumber();
    </script>
@endsection
@stop
