@extends('master')

@section('title', 'Email Template')

@section('styles')

    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/summernote/summernote-bs4.min.css') }}">

@endsection

@section('content')
    @php
        $emailTemplateId = Request::segment(2);
        $languageId = Request::segment(3);
    @endphp
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="regForm"
                            action="{{ route('update-email-template', ['emailTemplateId' => $emailTemplateId, 'languageId' => $languageId]) }}"
                            method="post">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Subject -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="subject">Subject</label>
                                                <input value="{{ $emailTemplate->template_subject }}" name="subject"
                                                    type="text" class="form-control" id="subject"
                                                    placeholder="Subject">
                                            </div>
                                        </div>

                                        <!-- Email Content -->
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="description">Enter Description</label>
                                                <textarea id="summernote" name="description" style="display: none;">{!! $emailTemplate->template_body !!} </textarea>
                                            </div>
                                            @if ($errors->has('title'))
                                                <span class="text-danger">{{ $errors->first('description') }}</span>
                                            @endif
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('list-email-template', ['languageId' => $languageId]) }}" type="submit"
                                    class="btn btn-secondary">Cancel</a>
                            </div>

                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@stop

@section('scripts')
    <!-- Summernote -->
    <script src="{{ asset('assets/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(document).ready(function() {

            // Summernote
            $("#summernote").summernote({
                height: 300,
                toolbar: [
                    ['style', ['style', 'code']],
                    ['font', ['bold', 'italic', 'underline']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['para', ['ol', 'ul', 'paragraph']],
                    ['insert', ['picture', 'codeview']],
                ]
            });


        });
    </script>
@endsection
