@extends('master')

@section('title', 'Contact Us')

@section('styles')

@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form id="regForm"
                            action="{{ route('send-contact-us-reply', ['contactUsId' => Request::segment(2)]) }}"
                            method="post">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Email -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input value="{{ $contactUs->email }}" name="email" type="text"
                                                    class="form-control" id="email" placeholder="Email" readonly>
                                            </div>
                                        </div>

                                        <!-- Subject -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="subject">Subject</label>
                                                <input value="{{ $contactUs->subject }}" name="subject" type="text"
                                                    class="form-control" id="subject" placeholder="Subject" readonly>
                                            </div>
                                        </div>

                                        <!-- Message -->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="message">Message</label>
                                                <textarea class="form-control" rows="10" id="message" name="message" placeholder="Message" readonly>{{ $contactUs->message }}</textarea>
                                            </div>
                                        </div>

                                        <!-- Email Content -->
                                        @if ($contactUs->is_replied)
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="subject">Reply Message</label>
                                                    <textarea class="form-control" rows="10" id="emailcontent" name="emailcontent" placeholder="Enter reply" disabled> {{ $contactUs->reply_message }}</textarea>
                                                </div>
                                            </div>
                                        @else
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="subject"class="required">Reply</label>
                                                    <textarea class="form-control" rows="10" id="emailcontent" name="emailcontent" placeholder="Enter reply"></textarea>
                                                </div>
                                                @if ($errors->has('emailcontent'))
                                                    <span class="text-danger">{{ $errors->first('emailcontent') }}</span>
                                                @endif
                                            </div>
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                @if (!$contactUs->is_replied)
                                    <button type="submit" class="btn add-btn"
                                        style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                    <a href="{{ route('redirect-contact-us-list') }}" type="submit"
                                        class="btn btn-secondary">Cancel</a>
                                @else
                                    <a href="{{ route('redirect-contact-us-list') }}" type="submit"
                                        class="btn btn-secondary">Back</a>
                                @endif
                            </div>
                        </form>
                    </div>
                    <!-- /.card-body -->


                </div>
                <!-- /.card -->


            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>

@stop

@section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(document).ready(function() {
            $("#regForm").validate({
                rules: {
                    emailcontent: {
                        required: true,
                    },
                },
                messages: {
                    emailcontent: {
                        required: "Email content is required",
                    },
                }
            });
        });
    </script>
@endsection
