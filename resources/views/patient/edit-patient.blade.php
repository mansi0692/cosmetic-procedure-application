@extends('master')

@section('title', 'Patient')

@section('styles')

@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('update-patient', ['patientId' => Request::segment(2)]) }}" method="post"
                            id="regForm">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- First name -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="full_name"class="required">Name</label>
                                                <input value="{{ $patientData->full_name }}" name="full_name" type="text"
                                                    class="form-control" id="full_name" placeholder="Name">
                                                @if ($errors->has('full_name'))
                                                    <span class="text-danger">{{ $errors->first('full_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Username -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="username"class="required">Username</label>
                                                <input value="{{ $patientData->username }}" name="username" type="username"
                                                    class="form-control" id="username" placeholder="Username">
                                                @if ($errors->has('username'))
                                                    <span class="text-danger">{{ $errors->first('username') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Email -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email"class="required">Email address</label>
                                                <input value="{{ $patientData->email }}" name="email" type="email"
                                                    class="form-control" id="email" placeholder="Email address">
                                                @if ($errors->has('email'))
                                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Country -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="country"class="required">Country</label>
                                                <select class="form-control" name="country">
                                                    @if (isset($patientData->country_id))
                                                        @foreach ($country as $data)
                                                            @if ($data->id == $patientData->country_id)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @else
                                                                <option value="{{ $data->id }}"> {{ $data->name }}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option value=""></option>
                                                        @foreach ($country as $data)
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('country'))
                                                    <span class="text-danger">{{ $errors->first('country') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- State -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="state"class="required">State</label>
                                                <select class="form-control" name="state" id="state">
                                                    @if (isset($patientData->state_id))
                                                        @foreach ($state as $data)
                                                            @if ($data->id == $patientData->state_id)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @else
                                                                <option value="{{ $data->id }}"> {{ $data->name }}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option value=""></option>
                                                        @foreach ($state as $data)
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('state'))
                                                    <span class="text-danger">{{ $errors->first('state') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- City -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city"class="required">City</label>
                                                <select class="form-control" name="city">
                                                    @if (isset($patientData->city_id))
                                                        @foreach ($city as $data)
                                                            @if ($data->id == $patientData->city_id)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @else
                                                                <option value="{{ $data->id }}"> {{ $data->name }}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option value=""></option>
                                                        @foreach ($city as $data)
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('city'))
                                                    <span class="text-danger">{{ $errors->first('city') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <!-- Zipcode -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="zipcode"class="required">Zipcode</label>
                                                <input value="{{ $patientData->zipcode }}" name="zipcode" type="text"
                                                    class="form-control" id="zipcode" placeholder="Zipcode">
                                                @if ($errors->has('zipcode'))
                                                    <span class="text-danger">{{ $errors->first('zipcode') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <!-- Phone No. -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="phone_number"class="required">Phone No.</label>
                                                <input value="{{ $patientData->phone_number }}" name="phone_number"
                                                    type="text" class="form-control" id="phone_number"
                                                    placeholder="Phone No.">
                                                @if ($errors->has('phone_number'))
                                                    <span class="text-danger">{{ $errors->first('phone_number') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('redirect-patient-list') }}" type="submit"
                                    class="btn btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            var stateId = $('select[name="state"]').val();
            getCItyFromState(stateId);
            $('select[name="state"]').on('change', function() {
                stateId = $('select[name="state"]').val();
                getCItyFromState(stateId);
            });
        });

        function getCItyFromState(stateId) {
            if (stateId) {
                $.ajax({
                    url: "{{ url('/get-city-from-state/') }}/" + stateId,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        $('select[name="city"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="city"]').append('<option value="' + value.id + '">' + value
                                .name + '</option>');
                        });
                    }
                });
            } else {
                $('select[name="city"]').empty();
            }
        }
        $(function() {
            jQuery.validator.addMethod("noSpace", function(value, element) {
                return (value.trim() == value) && (value.indexOf(" ") < 0);
            });
            jQuery.validator.addMethod("zipcodeRegex", function(value, element) {
                return /^(?:(\d{5})(?:[ \-](\d{4}))?)$/i.test(value);
            }, 'Please enter a valid zipcode');
            jQuery.validator.addMethod("email", function(value, element) {
                return /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
            });
            jQuery.validator.addMethod("tendigit", function(value, element) {
                if (value.length === 10) {
                    return true;
                }
                return false;
            }, "Phone no should be exact 10 digit");
            $("#regForm").validate({
                rules: {
                    full_name: {
                        required: true,
                        minlength: 3,
                        maxlength: 50,
                    },
                    username: {
                        required: true,
                        noSpace: true,
                        minlength: 3,
                        maxlength: 50,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    state: {
                        required: true,
                    },
                    state: {
                        required: true,
                    },
                    country: {
                        required: true,
                    },
                    zipcode: {
                        zipcodeRegex: true,
                    },
                    phone_number: {
                        required: true,
                        digits: true,
                        tendigit: true,
                    },
                },
                messages: {
                    full_name: {
                        required: "Full name required",
                    },
                    username: {
                        required: "Username name required",
                    },
                    email: {
                        required: "Email is required",
                        email: "Please enter a valid email"
                    },
                    city: {
                        required: "Please select city",
                    },
                    state: {
                        required: "Please select state",
                    },
                    country: {
                        required: "Please select state",
                    },
                    phone_number: {
                        required: "Phone no required",
                        digits: "Please enter numeric value",
                        maxlength: "Phone no not more than 10 digits"
                    },
                }
            });
        });
    </script>
@endsection
