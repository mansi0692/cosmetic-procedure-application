@extends('master')

@section('title', 'Patient')

@section('styles')

@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('update-patient', ['patientId' => Request::segment(2)]) }}" method="post">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Profile Image -->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                @if ($patientData->profile_image != '')
                                                    <img src="{{ url('/storage/uploads/' . $patientData->profile_image) }}"
                                                        style="height: 200px; width:200px" alt="Profile image">
                                                @else
                                                    <img src="{{ asset('assets/images/default_profile.jpg') }}"
                                                        style="height: 200px; width:200px" alt="Profile image">
                                                @endif
                                            </div>
                                        </div>
                                        <!-- First name -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="full_name">Name</label>
                                                <input value="{{ $patientData->full_name }}" name="full_name" type="text"
                                                    class="form-control" id="full_name" placeholder="Name" disabled>
                                            </div>
                                        </div>
                                        <!-- Username -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="username">Username</label>
                                                <input value="{{ $patientData->username }}" name="username" type="text"
                                                    class="form-control" id="username" placeholder="Username" disabled>
                                            </div>
                                        </div>
                                        <!-- Email -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email">Email address</label>
                                                <input value="{{ $patientData->email }}" name="email" type="email"
                                                    class="form-control" id="email" placeholder="Email address"
                                                    disabled>
                                            </div>
                                        </div> 
                                        <!-- Country -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="country">Country</label>
                                                <select class="form-control" name="country" disabled>
                                                    @if (isset($patientData->country_id))
                                                        @foreach ($country as $data)
                                                            @if ($data->id == $patientData->country_id)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option selected></option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>                                       
                                        <!-- State -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="state">State</label>
                                                <select class="form-control" name="state" disabled>
                                                    @if (isset($patientData->state_id))
                                                        @foreach ($state as $data)
                                                            @if ($data->id == $patientData->state_id)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option selected></option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>                                        
                                        <!-- City -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city">City</label>
                                                <select class="form-control" name="city" disabled>
                                                    @if (isset($patientData->city_id))
                                                        @foreach ($city as $data)
                                                            @if ($data->id == $patientData->city_id)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option selected></option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Zipcode -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="zipcode">Zipcode</label>
                                                <input value="{{ $patientData->zipcode }}" name="zipcode" type="text"
                                                    class="form-control" id="zipcode" placeholder="Zipcode" disabled>
                                            </div>
                                        </div>

                                        <!-- Phone No. -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="phone_number">Phone No.</label>
                                                <input value="{{ $patientData->phone_number }}" name="phone_number"
                                                    type="text" class="form-control" id="phone_number"
                                                    placeholder="Phone No." disabled>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <a href="{{ route('redirect-patient-list') }}" type="submit"
                                    class="btn btn-secondary">Back</a>
                            </div>
                    </div>
                    <!-- /.card-body -->
                    </form>
                </div>
                <!-- /.card -->


            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')

@endsection
