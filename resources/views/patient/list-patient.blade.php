@extends('master')

@section('title', 'Patient')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="category-content-wrapper">
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if (session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    <table id="patientList" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>Name</th>
                                                <th>Email address</th>
                                                <th>Country</th>
                                                <th>City</th>
                                                <th>State</th>
                                                <th>Status</th>
                                                <th class="no-sort">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($patients as $data)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ strlen($data->full_name) >= 50 ? substr($data->full_name, 0, 20) . '...' : $data->full_name }}
                                                    </td>
                                                    <td>{{ $data->email }}</td>
                                                    <td>{{ $data->country_name }}</td>
                                                    <td>{{ $data->city_name }}</td>
                                                    <td>{{ $data->state_name }}</td>
                                                    @if ($data->status)
                                                        <td>Active</td>
                                                    @else
                                                        <td>Inactive</td>
                                                    @endif
                                                    <td>
                                                        <a href="{{ route('edit-patient', ['patientId' => $data->id]) }}"
                                                            title="Edit">
                                                            <i class="fa fa-edit" aria-hidden="true"></i></a>
                                                        <a href="{{ route('view-patient', ['patientId' => $data->id]) }}"
                                                            title="View">
                                                            <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        @if ($data->status)
                                                            <a href="{{ route('change-status-patient', ['status' => 'active', 'patientId' => $data->id]) }}"
                                                                title="Inactive">
                                                                <i class="fa fa-ban" aria-hidden="true"></i>
                                                            </a>
                                                        @else
                                                            <a href="{{ route('change-status-patient', ['status' => 'inactive', 'patientId' => $data->id]) }}"
                                                                title="Active">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </a>
                                                        @endif
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#patientList").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [7]
                }]
            });

        });

        var addSerialNumber = function() {
            var i = 1
            $('table tbody tr').each(function(index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };

        addSerialNumber();
    </script>
@endsection
