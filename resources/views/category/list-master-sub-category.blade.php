@extends('master')

@section('title', 'Category')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
    @php
        $mainCategory = config('constants.main_categories');
        foreach ($mainCategory as $key => $value) {
            if (Request::segment(2) == $key) {
                $label = $value;
            }
        }
    @endphp
    <div class="category-content-wrapper">
        @if (Request::segment(3) == 0)
            <a href="{{ route('add-master-sub-category', ['masterCategoryId' => Request::segment(2)]) }}"
                class="btn add-btn float-right"
                style="background-color: #FF008A; margin: 15px; color: white; width: 80px">Add</a>
        @endif

        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if (session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    @if (Request::segment(2) == 1)
                                        <div class="navigation-tab" style="margin-bottom: 10px;">
                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="{{ Request::segment(3) == 0 ? 'nav-link active' : 'nav-link' }}"
                                                        id="certification" aria-current="page"
                                                        href="{{ route('list-master-sub-category', ['masterCategoryId' => Request::segment(2), 'isOther' => 0]) }}">Certification</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="{{ Request::segment(3) == 1 ? 'nav-link active' : 'nav-link' }}"
                                                        href="{{ route('list-master-sub-category', ['masterCategoryId' => Request::segment(2), 'isOther' => 1]) }}"
                                                        id="other">Other</a>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif

                                    <table id="subCategory" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>{{ ucfirst($label) }}</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($masterSubCategory as $data)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $data->label }}</td>
                                                    <td>
                                                        <a href="{{ route('edit-master-sub-category', ['masterSubCategoryId' => $data->id, 'masterCategoryId' => Request::segment(2), 'isOther' => 0]) }}"
                                                            title="Edit">
                                                            <i class="fa fa-edit" aria-hidden="true"></i></a>
                                                        @if (Request::segment(3) == 0)
                                                            @if ($data->status == 1)
                                                                <a href="{{ route('change-master-sub-category-status', ['status' => 'active', 'masterSubCategoryId' => $data->id]) }}"
                                                                    title="Inactive">
                                                                    <i class="fa fa-ban" aria-hidden="true"></i>
                                                                </a>
                                                            @else
                                                                <a href="{{ route('change-master-sub-category-status', ['status' => 'inactive', 'masterSubCategoryId' => $data->id]) }}"
                                                                    title="Active">
                                                                    <i class="fa fa-check" aria-hidden="true"></i>
                                                                </a>
                                                            @endif
                                                        @endif
                                                        @if (Request::segment(3) == 1 && $data->is_other)
                                                            <a href="{{ route('approve-other-sub-category', ['masterSubCategoryId' => $data->id]) }}"
                                                                title="Approve">
                                                                <i class="fa fa-check-circle"></i>
                                                            </a>
                                                        @elseif(Request::segment(3) == 0 && $data->is_other)
                                                            <a href="{{ route('approve-other-sub-category', ['masterSubCategoryId' => $data->id]) }}"
                                                                title="Disapprove">
                                                                <i class="fa fa-window-close"></i>
                                                            </a>
                                                        @endif
                                                        <a onclick="return confirm('<?php echo trans('messages.label.ARE_YOU_SURE_TO_DELETE_THIS_RECORD'); ?>')"
                                                            href="{{ route('delete-master-sub-category', ['masterSubCategoryId' => $data->id]) }}"
                                                            title="Delete">
                                                            <i class="fa fa-trash" aria-hidden="true"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')

    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#subCategory").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [2]
                }]
            });
        });

        var addSerialNumber = function() {
            var i = 1
            $('table tbody tr').each(function(index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };

        addSerialNumber();
    </script>
@endsection
