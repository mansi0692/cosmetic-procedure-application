@extends('master')

@section('title', 'Category')

@section('styles')

@endsection

@section('content')
    <?php
    $mainCategory = config('constants.main_categories');
    foreach ($mainCategory as $key => $value) {
        if (Request::segment(3) == $key) {
            $label = $value;
        }
    }
    function getLabelByLanguage($language_id, $objArray)
    {
        foreach ($objArray as $obj) {
            if ($language_id == $obj['language_id']) {
                return $obj['label'];
            }
        }
        return null;
    }
    ?>
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <form id="regForm"
                            action="{{ route('update-master-sub-category', ['masterSubCategoryId' => Request::segment(2), 'masterCategoryId' => Request::segment(3), 'isOther' => 0]) }}"
                            method="post">
                            @csrf
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            @if (in_array($masterSubCategory->master_category_id, [1, 5, 6]))
                                                <div class="form-group">
                                                    <label for="sub_category" class="required">
                                                        {{ ucfirst($label) }}
                                                    </label>

                                                    <input value="{{ old('sub_category') ?? $masterSubCategory->label }}"
                                                        name="sub_category" type="text" id="sub_category"
                                                        class="form-control" placeholder="{{ ucfirst($label) }}">
                                                </div>
                                            @else
                                                @foreach ($languages as $language)
                                                    @php
                                                        $inputName = 'sub_category_' . $language->id;
                                                    @endphp

                                                    <div class="form-group">
                                                        <label for="{{ $inputName }}" class="required">
                                                            {{ ucfirst($label) }} ({{ $language->title }})
                                                        </label>

                                                        <input
                                                            value="{{ old($inputName) ?? getLabelByLanguage($language->id, $masterSubCategoryChild) }}"
                                                            name="{{ $inputName }}" type="text"
                                                            id="{{ $inputName }}" class="form-control"
                                                            placeholder="{{ ucfirst($label) }} ({{ $language->title }})">
                                                    </div>
                                                    @if ($errors->has($inputName))
                                                        <span class="text-danger">
                                                            {{ $errors->first($inputName) }}
                                                        </span>
                                                    @endif
                                                @endforeach
                                            @endif
                                            <input type="hidden" value="{{ $label }}" name="category">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->

                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('redirect-list', ['masterCategoryId' => Request::segment(3), 'isOther' => 0]) }}"
                                    type="submit" class="btn btn-secondary">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
    </section>
@stop
@section('scripts')
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <script>
        var label = '<?php echo $label; ?>';
        label = label[0].toUpperCase() + label.slice(1);
        var masterCategoryId = parseInt('<?php echo $masterSubCategory->master_category_id; ?>');
        var allLanguages = '<?php echo $languages; ?>';
    </script>

    <script src="{{ asset('assets/js/modules/sub-category-validation.js') }}"></script>
@stop
