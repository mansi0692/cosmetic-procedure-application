@extends('master')

@section('title', 'Category')
@section('content')
    <div>
        @if (session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
        <a href="{{ route('add-category') }}" class="btn add-btn float-right" style="background-color: #FF008A; margin: 10px; color: white">Add</a>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Type</th>
                    <th>Label</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @if (!empty($category) && $category->count())
                    @foreach ($category as $key => $value)
                        <tr>
                            <th scope="row">{{ $value->id }}</th>
                            <td>{{ str_replace('_',' ',ucwords($value->type,'_')) }}</td>
                            <td>{{ $value->label }}</td>
                            <td>
                                <a href="{{ route('edit-category', ['categoryId' => $value->id]) }}"
                                    class="btn btn-primary"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                @if ($value->status == 1)
                                    <a href="{{ route('change-status', ['status' => 'active', 'categoryId' => $value->id]) }}"
                                        class="btn btn-secondary"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                @else
                                    <a href="{{ route('change-status', ['status' => 'inactive', 'categoryId' => $value->id]) }}"
                                        class="btn btn-secondary"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                @endif

                                <a onclick="return confirm('<?php echo trans('messages.label.ARE_YOU_SURE_TO_DELETE_THIS_RECORD'); ?>')"
                                    href="{{ route('delete-category', ['categoryId' => $value->id]) }}"
                                    class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <th scope="row">There are no data.</th>
                    </tr>
                @endif
            </tbody>
        </table>
        {!! $category->links() !!}
    </div>

@stop
