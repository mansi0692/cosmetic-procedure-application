@extends('master')

@section('title', 'Doctor Documents')

@section('styles')

@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="card card-primary">
                <div class="card-body">
                    <div class="row" style="margin-left: 265px">
                        @if (sizeof($docArray))
                            @foreach ($docArray as $data)
                                @if ($data['type'] == 'doc' || $data['type'] == 'docx')
                                    <div class="col-sm-6 col-md-4 col-lg-3" style="width: 50px">
                                        <figure>
                                            <a href="{{ $data['doc_url'] }}" target="_blank"
                                                title="Download" class="delete-icon"
                                                style="float: right; margin-right: 110px;" download>
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                            </a>
                                            <a href="#" title="Upload" class="uploaded-image xls-type">
                                                <div class="file-wrapper">
                                                    <img src="{{ asset('assets/images/document.png') }}" alt="DOC"
                                                        style="height: 150px;width:150px;cursor: default" />
                                                </div>
                                            </a>
                                            <figcaption style="margin-left: 25px;">{{ $data['file_name'] }}
                                            </figcaption>
                                        </figure>
                                    </div>
                                @endif
                                @if ($data['type'] == 'pdf')
                                    <div class="col-sm-6 col-md-4 col-lg-3" style="width: 50px">
                                        <figure>
                                            <a href="{{ $data['doc_url'] }}" target="_blank"
                                                title="Download" class="delete-icon"
                                                style="float: right; margin-right: 110px;" download>
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                            </a>
                                            <a href="#" title="Upload" class="uploaded-image xls-type">
                                                <div class="file-wrapper">
                                                    <img src="{{ asset('assets/images/pdf.png') }}" alt="PDF"
                                                        style="height: 150px;width:150px;cursor: default" />
                                                </div>
                                            </a>
                                            <figcaption style="margin-left: 25px;">{{ $data['file_name'] }}
                                            </figcaption>
                                        </figure>
                                    </div>
                                @endif
                                @if ($data['type'] == 'xls' || $data['type'] == 'xlsx')
                                    <div class="col-sm-6 col-md-4 col-lg-3" style="width: 50px">
                                        <figure>
                                            <a href="{{ $data['doc_url'] }}" target="_blank"
                                                title="Download" class="delete-icon"
                                                style="float: right; margin-right: 110px;" download>
                                                <i class="fa fa-download" aria-hidden="true"></i>
                                            </a>
                                            <a href="#" title="Upload" class="uploaded-image xls-type">
                                                <div class="file-wrapper">
                                                    <img src="{{ asset('assets/images/excel.png') }}" alt="DOC"
                                                        style="height: 150px;width:150px;cursor: default" />
                                                </div>
                                            </a>
                                            <figcaption style="margin-left: 25px;">{{ $data['file_name'] }}
                                            </figcaption>
                                        </figure>
                                    </div>
                                @endif
                            @endforeach
                        @else
                            <label> No document found</label>

                        @endif
                    </div>
                </div>
                <div class="card-footer" style="margin-left: 250px;">
                    @if ($isDocumentVerified)
                        <a href="" type="button" class="btn btn-secondary"
                            style="margin: 10px; color: white; pointer-events: none;cursor: default"
                            disabled>Verify</button>
                        @else
                            <a href="{{ route('verify-doctor', ['doctorId' => Request::segment(2)]) }}" type="button"
                                class="btn add-btn"
                                style="background-color: #FF008A; margin: 10px; color: white">Verify</button>
                    @endif

                    <a href="{{ route('redirect-doctor-list') }}" type="submit" class="btn btn-secondary">Cancel</a>
                </div>
            </div>
        </div>
    </section>
@stop

@section('scripts')

@endsection
