@extends('master')

@section('title', 'Doctor')

@section('styles')
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
        href="{{ asset('assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('update-doctor', ['doctorId' => Request::segment(2)]) }}" method="post"
                            id="regForm">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- First name -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name" class="required">First name</label>
                                                <input value="{{ $doctorData->first_name }}" name="first_name"
                                                    type="text" class="form-control" id="first_name"
                                                    placeholder="First Name">
                                                @if ($errors->has('first_name'))
                                                    <span class="text-danger">{{ $errors->first('first_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Last name -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="last_name"class="required">Last name</label>
                                                <input value="{{ $doctorData->last_name }}" name="last_name" type="text"
                                                    class="form-control" id="last_name" placeholder="Last name">
                                                @if ($errors->has('last_name'))
                                                    <span class="text-danger">{{ $errors->first('last_name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Occupation -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="occupation"class="required">Board Certification</label>
                                                <select class="form-control" name='occupation'>
                                                    @foreach ($occuption as $data)
                                                        @if ($data->id == $doctorData->occupation_id)
                                                            <option value="{{ $data->label }}" selected>
                                                                {{ $data->label }} </option>
                                                        @else
                                                            <option value="{{ $data->label }}"> {{ $data->label }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('occupation'))
                                                    <span class="text-danger">{{ $errors->first('occupation') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <!-- Specialty -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="speciality"class="required">Sub Speciality</label>
                                                <select class="form-control" name="speciality">
                                                    @foreach ($specialty as $data)
                                                        @if ($data->id == $doctorData->speciality_id)
                                                            <option value="{{ $data->label }}" selected>
                                                                {{ $data->label }} </option>
                                                        @else
                                                            <option value="{{ $data->label }}"> {{ $data->label }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                @if ($errors->has('speciality'))
                                                    <span class="text-danger">{{ $errors->first('speciality') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <!-- Office addresses & contact information -->
                            <h5 style="font-weight: bold; margin-left: 20px"> Office addresses & contact information </h5>
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="street_address"class="required">Street address</label>
                                                <input value="{{ $doctorData->street_address }}" name="street_address"
                                                    type="text" class="form-control" id="street_address"
                                                    placeholder="Street address">
                                                @if ($errors->has('street_address'))
                                                    <span class="text-danger">{{ $errors->first('street_address') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="location_email"class="required">Email</label>
                                                <input value="{{ $doctorData->location_email }}" name="location_email" type="email"
                                                    class="form-control" id="location_email" placeholder="Email">
                                                @if ($errors->has('location_email'))
                                                    <span class="text-danger">{{ $errors->first('location_email') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="state_of_office"class="required">State</label>
                                                <select class="form-control" name="state_of_office">
                                                    @if (isset($doctorData->state_of_office))
                                                        @foreach ($state as $data)
                                                            @if ($data->id == $doctorData->state_of_office)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @else
                                                                <option value="{{ $data->id }}"> {{ $data->name }}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option value="" selected> </option>
                                                        @foreach ($state as $data)
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('state_of_office'))
                                                    <span class="text-danger">{{ $errors->first('state_of_office') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city_of_profession"class="required">City</label>
                                                <select class="form-control" name="city_of_profession">
                                                    @if (isset($doctorData->city_of_profession))
                                                        @foreach ($city as $data)
                                                            @if ($data->id == $doctorData->city_of_profession)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @else
                                                                <option value="{{ $data->id }}"> {{ $data->name }}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option value="" selected> </option>
                                                        @foreach ($city as $data)
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('city_of_profession'))
                                                    <span class="text-danger">{{ $errors->first('city_of_profession') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="zip_code"class="required">Zip code</label>
                                                <input value="{{ $doctorData->zipcode }}" name="zip_code" type="text"
                                                    class="form-control" id="zip_code" placeholder="Zip code">
                                                @if ($errors->has('zip_code'))
                                                    <span class="text-danger">{{ $errors->first('zip_code') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="office_phone">Office phone</label>
                                                <input value="{{ $doctorData->office_phone }}" name="office_phone"
                                                    type="text" class="form-control" id="office_phone"
                                                    placeholder="Office phone">
                                                @if ($errors->has('office_phone'))
                                                    <span class="text-danger">{{ $errors->first('office_phone') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="office_fax">Office fax</label>
                                                <input value="{{ $doctorData->office_fax }}" name="office_fax"
                                                    type="text" class="form-control" id="office_fax"
                                                    placeholder="Office fax">
                                                @if ($errors->has('office_fax'))
                                                    <span class="text-danger">{{ $errors->first('office_fax') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <!-- Healthcare professional information -->
                            <h5 style="font-weight: bold; margin-left: 20px"> Healthcare professional information </h5>
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="npi_number"class="required">NPI Number</label>
                                                <input value="{{ $doctorData->npi_number }}" name="npi_number"
                                                    type="text" class="form-control" id="npi_number"
                                                    placeholder="NPI Number">
                                                @if ($errors->has('npi_number'))
                                                    <span
                                                        class="text-danger">{{ $errors->first('npi_number') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="birth_date"class="required">Date of birth</label>
                                                <div class="input-group date" id="birth_date"
                                                    data-target-input="nearest">
                                                    <input value="{{ $doctorData->date_of_birth }}" name="birth_date"
                                                        type="text" class="form-control datetimepicker-input"
                                                        data-error="#birth_error" data-target="#birth_date">
                                                    <div class="input-group-append" data-target="#birth_date"
                                                        data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                                <span id="birth_error"></span>
                                            </div>
                                            @if ($errors->has('birth_date'))
                                                <span class="text-danger">{{ $errors->first('birth_date') }}</span>
                                            @endif
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="state"class="required">State</label>
                                                <select class="form-control" name="state">
                                                    @if (isset($doctorData->state_id))
                                                        @foreach ($state as $data)
                                                            @if ($data->id == $doctorData->state_id)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @else
                                                                <option value="{{ $data->id }}"> {{ $data->name }}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option value="" selected> </option>
                                                        @foreach ($state as $data)
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('state'))
                                                    <span class="text-danger">{{ $errors->first('state') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city"class="required">City</label>
                                                <select class="form-control" name="city">
                                                    @if (isset($doctorData->city_id))
                                                        @foreach ($city as $data)
                                                            @if ($data->id == $doctorData->city_id)
                                                                <option value="{{ $data->id }}" selected>
                                                                    {{ $data->name }} </option>
                                                            @else
                                                                <option value="{{ $data->id }}"> {{ $data->name }}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    @else
                                                        <option value="" selected> </option>
                                                        @foreach ($city as $data)
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                </select>
                                                @if ($errors->has('city'))
                                                    <span class="text-danger">{{ $errors->first('city') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <!-- Almost done -->
                            <h5 style="font-weight: bold; margin-left: 20px"> Almost done </h5>
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email"class="required">Email address</label>
                                                <input value="{{ $doctorData->email }}" name="email" type="email"
                                                    class="form-control" id="email" placeholder="Email address">
                                                @if ($errors->has('email'))
                                                    <span class="text-danger">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="password"class="required">Password</label>
                                                <input value="" name="password" type="password"
                                                    class="form-control" id="password" placeholder="Password">
                                                @if ($errors->has('password'))
                                                    <span class="text-danger">{{ $errors->first('password') }}</span>
                                                @endif
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn add-btn"
                                    style="background-color: #FF008A; margin: 10px; color: white">Submit</button>
                                <a href="{{ route('redirect-doctor-list') }}" type="submit"
                                    class="btn btn-secondary">Cancel</a>
                            </div>
                    </div>
                    <!-- /.card-body -->


                    </form>
                </div>
                <!-- /.card -->
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')
    <!-- date-range-picker -->
    <script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="{{ asset('assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- AdminLTE App -->
    <!-- AdminLTE for demo purposes -->
    <script>
        $(function() {

            jQuery.validator.addMethod("noSpace", function(value, element) {
                return value.indexOf(" ") < 0 && value != "";
            }, "White space not allowed");
            jQuery.validator.addMethod("email", function(value, element) {
                return /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
            });
            jQuery.validator.addMethod("location_email", function(value, element) {
                return /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i.test(value);
            });
            jQuery.validator.addMethod("passwordRegex", function(value, element) {
                return /^(?=.*[a-z])[A-Za-z0-9\d=!\-@._*]+$/.test(value);
            });
            jQuery.validator.addMethod("lettersonly", function(value, element) {
                return this.optional(element) || /^[a-z]+$/i.test(value);
            });
            jQuery.validator.addMethod("alphanumeric", function(value, element) {
                return this.optional(element) || /^[a-z0-9]+$/i.test(value);
            });
            jQuery.validator.addMethod("zipcodeRegex", function(value, element) {
                return /^(?:(\d{5})(?:[ \-](\d{4}))?)$/i.test(value);
            }, 'Please enter a valid zipcode');
            $("#regForm").validate({
                rules: {
                    first_name: {
                        required: true,
                        lettersonly: true,
                        maxlength: 50
                    },
                    last_name: {
                        required: true,
                        lettersonly: true,
                        maxlength: 50
                    },
                    occupation: {
                        required: true,
                    },
                    speciality: {
                        required: true,
                    },
                    street_address: {
                        required: true,
                    },
                    location_email: {
                        required: true,
                        email: true
                    },
                    office_phone: {
                        maxlength: 10,
                        digits: true,
                    },
                    office_fax: {
                        minlength: 7,
                        maxlength: 12,
                        digits: true,
                    },
                    zip_code: {
                        required: true,
                        minlength: 5,
                        digits: true,
                        zipcodeRegex: true,
                    },
                    city: {
                        required: true,
                    },
                    city_of_profession: {
                        required: true,
                    },
                    npi_number: {
                        required: true,
                        maxlength: 15,
                        digits: true
                    },
                    birth_date: {
                        required: true,
                    },
                    state: {
                        required: true,
                    },
                    state_of_office: {
                        required: true,
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    password: {
                        // noSpace: true,
                        passwordRegex: true
                    },
                },
                messages: {
                    first_name: {
                        required: "First name required",
                        lettersonly: "Please enter valid first name",
                    },
                    last_name: {
                        required: "Last name required",
                        lettersonly: "Please enter valid last name",
                    },
                    occupation: {
                        required: "Please select occupation",
                    },
                    speciality: {
                        required: "Please select speciality",
                    },
                    street_address: {
                        required: "Street address required",
                    },
                    location_email: {
                        required: "Email is required",
                        email: "Please enter a valid email"
                    },
                    zip_code: {
                        required: "Zipcode address required",
                        minlength: "Zipcode must be at least 6 characters long",
                        digits: "Please enter numeric value"
                    },
                    city: {
                        required: "Please select city",
                    },
                    city_of_profession: {
                        required: "Please select city",
                    },
                    npi_number: {
                        required: "NPI Number required",
                        digits: "Please enter numeric value"
                    },
                    birth_date: {
                        required: "Birth date required",
                    },
                    state: {
                        required: "Please select state",
                    },
                    state_of_office: {
                        required: "Please select state",
                    },
                    email: {
                        required: "Email is required",
                        email: "Please enter a valid email"
                    },
                    password: {
                        passwordRegex: "Password should contain one alphabet and one number"
                    },
                },
                errorPlacement: function(error, element) {
                    var placement = $(element).data('error');
                    if (placement) {
                        $(placement).append(error)
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
        });
        $('#birth_date').datetimepicker({
            format: 'L'
        });
        $(document).ready(function() {
            var stateId = $('select[name="state"]').val();
            var selectedVal = $('select[name="city"]').val();
            getCItyFromState(stateId,"city",selectedVal);
            var stateId = $('select[name="state_of_office"]').val();
            var selectedVal = $('select[name="city_of_profession"]').val();
            getCItyFromState(stateId,"city_of_profession",selectedVal);
            $('select[name="state"], select[name="state_of_office"]').on('change', function() {
                stateId = $(this).val();
                if(this.name == "state")
                {
                    cityField = "city";
                }else {
                    cityField = "city_of_profession";
                }
                getCItyFromState(stateId,cityField,0)
            });
        });

        function getCItyFromState(stateId,cityField,selectedVal) {
            if (stateId) {
                $.ajax({
                    url: "{{ url('/get-city-from-state/') }}/" + stateId,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        $('select[name="' + cityField + '"]').empty();
                        $.each(data, function(key, value) {
                            if(value.id == selectedVal)
                            {
                                $('select[name="' + cityField + '"]').append('<option value="' + value.id + '" selected>' + value.name + '</option>');
                            }else {
                                $('select[name="' + cityField + '"]').append('<option value="' + value.id + '">' + value.name + '</option>');
                            }
                        });
                    }
                });
            } else {
                $('select[name="' + cityField + '"]').empty();
            }
        }
    </script>
@endsection
