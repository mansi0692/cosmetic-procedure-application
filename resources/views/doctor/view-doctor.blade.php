@extends('master')

@section('title', 'Doctor')

@section('styles')
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/adminlte.min.css') }}">

    <!-- daterange picker -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/daterangepicker/daterangepicker.css') }}">
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form>
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- Profile Image -->
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                @if ($doctorData->profile_image != '')
                                                    <img src="{{ url('/storage/uploads/' . $doctorData->profile_image) }}"
                                                        style="height: 200px; width:200px" alt="Profile image">
                                                @else
                                                    <img src="{{ asset('assets/images/no-profile.jpg') }}"
                                                        style="height: 200px; width:200px" alt="Profile image">
                                                @endif
                                            </div>
                                        </div>
                                        <!-- First name -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="first_name">First name</label>
                                                <input value="{{ $doctorData->first_name }}" name="first_name" type="text"
                                                    class="form-control" id="first_name" placeholder="First Name" disabled>
                                            </div>
                                        </div>
                                        <!-- Last name -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="last_name">Last name</label>
                                                <input value="{{ $doctorData->last_name }}" name="last_name" type="text"
                                                    class="form-control" id="last_name" placeholder="Last name" disabled>
                                            </div>
                                        </div>
                                        <!-- Occupation -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="occupation">Board Certification</label>
                                                <select class="form-control" name='occupation' disabled>
                                                    @foreach ($occuption as $data)
                                                    <option value="" selected disabled>Board Certification</option>
                                                        @if ($data->id == $doctorData->occupation_id)
                                                            <option value="{{ $data->id }}" selected>
                                                                {{ $data->label }} </option>
                                                        @else
                                                            <option value="{{ $data->id }}"> {{ $data->label }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Specialty -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="speciality">Sub Speciality</label>
                                                <select class="form-control" name="speciality" disabled>
                                                    <option value="" selected disabled>Sub Speciality</option>
                                                    @foreach ($specialty as $data)
                                                        @if ($data->id == $doctorData->speciality_id)
                                                            <option value="{{ $data->id }}" selected>
                                                                {{ $data->label }} </option>
                                                        @else
                                                            <option value="{{ $data->id }}"> {{ $data->label }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <!-- Office addresses & contact information -->
                            <h6 style="font-weight: bold; margin-left: 20px"> Office addresses & contact information </h6>
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="street_address">Street address</label>
                                                <input value="{{ $doctorData->street_address }}" name="street_address"
                                                    type="text" class="form-control" id="street_address"
                                                    placeholder="Street address" disabled>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="location_email">Email</label>
                                                <input value="{{ $doctorData->location_email }}" name="location_email"
                                                    type="text" class="form-control" id="location_email"
                                                    placeholder="Email" disabled>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="state_of_office">State</label>
                                                <select class="form-control" name="state_of_office" disabled>
                                                    <option value="" selected disabled>State</option>
                                                    @foreach ($state as $data)
                                                        @if ($data->id == $doctorData->state_of_office)
                                                            <option value="{{ $data->id }}" selected>
                                                                {{ $data->name }} </option>
                                                        @else
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city_of_profession">City</label>
                                                <select class="form-control" name="city_of_profession" disabled>
                                                    <option value="" selected disabled>City</option>
                                                    @foreach ($city as $data)
                                                        @if ($data->id == $doctorData->city_of_profession)
                                                            <option value="{{ $data->id }}" selected>
                                                                {{ $data->name }} </option>
                                                        @else
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="zip_code">Zip code</label>
                                                <input value="{{ $doctorData->zipcode }}" name="zip_code" type="text"
                                                    class="form-control" id="zip_code" placeholder="Zip code" disabled>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="office_phone">Office phone</label>
                                                <input value="{{ $doctorData->office_phone }}" name="office_phone"
                                                    type="text" class="form-control" id="office_phone"
                                                    placeholder="Office phone" disabled>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="office_fax">Office fax</label>
                                                <input value="{{ $doctorData->office_fax }}" name="office_fax"
                                                    type="text" class="form-control" id="office_fax"
                                                    placeholder="Office fax" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <!-- Healthcare professional information -->
                            <h6 style="font-weight: bold; margin-left: 20px"> Healthcare professional information </h6>
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="npi_number">NPI Number</label>
                                                <input value="{{ $doctorData->npi_number }}" name="npi_number" type="text"
                                                    class="form-control" id="npi_number" placeholder="NPI Number" disabled>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="birth_date">Date of birth</label>
                                                <div class="input-group date" id="birth_date" data-target-input="nearest">
                                                    <input value="{{ $doctorData->date_of_birth }}" name="birth_date"
                                                        type="text" class="form-control datetimepicker-input"
                                                        data-target="#birth_date" disabled>
                                                    <div class="input-group-append" data-target="#birth_date"
                                                        data-toggle="datetimepicker">
                                                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="state">State</label>
                                                <select class="form-control" name="state" disabled>
                                                    @foreach ($state as $data)
                                                        @if ($data->id == $doctorData->state_id)
                                                            <option value="{{ $data->id }}" selected>
                                                                {{ $data->name }} </option>
                                                        @else
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city">City</label>
                                                <select class="form-control" name="city" disabled>
                                                    @foreach ($city as $data)
                                                        @if ($data->id == $doctorData->city_id)
                                                            <option value="{{ $data->id }}" selected>
                                                                {{ $data->name }} </option>
                                                        @else
                                                            <option value="{{ $data->id }}"> {{ $data->name }}
                                                            </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <hr>

                            <!-- Almost done -->
                            <h6 style="font-weight: bold; margin-left: 20px"> Almost done </h6>
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email">Email address</label>
                                                <input value="{{ $doctorData->email }}" name="email" type="email"
                                                    class="form-control" id="email" placeholder="Email address" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <a href="{{ route('redirect-doctor-list') }}" type="submit" class="btn btn-secondary">Back</a>
                    </div>
                    </form>
                </div>
                <!-- /.card -->


            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')
    <!-- jQuery -->
    <script src="{{ asset('assets//jquery/jquery.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- bs-custom-file-input -->
    <script src="{{ asset('assets/plugins/bs-custom-file-input/bs-custom-file-input.min.js') }}"></script>
@endsection
