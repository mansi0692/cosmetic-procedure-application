@extends('master')

@section('title', 'Doctor')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="category-content-wrapper">
        <a href="{{ route('add-doctor') }}" class="btn add-btn float-right"
            style="background-color: #FF008A; margin: 15px; color: white; width: 80px">Add</a>
        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('success'))
                                <div class="alert alert-success">
                                    {{ session()->get('success') }}
                                </div>
                            @endif
                            @if (session()->has('error'))
                                <div class="alert alert-danger">
                                    {{ session()->get('error') }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    <table id="doctors" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th style="padding-right: 25px">Sr No.</th>
                                                <th>Name</th>
                                                <th>Email address</th>
                                                <th style="width: 160px">Speciality</th>
                                                <th>Status</th>
                                                <th>Approved</th>
                                                <th>Verified</th>
                                                <th class="no-sort">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($doctors as $data)
                                                <tr>
                                                    <td></td>
                                                    @php
                                                        $fullName = $data->first_name . ' ' . $data->last_name;
                                                    @endphp
                                                    <td>{{ strlen($fullName) >= 50 ? substr($fullName, 0, 20) . '...' : $fullName }}
                                                    </td>
                                                    <td>{{ $data->email }}</td>
                                                    <td>{{ $data->speciality }}</td>
                                                    @if ($data->status)
                                                        <td>Active</td>
                                                    @else
                                                        <td>Inactive</td>
                                                    @endif
                                                    @if ($data->is_verified == 1)
                                                        <td>Approved</td>
                                                    @else
                                                        <td>Not Approved</td>
                                                    @endif
                                                    @if ($data->is_document_verified == 1)
                                                        <td>Verified</td>
                                                    @else
                                                        <td>Not Verified</td>
                                                    @endif
                                                    <td>
                                                        <a href="{{ URL::to('document-doctor/' . $data->id) }}"
                                                            title="Verify">
                                                            <i class="fa fa-file"></i>
                                                        </a>

                                                        <a href="{{ URL::to('edit-doctor/' . $data->id) }}" title="Edit">
                                                            <i class="fa fa-edit" aria-hidden="true"></i></a>
                                                        <a href="{{ URL::to('view-doctor/' . $data->id) }}" title="View">
                                                            <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        @if (!$data->is_verified)
                                                            <a href="{{ URL::to('approve-doctor/' . $data->id) }}"
                                                                title="Approve">
                                                                <i class="fa fa-plus-square" aria-hidden="true"></i></a>
                                                        @endif
                                                        @if ($data->status)
                                                            <a href="{{ URL::to('change-status-doctor/' . $data->id . '/active') }}"
                                                                title="Inactive">
                                                                <i class="fa fa-ban" aria-hidden="true"></i>
                                                            </a>
                                                        @else
                                                            <a href="{{ URL::to('change-status-doctor/' . $data->id . '/inactive') }}"
                                                                title="Active">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </a>
                                                        @endif
                                                        @if ($data->is_featured)
                                                            <a href="{{ URL::to('like-doctor/' . $data->id) }}"
                                                                title="Feature">
                                                                <i class="fa fa-heart" aria-hidden="true"></i></a>
                                                        @else
                                                            <a href="{{ URL::to('like-doctor/' . $data->id) }}"
                                                                title="Feature">
                                                                <i class="far fa-heart" aria-hidden></i>
                                                            </a>
                                                        @endif


                                                        <a onclick="return confirm('<?php echo trans('messages.label.ARE_YOU_SURE_TO_DELETE_THIS_RECORD'); ?>')"
                                                            href="{{ URL::to('delete-doctor/' . $data->id) }}">
                                                            <i class="fa fa-trash" aria-hidden="true" title="Delete"></i>
                                                        </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#doctors").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [7]
                }]
            });
        });

        var addSerialNumber = function() {
            var i = 1
            $('table tbody tr').each(function(index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };

        addSerialNumber();
    </script>
@endsection
