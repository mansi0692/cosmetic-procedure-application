@extends('master')

@section('title', 'Patient Review')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">

@endsection

@section('content')
    <div class="category-content-wrapper">
        <a href="{{ route('redirect-patient-rating-list') }}" class="btn btn-secondary float-right"
            style="margin: 15px">Back</a>

        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    {{ $errors->first() }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    <table id="reviewList" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                @foreach ($patientRating['headers'] as $item)
                                                    <th>{{ ucfirst($item) }}</th>
                                                @endforeach
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($patientRating['data'] as $data)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $data['email'] }}</td>
                                                    <td>{{ $data['first_name'] }}</td>
                                                    <td>{{ $data['last_name'] }}</td>
                                                    <td>{{ $data['rating'] }}</td>
                                                    <td>{{ $data['bedside_manner'] }}</td>
                                                    <td>{{ $data['answered_questions'] }}</td>
                                                    <td>{{ $data['after_care'] }}</td>
                                                    <td>{{ $data['time_spent'] }}</td>
                                                    <td>{{ $data['responsiveness'] }}</td>
                                                    <td>{{ $data['courtesy'] }}</td>
                                                    <td>{{ $data['payment_process'] }}</td>
                                                    <td>{{ $data['wait_times'] }}</td>
                                                    <td>{{ $data['review'] }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#reviewList").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": []
                }]
            });
        });

        var addSerialNumber = function() {
            var i = 1
            $('table tbody tr').each(function(index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };

        addSerialNumber();
    </script>
@endsection
