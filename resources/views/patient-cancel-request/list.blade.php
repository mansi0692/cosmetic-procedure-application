@extends('master')

@section('title', 'Patient Cancel Request')

@section('styles')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
@endsection

@section('content')
    <div class="category-content-wrapper">

        <div class="content-wrapper">

            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <section class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
                            @if (session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <div class="card">

                                <div class="card-body">
                                    <div class="navigation-tab" style="margin-bottom: 10px;">
                                        <ul class="nav nav-tabs">
                                            <li class="nav-item">
                                                <a class="{{ Request::segment(1) == 'patient-cancel-request-list-pending' ? 'nav-link active' : 'nav-link' }}"
                                                    id="certification" aria-current="page"
                                                    href="{{ route('patient-cancel-request-list-pending') }}">Pending</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="{{ Request::segment(1) == 'patient-cancel-request-list-approved' ? 'nav-link active' : 'nav-link' }}"
                                                    href="{{ route('patient-cancel-request-list-approved') }}"
                                                    id="other">Approved</a>
                                            </li>
                                        </ul>
                                    </div>
                                    <table id="pendingList" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Sr No.</th>
                                                <th>Name</th>
                                                <th>Email address</th>
                                                <th class="no-sort">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($patientCancelRequest as $data)
                                                <tr>
                                                    <td></td>
                                                    <td>{{ $data->full_name }}</td>
                                                    <td>{{ $data->email }}</td>
                                                    <td>
                                                        <a href="{{ route('view-patient-cancel-request', ['patientId' => $data->id]) }}"
                                                            title="View">
                                                            <i class="fa fa-eye" aria-hidden="true"></i></a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.container-fluid -->
            </section>
        </div>
    </div>

@stop

@section('scripts')
    <!-- DataTables  & Plugins -->
    <script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <!-- Page specific script -->
    <script>
        $(function() {
            $("#pendingList").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": [],
                "aoColumnDefs": [{
                    "bSortable": false,
                    "aTargets": [3]
                }]
            });
        });

        var addSerialNumber = function() {
            var i = 1
            $('table tbody tr').each(function(index) {
                $(this).find('td:nth-child(1)').html(index + 1);
            });
        };

        addSerialNumber();
    </script>
@endsection
