@extends('master')

@section('title', 'Patient Cancel Request')

@section('styles')

@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            <div class="row" style="margin-left: 19%">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="card card-primary">

                        <!-- /.card-header -->
                        <!-- form start -->
                        <form action="{{ route('patient-cancle-request-approve', ['patientId' => Request::segment(2)]) }}"
                            method="post">
                            @csrf
                            <!-- Personal information -->
                            <div class="card-body">
                                <div class="form-group">
                                    <div class="row">
                                        <!-- First name -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="full_name">Name</label>
                                                <input value="{{ $patientData->full_name }}" name="full_name" type="text"
                                                    class="form-control" id="full_name" placeholder="Name" disabled>
                                            </div>
                                        </div>
                                        <!-- Email -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="email">Email address</label>
                                                <input value="{{ $patientData->email }}" name="email" type="email"
                                                    class="form-control" id="email" placeholder="Email address"
                                                    disabled>
                                            </div>
                                        </div>
                                        <!-- Request status -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="full_name">Request status</label>
                                                <input
                                                    value="{{ $patientData->cancel_request == 0 ? 'Pending' : 'Accepted' }}"
                                                    name="full_name" type="text" class="form-control" id="full_name"
                                                    placeholder="Request status" disabled>
                                            </div>
                                        </div>

                                        <!-- Reason -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="full_name">Reason</label>
                                                <textarea value="{{ $patientData->cancel_reason }}" rows="1" name="full_name" type="text" class="form-control" id="full_name"
                                                    placeholder="Reason" disabled>{{ $patientData->cancel_reason }}</textarea>
                                            </div>
                                        </div>
                                        <!-- Address -->
                                        {{-- <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="address">Address</label>
                                                <input value="{{ $patientData->address }}" name="address" type="text"
                                                    class="form-control" id="address" placeholder="Address" disabled>
                                            </div>
                                        </div>
                                        <!-- State -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="state">State</label>
                                                <select class="form-control" name="state" disabled>
                                                    @foreach ($state as $data)
                                                        @if ($data->id == $patientData->state_id)
                                                            <option value="{{ $data->id }}" selected>
                                                                {{ $data->name }} </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!-- Country -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="country">Country</label>
                                                <select class="form-control" name="country" disabled>
                                                    @foreach ($country as $data)
                                                        @if ($data->id == $patientData->country_id)
                                                            <option value="{{ $data->id }}" selected>
                                                                {{ $data->name }} </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <!-- City -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="city">City</label>
                                                <select class="form-control" name="city" disabled>
                                                    @foreach ($city as $data)
                                                        @if ($data->id == $patientData->city_id)
                                                            <option value="{{ $data->id }}" selected>
                                                                {{ $data->name }} </option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>

                                        <!-- Zipcode -->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="zipcode">Zipcode</label>
                                                <input value="{{ $patientData->zipcode }}" name="zipcode" type="text"
                                                    class="form-control" id="zipcode" placeholder="Zipcode" disabled>
                                            </div>
                                        </div> --}}

                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                @if ($patientData->cancel_request == 0)
                                    <button type="submit" class="btn add-btn"
                                        style="background-color: #FF008A; margin: 10px; color: white">Approve</button>
                                @endif

                                <a href="{{ route('return-patient-cancel-request-list') }}" type="submit"
                                    class="btn btn-secondary">Back</a>
                            </div>
                        </form>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
            <!-- /.row -->
        </div><!-- /.container-fluid -->
    </section>
@stop

@section('scripts')

@endsection
