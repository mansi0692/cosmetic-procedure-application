<?php

use App\Events\TestEvent;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\CMSController;
use App\Http\Controllers\Admin\ContactUsController;
use App\Http\Controllers\Admin\CustomMailController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DoctorController;
use App\Http\Controllers\Admin\DoctorInviteController;
use App\Http\Controllers\Admin\DoctorPatientAdminController;
use App\Http\Controllers\Admin\EmailManagementController;
use App\Http\Controllers\Admin\SmsManagementController;
use App\Http\Controllers\Admin\MasterCategoryController;
use App\Http\Controllers\Admin\MasterSubCategoryController;
use App\Http\Controllers\Admin\PatientController;
use App\Http\Controllers\Admin\FAQCategoryController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\TopSpecialityController;
use App\Http\Controllers\Admin\UserController as AdminUserController;
use App\Http\Controllers\ScriptController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Middleware\DoctorAdminMiddleware;
use App\Http\Middleware\PatientAdminMiddleware;
use App\Http\Middleware\SuperAdminMiddleware;
use Database\Seeders\SuperAdminSeeder;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'login'])->name('login');
Route::get('logout', [AuthController::class, 'logout'])->name('logout');
Route::post('loginuser', [AuthController::class, 'userLogin'])->name('loginuser');
Route::get('index', [AuthController::class, 'index']);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

Route::post('changepassword', [AuthController::class, 'changepassword'])->name('changepassword');
Route::get('change-password', [AuthController::class, 'redirectToChangePassword'])->name('redirectToChangePassword');

// Category
Route::get('edit-category/{categoryId}', [CategoryController::class, 'editCategory'])->name('edit-category');
Route::get('change-status/{categoryId}/{status}', [CategoryController::class, 'changeStatus'])->name('change-status');
Route::get('delete-category/{categoryId}', [CategoryController::class, 'deleteCategory'])->name('delete-category');
Route::get('list-category', [CategoryController::class, 'index'])->name('list-category');
Route::get('add-category', [CategoryController::class, 'addCategory'])->name('add-category');
Route::post('save-category', [CategoryController::class, 'saveCategory'])->name('save-category');
Route::post('update-category/{categoryId}', [CategoryController::class, 'updateCategory'])->name('update-category');
// Route::get('redirect-list', [CategoryController::class, 'redirectList'])->name('redirect-list');

Route::get('get-city-from-state/{stateId}', [MasterCategoryController::class, 'getCityFromState'])->name('get-city-from-state');


// Master Category
Route::get('list-master-category', [MasterCategoryController::class, 'index'])->name('list-master-category');
Route::get('redirect-list/{masterCategoryId}/{isOther}', [MasterCategoryController::class, 'redirectList'])->name('redirect-list');

// Master SubCategory
Route::get('list-master-sub-category/{masterCategoryId}/{isOther}', [MasterSubCategoryController::class, 'listMasterSubCategory'])->name('list-master-sub-category');
Route::get('add-master-sub-category/{masterCategoryId}', [MasterSubCategoryController::class, 'addMasterSubCategory'])->name('add-master-sub-category');
Route::post('save-master-sub-category/{masterCategoryId}/{isOther}', [MasterSubCategoryController::class, 'saveMasterSubCategory'])->name('save-master-sub-category');
Route::get('edit-master-sub-category/{masterSubCategoryId}/{masterCategoryId}/{isOther}', [MasterSubCategoryController::class, 'editMasterSubCategory'])->name('edit-master-sub-category');
Route::post('update-master-sub-category/{masterSubCategoryId}/{masterCategoryId}/{isOther}', [MasterSubCategoryController::class, 'updateMasterSubCategory'])->name('update-master-sub-category');
Route::get('delete-master-sub-category/{masterSubCategoryId}', [MasterSubCategoryController::class, 'deleteMasterSubCategory'])->name('delete-master-sub-category');
Route::get('change-master-sub-category-status/{masterSubCategoryId}/{status}', [MasterSubCategoryController::class, 'changeMasterSubCategoryStatus'])->name('change-master-sub-category-status');
Route::get('approve-other-sub-category/{masterSubCategoryId}', [MasterSubCategoryController::class, 'approveMasterSubCategory'])->name('approve-other-sub-category');

// Doctor
Route::middleware([DoctorAdminMiddleware::class])->group(function () {
    Route::get('document-doctor/{doctorId}', [DoctorController::class, 'doctorDocuments'])->name('document-doctor');
    Route::get('list-doctor', [DoctorController::class, 'listDoctor'])->name('list-doctor');
    Route::get('change-status-doctor/{doctorId}/{status}', [DoctorController::class, 'changeStatus'])->name('change-status-doctor');
    Route::get('delete-doctor/{doctorId}', [DoctorController::class, 'deleteDoctor'])->name('delete-doctor');
    Route::get('add-doctor', [DoctorController::class, 'redirectAddDoctor'])->name('add-doctor');
    Route::post('save-doctor', [DoctorController::class, 'registerDoctor'])->name('save-doctor');
    Route::get('edit-doctor/{doctorId}', [DoctorController::class, 'editDoctor'])->name('edit-doctor');
    Route::get('redirect-doctor-list', [DoctorController::class, 'redirectDoctorList'])->name('redirect-doctor-list');
    Route::post('update-doctor/{doctorId}', [DoctorController::class, 'updateDoctor'])->name('update-doctor');
    Route::get('view-doctor/{doctorId}', [DoctorController::class, 'viewDoctor'])->name('view-doctor');
    Route::get('approve-doctor/{doctorId}', [DoctorController::class, 'approveDoctor'])->name('approve-doctor');
    Route::get('like-doctor/{doctorId}', [DoctorController::class, 'likeDoctor'])->name('like-doctor');
    Route::get('verify-doctor/{doctorId}', [DoctorController::class, 'verifyDoctor'])->name('verify-doctor');
    Route::get('list-doctor-review', [DoctorController::class, 'listDoctorReview'])->name('list-doctor-review');
    Route::get('doctor-rating/{doctorId}', [DoctorController::class, 'getDoctorRating'])->name('doctor-rating');
    Route::get('redirect-doctor-rating-list', [DoctorController::class, 'redirectDoctorRatingList'])->name('redirect-doctor-rating-list');
});

// Patient
Route::middleware([PatientAdminMiddleware::class])->group(function () {
    Route::get('list-patient', [PatientController::class, 'listPatient'])->name('list-patient');
    Route::get('change-status-patient/{patientId}/{status}', [PatientController::class, 'changeStatus'])->name('change-status-patient');
    Route::get('edit-patient/{patientId}', [PatientController::class, 'editPatient'])->name('edit-patient');
    Route::get('redirect-patient-list', [PatientController::class, 'redirectPatientList'])->name('redirect-patient-list');
    Route::post('update-patient/{patientId}', [PatientController::class, 'updatePatient'])->name('update-patient');
    Route::get('view-patient/{patientId}', [PatientController::class, 'viewPatient'])->name('view-patient');
    Route::get('patient-cancel-request-list-pending', [PatientController::class, 'listPendingPatinetCancleRequest'])->name('patient-cancel-request-list-pending');
    Route::get('patient-cancel-request-list-approved', [PatientController::class, 'listApprovedPatinetCancleRequest'])->name('patient-cancel-request-list-approved');
    Route::get('view-patient-cancel-request/{patientId}', [PatientController::class, 'viewPatientCancelRequest'])->name('view-patient-cancel-request');
    Route::post('patient-cancle-request-approve/{patientId}', [PatientController::class, 'patientCancleRequestAppprove'])->name('patient-cancle-request-approve');
    Route::get('return-patient-cancel-request-list', [PatientController::class, 'returnPatientCancelRequestList'])->name('return-patient-cancel-request-list');
    Route::get('list-patient-review', [PatientController::class, 'listPatientReview'])->name('list-patient-review');
    Route::get('patient-rating/{patientId}', [PatientController::class, 'getPatientRating'])->name('patient-rating');
    Route::get('redirect-patient-rating-list', [PatientController::class, 'redirectPatientRatingList'])->name('redirect-patient-rating-list');
});

// DoctorPatientAdmin
Route::middleware([SuperAdminMiddleware::class])->group(function () {
    Route::get('list-doctor-admin', [DoctorPatientAdminController::class, 'listDoctorAdmin'])->name('list-doctor-admin');
    Route::get('list-patient-admin', [DoctorPatientAdminController::class, 'listPatientAdmin'])->name('list-patient-admin');
    Route::get('add-doctor-patient-admin', [DoctorPatientAdminController::class, 'addDoctorPatientAdmin'])->name('add-doctor-patient-admin');
    Route::post('save-doctor-patient-admin', [DoctorPatientAdminController::class, 'saveDoctorPatientAdmin'])->name('save-doctor-patient-admin');
    Route::get('edit-doctor-patient-admin/{adminId}', [DoctorPatientAdminController::class, 'editDoctorPatientAdmin'])->name('edit-doctor-patient-admin');
    Route::post('update-doctor-patient-admin/{adminId}', [DoctorPatientAdminController::class, 'updateDoctorPatientAdmin'])->name('update-doctor-patient-admin');
    Route::get('redirect-admin-list', [DoctorPatientAdminController::class, 'returnAdminList'])->name('redirect-admin-list');
    Route::get('change-status-doctor-patient-admin/{adminId}/{status}', [DoctorPatientAdminController::class, 'changeStatus'])->name('change-status-doctor-patient-admin');
    Route::get('delete-doctor-patient-admin/{adminId}', [DoctorPatientAdminController::class, 'deleteAdmin'])->name('delete-doctor-patient-admin');
});

// Custom Mail Notification
Route::get('custom-mail-notification-list', [CustomMailController::class, 'listCustomMail'])->name('custom-mail-notification-list');
Route::get('custom-mail-notification-add', [CustomMailController::class, 'getDoctorPatient'])->name('custom-mail-notification-add');
Route::post('send-custom-mail', [CustomMailController::class, 'sendCustomMail'])->name('send-custom-mail');
Route::get('redirect-custom-mail-list', [CustomMailController::class, 'returnList'])->name('redirect-custom-mail-list');
Route::get('view-custom-mail-notification/{customMailId}', [CustomMailController::class, 'viewCustomMail'])->name('view-custom-mail-notification');

//Doctor Invite
Route::middleware([DoctorAdminMiddleware::class])->group(function () {
    Route::get('doctor-invite-list/{tabNumber}', [DoctorInviteController::class, 'list'])->name('doctor-invite-list');
    Route::get('doctor-invite-send-invite', [DoctorInviteController::class, 'doctorInvite'])->name('doctor-invite-send-invite');
    Route::post('send-doctor-invitation', [DoctorInviteController::class, 'sendInvitation'])->name('send-doctor-invitation');
});

//Contact Us
Route::get('contact-us-list', [ContactUsController::class, 'list'])->name('contact-us-list');
Route::get('contact-us-reply/{contactUsId}', [ContactUsController::class, 'getContactUs'])->name('contact-us-reply');
Route::post('send-contact-us-reply/{contactUsId}', [ContactUsController::class, 'sendReply'])->name('send-contact-us-reply');
Route::get('redirect-contact-us-list', [ContactUsController::class, 'redirectBack'])->name('redirect-contact-us-list');

// CMS Pages
Route::middleware([SuperAdminMiddleware::class])->group(function () {
    Route::get('cms-list/{languageId}', [CMSController::class, 'list'])->name('cms-list');
    Route::get('add-cms', [CMSController::class, 'addCMS'])->name('add-cms');
    Route::post('save-cms', [CMSController::class, 'saveCMS'])->name('save-cms');
    Route::get('redirect-cms-list', [CMSController::class, 'redirectBack'])->name('redirect-cms-list');
    Route::get('edit-cms/{cmsPageId}', [CMSController::class, 'editCMS'])->name('edit-cms');
    Route::post('update-cms/{cmsPageId}', [CMSController::class, 'updateCMS'])->name('update-cms');
    Route::get('change-status-cms/{cmsPageId}/{status}', [CMSController::class, 'changeStatus'])->name('change-status-cms');
    Route::get('delete-cms/{cmsPageId}', [CMSController::class, 'deleteCMS'])->name('delete-cms');
});

// Email Template Management
Route::get('list-email-template/{languageId}', [EmailManagementController::class, 'list'])->name('list-email-template');
Route::get('change-status-email-template/{emailTemplateId}/{status}', [EmailManagementController::class, 'changeStatus'])->name('change-status-email-template');
Route::get('edit-email-template/{emailTemplateId}/{languageId}', [EmailManagementController::class, 'editEmailTemplate'])->name('edit-email-template');
Route::post('update-email-template/{emailTemplateId}/{languageId}', [EmailManagementController::class, 'update'])->name('update-email-template');

// Sms Template Management
Route::get('list-sms-template/{languageId}', [SmsManagementController::class, 'list'])->name('list-sms-template');
Route::get('change-status-sms-template/{smsTemplateId}/{status}', [SmsManagementController::class, 'changeStatus'])->name('change-status-sms-template');
Route::get('edit-sms-template/{smsTemplateId}/{languageId}', [SmsManagementController::class, 'editSmsTemplate'])->name('edit-sms-template');
Route::post('update-sms-template/{smsTemplateId}/{languageId}', [SmsManagementController::class, 'update'])->name('update-sms-template');

// FAQ
Route::get('list-faq-category/{languageId}', [FAQCategoryController::class, 'list'])->name('list-faq-category');
Route::get('edit-faq-category/{categoryId}/{languageId}', [FAQCategoryController::class, 'edit'])->name('edit-faq-category');
Route::post('save-faq-category/{faqCategoryId}/{languageId}', [FAQCategoryController::class, 'update'])->name('save-faq-category');
Route::get('list-faqs/{faqCategoryId}/{languageId}', [FaqController::class, 'list'])->name('list-faqs');
Route::get('change-faqs-status/{status}/{faqsId}', [FaqController::class, 'changeStatus'])->name('change-faqs-status');
Route::get('add-faqs/{faqsId}/{languageId}', [FaqController::class, 'create'])->name('add-faqs');
Route::post('save-faqs/{faqsId}/{languageId}', [FaqController::class, 'store'])->name('save-faqs');
Route::get('edit-faqs/{faqsId}/{faqCategoryId}/{languageId}', [FaqController::class, 'edit'])->name('edit-faqs');
Route::post('update-faqs/{faqsId}/{faqCategoryId}/{languageId}', [FaqController::class, 'update'])->name('update-faqs');
Route::get('delete-faqs/{faqsId}', [FaqController::class, 'destroy'])->name('delete-faqs');

Route::get('copy_category/{id}/{languageId}', [ScriptController::class, 'copyData']);

Route::get('test', function () {
    event(new TestEvent('hello world'));
});


// Route::get('send_sms', [ScriptController::class, 'sendSms']);

//Top Specialities
Route::get('list-top-speciality', [TopSpecialityController::class, 'listTopSpeciality'])->name('list-top-speciality');
Route::get('add-top-speciality', [TopSpecialityController::class, 'addTopSpeciality'])->name('add-top-speciality');
Route::post('save-top-speciality', [TopSpecialityController::class, 'saveTopSpeciality'])->name('save-top-speciality');
Route::get('edit-top-speciality/{id}', [TopSpecialityController::class, 'editTopSpeciality'])->name('edit-top-speciality');
Route::post('update-top-speciality/{id}', [TopSpecialityController::class, 'updateTopSpeciality'])->name('update-top-speciality');
