<?php

use App\Events\TestEvent;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\api\CMSController;
use App\Http\Controllers\api\FAQController;
use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\DoctorController;
use App\Http\Controllers\api\MessageController;
use App\Http\Controllers\api\PatientController;
use App\Http\Controllers\api\CalendarController;
use App\Http\Controllers\api\DoctorInviteController;
use App\Http\Controllers\api\MasterDataController;
use App\Http\Controllers\api\NotificationController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API.!
|
*/

Broadcast::routes(['middleware' => ['auth:sanctum']]);


Route::group(
    ['middleware' => ['localization']],
    function () {
        Route::post('login', [AuthController::class, 'login']);
        Route::post('patient-register', [AuthController::class, 'patientRegistration']);

        Route::post('sociallogin/{provider}', [AuthController::class, 'socialLogin']);
        Route::post('auth/twitter', [AuthController::class, 'twitter']);

        //  Social login api for mobile app only
        Route::post('social-login', [AuthController::class, 'socialLoginFromMobile']);

        Route::post('sent-otp', [AuthController::class, 'sendOtp']);
        Route::post('verify-otp', [AuthController::class, 'verifyOtp']);

        Route::post('reset-password', [AuthController::class, 'resetPassword']);

        Route::get('countries', [MasterDataController::class, 'getAllCountries']);
        Route::post('cities', [MasterDataController::class, 'getCitiesFromState']);
        Route::post('state', [MasterDataController::class, 'getAllStates']);

        Route::post('find-doctor-profile', [DoctorController::class, 'searchDoctorProfile']);
        Route::post('find-doctor', [DoctorController::class, 'findDoctorWithNpiNumber']);
        Route::post('register-doctor', [DoctorController::class, 'registerDoctor']);
        Route::post('search-doctor', [DoctorController::class, 'searchDoctors']);
        Route::get('featured-doctor-list', [DoctorController::class, 'featuredDoctorList']);
        Route::post('doctors-feeds-list', [DoctorController::class, 'doctorsAndFeeds']);
        Route::get('top-specialities-doctor-list', [DoctorController::class, 'topSpecialitiesDoctorList']);
        Route::post('top-doctor-list', [DoctorController::class, 'topDoctorList']);

        Route::post('get-doctor-information', [DoctorController::class, 'getProfileInformation']);
        Route::post('get-patient-information', [PatientController::class, 'getProfileInformation']);
        Route::post('get-cms-content', [CMSController::class, 'getCmsContent']);
        Route::post('doctor-compare', [DoctorController::class, 'compareDoctor']);

        //all feed without login
        Route::post('all-feeds', [PatientController::class, 'getFeedList']);
        Route::get('feed-details/{id}', [DoctorController::class, 'getFeedListDetails']);
        Route::post('get-appoinment-slot', [DoctorController::class, 'getAppoinmentSlot']);
        Route::post('get-review-list', [DoctorController::class, 'getReviewList']);

        //FAQ
        Route::get('faq-categories', [FAQController::class, 'getFAQCategoryList']);
        Route::post('faqs', [FAQController::class, 'getFAQList']);

        Route::group(['middleware' => ['auth:sanctum']], function () {
            Route::post('logout', [AuthController::class, 'logout']);
            Route::get('get-profile-information', [DoctorController::class, 'getProfileInformation']);
            Route::post('save-profile-information', [DoctorController::class, 'saveProfileInformation']);
            Route::post('save-overview-information', [DoctorController::class, 'saveOverViewInformation']);
            Route::post('delete-profile-information', [DoctorController::class, 'deleteProfileInformation']);
            Route::post('save-settings', [DoctorController::class, 'saveSettings']);
            Route::post('change-password', [AuthController::class, 'changePassword']);
            Route::post('add-doctor-location', [DoctorController::class, 'addDoctorLocation']);
            Route::post('update-doctor-location', [DoctorController::class, 'updateDoctorLocation']);
            Route::post('get-doctor-location', [DoctorController::class, 'getDoctorLocation']);
            Route::post('upload-photos-videos', [DoctorController::class, 'uploadPhotosVideos']);
            Route::post('update-media-order', [DoctorController::class, 'updateMediaOrder']);
            Route::post('upload-proof-doc', [DoctorController::class, 'uploadProofDoc']);
            Route::post('update-media-description', [DoctorController::class, 'updateMediaDescription']);
            Route::post('upload-registration-doc', [DoctorController::class, 'uploadRegistrationDoc']);
            Route::post('update-patient-profile', [PatientController::class, 'updatePatientProfile']);
            Route::post('get-doctor-appointment-list', [DoctorController::class, 'getDoctorAppointmentList']);
            Route::post('get-doctor-appointment-detail', [DoctorController::class, 'getDoctorAppointmentDetail']);
            Route::post('get-doctor-appointment-booking-form', [DoctorController::class, 'getDoctorAppointmentBookingForm']);
            Route::post('doctor-reschedule-bookings', [PatientController::class, 'patientRescheduleBookings']);
            Route::post('doctor-appointment-action', [DoctorController::class, 'doctorAppointmentAction']);
            Route::post('join-appointment-action', [DoctorController::class, 'joinAppointmentAction']);

            // Feed
            Route::post('create-feed', [DoctorController::class, 'createFeed']);
            Route::post('update-feed', [DoctorController::class, 'updateFeed']);
            Route::delete('delete-feed/{id}', [DoctorController::class, 'deleteFeed']);
            Route::post('get-feed-list-data', [DoctorController::class, 'getFeedListData']);
            Route::get('get-feed-details/{id}', [DoctorController::class, 'getFeedListDetails']);
            Route::post('add-feed-like', [DoctorController::class, 'addFeedLike']);
            Route::post('add-feed-comment', [DoctorController::class, 'addFeedComment']);
            Route::post('follow-doctor', [DoctorController::class, 'followDoctor']);
            // end Feed

            Route::get('doctor-dashboard', [DoctorController::class, 'doctorDashboard']);

            // Favorite Management
            Route::post('add-favorite-group', [PatientController::class, 'addFavoriteGroup']);
            Route::post('update-favorite-group', [PatientController::class, 'updateFavoriteGroup']);
            Route::post('delete-favorite-group', [PatientController::class, 'deleteFavoriteGroup']);
            Route::post('favorite-group-list', [PatientController::class, 'favoriteGroupList']);
            Route::post('doctor-add-to-favorite', [PatientController::class, 'doctorAddToFavorite']);
            Route::post('doctor-move-to-new-group', [PatientController::class, 'doctorMoveToNewGroup']);
            Route::post('doctor-remove-from-favorite', [PatientController::class, 'doctorRemoveFromFavorite']);
            Route::post('favorite-doctor-list', [PatientController::class, 'favoriteDoctorList']);

            Route::post('patient-cancel-account', [PatientController::class, 'cancelPatientAccount']);
            Route::post('patient-contact-us', [PatientController::class, 'contactUsPatient']);
            Route::post('join-waiting-list', [PatientController::class, 'joinWaitList']);
            Route::get('get-appoinment-patient-list', [PatientController::class, 'getAppoinmentPatientList']);
            Route::post('add-other-patinet-details', [PatientController::class, 'addOtherPatientDetails']);
            Route::post('update-other-patinet-details', [PatientController::class, 'updateOtherPatientDetails']);
            Route::post('book-apoinment', [PatientController::class, 'bookAppoinment']);
            Route::post('patient-bookings/{status}', [PatientController::class, 'patientBookings']);
            Route::post('add-doctor-review', [PatientController::class, 'addDoctorReview']);
            Route::post('get-doctor-review', [PatientController::class, 'getDoctorReview']);
            Route::post('patient-reschedule-bookings', [PatientController::class, 'patientRescheduleBookings']);
            Route::post('reschedule-booking-status', [PatientController::class, 'rescheduleBookingStatus']);
            Route::post('cancel-booking', [PatientController::class, 'cancelBooking']);
            Route::post('book-appintment-frontend', [PatientController::class, 'bookAppoinmentNew']);
            Route::post('get-all-feeds', [PatientController::class, 'getFeedList']);
            Route::post('get-doctors-feeds-list', [DoctorController::class, 'doctorsAndFeeds']);

            // Calendar Management
            Route::post('block-doctor-slot', [CalendarController::class, 'blockDoctorSlot']);
            Route::post('manage-working-time', [CalendarController::class, 'manageWorkingTime']);
            Route::post('get-working-time', [CalendarController::class, 'getWorkingTime']);
            Route::post('delete-working-time', [CalendarController::class, 'deleteWorkingTime']);
            Route::post('get-doctor-appointment', [CalendarController::class, 'getDoctorAppointment']);
            Route::post('no-appointment-schedule', [PatientController::class, 'noAppointmentSchedule']);

            // Chat
            Route::post('sent-message', [MessageController::class, 'store']);
            Route::post('fetch-message', [MessageController::class, 'getMessages']);
            Route::post('delete-chat', [MessageController::class, 'deleteChat']);
            Route::post('patient-chat-request', [MessageController::class, 'sendChatRequest']);
            Route::get('patient-chat-request-list', [MessageController::class, 'chatRequestList']);
            Route::post('read-message', [MessageController::class, 'readAllMessages']);

            //Push notification
            Route::get('get-all-notifications', [NotificationController::class, 'getAllNotifications']);
            Route::post('notification-status-update', [NotificationController::class, 'updateNotificationStatus']);

            //Invite-doctor
            Route::post('invite-doctor', [DoctorInviteController::class, 'inviteDoctor']);
            Route::post('request-admin-to-invite-doctor', [DoctorInviteController::class, 'requestAdminToInviteDoctor']);
        });
        Route::post('master-categories', [MasterDataController::class, 'getCategoriesFromType']);
        Route::post('get-symptoms', [MasterDataController::class, 'getSymptoms']);
        Route::post('get-filters-with-count', [MasterDataController::class, 'getFiltersWithCount']);
        Route::post('get-city-state-id', [MasterDataController::class, 'getCityStateId']);
        Route::post('get-location', [MasterDataController::class, 'getLocation']);
        Route::post('get-top-speciality', [MasterDataController::class, 'getTopSpeciality']);
    }
);

Route::post('test', function (Request $request) {
    $message = $request->all();
    event(new TestEvent(json_encode($message)));
});
