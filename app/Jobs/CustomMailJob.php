<?php

namespace App\Jobs;

use App\Mail\CustomMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class CustomMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $email;
    protected $content;
    protected $subject;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $content, $subject)
    {
        $this->email = $email;
        $this->content = $content;
        $this->subject = $subject;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $email = new CustomMail($this->content, $this->subject);
        Mail::to($this->email)->send($email);
    }
}
