<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Thomasjohnkane\Snooze\Traits\SnoozeNotifiable;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ConfirmedAppointmentCron::class,
        Commands\ExpiredAppointmentCron::class,
        Commands\AppointmentReminderCron::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('confirmedAppointment:cron')->dailyAt('23:50');

        $schedule->command('expired:cron')->dailyAt('00:10');

        $schedule->command('appointment-reminder:cron')->everyFiveMinutes();

        $schedule->command('appointment_reminder_before_fifteen_minute:cron')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
