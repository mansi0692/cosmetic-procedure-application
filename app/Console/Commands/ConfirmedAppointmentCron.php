<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Appointment;
use App\Models\AppointmentPatientDetail;
use App\Models\City;
use App\Models\DoctorInformation;
use App\Models\DoctorLocation;
use App\Models\PatientInformation;
use App\Services\EmailNotificationService;
use App\Services\SmsNotificationService;
use Carbon\Carbon;

class ConfirmedAppointmentCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'confirmedAppointment:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command run for updated today appointment in completed status';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("confirmed appointment cron started");

        $appointmentData = Appointment::where('appointments.status', config('constants.appointment_status.1'))
            ->whereDate('appointments.appointment_date', '<=', Carbon::today())->get();

        foreach ($appointmentData as $appointment) {
            
            Appointment::where('id',$appointment->id)->update(['appointments.status' => config('constants.appointment_status.2')]);
            
            $appointmentPatientDetails = AppointmentPatientDetail::where('id', $appointment->appointment_patient_id)->first();
            $this->sendReviewMail($appointment, $appointmentPatientDetails);
            
        }
        
        ($appointmentData) ? $this->info('Updated successfully') : $this->info('Data not Found');

        \Log::info("confirmed appointment cron completed!");
    }

    public function sendReviewMail($appointment, $appointmentPatientDetails)
    {
        $providerInfo = DoctorInformation::where('user_id', $appointment->doctor_id)
            ->leftjoin('users', 'doctor_information.user_id', 'users.id')
            ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
            ->select('doctor_information.specialty_id', 'doctor_information.*', 'users.email', 'master_sub_categories.label')
            ->first();

        $patientInfo = PatientInformation::where('user_id', $appointmentPatientDetails->patient_id)
            ->leftjoin('users', 'patient_information.user_id', 'users.id')
            ->first();

        $year = date("Y");
        $providerLocation = DoctorLocation::where('doctor_id', $appointment->doctor_id)->first();
        $location = City::find($providerLocation->city_id);
        $providerFirstName = $providerInfo->first_name ?? '';
        $providerLastName = $providerInfo->last_name ?? '';
        $patientName = $patientInfo->full_name;
        $patientFirstName = explode(' ', $patientName)[0] ?? '';
        $suffix = $providerInfo->suffix ?? '';
        $doctorImage = $providerInfo->profile_image != '' ? getImage($providerInfo->profile_image, $appointment->doctor_id) : asset('/assets/images/default_profile.jpg');
        $specialty = $providerInfo->label ?? '';

        $siteURL = env('FRONT_APP_URL');
        $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
        $contactusURL = env('FRONT_APP_URL') . 'patient/home';
        $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
        $imgURL = asset('/assets/images') . '/';
        $patientLeaveReviewUrl = env('FRONT_APP_URL') . 'patient/my-past-booking';

        $resultMainArray = array(
            "{{patientLeaveReviewUrl}}",
            "{{siteUrl}}",
            "{{contactUsUrl}}",
            "{{privacyPolicyUrl}}",
            "{{termsUrl}}",
            "{{patientFirstName}}",
            "{{providerFirstName}}",
            "{{providerLastName}}",
            "{{providerSuffix}}",
            "{{providerSpecialty}}",
            "{{providerLocationName}}",
            "{{providerAddress}}",
            "{{providerPhoneNumberFormatted}}",
            "{{providerProfilePhoto}}",
            "{{imgUrl}}",
            "{{currentYear}}",
        );

        $resultReplaceArray = array(
            $patientLeaveReviewUrl,
            $siteURL,
            $contactusURL,
            $privacyURL,
            $termsURL,
            $patientFirstName,
            $providerFirstName,
            $providerLastName,
            $suffix,
            $specialty,
            $location->name ?? '',
            $providerLocation->address ?? '',
            $providerLocation->office_phone_number ?? '',
            $doctorImage,
            $imgURL,
            $year
        );

        $emailTemplateData = getEmailTemplateData('leave_review_patient');

        $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
        $subject = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
        $content = $emailMiddleBody;
        $email = $patientInfo->email;
        $mailNotification = new EmailNotificationService();
        $mailNotification->sendEmail($email, $content, $subject);

        if ($appointment->allow_text_messages == 1) {
            $smsNotification = new SmsNotificationService();
            $smsNotification->sendSms(
                $appointmentPatientDetails->patient_phone_number ?? NULL,
                str_replace(
                    array("{{doctorName}}"),
                    array($providerFirstName . " " . $providerLastName),
                    getSmsTemplateData('leave_review_patient')
                )
            );
        }
    }
}
