<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Doctor;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class UpdateDoctorSpecialty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update_doctor_specialty';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get specialty from npi registry and update it.';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("Specialty data update started" . Carbon::now());
        set_time_limit(0);
        $doctors = Doctor::select('id', 'npi_number', 'specialties')->where('has_updated', 0)->where('is_updated', 0)->where('npi_number', '!=', '1689619843')->chunk(
            2000,
            function ($doctors) {
                foreach ($doctors as $doctor) {
                    $npiNumber = $doctor->npi_number;
                    $doctorSpecialty = json_encode($doctor->specialties);
                    $url = 'https://npiregistry.cms.hhs.gov/api/?number=' . $npiNumber . '&version=2.1';

                    $response = Http::get($url);
                    $result = $response->json('results');

                    if (!empty($result)) {
                        if (isset($result[0]['taxonomies'])) {
                            $desc = array_column($result[0]['taxonomies'], 'desc');
                            $specialty = json_encode($desc);
                            if ($doctorSpecialty != $specialty) {
                                $doctor->specialties = $desc;
                                $doctor->updated_at = Carbon::now();
                                $doctor->has_updated = 1;
                            }
                            $doctor->is_updated = 1;
                            $doctor->save();
                        }
                    }
                }
            }
        );

        Log::info("Specialty data update completed" . Carbon::now());
    }
}
