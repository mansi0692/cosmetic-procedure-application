<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use App\Models\AppointmentPatientDetail;
use App\Models\City;
use App\Models\DoctorInformation;
use App\Models\DoctorLocation;
use App\Models\PatientInformation;
use App\Services\EmailNotificationService;
use App\Services\SmsNotificationService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Traits\NotificationTrait;

class AppointmentReminderCron extends Command
{
    use NotificationTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appointment-reminder:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This commad will send reminder by push notification to doctor & patient for upcoming appointment before 24 hrs and 1 hrs';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("appointment-reminder cron started" . Carbon::now());
        $start_date = Carbon::now()->addDay()->format('Y-m-d');
        $startTime = Carbon::now()->format('H:i:s');
        $addTime = Carbon::now()->addMinute(2)->format('H:i:s');

        $appointmentData = Appointment::where('appointments.status', config('constants.appointment_status.1'))
            ->whereDate('appointment_date', '=', $start_date)
            ->where('start_time', '>=', $startTime)
            ->where('start_time', '<', $addTime)
            ->get();

        if (!empty($appointmentData)) {
            foreach ($appointmentData as $appointment) {

                $doctorDetails = DoctorInformation::where('user_id', $appointment->doctor_id)->first();
                $appointmentPatientDetails = AppointmentPatientDetail::where('id', $appointment->appointment_patient_id)->first();
                $appointmentTiming = date("M d", strtotime($appointment->appointment_date)) . ' at ' . date("H A", strtotime($appointment->start_time));

                if ($doctorDetails)
                    $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                $notificationTitle = 'Appointment Reminder';

                $notiDocDetails = $notiPatDetails = array();

                $notiDocDetails['templateName'] = 'appointment_update_doctor';
                $notiDocDetails['patientName'] = $appointmentPatientDetails->patient_name;

                $notiPatDetails['templateName'] = 'appointment_update_patient';
                $notiPatDetails['doctorName'] = $doctorName;

                $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_reminder_title';
                $notiDocDetails['appointmentStatus'] = $notiPatDetails['appointmentStatus'] = $appointment->status;
                $notiDocDetails['appointmentTiming'] = $notiPatDetails['appointmentTiming'] = $appointmentTiming;

                if ($appointment->consultant_type == 'video') {
                    $notiDocDetails['templateName'] = 'appointment_update_doctor_video';
                    $notiPatDetails['templateName'] = 'appointment_update_patient_video';
                }

                $this->sendPushNotification($notificationTitle, $notiDocDetails, $appointment->doctor_id, $appointment->id, 'appointment_reminder');
                $this->sendPushNotification($notificationTitle, $notiPatDetails, $appointmentPatientDetails->patient_id, $appointment->id, 'appointment_reminder');
                
                if ($appointment->allow_text_messages == 1) {
                    $smsNotification = new SmsNotificationService();
                    $smsNotification->sendSms(
                        $appointmentPatientDetails->patient_phone_number ?? NULL,
                        str_replace(
                            array("{{doctorName}}", "{{appointmentTiming}}"),
                            array($doctorName, $appointmentTiming),
                            getSmsTemplateData('appointment_reminder')
                        )
                    );
                }

                $this->sendRemiderMail($appointment, $appointmentPatientDetails);
                ($appointment) ? $this->info('Reminder sent successfully') : $this->info('Data not Found');
            }
        }

        Log::info("appointment-reminder cron ended" . Carbon::now());
    }

    public function sendRemiderMail($appointment, $appointmentPatientDetails)
    {
        $providerInfo = DoctorInformation::where('user_id', $appointment->doctor_id)
            ->leftjoin('users', 'doctor_information.user_id', 'users.id')
            ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
            ->select('doctor_information.specialty_id', 'doctor_information.*', 'users.email', 'master_sub_categories.label')
            ->first();

        $patientInfo = PatientInformation::where('user_id', $appointmentPatientDetails->patient_id)
            ->leftjoin('users', 'patient_information.user_id', 'users.id')
            ->first();

        $year = date("Y");
        $day = date('D', strtotime($appointment->appointment_date));
        $date = date('jS M,Y', strtotime($appointment->appointment_date));
        $time = date("H:i",strtotime($appointment->start_time));
        $providerLocation = DoctorLocation::where('doctor_id', $appointment->doctor_id)->first();
        $location = City::find($providerLocation->city_id);
        $providerFirstName = $providerInfo->first_name ?? '';
        $providerLastName = $providerInfo->last_name ?? '';
        $patientName = $patientInfo->full_name;
        $patientFirstName = explode(' ', $patientName)[0] ?? '';
        $patientLastName = explode(' ', $patientName)[1] ?? '';
        $suffix = $providerInfo->suffix ?? '';
        $patientImage = $patientInfo->profile_image != '' ? getImage($patientInfo->profile_image, $patientInfo->user_id) : asset('/assets/images/default_profile.jpg');
        $doctorImage = $providerInfo->profile_image != '' ? getImage($providerInfo->profile_image, $appointment->doctor_id) : asset('/assets/images/default_profile.jpg');
        $specialty = $providerInfo->label ?? '';

        $siteURL = env('FRONT_APP_URL');
        $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
        $contactusURL = env('FRONT_APP_URL') . 'patient/home';
        $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
        $imgURL = asset('/assets/images') . '/';
        $patientReschduleURL = env('FRONT_APP_URL') . 'patient/my-upcoming-booking';
        $patientLeaveReviewUrl = env('FRONT_APP_URL') . 'patient/my-past-booking';

        $resultMainArray = array(
            "{{appointmentDayName}}",
            "{{appointmentMonthDate}}",
            "{{appointmentTime}}",
            "{{patientLeaveReviewUrl}}",
            "{{patientRescheduleAppointmentUrl}}",
            "{{patientCancelAppointmentUrl}}",
            "{{siteUrl}}",
            "{{contactUsUrl}}",
            "{{privacyPolicyUrl}}",
            "{{termsUrl}}",
            "{{patientFirstName}}",
            "{{patientShortLastName}}",
            "{{providerFirstName}}",
            "{{providerLastName}}",
            "{{providerSuffix}}",
            "{{providerSpecialty}}",
            "{{providerLocationName}}",
            "{{providerAddress}}",
            "{{providerPhoneNumberFormatted}}",
            "{{providerProfilePhoto}}",
            "{{patientProfilePhoto}}",
            "{{imgUrl}}",
            "{{currentYear}}",
        );

        $resultReplaceArray = array(
            $day,
            $date,
            $time,
            $patientLeaveReviewUrl,
            $patientReschduleURL,
            $patientReschduleURL,
            $siteURL,
            $contactusURL,
            $privacyURL,
            $termsURL,
            $patientFirstName,
            $patientLastName,
            $providerFirstName,
            $providerLastName,
            $suffix,
            $specialty,
            $location->name ?? '',
            $providerLocation->address ?? '',
            $providerLocation->office_phone_number ?? '',
            $doctorImage,
            $patientImage,
            $imgURL,
            $year
        );

        if ($appointment->consultant_type == 'in person') {
            $emailTemplateData = getEmailTemplateData('appointment_reminder');
        } else if ($appointment->consultant_type == 'video')
            $emailTemplateData = getEmailTemplateData('appointment_reminder_video');

        $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
        $subject = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
        $content = $emailMiddleBody;
        $email = $patientInfo->email;
        $mailNotification = new EmailNotificationService();
        $mailNotification->sendEmail($email, $content, $subject);
		
    }
}
