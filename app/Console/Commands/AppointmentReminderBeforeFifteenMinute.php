<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use App\Models\AppointmentPatientDetail;
use App\Models\City;
use App\Models\DoctorInformation;
use App\Models\DoctorLocation;
use App\Models\PatientInformation;
use App\Services\EmailNotificationService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class AppointmentReminderBeforeFifteenMinute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'appointment_reminder_before_fifteen_minute:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Log::info("appointment_reminder_before_fifteen_minute cron started" . Carbon::now());
        $afterTime = Carbon::now()->addMinutes(15)->format("h:i");
        $startAfterTime = $afterTime . ':00';
        $endAfterTime = $afterTime . ':59';

        $appointmentReminderBeforeFifteenMinute = Appointment::where('appointments.status', config('constants.appointment_status.1'))
            ->where('consultant_type', 'video')
            ->whereDate('appointment_date', '=', Carbon::now()->today())
            ->whereBetween('start_time', [$startAfterTime, $endAfterTime])
            ->get();

        if (!empty($appointmentReminderBeforeFifteenMinute)) {
            foreach ($appointmentReminderBeforeFifteenMinute as $appointment) {

                $appointmentPatientDetails = AppointmentPatientDetail::where('id', $appointment->appointment_patient_id)->first();

                $this->sendRemiderMail($appointment, $appointmentPatientDetails);
                ($appointment) ? $this->info('Reminder before fifteen minute sent successfully') : $this->info('No appointments available after 15 minute' . Carbon::now()->format('h:i:s'));
            }
        }
        Log::info("appointment_reminder_before_fifteen_minute cron ended" . Carbon::now());
    }

    public function sendRemiderMail($appointment, $appointmentPatientDetails)
    {
        $providerInfo = DoctorInformation::where('user_id', $appointment->doctor_id)
            ->leftjoin('users', 'doctor_information.user_id', 'users.id')
            ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
            ->select('doctor_information.specialty_id', 'doctor_information.*', 'users.email', 'master_sub_categories.label')
            ->first();

        $patientInfo = PatientInformation::where('user_id', $appointmentPatientDetails->patient_id)
            ->leftjoin('users', 'patient_information.user_id', 'users.id')
            ->first();

        $year = date("Y");
        $day = date('D', strtotime($appointment->appointment_date));
        $date = date('jS M,Y', strtotime($appointment->appointment_date));
        $time = date("H:i", strtotime($appointment->start_time));
        $providerLocation = DoctorLocation::where('doctor_id', $appointment->doctor_id)->first();
        $location = City::find($providerLocation->city_id);
        $providerFirstName = $providerInfo->first_name ?? '';
        $providerLastName = $providerInfo->last_name ?? '';
        $patientName = $patientInfo->full_name;
        $patientFirstName = explode(' ', $patientName)[0] ?? '';
        $patientLastName = explode(' ', $patientName)[1] ?? '';
        $suffix = $providerInfo->suffix ?? '';
        $patientImage = $patientInfo->profile_image != '' ? getImage($patientInfo->profile_image, $patientInfo->user_id) : asset('/assets/images/default_profile.jpg');
        $doctorImage = $providerInfo->profile_image != '' ? getImage($providerInfo->profile_image, $appointment->doctor_id) : asset('/assets/images/default_profile.jpg');
        $specialty = $providerInfo->label ?? '';

        $siteURL = env('FRONT_APP_URL');
        $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
        $contactusURL = env('FRONT_APP_URL') . 'patient/home';
        $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
        $imgURL = asset('/assets/images') . '/';
        $patientReschduleURL = env('FRONT_APP_URL') . 'patient/my-upcoming-booking';
        $providerWaitingRoomURL = env('FRONT_APP_URL') . 'doctor/appointments-today';
        $providerSeoURL = env('FRONT_APP_URL') . 'patient/doctor-profile/' . $appointment->doctor_id . '/' . $providerLocation->city_id;

        $resultMainArray = array(
            "{{appointmentDayName}}",
            "{{appointmentMonthDateSuffix}}",
            "{{appointmentMonthDate}}",
            "{{appointmentYear}}",
            "{{appointmentTime}}",
            "{{providerVideoVisitAppointmentUrl}}",
            "{{patientRescheduleAppointmentUrl}}",
            "{{patientCancelAppointmentUrl}}",
            "{{providerSeoUrl}}",
            "{{siteUrl}}",
            "{{contactUsUrl}}",
            "{{privacyPolicyUrl}}",
            "{{termsUrl}}",
            "{{patientFirstName}}",
            "{{patientShortLastName}}",
            "{{providerFirstName}}",
            "{{providerLastName}}",
            "{{providerSuffix}}",
            "{{providerSpecialty}}",
            "{{providerLocationName}}",
            "{{providerAddress}}",
            "{{providerPhoneNumberFormatted}}",
            "{{providerProfilePhoto}}",
            "{{patientProfilePhoto}}",
            "{{imgUrl}}",
            "{{currentYear}}",
        );

        $resultReplaceArray = array(
            $day,
            $date,
            $date,
            $year,
            $time,
            $providerWaitingRoomURL,
            $patientReschduleURL,
            $patientReschduleURL,
            $providerSeoURL,
            $siteURL,
            $contactusURL,
            $privacyURL,
            $termsURL,
            $patientFirstName,
            $patientLastName,
            $providerFirstName,
            $providerLastName,
            $suffix,
            $specialty,
            $location->name ?? '',
            $providerLocation->address ?? '',
            $providerLocation->office_phone_number ?? '',
            $doctorImage,
            $patientImage,
            $imgURL,
            $year
        );

        if ($appointment->consultant_type == 'video') {
            $emailTemplateData = getEmailTemplateData('appointment_video_visit_begin_to_dr');
            $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
            $subject = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
            $content = $emailMiddleBody;
            $email = $providerInfo->email;
            $mailNotification = new EmailNotificationService();
            $mailNotification->sendEmail($email, $content, $subject);
        }
        if ($appointment->consultant_type == 'video') {
            $emailTemplateData = getEmailTemplateData('appointment_video_visit_begin_to_patient');
            $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
            $subject = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
            $content = $emailMiddleBody;
            $email = $patientInfo->email;
            $mailNotification = new EmailNotificationService();
            $mailNotification->sendEmail($email, $content, $subject);
        }
    }
}
