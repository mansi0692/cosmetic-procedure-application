<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use App\Models\AppointmentReschedule;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class ExpiredAppointmentCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'expired:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will run every 12:10AM and change status to expired if appointment is not confirmed befor today';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \Log::info("expired appointment cron started");

        $checkStatusArray = [
            config('constants.appointment_status.0'),
            config('constants.appointment_status.4'),
            config('constants.appointment_status.5'),
            config('constants.appointment_status.6'),
        ];

        $appointmentData = Appointment::whereIn('appointments.status', $checkStatusArray)
            ->whereDate('appointments.appointment_date', '<=', Carbon::today())
            ->update(['appointments.status' => config('constants.appointment_status.9')]);

        $appointmentData = AppointmentReschedule::where('status', config('constants.appointment_status.10'))
            ->whereDate('appointment_date', '<=', Carbon::today())
            ->update(['status' => config('constants.appointment_status.8')]);

        ($appointmentData) ? $this->info('Updated successfully') : $this->info('Data not Found');

        \Log::info("expired appointment cron completed!");
    }
}
