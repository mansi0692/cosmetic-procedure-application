<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\Api\ResponseHelper;
use App\Interfaces\NotificationInterface;
use App\Traits\RestExceptionHandlerTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use App\Transformations\NotificationTransformable;
use Validator;
use Exception;

class NotificationController extends Controller
{
    use RestExceptionHandlerTrait, NotificationTransformable;

    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    /**
     * messageInterface
     *
     * @var mixed
     */
    protected $notificationInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $notificationInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, NotificationInterface $notificationInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->notificationInterface = $notificationInterface;
    }

    /**
     * getAllNotifications
     *
     * @return JsonResponse
     */
    public function getAllNotifications()
    {
        $loggedInUserId = auth()->user()->id;

        try {
            $message = $this->notificationInterface->getAllNotifications($loggedInUserId);
            $apiData = (!empty($message)) ?  $this->transformNotificationData($message) : [];

            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($message)) ? trans('api/messages.success.NOTIFICATIONS_DATA_FETCH')
                : trans('api/messages.error.NO_DATA_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('NotificationController/getAllNotifications', $ex->getMessage());
            return $this->internalServerError($ex->getMessage());
        }
    }

    /**
     * updateNotificationStatus
     *
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function updateNotificationStatus(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $validator = Validator::make($input, [
            'notification_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $update =  $this->notificationInterface->updateNotificationStatus($input, $loggedInUserId);
            $apiData = [];
            
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($update) ? trans('api/messages.success.MESSAGE_NOTIFICATION_STATUS_UPDATE')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('NotificationController/updateNotificationStatus', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
