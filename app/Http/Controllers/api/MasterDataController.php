<?php

namespace App\Http\Controllers\api;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\MasterDataInterface;
use Exception;
use URL;
use Illuminate\Support\Facades\Http;
use App\Traits\RestExceptionHandlerTrait;

class MasterDataController extends Controller
{

    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $masterDataInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $masterDataInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, MasterDataInterface $masterDataInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->masterDataInterface = $masterDataInterface;
    }

    /**
     * getAllCountries
     *
     * @return JsonResponse
     */
    public function getAllCountries(): JsonResponse
    {
        try {
            $countries =  $this->masterDataInterface->getAllCountries();

            $apiData =  $countries->toArray();
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_COUNTRY_LISTING')
                : trans('api/messages.success.MESSAGE_NO_COUNTRY_FOUND');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MasterDataController/getAllCountries', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getCitiesFromState
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function getCitiesFromState(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'state_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $stateId = $input['state_id'];
            $cityList =  $this->masterDataInterface->getCitiesFromState($stateId);

            $apiData =  $cityList->toArray();
            $apiStatus = (!empty($apiData)) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_CITY_LISTING')
                : trans('api/messages.success.MESSAGE_NO_CITY_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MasterDataController/getCitiesFromState', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getAllStates
     *
     * @return JsonResponse
     */
    public function getAllStates(): JsonResponse
    {
        try {
            $states =  $this->masterDataInterface->getAllStates();
            $other = [
                'id' => '',
                'name' => 'All'
            ];
            $states->push($other);
            $apiData =  $states->toArray();
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_STATE_LISTING')
                : trans('api/messages.success.MESSAGE_NO_STATE_FOUND');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MasterDataController/getAllStates', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getCityStateId
     *
     * @return JsonResponse
     */
    public function getCityStateId(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'city_name' => 'required',
            'state_name' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $cityName = $input['city_name'];
            $stateName = $input['state_name'];

            $apiData = $this->masterDataInterface->getCityStateId($cityName, $stateName);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_CITY_STATE_ID') : trans('api/messages.success.MESSAGE_CITY_STATE_ID_NOT_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MasterDataController/getCityStateId', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getCategoriesFromType
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function getCategoriesFromType(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $type = $input['type'];
            $categories =  $this->masterDataInterface->getCategoriesFromType($type);

            $apiData =  $categories->toArray();
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_CATEGORY_LISTING')
                : trans('api/messages.success.MESSAGE_NO_CATEGORY_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MasterDataController/getCategoriesFromType', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getSymptoms
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function getSymptoms(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'specialty_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $specialtyId = $input['specialty_id'];
            $symptoms =  $this->masterDataInterface->getSymptoms($specialtyId);

            $apiData =  $symptoms->toArray();
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_CATEGORY_LISTING')
                : trans('api/messages.success.MESSAGE_NO_CATEGORY_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MasterDataController/getSymptoms', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getFiltersWithCount
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function getFiltersWithCount(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $type = $input['type'];
            $categories =  $this->masterDataInterface->getFiltersWithCount($type);

            $apiData =  $categories->toArray();
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_CATEGORY_LISTING')
                : trans('api/messages.success.MESSAGE_NO_CATEGORY_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MasterDataController/getFiltersWithCount', $ex->getMessage());
            return $this->internalServerError();
        }
    }


    /**
     * getLocation
     * @author Heli Patel
     *
     * @param $request
     * @return JsonResponse
     */
    public function getLocation(Request $request): JsonResponse
    {
        try {
            $input = $request->toArray();
            $apiData =  $this->masterDataInterface->getLocation($input);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_LOCATION_FOUND')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MasterDataController/getLocation', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    public function getTopSpeciality(Request $request): JsonResponse
    {
        try {
            $topSpeciality =  $this->masterDataInterface->getTopSpeciality();
            $apiData =  $topSpeciality->toArray();
            foreach ($apiData as $key => $value) {
                foreach ($value as $keyV => $valueV) {
                    if ($keyV == 'specialty_image') {
                        if (!empty($valueV)) {
                            if (isset($value['doctor_id'])) {
                                $apiData[$key][$keyV] = config('constants.get_file_path') . $value['doctor_id'] . '/' . $valueV;
                            }
                            if (isset($value['unreg_doctor_id'])) { // unregistered doctor
                                $apiData[$key][$keyV] = config('constants.get_file_path') . 'top-doctors/' . $valueV;
                            }
                        } else {
                            $apiData[$key][$keyV] = URL::asset('/assets/images/no-profile.jpg');
                        }
                    }
                }
            }
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_TOP_SPECIALITY_FOUND')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MasterDataController/getTopSpeciality', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
