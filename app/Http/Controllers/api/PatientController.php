<?php

namespace App\Http\Controllers\api;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\DoctorInterface;
use App\Interfaces\PatientInterface;
use App\Services\EmailNotificationService;
use Exception;
use App\Traits\RestExceptionHandlerTrait;
use App\Transformations\PatientTransformable;
use Illuminate\Support\Facades\Auth;
use stdClass;

class PatientController extends Controller
{

    use RestExceptionHandlerTrait, PatientTransformable;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $patientInterface, $doctorInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $patientInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, PatientInterface $patientInterface, DoctorInterface $doctorInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->patientInterface = $patientInterface;
        $this->doctorInterface = $doctorInterface;
    }

    /**
     * cancelPatientAccount
     *
     * @param  mixed $responseHelper
     * @param  Request $request
     * @return JsonResponse
     */

    public function contactUsPatient(Request $request): JsonResponse
    {
        try {
            $input = $request->toArray();

            if (!$request->doctor_search) {
                $validator = Validator::make($input, [
                    'subject' => 'required|max:255',
                    'message' => 'required',
                ]);

                if ($validator->fails()) {
                    return $this->responseHelper->error(
                        Response::HTTP_UNPROCESSABLE_ENTITY,
                        $validator->errors()->first()
                    );
                }
            }
            $patient = $this->patientInterface->contactUsPatient(
                auth()->user()->id,
                $input['subject'],
                $input['message'],
                auth()->user()->email
            );

            $apiData =  $patient->toArray();
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_CONTACT_US_PATIENT')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Contact us patient');
            return $this->responseHelper->success($apiStatus, $apiMessage, []);
        } catch (Exception $ex) {
            addErrorLog('PatientController/contactUsPatient', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * cancelPatientAccount
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function cancelPatientAccount(Request $request): JsonResponse
    {
        try {
            $input = $request->toArray();
            if (!$request->doctor_search) {
                $validator = Validator::make($input, [
                    'cancellation_reason' => 'required|max:255',
                ]);

                if ($validator->fails()) {
                    return $this->responseHelper->error(
                        Response::HTTP_UNPROCESSABLE_ENTITY,
                        $validator->errors()->first()
                    );
                }
            }

            $patient = $this->patientInterface->cancelPatientAccount(auth()->user()->id, $input['cancellation_reason']);
            $apiData =  $patient->toArray();;
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_PATIENT_ACCOUNT_CANCELLED')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Cancel patient account');
            return $this->responseHelper->success($apiStatus, $apiMessage, []);
        } catch (Exception $ex) {
            addErrorLog('PatientController/cancelPatientAccount', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Update Patient Profile Submitted during Sign up
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function updatePatientProfile(Request $request): JsonResponse
    {
        $patientData = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $validator = Validator::make(
            $patientData,
            [
                'profile_image' => 'mimes:jpeg,jpg,png|max:10000',
                'email' => 'email|unique:users,email,' . $loggedInUserId . ",id,deleted_at,NULL",
                'username' => 'unique:patient_information,username,' . $loggedInUserId . ',user_id,deleted_at,NULL',
                'phone_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users,phone_number,' . $loggedInUserId . ',id,deleted_at,NULL',
            ],
            [
                'profile_image.mimes' => 'File must be type of jpeg,jpg,png',
                'profile_image.max' => 'Maximum allowed size for profile image is 10MB',
            ]
        );

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $data = [
                'full_name' => $patientData['full_name'] ?? '',
                'phone_number' => $patientData['phone_number'] ?? '',
                'email' => $patientData['email'] ?? '',
                'country_id' => $patientData['country_id'] ?? Null,
                'username' => $patientData['username'] ?? Null,
                'city_id' => $patientData['city_id'] ?? Null,
                'state_id' => $patientData['state_id'] ?? Null,
                'zipcode' => $patientData['zipcode'] ?? '',
                'address' => $patientData['address'] ?? '',
            ];

            if (isset($patientData['profile_image'])) {
                $imageName =  uploadImage($patientData['profile_image'], $loggedInUserId, 'profile_image');
                $data['profile_image'] = $imageName;
            }

            $updatePatientData = $this->patientInterface->updatePatientProfile($loggedInUserId, $data);
            $apiStatus = ($updatePatientData) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($updatePatientData) ? trans('api/messages.success.MESSAGE_PATIENT_UPDATE')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Update patient profile');
            return $this->responseHelper->success($apiStatus, $apiMessage, $updatePatientData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/updatePatientProfile', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get patient profile information
     *
     * @param  mixed $responseHelper
     * @param  Request $request
     * @return JsonResponse
     */
    public function getProfileInformation(Request $request): JsonResponse
    {
        $input = $request->toArray();

        !empty(auth()->user()) ? $loggedInUserId = auth()->user()->id : null;

        $rules = [];

        if (empty($loggedInUserId)) {
            $rules["patient_id"] = "required";
        }

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $profileInformation = $this->patientInterface->getPatientProfileInformation($loggedInUserId ?? $input['patient_id']);
            if (!empty($profileInformation['profile_image']))
                $profileInformation['profile_image'] = !empty($profileInformation['profile_image']) ? getImage($profileInformation['profile_image'], $loggedInUserId ?? $input['patient_id']) : '';

            $apiStatus = ($profileInformation) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($profileInformation) ? trans('api/messages.success.MESSAGE_PATIENT_GET_PROFILE')
                : trans('api/messages.error.PROFILE_DATA_NOT_FOUND');
            addLog('Get profile information');
            return $this->responseHelper->success($apiStatus, $apiMessage, !empty($profileInformation) ? $profileInformation->toArray() : []);
        } catch (Exception $ex) {
            addErrorLog('PatientController/getProfileInformation', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * No appointment open
     *
     * @param  mixed $responseHelper
     * @param  Request $request
     * @return JsonResponse
     */
    public function noAppointmentSchedule(Request $request): JsonResponse
    {
        $input = $request->toArray();

        if (!$request->doctor_search) {
            $validator = Validator::make($input, [
                'doctor_id' => 'required',
                'patient_name' => 'required',
                'phone_number' => 'required',
                'consultancy_type' => 'required',
                'consultancy_reason' => 'required|max:500',
            ]);

            if ($validator->fails()) {
                return $this->responseHelper->error(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $validator->errors()->first()
                );
            }
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $dataParams = array(
                'user_id' => Auth::user()->id,
                'doctor_id' => $input['doctor_id'],
                'patient_name' => $input['patient_name'],
                'booking_id' =>  getBookingId(9),
                'appointment_for' => 'someone else',
                'phone_number' => $input['phone_number'],
                'consultancy_type' => $input['consultancy_type'] == "in_person" ? "in person" : "video",
                'consultancy_reason' => $input['consultancy_reason'],
                'age' => $input['age'],
                'address' => $input['address'],
                'additional_information' => $input['additional_information'],
                'location_id' => $input['location_id'],
                'created_by' => $loggedInUserId
            );

            $patient = $this->patientInterface->noAppointmentSchedule($dataParams);

            $apiData =  $patient->toArray();
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_SUCCESSFULLY_RECEIVED_YOU_APPOINMENT_REQUEST')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            return $this->responseHelper->success($apiStatus, $apiMessage, []);
            addLog('No appointment schedule');
        } catch (Exception $ex) {
            addErrorLog('PatientController/noAppointmentSchedule', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * joinWaitList
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function joinWaitList(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;
        if (!$request->doctor_search) {
            $validator = Validator::make($input, [
                'preferred_date' => 'required',
                'preferred_time' => 'required',
                'doctor_id' => 'required',
                'patient_name' => 'required',
                'phone_number' => 'required',
                'consultancy_type' => 'required',
                'consultancy_reason' => 'required|max:500',
                'location_id' => 'required_if:consultancy_type,==,in_person',
                'booking_for' => 'required',
                'state_id' => 'required',
                'city_id' => 'required',
                'country_id' => 'required',
                'patient_id' => 'required'
            ]);

            if ($validator->fails()) {
                return $this->responseHelper->error(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $validator->errors()->first()
                );
            }
        }

        try {
            $dataParams = new stdClass();
            $dataParams->user_id = $loggedInUserId;
            $dataParams->appointment_date = date("Y-m-d H:i:s", strtotime($input['preferred_date'] . " " . $input['preferred_time'] . ":00"));
            $dataParams->preferred_time = isset($input['preferred_time']) ? date("H:i", strtotime($input['preferred_time'])) : NULL;
            $dataParams->doctor_id = $input['doctor_id'];
            $dataParams->patient_name = $input['patient_name'];
            $dataParams->appointment_for = $input['booking_for'] == 'self' ? 'self' : 'someone else';
            $dataParams->phone_number = $input['phone_number'];
            $dataParams->consultancy_type = $input['consultancy_type'] == "in_person" ? "in person" : "video";
            $dataParams->consultancy_reason = $input['consultancy_reason'];
            $dataParams->age = $input['age'];
            $dataParams->state_id = $input['state_id'];
            $dataParams->city_id = $input['city_id'];
            $dataParams->country_id = $input['country_id'];
            $dataParams->created_by =  $loggedInUserId;
            $dataParams->address = isset($input['address']) ? $input['address'] : NULL;
            $dataParams->additional_information = isset($input['additional_information']) ? $input['additional_information'] : NULL;
            $dataParams->location_id = isset($input['location_id']) ? $input['location_id'] : NULL;
            $dataParams->patient_id = $input['patient_id'];

            $appointments = $this->patientInterface->joinWaitList($dataParams);

            if (!empty($appointments['email_data'])) {
                foreach ($appointments['email_data'] as $data) {
                    if (!empty($data['subject']) && !empty($data['email']) && !empty($data['content'])) {
                        $content = $data['content'];
                        $subject = $data['subject'];
                        $email = $data['email'];
                        $mailNotification = new EmailNotificationService();
                        $mailNotification->sendEmail($email, $content, $subject);
                    }
                }
            }
            $apiData =  [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.MESSAGE_SUCCESSFULLY_RECEIVED_YOU_JOIN_REQUEST');
            addLog('Join wait list');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/joinWaitList', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Create doctor favorite group
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function addFavoriteGroup(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'name' => 'required|max:255|unique:doctor_favorite_group,name,NULL,id,deleted_at,NULL',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $apiData = $this->doctorInterface->addFavoriteGroup($input, $loggedInUserId);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.MESSAGE_ADD_FAVORITE_GROUP');
            addLog('Add favorite group');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/addFavoriteGroup', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * update doctor favorite group
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function updateFavoriteGroup(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_favorite_group_id' => 'required|integer',
            'name' => 'required|max:255|unique:doctor_favorite_group,name,' . $input['doctor_favorite_group_id'] . ',id,deleted_at,NULL',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $apiData = $this->doctorInterface->updateFavoriteGroup($input, $loggedInUserId);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.MESSAGE_UPDATE_FAVORITE_GROUP');
            addLog('Update favorite group');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/updateFavoriteGroup', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Delete favorite doctor group
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function deleteFavoriteGroup(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_favorite_group_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $favoriteDoctor = $this->doctorInterface->deleteFavoriteGroup($input, $loggedInUserId);

            $apiData =  [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($favoriteDoctor) ? trans('api/messages.success.MESSAGE_DELETE_FAVORITE_GROUP')
                : trans('api/messages.error.NO_DATA_FOUND', ['module' => 'Favorite Group']);
            addLog('Delete favorite group');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/deleteFavoriteGroup', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Favorite group list
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function favoriteGroupList(Request $request): JsonResponse
    {
        try {
            $loggedInUserId = auth()->user()->id;
            $apiData = $this->doctorInterface->favoriteGroupList($loggedInUserId);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_FAVORITE_GROUP_LIST')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Favorite group list');
            return $this->responseHelper->success($apiStatus, $apiMessage,  $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/favoriteGroupList', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Add doctor to favorite
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function doctorAddToFavorite(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_id' => 'required',
            'doctor_favorite_group_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $apiData = $this->doctorInterface->doctorAddToFavorite($input, $loggedInUserId);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.MESSAGE_DOCTOR_ADD_TO_FAVORITE');
            addLog('Doctor add to favorite');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/doctorAddToFavorite', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Move favorite doctor to new group
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function doctorMoveToNewGroup(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_favorites_id' => 'required',
            'new_doctor_favorite_group_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $apiData = $this->doctorInterface->doctorMoveToNewGroup($input, $loggedInUserId);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.MESSAGE_DOCTOR_ADD_TO_FAVORITE');
            addLog('Doctor move to new group');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/doctorMoveToNewGroup', $ex->getMessage());
            return $this->internalServerError();
        }
    }


    /**
     * Remove doctor from favorite list
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function doctorRemoveFromFavorite(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_id' => 'required',
            'doctor_favorite_group_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $favoriteDoctor = $this->doctorInterface->doctorRemoveFromFavorite($input, $loggedInUserId);

            $apiData =  [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($favoriteDoctor) ? trans('api/messages.success.MESSAGE_DOCTOR_REMOVE_FROM_FAVORITE_LIST')
                : trans('api/messages.error.NO_DATA_FOUND', ['module' => 'Favorite Doctor']);
            addLog('Doctor remove from favorite');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/doctorRemoveFromFavorite', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Doctor favorite list
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function favoriteDoctorList(Request $request): JsonResponse
    {
        $loggedInUserId = auth()->user()->id;
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'current_coordinates' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $favoriteDoctor = $this->doctorInterface->favoriteDoctorList($input, $loggedInUserId);

            $apiData = $this->transformFavoriteDoctor($favoriteDoctor);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_DOCTOR_FAVORITE_LIST')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Favorite doctor list');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/favoriteDoctorList', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get appoinment patient list
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function getAppoinmentPatientList(Request $request): JsonResponse
    {
        $loggedInUserId = auth()->user()->id;

        try {
            $appoinmentPatientList = $this->patientInterface->getAppoinmentPatientList($loggedInUserId);

            $apiData = $appoinmentPatientList;
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_PATIENT_LIST')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get appoinment patient list');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/getAppoinmentPatientList', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Add other patient details
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function addOtherPatientDetails(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;
        $validator = Validator::make($input, [
            'booking_for' => ['required', function ($attribute, $value, $fail) use ($loggedInUserId, $input) {

                if ($value == 'other' && $input['id'] == 0) {
                    $data = $this->patientInterface->getAppoinmentPatientList($loggedInUserId)['patient_list'];
                    $data = array_filter($data, function ($var) {

                        if ($var['appointment_for'] == 'someone else') {
                            return $var;
                        }
                    });

                    if (count($data) >= 5) {
                        $fail('You cannot add more than 6 members, Kindly Contact admin for more details.');
                    }
                }
            }],
            'full_name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }
        $dataParams = [
            'patient_id' => $loggedInUserId,
            'full_name' => $input['full_name'],
            'mobile_number' => $input['mobile_number'],
            'address' => $input['address'],
            'state' => $input['state'],
            'city' => $input['city'],
            'age' => $input['age'],
        ];

        try {
            $appoinmentPatientList = $this->patientInterface->addOtherPatientDetails($dataParams);

            $apiData = $appoinmentPatientList;
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_PATIENT_DETAILS_ADD')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Add other patient details');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/addOtherPatientDetails', $ex->getMessage());
            return $this->internalServerError();
        }
    }
    /**
     * Update other patient details
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function updateOtherPatientDetails(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId =  $loggedInUserId = auth()->user()->id;;
        $validator = Validator::make($input, [
            'full_name' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }
        $dataParams = [
            'booking_for' => $input['booking_for'] == 'self' ? 'self' : 'someone else',
            'id' => $input['id'] ?? Null,
            'patient_id' => $loggedInUserId,
            'full_name' => $input['full_name'],
            'mobile_number' => $input['mobile_number'],
            'address' => $input['address'],
            'state' => $input['state'],
            'city' => $input['city'],
            'age' => $input['age'],
        ];

        try {
            $appoinmentPatientList = $this->patientInterface->updateOtherPatientDetails($dataParams);

            $apiData = $appoinmentPatientList;
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_PATIENT_DETAILS_UPDATE')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Update other patient details');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/updateOtherPatientDetails', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Book appoinment
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function bookAppoinment(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;
        $validator = Validator::make($input, [
            'consultancy_reason' => 'required|max:500'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        if (!empty($input['registration_form_details'])) {
            $registrationFormDetails = $input['registration_form_details'];
            $registrationFormArray = [];

            foreach ($registrationFormDetails as $formDetails) {

                if (!empty($formDetails['registration_form'])) {
                    $registrationFormArray[] =  [
                        'file' => uploadImage($formDetails['registration_form'], $loggedInUserId, 'files'),
                        'doctor_id' => $formDetails['doctor_id'],
                    ];
                }
            }
        }


        $dataParams = [
            'id' => $input['id'] ?? '',
            'patient_id' => $loggedInUserId,
            'consultancy_reason' => $input['consultancy_reason'] ?? '',
            'is_first_available' => $input['is_first_available'],
            'allow_text_messages' => $input['allow_text_messages'],
            'registration_form' => $registrationFormArray ?? '',
            'additional_note' => isset($input['additional_note']) ? $input['additional_note'] : NULL,
            'booking_details' => isset($input['booking_details']) ? $input['booking_details'] : NULL,
        ];


        try {
            $appoinmentPatientList = $this->patientInterface->bookAppoinment($dataParams);

            $apiData = !empty($appoinmentPatientList) ? $appoinmentPatientList->toArray() : [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_APPOINMENT_BOOK')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Book appoinment');
            return $this->responseHelper->success($apiStatus, $apiMessage, []);
        } catch (Exception $ex) {
            addErrorLog('PatientController/bookAppoinment', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Patient Bookings
     **
     * @author Heli Patel
     * @return JsonResponse
     */
    public function patientBookings(Request $request, $status): JsonResponse
    {
        if ($status != 'past-bookings' && $status != 'upcoming-bookings' && $status != 'no-appointment-schedule') {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                'Please enter a valid url'
            );
        }

        $input = $request->toArray();
        $validator = Validator::make($input, [
            'current_page' => 'required|integer',
            'per_page' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $loggedInUserId = auth()->user()->id;

        $currentPage = $input['current_page'];
        $perPage =  $input['per_page'];
        $offset = (($currentPage - 1) * $perPage) - 1;
        $limit = $perPage;

        try {
            $patientBookings = $this->patientInterface->patientBookings($status, $loggedInUserId, $offset, $limit, $currentPage);
            $apiData = $this->transformPatientAppointments($patientBookings, $offset, $limit, $currentPage);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_PATIENT_BOOKING_LIST')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Patient bookings');
            return $this->responseHelper->successWithPagination($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/patientBookings', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Patient Reschedule Bookings
     **
     * @author Heli Patel
     * @return JsonResponse
     */
    public function patientRescheduleBookings(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $validator = Validator::make($input, [
            'consultancy_type' => 'required',
            'consultancy_reason' => 'required|max:500',
            'appointment_date' => 'required',
            'slot_time' => 'required',
            'appointment_id' => 'required',
            'location_id' => 'required_if:consultancy_type,==,in_person',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $rescheduleAppointment = $this->patientInterface->patientRescheduleBookings($input, $loggedInUserId);

            if (!empty($rescheduleAppointment['subject']) && !empty($rescheduleAppointment['email']) && !empty($rescheduleAppointment['content'])) {
                $content = $rescheduleAppointment['content'];
                $subject = $rescheduleAppointment['subject'];
                $email = $rescheduleAppointment['email'];
                $mailNotification = new EmailNotificationService();
                $mailNotification->sendEmail($email, $content, $subject);
            }
            $apiData = $this->transformPatientAppointments($rescheduleAppointment);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_RESCHEDULE_BOOKING')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Patient reschedule bookings');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/patientRescheduleBookings', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Accept/reject rescheduled request
     **
     * @author Heli Patel
     * @return JsonResponse
     */
    public function rescheduleBookingStatus(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $rules = [
            'status' => 'required',
            "appointment_reschedule_id" => "required",
            "appointment_id" => "required",
            'declined_reason' => 'required_if:status,==,rejected'
        ];

        if (Auth::user()->role_id == 2) {
            $rules['consultancy_type'] = 'required';
            $rules['consultancy_url'] = 'required_if:consultancy_type,==,video_consultancy';
        }

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $loggedInUserId = auth()->user()->id;

        try {
            $bookingData = $this->patientInterface->rescheduleBookingStatus($input, $loggedInUserId);

            if (!empty($bookingData['subject']) && !empty($bookingData['email']) && !empty($bookingData['content'])) {
                $content = $bookingData['content'];
                $subject = $bookingData['subject'];
                $email = $bookingData['email'];
                $mailNotification = new EmailNotificationService();
                $mailNotification->sendEmail($email, $content, $subject);
            }
            $apiData = [];

            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($bookingData) ? trans('api/messages.success.MESSAGE_PATIENT_RESCHEDULE_ACCEPT')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Reschedule booking status');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/rescheduleBookingStatus', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Add doctor review
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function addDoctorReview(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $validator = Validator::make($input, [
            'booking_id' => 'required',
            'bedside_manner'  => 'numeric|min:1|max:5',
            'answered_questions' => 'numeric|min:1|max:5',
            'after_care'  => 'numeric|min:1|max:5',
            'time_spent'  => 'numeric|min:1|max:5',
            'responsiveness'  => 'numeric|min:1|max:5',
            'courtesy'  => 'numeric|min:1|max:5',
            'payment_process'  => 'numeric|min:1|max:5',
            'wait_times'  => 'numeric|min:1|max:5',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $dataParams = [
            'patient_id' => $loggedInUserId,
            'booking_id' => $input['booking_id'],
            'bedside_manner' => isset($input['bedside_manner']) ? $input['bedside_manner'] : 0,
            'answered_questions' => isset($input['answered_questions']) ? $input['answered_questions'] : 0,
            'after_care' => isset($input['after_care']) ? $input['after_care'] : 0,
            'time_spent' => isset($input['time_spent']) ? $input['time_spent'] : 0,
            'responsiveness' => isset($input['responsiveness']) ? $input['responsiveness'] : 0,
            'courtesy' => isset($input['courtesy']) ? $input['courtesy'] : 0,
            'payment_process' => isset($input['payment_process']) ? $input['payment_process'] : 0,
            'wait_times' => isset($input['wait_times']) ? $input['wait_times'] : 0,
            'review' => isset($input['review']) ? $input['review'] : NULL,
        ];

        try {
            $appoinmentPatientList = $this->patientInterface->addDoctorReview($dataParams);

            $apiData = $appoinmentPatientList->toArray();
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_DOCTOR_RATING_ADDED')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Add doctor review');
            return $this->responseHelper->success($apiStatus, $apiMessage, []);
        } catch (Exception $ex) {
            addErrorLog('PatientController/addDoctorReview', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get doctor review
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function getDoctorReview(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'booking_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $appoinmentPatientList = $this->patientInterface->getDoctorReview($input['booking_id']);

            $apiData = !empty($appoinmentPatientList) ? $appoinmentPatientList->toArray() : [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_DOCTOR_RATING_FETCH')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get doctor review');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/getDoctorReview', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Cancel appointment
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function cancelBooking(Request $request): JsonResponse
    {
        $loggedInUserId = auth()->user()->id;
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'appointment_id' => 'required',
            'declined_reason' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $cancelBooking = $this->patientInterface->cancelBooking($input, $loggedInUserId);

            if (!empty($cancelBooking['doctor']['subject']) && !empty($cancelBooking['doctor']['content']) && !empty($cancelBooking['doctor']['email']) && !empty($cancelBooking['patient']['subject']) && !empty($cancelBooking['patient']['content']) && !empty($cancelBooking['patient']['email'])) {
                $mailNotification = new EmailNotificationService();
                foreach ($cancelBooking as $val) {
                    $content = $val['content'];
                    $subject = $val['subject'];
                    $email = $val['email'];
                    $mailNotification->sendEmail($email, $content, $subject);
                }
            }
            $apiData = [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($cancelBooking) ? trans('api/messages.success.MESSAGE_CANCEL_BOOKING')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Cancel booking');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('PatientController/cancelBooking', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * bookAppoinmentNew
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function bookAppoinmentNew(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $rules = [
            "consultancy_reason" => 'max:500',
            'booking_for' => ['required', function ($attribute, $value, $fail) use ($loggedInUserId, $input) {

                if ($value == 'other' && $input['id'] == 0) {
                    $data = $this->patientInterface->getAppoinmentPatientList($loggedInUserId)['patient_list'];
                    $data = array_filter($data, function ($var) {

                        if ($var['appointment_for'] == 'someone else') {
                            return $var;
                        }
                    });

                    if (count($data) >= 5) {
                        $fail('You cannot add more than 6 members, Kindly Contact admin for more details.');
                    }
                }
            }],
            'consultancy_type' => 'required',
            'patient_name' => 'required',
            'phone_number' => 'required',
            'state_id' => 'required',
            'city_id' => 'required',
            'country_id' => 'required',
            'location' => 'required_if:consultancy_type,==,in_person',
            'slot_date' => 'required',
            'slot_time' => 'required',
            'slot_end_time' => 'required_if:consultancy_type,==,video_consultancy',
            'registration_form' => "mimes:pdf,doc,docx|max:250000",
        ];

        if (!empty($input['doctor_id']) && count($input['doctor_id']) > 1) {
            $rules['registration_form'] = 'mimes:pdf,doc,docx|max:250000';
        }

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        if (!empty($input['registration_form']))
            $fileName =  uploadImage($input['registration_form'], $loggedInUserId, 'files');

        if (!empty($input['registration_form_details']) && empty($input['registration_form'])) {
            $registrationFormDetails = $input['registration_form_details'];

            foreach ($registrationFormDetails as $formDetails) {
                $registrationFormArray[] =  [
                    'file' => uploadImage($formDetails['registration_form'], $loggedInUserId, 'files'),
                    'doctor_id' => $formDetails['doctor_id'],
                ];
            }
        }

        $dataParams = [
            'patient_id' => $loggedInUserId,
            'doctor_id' => isset($input['doctor_id']) ? $input['doctor_id'] : NULL,
            'booking_for' => $input['booking_for'] == 'self' ? 'self' : 'someone else',
            'patient_name' => isset($input['patient_name']) ? $input['patient_name'] : NULL,
            'phone_number' => isset($input['phone_number']) ? $input['phone_number'] : NULL,
            'address' => isset($input['address']) ? $input['address'] : NULL,
            'age' => isset($input['age']) ? $input['age'] : NULL,
            'state_id' => $input['state_id'],
            'city_id' => $input['city_id'],
            'country_id' => $input['country_id'],
            'registration_form' => $fileName ?? $registrationFormArray ?? '',
            'additional_note' => isset($input['additional_note']) ? $input['additional_note'] : NULL,
            'location' => $input['location'] ?? Null,
            'appoinment_date' => date("Y-m-d H:i:s", strtotime($input['slot_date'] . " " . $input['slot_time'])),
            'start_time' => date("H:i", strtotime($input['slot_time'])),
            'end_time' => isset($input['slot_end_time']) ? date("H:i", strtotime($input['slot_end_time'])) : NULL,
            'consultancy_reason' => $input['consultancy_reason'],
            'consultancy_type' => $input['consultancy_type'] == 'in_person' ? "in person" : 'video',
            'terms_and_conditions' => isset($input['terms_and_conditions']) ? $input['terms_and_conditions'] : NULL,
            'is_first_available' => isset($input['is_first_available']) ? $input['is_first_available'] : NULL,
            'allow_text_messages' => isset($input['allow_text_messages']) ? $input['allow_text_messages'] : NULL,
            'id' => isset($input['id']) ? $input['id'] : NULL,
            'booking_details' => isset($input['booking_details']) ? $input['booking_details'] : NULL,
        ];

        try {
            $appoinmentPatientList = $this->patientInterface->bookAppoinmentNew($dataParams);
            foreach ($appoinmentPatientList['email'] as $data) {
                if (!empty($data['subject']) && !empty($data['email']) && !empty($data['content'])) {
                    $content = $data['content'];
                    $subject = $data['subject'];
                    $email = $data['email'];
                    $mailNotification = new EmailNotificationService();
                    $mailNotification->sendEmail($email, $content, $subject);
                }
            }
            $apiData = !empty($appoinmentPatientList) ? $appoinmentPatientList->toArray() : [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_APPOINMENT_BOOK')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Book appoinment new');
            return $this->responseHelper->success($apiStatus, $apiMessage, []);
        } catch (Exception $ex) {
            addErrorLog('PatientController/bookAppoinmentNew', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getFeedList
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function getFeedList(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'per_page' => 'required',
            'current_page' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $currentPage = $input['current_page'];
        $perPage =  $input['per_page'];
        $offset = (($currentPage - 1) * $perPage) ;
        $limit = $perPage;

        try {
            $apiData = [];

            $feedData = $this->patientInterface->getFeedListData($input, $offset, $limit, $currentPage);

            $apiData =  $this->transformFeedList($feedData, $offset, $limit, $currentPage);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (count($apiData) > 0) ? trans('api/messages.success.MESSAGE_FEED_LISTING')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get feed list');
            return $this->responseHelper->successWithPagination($apiStatus, $apiMessage, $apiData, [], false);
        } catch (Exception $ex) {
            addErrorLog('PatientController/getFeedList', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
