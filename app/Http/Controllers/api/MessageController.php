<?php

namespace App\Http\Controllers\api;

use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Traits\RestExceptionHandlerTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Exception;
use App\Interfaces\MessageInterface;
use App\Transformations\MessageTransformable;


class MessageController extends Controller
{

    use RestExceptionHandlerTrait, MessageTransformable;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    /**
     * messageInterface
     *
     * @var mixed
     */
    protected $messageInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $messageInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, MessageInterface $messageInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->messageInterface = $messageInterface;
    }


    /**
     * store
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'to_user_id' => 'required',
            'message' => 'required_if:file,null',
            'file' => 'required_if:message,null|max:10240|mimes:doc,docx,pdf,png,jpg,jpeg,png,svg,mp4,mkv,webm',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $loggedInUserId = auth()->user()->id;        
        try {

            $message = $this->messageInterface->store($input);
            $apiData = (!empty($message)) ?  $this->transformMessageData($message, $loggedInUserId) : [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($message)) ? trans('api/messages.success.MESSAGE_SENT')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MessageController/store', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getMessages
     *
     * @param  mixed $id
     * @return JsonResponse
     */
    public function getMessages(Request $request)
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'from_user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }
        $loggedInUserId = auth()->user()->id;
        $fromUserId = $input['from_user_id'];

        try {
            $message = $this->messageInterface->getMessages($loggedInUserId, $fromUserId);
            $apiData = (!empty($message)) ? $this->transformMessageData($message, $fromUserId) : [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($message)) ? trans('api/messages.success.MESSAGE_CHAT_MESSAGE_FETCH')
                : trans('api/messages.success.NO_MESSAGE_FOUND');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MessageController/getMessages', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * deleteChat
     *
     * @param  mixed $id
     * @return JsonResponse
     */
    public function deleteChat(Request $request)
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'patient_id' => 'required',
            'doctor_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $patientId = $input['patient_id'];
        $doctorId = $input['doctor_id'];

        try {
            $deleteChat = $this->messageInterface->deleteChat($patientId, $doctorId);

            $apiData = [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage =  trans('api/messages.success.MESSAGE_CHAT_REQUEST_DELETE');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MessageController/deleteChat', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * sendChatRequest
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function sendChatRequest(Request $request)
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'patient_id' => 'required|integer',
            'doctor_id' => 'required|integer',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }
        try {
            $temp = $this->messageInterface->sendChatRequest($input);
            $apiData = [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($temp)) ? trans('api/messages.success.MESSAGE_CHAT_REQUEST_UPDATE')
                : trans('api/messages.success.NO_MESSAGE_FOUND');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MessageController/sendChatRequest', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * chatRequestList
     *
     * @return JsonResponse
     */
    public function chatRequestList(): JsonResponse
    {
        $loggedInUserId = auth()->user()->id;
        try {
            $apiData = [];
            $chatList = $this->messageInterface->chatRequestList($loggedInUserId);
            $apiData = $this->transformChatRequestList($chatList);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_CHAT_REQUEST_LIST_FETCH')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MessageController/chatRequestList', $ex->getMessage());
            return $this->internalServerError();
        }
    }


    /**
     * readAllMessages
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function readAllMessages(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'from_user_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }
        $loggedInUserId = auth()->user()->id;

        $fromUserId = $input['from_user_id'];

        try {
            $this->messageInterface->readAllMessages($loggedInUserId, $fromUserId);
            $apiData = [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.MESSAGE_CHAT_MESSAGE_READ');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('MessageController/readAllMessages', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
