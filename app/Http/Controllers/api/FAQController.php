<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Traits\RestExceptionHandlerTrait;
use App\Transformations\FAQTransformable;
use App\Helpers\Api\ResponseHelper;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use App\Interfaces\FAQCategoryInterface;
use App\Interfaces\FAQInterface;
use Validator;
use Exception;


class FAQController extends Controller
{

    use RestExceptionHandlerTrait, FAQTransformable;

    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    /**
     * faqCategoryInterface
     *
     * @var mixed
     */
    private $faqCategoryInterface;

    /**
     * faqInterface
     *
     * @var mixed
     */
    private $faqInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $faqCategoryInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, FAQCategoryInterface $faqCategoryInterface, FAQInterface $faqInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->faqCategoryInterface = $faqCategoryInterface;
        $this->faqInterface = $faqInterface;
    }

    /**
     * getFAQCategoryList
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getFAQCategoryList(Request $request): JsonResponse
    {

        try {
            $faqCategoryList = $this->faqCategoryInterface->getAllCategories();

            $apiData = $this->transformFAQCategoryData($faqCategoryList);
            $apiStatus =  Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_FAQ_CATEGORY_LISTING')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('FAQController/getFAQCategoryList', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getFAQList
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getFAQList(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'faq_category_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $faqCategoryId = $input['faq_category_id'];

        try {
            $faqList = $this->faqInterface->getFAQList($faqCategoryId);

            $apiData = $this->transformFAQData($faqList->toArray());
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_FAQ_LISTING')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('FAQController/getFAQList', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
