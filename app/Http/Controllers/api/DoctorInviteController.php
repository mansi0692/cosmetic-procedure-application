<?php

namespace App\Http\Controllers\api;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Helpers\Api\ResponseHelper;
use App\Interfaces\DoctorInviteInterface;
use App\Models\DoctorInvite;
use App\Traits\RestExceptionHandlerTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class DoctorInviteController extends Controller
{
    use RestExceptionHandlerTrait;

    /**
     * $responseHelper
     *
     * @var mixed
     */
    private $responseHelper;
    /**
     * $doctorInviteInterface
     *
     * @var mixed
     */
    protected $doctorInviteInterface;

    public function __construct(ResponseHelper $responseHelper, DoctorInviteInterface $doctorInviteInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->doctorInviteInterface = $doctorInviteInterface;
    }

    /**
     * inviteDoctor
     *
     * @param Request $request
     * 
     * @return JsonResponse
     */
    public function inviteDoctor(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'email' => [
                'required', 'email',
                function ($attribute, $value, $fail) {
                    $inviteDoctor = DoctorInvite::where('email', $value)->first();
                    $validateEmail = User::where('email', $value)->whereNull('deleted_at')->first();

                    if (isset($validateEmail)) {
                        $fail(trans('api/messages.success.MESSAGE_DOCTOR_REGISTRATION_EMAIL_EXIST'));
                    }
                    if (isset($inviteDoctor)) {
                        $fail(trans('api/messages.success.MESSAGE_DOCTOR_INVITATION_EMAIL_EXIST'));
                    }
                }
            ]
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $email = $input['email'];

        try {
            $inviteDoctor = $this->doctorInviteInterface->sendInvitation($email);
            $apiData = [];
            $apiStatus =  Response::HTTP_OK;
            $apiMessage = (!empty($inviteDoctor)) ? trans('api/messages.success.MESSAGE_INVITATION_SENT')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorInviteController/inviteDoctor', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    public function requestAdminToInviteDoctor(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $doctorId = $input['doctor_id'];
        
        try {
            $inviteDoctor = $this->doctorInviteInterface->requestAdminToInviteDoctor($doctorId);
            $apiData = [];
            $apiStatus =  Response::HTTP_OK;
            $apiMessage = (!empty($inviteDoctor)) ? trans('api/messages.success.MESSAGE_ADMIN_NOTIFIED')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorInviteController/requestAdminToInviteDoctor', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
