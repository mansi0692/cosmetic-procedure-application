<?php

namespace App\Http\Controllers\api;

use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\CMSInterface;
use App\Interfaces\UserInterface;
use App\Traits\RestExceptionHandlerTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use Exception;

class CMSController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    /**
     * userInterface
     *
     * @var mixed
     */
    protected $userInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $authInterface
     * @param  mixed $userInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper,CMSInterface $cmsInterface, UserInterface $userInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->userInterface = $userInterface;
        $this->cmsInterface = $cmsInterface;
    }

    /**
     * Get cms content by url
     **
     * @author Heli Patel
     * @param  mixed $request
     * @return JsonResponse
     */
    public function getCmsContent(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'url' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $cmsData = $this->cmsInterface->getCmsContent($input['url']);
            $apiData =  $cmsData->toArray();
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_CMS_CONTENT')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('CMSController/getCmsContent', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
