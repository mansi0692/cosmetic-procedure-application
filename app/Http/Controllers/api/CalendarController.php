<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Helpers\Api\ResponseHelper;
use App\Traits\RestExceptionHandlerTrait;
use App\Interfaces\CalendarInterface;
use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Transformations\CalendarTransformable;
use Validator;
use Exception;


class CalendarController extends Controller
{
    use RestExceptionHandlerTrait, CalendarTransformable;

    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $calendarInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $calendarInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, CalendarInterface $calendarInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->calendarInterface = $calendarInterface;
    }

    /**
     * Book Slot For Doctor
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function blockDoctorSlot(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;
        $validator = Validator::make($input, [
            'location_id' => 'required',
            'date' => 'required',
            'start_time' => 'required|date_format:h:i A',
            'end_time' => 'required|date_format:h:i A',
        ]);

        $appointmentDate =  date('Y-m-d', strtotime($input['date']));
        $startTime =  date('H:i', strtotime($input['start_time']));
        $endTime =  date('H:i', strtotime($input['end_time']));

        $getDoctorAppointment = Appointment::whereDate('appointment_date', '=', $appointmentDate)
            ->where(function ($query) use ($startTime, $endTime) {
                $query->where('start_time', '<=', $startTime);
                $query->where('end_time', '>=', $endTime);
                $query->OrWhereBetween('start_time', [date('H:i', (strtotime($startTime) + 60)), date('H:i:', (strtotime($endTime) - 60))]);
                $query->OrWhereBetween('end_time', [date('H:i', (strtotime($startTime) + 60)), date('H:i:', (strtotime($endTime) - 60))]);
            })->first();
            
        if ($getDoctorAppointment) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                trans('api/messages.error.MESSAGE_CALENDAR_BLOCK_SLOT_ERROR')
            );
        }

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {

            $blockDoctorSlot = $this->calendarInterface->blockDoctorSlot($input, $loggedInUserId);
            $apiData =  $this->transformBookingData($blockDoctorSlot);

            $apiStatus = ($blockDoctorSlot) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($blockDoctorSlot) ? trans('api/messages.success.MESSAGE_CALENDAR_BLOCK_SLOT')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('CalendarController/blockDoctorSlot', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get Working Time
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function getWorkingTime(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;
        $validator = Validator::make($input, [
            'location_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $getWorkingTime = $this->calendarInterface->getWorkingTime($input, $loggedInUserId);
            $apiData =  ($getWorkingTime) ? $this->transformDoctorWorkTiming($getWorkingTime) : [];

            $apiStatus =  Response::HTTP_OK;
            $apiMessage = ($getWorkingTime) ? trans('api/messages.success.MESSAGE_DOCTOR_WORKING_TIME_FETCHED')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('CalendarController/getWorkingTime', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Manage Working Time for Doctor
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function manageWorkingTime(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;
        $validator = Validator::make($input, [
            'location_id' => 'required',
            'duration' => 'required',
            'work_timing' => 'array',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $workItemData = $this->calendarInterface->manageWorkingTime($input, $loggedInUserId);
            $apiData =  $this->transformDoctorWorkTiming($workItemData);

            $apiStatus = ($workItemData) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($workItemData) ? trans('api/messages.success.MESSAGE_DOCTOR_WORKING_TIME_UPDATED')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('CalendarController/manageWorkingTime', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get Slot For Doctor
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function getDoctorAppointment(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $validator = Validator::make($input, [
            'location_id' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $getDoctorAppointment = $this->calendarInterface->getDoctorAppointment($input, $loggedInUserId);
            $apiData =  $this->transformDoctorAppointments($getDoctorAppointment);

            $apiStatus = ($getDoctorAppointment) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($getDoctorAppointment) ? trans('api/messages.success.MESSAGE_DOCTOR_APPOINTMENT_FETCHED')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('CalendarController/getDoctorAppointment', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Delete Working Time
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function deleteWorkingTime(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;
        $validator = Validator::make($input, [
            'working_time_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $getWorkingTime = $this->calendarInterface->deleteWorkingTime($input, $loggedInUserId);
            $apiData =  [];

            $apiStatus = ($getWorkingTime) ? Response::HTTP_OK : Response::HTTP_NO_CONTENT;
            $apiMessage = ($getWorkingTime) ? trans('api/messages.success.MESSAGE_DOCTOR_WORKING_TIME_DELETED')
                : trans('api/messages.error.MESSAGE_NO_DATA_FOUND');

            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('CalendarController/deleteWorkingTime', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
