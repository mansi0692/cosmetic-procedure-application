<?php

namespace App\Http\Controllers\api;

use Exception;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Interfaces\AuthInterface;
use App\Interfaces\UserInterface;
use Illuminate\Http\JsonResponse;
use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use App\Traits\RestExceptionHandlerTrait;
use App\Transformations\UserTransformable;
use PhpParser\Node\Expr\Cast\Array_;

class AuthController extends Controller
{
    use UserTransformable, RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;


    /**
     * authInterface
     *
     * @var mixed
     */
    protected $authInterface;


    /**
     * userInterface
     *
     * @var mixed
     */
    protected $userInterface;


    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $authInterface
     * @param  mixed $userInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, AuthInterface $authInterface, UserInterface $userInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->authInterface = $authInterface;
        $this->userInterface = $userInterface;
    }

    /**
     * login function
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'email_or_phone' => 'required',
            'password' => 'required|min:8',
            'role_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }
        $emailOrPhone = $input['email_or_phone'];
        $password = $input['password'];
        $roleId = $input['role_id'];
        $deviceData = [
            'platform' => $input['platform'] ?? '',
            'device_token' => $input['device_token'] ?? '',
        ];

        if (!is_numeric($emailOrPhone)) {
            $emailValidation = Validator::make($input, [
                'email_or_phone' => 'email',
            ], ['email' => trans('api/messages.error.EMAIL_FORMAT')]);
            if ($emailValidation->fails()) {
                return $this->responseHelper->error(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $emailValidation->errors()->first()
                );
            }
        } else {
            $phoneValidation = Validator::make($input, [
                'email_or_phone' => 'numeric|min:10',
            ], ['min' => trans('api/messages.error.PHONE_FORMAT')]);
            if ($phoneValidation->fails()) {
                return $this->responseHelper->error(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $phoneValidation->errors()->first()
                );
            }
        }

        $user = $this->authInterface->login($emailOrPhone, $password, $roleId, $deviceData);
        if (is_null($user)) {

            return $this->responseHelper->error(
                Response::HTTP_UNAUTHORIZED,
                trans('api/messages.error.NO_DATA_FOUND', ['module' => 'User']),
            );
            // return $this->modelNotFound(404, 'User Not Found.');
        }

        if (!$user->status) {
            return $this->responseHelper->error(
                Response::HTTP_UNAUTHORIZED,
                trans('api/messages.error.USER_NOT_ACTIVATED')
            );
        }

        if (!$user->is_verified) {
            return $this->responseHelper->error(
                Response::HTTP_FORBIDDEN,
                trans('api/messages.error.USER_NOT_VERIFIED')
            );
        }

        if (!$user || !Hash::check($password, $user->password)) {
            return $this->responseHelper->error(
                Response::HTTP_UNAUTHORIZED,
                trans('api/messages.error.MESSAGE_INVALID_CREDENTIAL')
            );
        }

        if ($user->role_id == config('constants.roles.doctor')) {
            $userInformation = $this->authInterface->getDoctorInformation($user->id);
        } else {
            $userInformation = $this->authInterface->getPatientInformation($user->id);
        }

        $token = $user->createToken('api token')->plainTextToken;
        $apiData =  $this->transformUser($userInformation, $token);
        $apiStatus = Response::HTTP_OK;
        $apiMessage =   trans('api/messages.success.LOGIN_SUCCESS');
        return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
    }

    /**
     * patientRegistration
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function patientRegistration(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'full_name' => 'required',
            'phone_number' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:10|unique:users,phone_number,NULL,id,deleted_at,NULL',
            'email' => 'required|regex:/(.+)@(.+)\.(.+)/i|unique:users,email,NULL,id,deleted_at,NULL',
            'username' => 'required|unique:patient_information,username,NULL,id,deleted_at,NULL',
            'country' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zip_code' => 'required|regex:/^(?:(\d{5})(?:[ \-](\d{4}))?)$/i',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password',
        ], [
            'phone_number.min' => trans('api/messages.error.PHONE_FORMAT'),
        ]);

        $deviceData = [
            'platform' => $input['platform'] ?? '',
            'device_token' => $input['device_token'] ?? '',
        ];

        if ($validator->fails()) {
            $errors = [];

            $target = [
                "The phone number has already been taken.",
                "The email has already been taken.",
                "The username has already been taken."
            ];

            foreach ($validator->errors()->all() as $error) {
                if (in_array($error, $target)) {
                    array_push($errors, $error);
                }
            }

            if (!empty($errors)) {
                return $this->responseHelper->error(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    implode(",", $errors),
                );
            }

            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $userInfo = $this->authInterface->registerPatient($input, $deviceData);
            $apiData =  $this->transformUser($userInfo);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.REGISTRATION_SUCCESS', ['user_type' => 'Patient']);
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('AuthController/patientRegistration', $ex->getMessage());
            return $this->internalServerError();
        }
    }


    /**
     * socialLogin
     *
     * @param  mixed $provider
     * @param  mixed $request
     * @return JsonResponse
     */
    public function socialLogin($provider, Request $request): JsonResponse
    {
        try {
            // Socialite will pick response data automatic
            if ($provider == 'twitter') {
                $data = $request->toArray();

                $config = json_decode($data['config']['data']);
                $oauthToken = $config->oauth_token;
                $oauthVerifier = $config->oauth_verifier;

                $client = new \GuzzleHttp\Client();

                $response = $client->request('POST', 'https://api.twitter.com/oauth/access_token', [
                    'form_params' => [
                        'oauth_token' => $oauthToken,
                        'oauth_verifier' => $oauthVerifier
                    ]
                ]);

                $result = $response->getBody()->getContents();
                $twitterResponse = explode('&', $result);
                $accessToken = substr($twitterResponse[0], strpos($twitterResponse[0], "=") + 1);
                $accessTokenSecret = substr($twitterResponse[1], strpos($twitterResponse[1], "=") + 1);
                $userDetails = Socialite::driver($provider)->userFromTokenAndSecret($accessToken, $accessTokenSecret);
            } else {
                $userDetails = Socialite::driver($provider)->stateless()->user();
            }
            $token = $userDetails->token;
            $email = $userDetails->email;
            $isUserExists = $this->authInterface->checkUserExistByEmailOrToken($email, $token);
            if (is_null($isUserExists)) {
                $userInformation = [
                    'full_name' => $userDetails->name,
                    'phone_number' => null,
                    'email' => $userDetails->email,
                    'country' => null,
                    'city' => null,
                    'zip_code' => null,
                    'password' => 'social@123',
                    'social_account_type' => $provider,
                    'social_account_token' => isset($userDetails->token) ? $userDetails->token : null,
                    'username' => $userDetails->name,
                ];

                $input = $request->toArray();
                $deviceData = [
                    'platform' => $input['platform'] ?? '',
                    'device_token' => $input['device_token'] ?? '',
                ];

                $user = $this->authInterface->registerPatient($userInformation, $deviceData);
            } else {
                $userId = $isUserExists->id;
                $user = $this->authInterface->getPatientInformation($userId);
            }

            $token = $user->createToken($user->full_name)->plainTextToken;
            $apiData =  $this->transformUser($user, $token);
            $apiStatus = Response::HTTP_OK;
            $apiMessage =  !is_null($isUserExists) ? trans('api/messages.success.LOGIN_SUCCESS')
                : trans('api/messages.success.REGISTRATION_SUCCESS', ['user_type' => 'Patient']);
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('AuthController/socialLogin', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * sendOtp
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function sendOtp(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'email_or_phone' => 'required',
            'role_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }
        $isPhoneNumber = true;
        if (!is_numeric($input['email_or_phone'])) {
            $emailValidation = Validator::make($input, [
                'email_or_phone' => 'email',
            ], ['email' => trans('api/messages.error.EMAIL_FORMAT')]);
            if ($emailValidation->fails()) {
                return $this->responseHelper->error(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $emailValidation->errors()->first()
                );
            }
            $isPhoneNumber = false;
        } else {
            $phoneValidation = Validator::make($input, [
                'email_or_phone' => 'numeric|min:10',
            ], ['min' => trans('api/messages.error.PHONE_FORMAT')]);
            if ($phoneValidation->fails()) {
                return $this->responseHelper->error(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $phoneValidation->errors()->first()
                );
            }
            $isPhoneNumber = true;
        }

        try {
            $userName = $input['email_or_phone'];
            $user = $this->userInterface->findUser($input['email_or_phone'], $input['role_id']);

            if (is_null($user)) {
                $recordNotFoundMsg = trans('api/messages.error.NO_DATA_FOUND', ['module' => 'User']);
                return $this->responseHelper->errorWithSuccessKey(
                    Response::HTTP_OK,
                    $recordNotFoundMsg,
                    false
                );
            }

            if ($user->otp_count >= config('constants.otp_limit')) {
                $lastOtpSentTime = $this->authInterface->lastOtpSentTime($user->id);
                $currentTime    = new Carbon(now());
                $hourDifference = $lastOtpSentTime->diffInHours($currentTime);
                if ($hourDifference > 1) {
                    $this->authInterface->resetOtpCount($user->id);
                }
            }

            if ($user->otp_count == config('constants.otp_limit') && $hourDifference < 1) {
                return $this->responseHelper->error(
                    Response::HTTP_TOO_MANY_REQUESTS,
                    trans('api/messages.error.OTP_LIMIT_EXCEED')
                );
            }

            $otpCount = $this->authInterface->sendOtpToUser($user, $isPhoneNumber);
            $apiData =  ['email_or_phone' => $userName, 'user_id' => $user->id, 'otp_count' => $otpCount];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.OTP_SENT');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('AuthController/sendOtp', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * verifyOtp
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function verifyOtp(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            // 'user_id' => 'required|numeric',
            'otp' => 'required|digits:6',
            'email_or_phone' => 'required',
            'role_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $userName = $input['email_or_phone'];
            $roleId = $input['role_id'];
            $otp = $input['otp'];
            $user = $this->userInterface->findUser($userName, $roleId);

            if (is_null($user)) {
                $recordNotFoundMsg = trans('api/messages.error.NO_DATA_FOUND', ['module' => 'User']);
                return $this->modelNotFound('', $recordNotFoundMsg);
            }

            $userId = $user->id;
            $userOtp = $this->authInterface->verifyOtp($userId, $otp);
            if (is_null($userOtp)) {
                $recordNotFoundMsg = trans('api/messages.error.INVALID_OTP');
                return $this->modelNotFound('', $recordNotFoundMsg);
            }

            if ($userOtp->otp !== $otp) {

                return $this->responseHelper->error(
                    Response::HTTP_OK,
                    trans('api/messages.error.OTP_NOT_MATCH', ['module' => 'User']),
                );

                // $recordNotFoundMsg = trans('api/messages.error.OTP_NOT_MATCH');
                // return $this->modelNotFound('', $recordNotFoundMsg);
            } else {
                $this->userInterface->verifyUser($userId);
                if ($user->role_id == config('constants.roles.doctor')) {
                    $userInformation = $this->authInterface->getDoctorInformation($user->id);
                } else {
                    $userInformation = $this->authInterface->getPatientInformation($user->id);
                }

                $token = $userInformation->createToken("api token")->plainTextToken;

                $apiData = $this->transformUser($userInformation, $token);
                $apiStatus = Response::HTTP_OK;
                $apiMessage = trans('api/messages.success.OTP_VERIFIED');
                return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
            }
        } catch (Exception $ex) {
            addErrorLog('AuthController/verifyOtp', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * twitter
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function twitter(Request $request): JsonResponse
    {
        // get oauthtoken from response
        $token = Socialite::driver('twitter')->redirect();
        $response = print_r($token, true);
        $regex = '/https?\:\/\/[^\" ]+/i';
        preg_match($regex, $response, $matches);
        // handle URL if found
        $url = $matches[0] ?? null;
        if (!$url) {
            return $this->internalServerError();
        }
        $token = explode('=', trim($url));
        $token = $token[1];
        return response()->json(['oauth_token' => $token]);
    }

    /**
     * resetPassword
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function resetPassword(Request $request): JsonResponse
    {

        $input = $request->toArray();
        $validator = Validator::make($input, [
            'email_or_phone' => 'required',
            'new_password' => 'required|min:8',
            'confirm_password' => 'required|same:new_password',
            'role_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        if (!is_numeric($input['email_or_phone'])) {
            $emailValidation = Validator::make($input, [
                'email_or_phone' => 'email',
            ], ['email' => trans('api/messages.error.EMAIL_FORMAT')]);
            if ($emailValidation->fails()) {
                return $this->responseHelper->error(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $emailValidation->errors()->first()
                );
            }
        } else {
            $phoneValidation = Validator::make($input, [
                'email_or_phone' => 'numeric|min:10',
            ], ['min' => trans('api/messages.error.PHONE_FORMAT')]);
            if ($phoneValidation->fails()) {
                return $this->responseHelper->error(
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $phoneValidation->errors()->first()
                );
            }
        }

        try {

            $user = $this->userInterface->findUser($input['email_or_phone'], $input['role_id']);

            if (is_null($user)) {
                $recordNotFoundMsg = trans('api/messages.error.NO_DATA_FOUND', ['module' => 'User']);
                return $this->modelNotFound('', $recordNotFoundMsg);
            }


            $this->authInterface->updateNewPassword($user->id, $input['new_password']);
            $apiData =  ['email_or_phone' => $input['email_or_phone']];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.PASSWORD_RESET');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('AuthController/resetPassword', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * logout
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        try {
            auth()->user()->currentAccessToken()->delete();
            $removeToken = !empty($input) && !empty($loggedInUserId) ? $this->authInterface->logout($input, $loggedInUserId) : '';
            
            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.LOGOUT_SUCCESS');
            return $this->responseHelper->success($apiStatus, $apiMessage);
        } catch (Exception $ex) {
            addErrorLog('AuthController/logout', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * changePassword
     **
     * @param  mixed $request
     * @return JsonResponse
     */
    public function changePassword(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'old_password' => 'required',
            'new_password' => 'required|min:8|different:old_password',
            'confirm_password' => 'required|same:new_password',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $password = auth()->user()->password;

            if (!Hash::check($input['old_password'], $password)) {
                return $this->responseHelper->error(
                    Response::HTTP_UNAUTHORIZED,
                    trans('api/messages.error.PASSWORD_NOT_MATCHED')
                );
            }
            $this->authInterface->updateNewPassword($loggedInUserId, $input['new_password']);
            $apiData =  [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.PASSWORD_CHANGED');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('AuthController/changePassword', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * socialLoginFromMobile
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function socialLoginFromMobile(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            // 'name' => 'required',
            'social_type' => 'required',
            'token' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }
        try {
            $name = $input['name'] ?? '';
            $email = $input['email'] ?? '';
            $socialType = $input['social_type'];
            $token = $input['token'];

            $deviceData = [
                'platform' => $input['platform'] ?? '',
                'device_token' => $input['device_token'] ?? '',
            ];

            $isUserExists = $this->authInterface->checkUserExistByEmailOrToken($email, $token);
            if (is_null($isUserExists)) {
                /*$validator = Validator::make($input, [
                    'name' => 'required',
                ]);

                if ($validator->fails()) {
                    return $this->responseHelper->error(
                        Response::HTTP_UNPROCESSABLE_ENTITY,
                        $validator->errors()->first()
                    );
                }*/

                $userInformation = [
                    'full_name' => $name,
                    'phone_number' => null,
                    'email' => !empty($email) ? $email : null,
                    'country' => null,
                    'city' => null,
                    'zip_code' => null,
                    'password' => 'social@123',
                    'social_account_type' => $socialType,
                    'social_account_token' => $token,
                    'username' => $name,
                ];
                $user = $this->authInterface->registerPatient($userInformation, $deviceData);
            } else {
                $userId = $isUserExists->id;
                $user = $this->authInterface->getPatientInformation($userId, $deviceData);
            }

            $token = $user->createToken($user->full_name)->plainTextToken;
            $apiData =  $this->transformUser($user, $token);
            $apiStatus = Response::HTTP_OK;
            $apiMessage =  !is_null($isUserExists) ? trans('api/messages.success.LOGIN_SUCCESS')
                : trans('api/messages.success.REGISTRATION_SUCCESS', ['user_type' => 'Patient']);
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('AuthController/socialLoginFromMobile', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
