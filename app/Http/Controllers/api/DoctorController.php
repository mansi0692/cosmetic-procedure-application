<?php

namespace App\Http\Controllers\api;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\DoctorInterface;
use App\Models\DoctorLocation;
use App\Models\User;
use App\Services\EmailNotificationService;
use Exception;
use App\Traits\RestExceptionHandlerTrait;
use stdClass;
use App\Transformations\DoctorTransformable;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Pawlox\VideoThumbnail\Facade\VideoThumbnail;
use Symfony\Component\Console\Input\Input;

class DoctorController extends Controller

{

    use RestExceptionHandlerTrait, DoctorTransformable;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $doctorInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $doctorInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, DoctorInterface $doctorInterface)
    {
        $this->responseHelper = $responseHelper;
        $this->doctorInterface = $doctorInterface;
    }

    /**
     * searchDoctorProfile
     *
     * @param  mixed $responseHelper
     * @param  Request $searchDoctorProfile
     * @return JsonResponse
     */
    public function searchDoctorProfile(Request $request): JsonResponse
    {
        try {
            $input = $request->toArray();
            $currentPage = $request->current_page;
            $perPage = $request->per_page;
            $firstPageLimit = 9;

            if ($currentPage == 1) {
                $offset = 0;
                $perPage = 9;
            } else {
                $offset = $firstPageLimit + ($currentPage - 2) * $perPage;
            }

            if (!$request->doctor_search) {
                $validator = Validator::make($input, [
                    'first_name' => 'required_without:last_name',
                    'last_name' => 'required_without:first_name',
                    'npi_number' => 'integer'
                ]);

                if ($validator->fails()) {
                    return $this->responseHelper->error(
                        Response::HTTP_UNPROCESSABLE_ENTITY,
                        $validator->errors()->first()
                    );
                }
            }

            $limit = $perPage;
            $currentPage = $currentPage;
            $firstName = $request->first_name;
            $lastName = $request->last_name;
            $state = $request->state;
            $npiNumber = !isset($npiNumber) ? $request->npi_number : null;
            $doctorSearch = $request->doctor_search;
            $doctors = $this->doctorInterface->searchDoctorProfile(
                $offset,
                $limit,
                $currentPage,
                $firstName,
                $lastName,
                $state,
                $npiNumber,
                $doctorSearch
            );

            if ($doctorSearch) {
                $apiData =  $doctors;
                $apiStatus = Response::HTTP_OK;
                $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_DOCTOR_LISTING')
                    : trans('api/messages.success.MESSAGE_NO_DOCTOR_FOUND');
                return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
            }
            $apiData =  $doctors;
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_DOCTOR_LISTING')
                : trans('api/messages.success.MESSAGE_NO_DOCTOR_FOUND');
            addLog('Search doctor profile');
            return $this->responseHelper->successWithPagination($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/searchDoctorProfile', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * findDoctorWithNpiNumber
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function findDoctorWithNpiNumber(Request $request): JsonResponse
    {
        try {
            $doctors = $this->doctorInterface->findDoctorWithNpiNumber($request->npi_number);
            $apiData = isset($doctors) ? $doctors : [];
            $apiStatus = (!empty($apiData)) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_DOCTOR_PROFILE_FOUND')
                : trans('api/messages.success.MESSAGE_NO_DOCTOR_PROFILE_FOUND');
            addLog('Find doctor with npi number');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/findDoctorWithNpiNumber', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * get doctor location
     *
     * @param  mixed $responseHelper
     * @param  Request $request
     * @return JsonResponse
     */
    public function getDoctorLocation(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $doctorLocationData = $this->doctorInterface->getDoctorLocation($input);
            $apiData = $this->transformDoctorLocation($doctorLocationData);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_DOCTOR_LOCATION_DATA_FETCH')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get doctor location');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/getDoctorLocation', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * add doctor location
     *
     * @param  mixed $responseHelper
     * @param  Request $doctorLocationData
     * @return JsonResponse
     */
    public function addDoctorLocation(Request $doctorLocationData): JsonResponse
    {
        $doctorLocationDataArray = $doctorLocationData->toArray();

        $validator = Validator::make($doctorLocationDataArray, [
            'address' => 'required',
            'state_id' => 'integer',
            'zipcode' => 'required|regex:/^(?:(\d{5})(?:[ \-](\d{4}))?)$/i',
            'city_id' => 'required|integer',
            'email' => ['required', function ($attribute, $value, $fail) {
                $data = DoctorLocation::where('location_email', $value)->get()->toArray();

                if (!empty($data)) {

                    array_walk($data, function ($value) use ($fail) {

                        if ($value['doctor_id'] != auth()->user()->id) {
                            $fail('Email already exist');
                        }
                    });
                }
            }],
            'latitude' => 'required',
            'longitude' => 'required',
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;

            $data = [
                'user_id' => $loggedInUserId,
                'email' => $doctorLocationDataArray['email'],
                'address' => $doctorLocationDataArray['address'],
                'city' => $doctorLocationDataArray['city_id'],
                'suite' => $doctorLocationDataArray['suite'] ?? NULL,
                'state' => $doctorLocationDataArray['state_id'] ?? NULL,
                'zipcode' => $doctorLocationDataArray['zipcode'],
                'latitude' => $doctorLocationDataArray['latitude'],
                'longitude' => $doctorLocationDataArray['longitude'],
                'office_phone_number' => $doctorLocationDataArray['office_phone_number'] ?? Null,
                'office_fax' => $doctorLocationDataArray['office_fax'] ?? Null,
                'ext' => $doctorLocationDataArray['ext'] ?? Null,
            ];

            $saveDoctorLocationData = $this->doctorInterface->saveDoctorLocationData($data, $loggedInUserId);
            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId);
            $apiData =  $this->transformDoctorInformation($profileInformation);

            $apiStatus = ($saveDoctorLocationData) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($saveDoctorLocationData) ? trans('api/messages.success.MESSAGE_DOCTOR_LOCATION_DATA_SAVE')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Add doctor location');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/addDoctorLocation', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * edit doctor location
     *
     * @param  mixed $responseHelper
     * @param  Request $doctorLocationData
     * @return JsonResponse
     */
    public function updateDoctorLocation(Request $doctorLocationData): JsonResponse
    {
        $doctorLocationDataArray = $doctorLocationData->toArray();

        $validator = Validator::make($doctorLocationDataArray, [
            'address' => 'required',
            'state_id' => 'integer',
            'zipcode' => 'required|regex:/^(?:(\d{5})(?:[ \-](\d{4}))?)$/i',
            'city_id' => 'required|integer',
            'email' => ['required', function ($attribute, $value, $fail) {
                $data = DoctorLocation::where('location_email', $value)->get()->toArray();

                if (!empty($data)) {

                    array_walk($data, function ($value) use ($fail) {

                        if ($value['doctor_id'] != auth()->user()->id) {
                            $fail('Email already exist');
                        }
                    });
                }
            }],
            'latitude' => 'required',
            'longitude' => 'required',
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;

            $data = [
                'address' => $doctorLocationDataArray['address'],
                'city_id' => $doctorLocationDataArray['city_id'],
                'location_email' => $doctorLocationDataArray['email'],
                'suite' => $doctorLocationDataArray['suite'] ?? NULL,
                'state_id' => $doctorLocationDataArray['state_id'] ?? NULL,
                'zipcode' => $doctorLocationDataArray['zipcode'],
                'latitude' => $doctorLocationDataArray['latitude'],
                'longitude' => $doctorLocationDataArray['longitude'],
                'office_phone_number' => $doctorLocationDataArray['office_phone_number'] ?? Null,
                'office_fax' => $doctorLocationDataArray['office_fax'] ?? Null,
                'ext' => $doctorLocationDataArray['ext'] ?? Null,
            ];

            $updateDoctorLocationData = $this->doctorInterface->updateDoctorLocationData($data, $doctorLocationDataArray['id']);
            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId);
            $apiData =  $this->transformDoctorInformation($profileInformation);
            $apiStatus = ($updateDoctorLocationData) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($updateDoctorLocationData) ? trans('api/messages.success.MESSAGE_DOCTOR_LOCATION_DATA_UPDATE')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Update doctor location');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/updateDoctorLocation', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * registerDoctor
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function registerDoctor(Request $request): JsonResponse
    {
        $input = $request->toArray();

        if (isset($input['npi_number'])) {
            $validator = Validator::make($input, [
                // 'medical_school' => 'required',
                // 'grad_year' => 'required|integer',
                'birth_date' => 'required',
                'state' => 'required',
                // 'office_state' => 'required',
                'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
                'password' => 'required',
                'npi_number' => 'unique:doctor_information,npi_number',
            ]);
        } else {
            $validator = Validator::make($input, [
                'first_name' => 'required',
                'last_name' => 'required',
                'occupation' => 'required',
                'speciality' => 'required',
                'street_address' => 'required',
                // 'suite' => 'required',
                'zip_code' => 'required|regex:/^(?:(\d{5})(?:[ \-](\d{4}))?)$/i',
                'city' => 'required',
                // 'office_city' => 'required',
                // 'medical_school' => 'required',
                // 'grad_year' => 'required|integer|digits:4|date_format:Y|before:today',
                'birth_date' => 'required',
                'state' => 'required',
                // 'office_state' => 'required',
                'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
                'password' => 'required',
                'npi_number' => 'unique:doctor_information,npi_number',
                'coordinates' => 'required'
            ]);
        }

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $currentCoordinates = explode(",", $input['coordinates']);

        try {
            $doctorParams = new stdClass();
            $doctorParams->first_name = $input['first_name'] ?? '';
            $doctorParams->last_name = $input['last_name'] ?? '';
            $doctorParams->occupation = $input['occupation'] ?? '';
            $doctorParams->speciality = $input['speciality'] ?? '';
            $doctorParams->street_address = $input['street_address'] ?? '';
            $doctorParams->location_email = $input['office_email'] ?? '';
            $doctorParams->zip_code = $input['zip_code'] ?? '';
            $doctorParams->office_city = $input['office_city'] ?? NULL;
            $doctorParams->office_state = $input['office_state'] ?? NULL;
            $doctorParams->office_phone = $input['office_phone'] ?? NULL;
            $doctorParams->office_fax = $input['office_fax'] ?? NULL;
            $doctorParams->npi_number = $input['npi_number'] ?? NULL;
            $doctorParams->state = $input['state'] ?? NULL;
            $doctorParams->city = $input['city'] ?? NULL;
            $doctorParams->birth_date = $input['birth_date'] ?? '';
            $doctorParams->email = $input['email'] ?? '';
            $doctorParams->password = $input['password'] ?? '';
            $doctorParams->latitude = $currentCoordinates[0] ?? '';
            $doctorParams->longitude = $currentCoordinates[1] ?? '';

            $doctors = $this->doctorInterface->registerDoctor($doctorParams);
            $apiData = [];
            $apiStatus = ($doctors) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctors) ? trans('api/messages.success.MESSAGE_DOCTOR_REGISTERED_SUCCESSFULLY')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Register doctor');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/registerDoctor', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getProfileInformation
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function getProfileInformation(Request $request): JsonResponse
    {
        $input = $request->toArray();

        !empty(auth()->user()) ? $loggedInUserId = auth()->user()->id : null;

        $rules = [];

        if (empty($loggedInUserId)) {
            $rules["doctor_id"] = "required";
        }

        $validator = Validator::make($input, $rules);
        $patientId = $input['patient_id'] ?? null;

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId ?? $input['doctor_id'], '', $patientId);
            $apiData =  is_null($profileInformation) ? [] : $this->transformDoctorInformation($profileInformation);
            $apiStatus = Response::HTTP_OK;
            $apiMessage =   is_null($profileInformation) ? trans('api/messages.success.MESSAGE_NO_DOCTOR_FOUND') : trans('api/messages.success.MESSAGE_PROFILE_INFORMATION');
            addLog('Get profile information');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/getProfileInformation', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * saveProfileInformation
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function saveProfileInformation(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make(
            $input,
            [
                'first_name' => 'required',
                'last_name' => 'required',
                'occupation_id' => 'required',
                'specialty_id' => 'required',
                // 'procedure_id' => 'required',
                // 'medical_school' => 'required',
                // 'graduation_year' => 'required',
                'date_of_birth' => 'required',
                'state' => 'required',
                'profile_image' => 'mimes:jpeg,jpg,png|max:10000',
            ],
            [
                'profile_image.mimes' => 'File must be type of jpeg,jpg,png',
                'profile_image.max' => 'Maximum allowed size for profile image is 10MB',
            ]
        );
        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }
        try {
            $loggedInUserId = auth()->user()->id;

            $data = [
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'occupation_id' => $input['occupation_id'],
                'procedure_id' => isset($input['procedure_id']) ? implode(",", $input['procedure_id']) : '',
                'specialty_id' => $input['specialty_id'],
                'symptom_id' => isset($input['symptom_id']) ? implode(",", $input['symptom_id']) : '',
                'medical_school' => $input['medical_school'] ?? '',
                'grad_year' => $input['graduation_year'] ?? '',
                'date_of_birth' => date('Y-m-d', strtotime($input['date_of_birth'])),
                'state_id' => $input['state'],
                'updated_at' => Carbon::now(),
                'updated_by' => $loggedInUserId,
            ];

            if (isset($input['profile_image'])) {
                $imageName =  uploadImage($input['profile_image'], $loggedInUserId, 'profile_image');
                $data['profile_image'] = $imageName;
            }
            $this->doctorInterface->saveProfileInformation($data, $loggedInUserId);
            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId);
            $apiData =  $this->transformDoctorInformation($profileInformation);
            $apiStatus = Response::HTTP_OK;
            $apiMessage =   trans('api/messages.success.SAVE_PROFILE_SUCCESS');
            addLog('Save profile information');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/saveProfileInformation', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * saveOverViewInformation
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function saveOverViewInformation(Request $request): JsonResponse
    {
        if (empty($request->all())) {
            return $this->invalidArgument(Response::HTTP_BAD_REQUEST, 'Bad Request');
        }
        $input = $request->toArray();
        try {
            $loggedInUserId = auth()->user()->id;
            $this->doctorInterface->saveOverviewInformation($input, $loggedInUserId);
            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId);
            $apiData =  $this->transformDoctorInformation($profileInformation);
            $apiStatus = Response::HTTP_OK;
            $apiMessage =   trans('api/messages.success.SAVE_OVERVIEW_SUCCESS');
            addLog('Save over view information');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/saveOverViewInformation', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * deleteProfileInformation
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function deleteProfileInformation(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'id' => 'required',
            'type' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $input = $request->toArray();
        try {
            $loggedInUserId = auth()->user()->id;
            $isDataExist = $this->doctorInterface->checkDataExists($input, $loggedInUserId);
            if (!$isDataExist) {
                return $this->modelNotFound(Response::HTTP_NOT_FOUND, trans('api/messages.error.PROFILE_DATA_NOT_FOUND'));
            }
            $this->doctorInterface->deleteProfileInformation($input, $loggedInUserId);
            $apiData =  [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage =   trans('api/messages.success.PROFILE_DATA_DELETE_SUCCESS');
            addLog('Delete profile information');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/deleteProfileInformation', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * saveSettings
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function saveSettings(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'in_person' => 'required_without:video_consultancy|numeric',
            'video_consultancy' => 'required_without:in_person|numeric',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            // Update in_person & video_consultancy in doctors information
            $data = [];
            if (isset($input['in_person'])) {
                $data['in_person'] = $input['in_person'];
            }
            if (isset($input['video_consultancy'])) {
                $data['video_consultancy'] = $input['video_consultancy'];
            }

            $doctors = $this->doctorInterface->saveProfileInformation($data, $loggedInUserId);

            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId);
            $apiData =  $this->transformDoctorInformation($profileInformation);
            $apiStatus = ($doctors) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctors) ? trans('api/messages.success.MESSAGE_DOCTOR_PROFILE_SETTINGS_UPDATED_SUCCESSFULLY')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Save settings');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/saveSettings', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Upload photos & videos for doctor profile
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function uploadPhotosVideos(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make(
            $input,
            [
                'files.*' => 'mimes:jpeg,jpg,png,flv,mp4,ogx,oga,ogv,ogg,webm,ts,m3u8,3gp,mov,avi,wmv|max:100000',
            ],
            [
                'files.*.mimes' => 'File must be type of jpeg,jpg,png,flv,mp4,ogx,oga,ogv,ogg,webm,ts,m3u8,3gp,mov,avi,wmv',
                'files.*.max' => 'Maximum allowed size for an image or video is 10MB',
            ]
        );

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;

            // Upload photos & videos
            foreach ($input['files'] as $file) {
                $fileName =  uploadImage($file, $loggedInUserId, 'files');

                if (strstr($file->getClientMimeType(), "video/")) {
                    $data[] = [
                        $fileName,
                        'video'
                    ];
                }

                if (strstr($file->getClientMimeType(), "image/")) {
                    $data[] = [
                        $fileName,
                        'image'
                    ];
                }
            }

            $doctors = $this->doctorInterface->uploadPhotosVideos($data, $loggedInUserId);

            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId);
            $apiData =  $this->transformDoctorInformation($profileInformation);
            $apiStatus = ($doctors) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctors) ? trans('api/messages.success.MESSAGE_DOCTOR_MEDIA_UPLOADED_SUCCESSFULLY')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Upload photos videos');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/uploadPhotosVideos', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Update media oder for uploaded photo/video
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function updateMediaOrder(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_media_id' => 'required',
            'new_doctor_media_id' => 'required',
            'order_no' => 'required',
            'new_order_no' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $data = [
                'doctor_media_id' => $input['doctor_media_id'],
                'new_doctor_media_id' => $input['new_doctor_media_id'],
                'order_no' => $input['order_no'],
                'new_order_no' => $input['new_order_no']
            ];

            $doctors = $this->doctorInterface->updateMediaOrder($data, $loggedInUserId);

            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId);
            $apiData =  $this->transformDoctorInformation($profileInformation);
            $apiStatus = ($doctors) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctors) ? trans('api/messages.success.MESSAGE_DOCTOR_MEDIA_UPDATED_SUCCESSFULLY')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Update media order');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/updateMediaOrder', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Upload doctor proof documents
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function uploadProofDoc(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make(
            $input,
            [
                'files.*' => 'mimes:doc,docx,pdf|max:10000',
            ],
            [
                'files.*.mimes' => 'File must be type of doc,docx,pdf',
                'files.*.max' => 'Maximum allowed size for a document is 10MB',
            ]
        );

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;

            // Upload proof docs
            foreach ($input['files'] as $file) {
                $fileName =  uploadImage($file, $loggedInUserId, 'files');

                if (strstr($file->getClientMimeType(), "image/")) {
                    $data[] = [
                        $fileName,
                        'image',
                        'proof'
                    ];
                } else {
                    $data[] = [
                        $fileName,
                        'document',
                        'proof'
                    ];
                }
            }

            $getDocuments = array_map(function ($fileDetailsArray) {
                return $fileDetailsArray[0];
            }, $data);

            $admin = User::where('role_id', 1)->first();
            $doctorData = $this->doctorInterface->uploadPhotosVideos($data, $loggedInUserId);

            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId);

            if ($doctorData) {
                $emailTemplateData = getEmailTemplateData('document_upload_notification');
                $documentNames = implode(", ", $getDocuments);

                $resultMainArray = array("[doctor_name]", "[document_name]");
                $doctorName = " Dr. " . $profileInformation->first_name . " " . $profileInformation->last_name;
                $resultReplaceArray = array($doctorName, $documentNames);
                $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);

                $mailNotification = new EmailNotificationService();
                $content = $emailMiddleBody;
                $subject = $emailTemplateData->template_subject;
                $mailNotification->sendEmail($admin->email, $content, $subject);
            }
            $apiData =  $this->transformDoctorInformation($profileInformation);
            $apiStatus = ($doctorData) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctorData) ? trans('api/messages.success.MESSAGE_DOCTOR_PROOF_UPLOADED_SUCCESSFULLY')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Upload proof doc');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/uploadProofDoc', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * search doctor details by location, speciality,gender,symptom
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function searchDoctors(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $rules = [
            'current_page' => 'required|integer',
            'per_page' => 'required|integer',
            'symptom' => 'array',
            'gender' => 'integer',
            'language' => 'array',
            'in_person' => 'integer',
            'video_consultancy' => 'integer',
            'distance' => 'numeric',
            'video_consultancy' => 'integer',
            'prev_doctor_count' => 'required|integer'
        ];

        if ($input['is_featured_listing'] == 0) {
            $rules["specialty"] = "required|integer";
            //$rules["location"] = "required";
        }

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $currentPage = $input['current_page'];
        $perPage =  $input['per_page'];
        $offset = (($currentPage - 1) * $perPage);
        $limit = $perPage;
        $currentPage = $currentPage;

        try {
            $data = [
                'location' => $input['location'] ?? '',
                'specialty' => $input['specialty'] ?? '',
                'gender' => $input['gender'] ?? '',
                'doctor_name' => $input['doctor_name'] ?? '',
                'symptom' => $input['symptom'] ?? '',
                'language' => $input['language'] ?? '',
                'distance' => $input['distance'] ?? '',
                'in_person' => $input['in_person'] ?? '',
                'video_consultancy' => $input['video_consultancy'] ?? '',
                'current_coordinates' => $input['current_coordinates'] ?? '',
                'is_featured_listing' => $input['is_featured_listing'] ?? '',
                'patient_id' => $input['patient_id'] ?? '',
                'start_date' => !empty($input['start_date']) ? date('Y-m-d', strtotime($input['start_date'])) : '',
                'end_date' => !empty($input['end_date']) ? date('Y-m-d', strtotime($input['end_date'])) : '',
                'is_favorite' => $input['is_favorite'] ?? '',
                'prev_doctor_count' => $input['prev_doctor_count']
            ];

            $apiData = [];
            $categories = [];
            $limit = $perPage;
            $currentPage = $currentPage;
            $doctorData = $this->doctorInterface->getDoctorsList($data, $offset, $limit, $currentPage);

            if (isset($doctorData['categories']) && !empty($doctorData['categories'])) {
                $categories = $doctorData['categories'];
                unset($doctorData['categories']);
            }

            $prevDoctorCount = $doctorData['prev_doctor_count'];
            unset($doctorData['prev_doctor_count']);

            $apiData =  $this->transformDoctorsList($doctorData, $offset, $limit, $currentPage, $tranform = 1);

            !empty($categories) ? ($apiData['categories'] = $categories) : $apiData;
            $apiData['prev_doctor_count'] = $prevDoctorCount;

            $apiStatus = ($doctorData) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctorData) ? trans('api/messages.success.MESSAGE_DOCTOR_LISTING')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Search doctors');
            return $this->responseHelper->successWithPagination($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/searchDoctors', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    public function doctorsAndFeeds(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $rules = [
            'current_page' => 'required|integer',
            'per_page' => 'required|integer'
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $currentPage = $input['current_page'];
        $perPage =  $input['per_page'];
        $offset = (($currentPage - 1) * $perPage);
        $limit = $perPage;
        $currentPage = $currentPage;

        try {
            $data = [
                'location' => $input['location'] ?? ''
            ];

            $apiData = [];
            $categories = [];
            $limit = $perPage;
            $currentPage = $currentPage;

            $doctorsAndFeedsData = $this->doctorInterface->doctorsAndFeeds($data, $offset, $limit, $currentPage);
            $doctorIds = $doctorsAndFeedsData['doctorIds'];
            unset($doctorsAndFeedsData['doctorIds']);
            $feedListData = $this->doctorInterface->getFeedListData(array('doctor_ids' => $doctorIds), $offset, $limit, $currentPage);

            $doctorFeeds = $this->transformFeedList($feedListData, $offset, $limit, $currentPage);
            $doctorFeedsArray = $doctorFeeds->toArray();
            $doctors =  $this->transformDoctorsList($doctorsAndFeedsData, $offset, $limit, $currentPage, $tranform = 1);
            $doctorsArray = $doctors->toArray();

            if ($doctorsArray['total'] >= $doctorFeedsArray['total']) {
                $apiData = $doctors;
                $apiData['feed_information'] = $doctorFeedsArray['data']['feed_information'];
            } else {
                $apiData = $doctorFeeds;
                $apiData['doctor_information'] = $doctorsArray['data']['doctor_information'] ?? [];
            }

            if ($doctorsArray['total'] == 0) {
                $apiData['doctor_information'] = [];
            }

            $apiStatus = ($doctorsAndFeedsData) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctorsAndFeedsData) ? trans('api/messages.success.MESSAGE_DOCTOR_LISTING') : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Doctors and feeds');
            return $this->responseHelper->successWithPagination($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/doctorsAndFeeds', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Update media description for uploaded photo/video
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function updateMediaDescription(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_media_id' => 'required',
            'description' => 'max:256',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $data = [
                'doctor_media_id' => $input['doctor_media_id'],
                'description' => $input['description'] ?? '',
            ];

            $doctorMedia = $this->doctorInterface->updateMediaDescription($data, $loggedInUserId);

            $profileInformation = $this->doctorInterface->getDoctorProfileInformation($loggedInUserId);
            $apiData =  $this->transformDoctorInformation($profileInformation);
            $apiStatus = ($doctorMedia) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctorMedia) ? trans('api/messages.success.MESSAGE_DOCTOR_MEDIA_UPDATED_SUCCESSFULLY')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Update media description');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/updateMediaDescription', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get featured doctor list
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function featuredDoctorList(Request $request): JsonResponse
    {
        $input = $request->toArray();

        try {
            $featuredDoctorsList = $this->doctorInterface->featuredDoctorList($input);
            $apiData =  $this->transformDoctorsList($featuredDoctorsList);

            $apiStatus = ($featuredDoctorsList) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($featuredDoctorsList) ? trans('api/messages.success.MESSAGE_DOCTOR_LISTING')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Featured doctor list');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/featuredDoctorList', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get top specialities doctor list
     **
     * @return JsonResponse
     */
    public function topSpecialitiesDoctorList(): JsonResponse
    {
        try {
            $doctors = $this->doctorInterface->topSpecialitiesDoctorList();

            $apiStatus = ($doctors) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctors) ? trans('api/messages.success.MESSAGE_DOCTOR_LISTING')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Top specialities doctor list');
            return $this->responseHelper->success($apiStatus, $apiMessage, $doctors);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/topSpecialitiesDoctorList', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    public function topDoctorList(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $rules = [
            'specialty' => 'required|integer',
            'current_page' => 'required|integer',
            'per_page' => 'required|integer'
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $currentPage = $input['current_page'];
        $limit =  $input['per_page'];
        $offset = (($currentPage - 1) * $limit);

        try {
            $data = [
                'specialty' => $input['specialty'] ?? ''
            ];

            $apiData = [];
            $doctorData = $this->doctorInterface->getTopDoctorsList($data, $offset, $limit, $currentPage);

            $apiData =  $this->transformDoctorsList($doctorData, $offset, $limit, $currentPage, $tranform = 1);
            $apiStatus = ($doctorData) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($doctorData) ? trans('api/messages.success.MESSAGE_DOCTOR_LISTING') : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Top doctor list');
            return $this->responseHelper->successWithPagination($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/topDoctorList', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Compare Doctors
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function compareDoctor(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'doctor_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $compareDoctor = $this->doctorInterface->compareDoctor($input['doctor_id']);
            $apiData =  $this->transformDoctorCompareData($compareDoctor);

            $apiStatus = ($compareDoctor) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($compareDoctor) ? trans('api/messages.success.MESSAGE_DOCTOR_LISTING')
                : trans('api/messages.error.MESSAGE_NO_DATA_FOUND');
            addLog('Compare doctor');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/compareDoctor', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Upload registration document
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function uploadRegistrationDoc(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            "file" => "required|mimes:pdf,doc|max:250000",
        ]);
        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $loggedInUserId = auth()->user()->id;
            $fileName =  uploadImage($input['file'], $loggedInUserId, 'files');

            $uploadRegistrationForm = $this->doctorInterface->uploadRegistrationDoc($loggedInUserId, $fileName);
            $apiData =  $uploadRegistrationForm->toArray();
            $apiStatus = ($uploadRegistrationForm) ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
            $apiMessage = ($uploadRegistrationForm) ? trans('api/messages.success.MESSAGE_DOCTOR_REGISTRATION_FORM_UPLOAD')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Upload registration doc');
            return $this->responseHelper->success($apiStatus, $apiMessage, []);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/uploadRegistrationDoc', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get appoinment slots
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function getAppoinmentSlot(Request $request): JsonResponse
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'doctor_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $appoinmentSlot = $this->doctorInterface->getAppoinmentSlot($input);
            $apiData = $this->transformDoctorWorkTiming($appoinmentSlot);

            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_DOCTOR_TIME_SLOT_LIST')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get appoinment slot');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/getAppoinmentSlot', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Appointment List
     **
     * @author Krishna Shah
     * @param  Request $request
     * @return JsonResponse
     */
    public function getDoctorAppointmentList(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'status' => 'required',
            'per_page' => 'required',
            'current_page' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $currentPage = $input['current_page'];
        $perPage =  $input['per_page'];

        $offset = (($currentPage - 1) * $perPage) - 1;
        $limit = $perPage;
        try {
            $data = [
                'status' => $input['status'] ?? '',
                'search_name' => $input['search_name'] ?? ''
            ];

            $apiData = [];

            $appointmentData = $this->doctorInterface->getDoctorsAppointmentList($data, $offset, $limit, $currentPage);

            $apiData =  $this->transformAppointmentList($appointmentData, $offset, $limit, $currentPage);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (!empty($apiData)) ? trans('api/messages.success.MESSAGE_APPOINTMENT_LISTING')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get doctor appointment list');
            return $this->responseHelper->successWithPagination($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/getDoctorAppointmentList', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Appointment Details
     **
     * @author Krishna Shah
     * @param  Request $request
     * @return JsonResponse
     */
    public function getDoctorAppointmentDetail(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'appointment_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $data = [
                'appointment_id' => $input['appointment_id'] ?? '',
            ];

            $apiData = [];

            $appointmentInfo = $this->doctorInterface->getDoctorsAppointmentList($data);

            $apiData =  $this->transformAppointmentDetails($appointmentInfo);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_APPOINTMENT_DETAILS')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get doctor appointment detail');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/getDoctorAppointmentDetail', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get doctor appointment booking form
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function getDoctorAppointmentBookingForm(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'doctor_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $doctorRegistrationForm = $this->doctorInterface->getDoctorAppointmentBookingForm($input['doctor_id']);
            $apiData = $doctorRegistrationForm;
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_DOCTOR_BOOKING_FORM')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get doctor appointment booking form');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/getDoctorAppointmentBookingForm', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get doctor appointment action
     * Doctor dashboard
     **
     * @param  Request $request
     * @return JsonResponse
     */

    public function doctorAppointmentAction(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;


        $rules = [
            'status' => 'required|in:cancelled,confirmed',
            "appointment_id" => "required",
            "declined_reason" => "required_if:status,==,cancelled",
            "consultancy_type" => "required_if:status,==,confirmed|in:video_consultancy,in_person",
        ];

        $validator = Validator::make($input, $rules);

        $validator->sometimes('consultancy_url', 'required', function ($input) {
            return $input->status == 'confirmed' && $input->consultancy_type == 'video_consultancy';
        });

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $appoinmentData = $this->doctorInterface->doctorAppointmentAction($input, $loggedInUserId);
            if (!empty($appoinmentData['subject'])  && !empty($appoinmentData['content']) && !empty($appoinmentData['email'])) {
                $content = $appoinmentData['content'];
                $subject = $appoinmentData['subject'];
                $email = $appoinmentData['email'];
                $mailNotification = new EmailNotificationService();
                $mailNotification->sendEmail($email, $content, $subject);
            }

            $apiData = [];

            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($appoinmentData) ? trans('api/messages.success.MESSAGE_PATIENT_RESCHEDULE_ACCEPT')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Doctor appointment action');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/doctorAppointmentAction', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Get doctor and patient joint appointment action
     **
     * @param  Request $request
     * @return JsonResponse
     */

    public function joinAppointmentAction(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $rules = [
            "appointment_id" => "required"
        ];
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $appoinmentData = $this->doctorInterface->joinAppointmentAction($input, $loggedInUserId);
            $apiData = [];

            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($appoinmentData) ? trans('api/messages.success.MESSAGE_PATIENT_RESCHEDULE_ACCEPT')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Join appointment action');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/joinAppointmentAction', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * doctorDashboard
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function doctorDashboard(Request $request): JsonResponse
    {
        $loggedInUserId = auth()->user()->id;

        try {
            $doctorDashboard = $this->doctorInterface->doctorDashboard($loggedInUserId);
            $apiData = $doctorDashboard;
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_DOCTOR_BOOKING_FORM')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Doctor dashboard');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/doctorDashboard', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Add News Feed action
     **
     * @param  Request $request
     * @return JsonResponse
     */

    public function createFeed(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $rules = [
            "title" => "required",
            "description" => "required",
            "file" => "mimes:jpeg,png,jpg,gif,svg,mp4,mov,ogg,qt",
        ];
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            if (isset($input['file'])) {

                $imageName =  uploadImage($input['file'], $loggedInUserId, 'file');
                $extension = strtolower(pathinfo($imageName, PATHINFO_EXTENSION));

                $fileType = '';
                $posterImage = '';

                if (in_array($extension, ['jpeg', 'png', 'jpg', 'svg'])) {
                    $fileType = 'image';
                } else {
                    $fileType = 'video';
                    $date = date('Y_m_d_H_i_s');
                    $file = pathinfo($input['file']->getClientOriginalName(), PATHINFO_FILENAME);
                    $posterImage =  $file . "_" . $date . '.jpg';

                    if (config('app.env') == 'local' || config('app.env') == 'staging') {
                        $thumbnailImage = VideoThumbnail::createThumbnail(
                            public_path('storage/uploads/' . $imageName),
                            public_path('storage/uploads/'),
                            $posterImage,
                            2,
                            1920,
                            1080
                        );
                    } else {
                        $folderPath = config('constants.get_file_path') . $loggedInUserId . '/' . $imageName;

                        $thumbnailImage = VideoThumbnail::createThumbnail(
                            $folderPath,
                            public_path('storage/uploads/'),
                            $posterImage,
                            2,
                            1920,
                            1080
                        );

                        $temp = asset('storage/uploads/' . $posterImage);
                        $folderPath = config('constants.s3.uploadUrl') . $loggedInUserId . '/' . $posterImage;
                        Storage::disk('s3')->put($folderPath, file_get_contents($temp), 'public');
                        unlink(public_path('storage/uploads/') . $posterImage);
                    }
                }

                $input['file'] = $imageName;
            }

            $feedData = $this->doctorInterface->saveFeed($input, $posterImage, $loggedInUserId);

            $apiData = !empty($feedData) ? $feedData->toArray() : [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($feedData) ? trans('api/messages.success.MESSAGE_FEED_ACTION_SUCCESSFULLY', ['type' => 'created'])
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Create feed');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/createFeed', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * updateFeed
     *
     * @param  mixed $request
     * @return JsonResponse
     */
    public function updateFeed(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $rules = [
            "title" => "required",
            "description" => "required",
            "file" => "mimes:jpeg,png,jpg,gif,svg,mp4,mov,ogg,qt",
            'id' => 'required'
        ];
        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $isFeedAvailable = $this->doctorInterface->findFeedFromId($input['id'], $loggedInUserId);
            $apiData = [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage  = '';
            $posterImage = '';

            if ($isFeedAvailable) {

                if (isset($input['file'])) {
                    $imageName =  uploadImage($input['file'], $loggedInUserId, 'file');

                    $extension = strtolower(pathinfo($imageName, PATHINFO_EXTENSION));
                    $fileType = '';

                    if (in_array($extension, ['jpeg', 'png', 'jpg'])) {
                        $fileType = 'image';
                    } else {
                        $fileType = 'video';
                        $date = date('Y_m_d_H_i_s');
                        $file = pathinfo($input['file']->getClientOriginalName(), PATHINFO_FILENAME);
                        $posterImage =  $file . "_" . $date . '.jpg';

                        $thumbnailImage = VideoThumbnail::createThumbnail(
                            public_path('storage/uploads/' . $imageName),
                            public_path('storage/uploads/'),
                            $posterImage,
                            2,
                            1920,
                            1080
                        );
                    }
                    $input['file'] = $imageName;
                }

                $feedData = $this->doctorInterface->updateFeed($input, $posterImage, $loggedInUserId);

                $apiData = !empty($feedData) ? $feedData->toArray() : [];
                $apiMessage = ($feedData) ? trans('api/messages.success.MESSAGE_FEED_ACTION_SUCCESSFULLY', ['type' => 'updated'])
                    : trans('api/messages.error.THERE_IS_SOME_ERROR');
            } else {
                $apiMessage = trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            }
            addLog('Update feed');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/updateFeed', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * deleteFeed
     *
     * @param  mixed $id
     * @return JsonResponse
     */
    public function deleteFeed($id): JsonResponse
    {

        $loggedInUserId = auth()->user()->id;

        try {
            $isFeedAvailable = $this->doctorInterface->findFeedFromId($id, $loggedInUserId);
            $apiData = [];
            $apiStatus = Response::HTTP_OK;
            if ($isFeedAvailable) {
                $feedData = $this->doctorInterface->deleteFeed($id, $loggedInUserId);

                $apiMessage = ($feedData) ? trans('api/messages.success.MESSAGE_FEED_ACTION_SUCCESSFULLY', ['type' => 'deleted'])
                    : trans('api/messages.error.THERE_IS_SOME_ERROR');
            } else {
                $apiMessage = trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            }
            addLog('Delete feed');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/deleteFeed', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Feed List
     **
     * @author Krishna Shah
     * @param  Request $request
     * @return JsonResponse
     */
    public function getFeedListData(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'per_page' => 'required',
            'current_page' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $currentPage = $input['current_page'];
        $perPage =  $input['per_page'];
        $offset = (($currentPage - 1) * $perPage);
        $limit = $perPage;
        $currentPage = $currentPage;

        try {

            $apiData = [];

            $feedData = $this->doctorInterface->getFeedListData($input, $offset, $limit, $currentPage);

            $apiData =  $this->transformFeedList($feedData, $offset, $limit, $currentPage);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = (count($apiData) > 0) ? trans('api/messages.success.MESSAGE_FEED_LISTING')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get feed list data');
            return $this->responseHelper->successWithPagination($apiStatus, $apiMessage, $apiData, [], false);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/getFeedListData', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Feed details
     **
     * @author Krishna Shah
     * @param  Request $request
     * @return JsonResponse
     */
    public function getFeedListDetails($id): JsonResponse
    {
        try {
            $apiData = [];
            $input = ['feed_id' => $id];
            $feedData = $this->doctorInterface->getFeedListData($input);

            $apiData =  $this->transformFeedDetails($feedData);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_FEED_DETAILS')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get feed list details');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/getFeedListDetails', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * getReviewList
     *
     * @param  mixed $request
     * @return void
     */
    public function getReviewList(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'doctor_id' => 'required',
            'per_page' => 'required',
            'current_page' => 'required',
            'most_relevant' => ['boolean', function ($attribute, $value, $fail) use ($input) {
                if ($input['most_relevant'] == 1 && ($input['new_added'] != 0 || $input['old_review'] != 0)) {
                    $fail('The ' . $attribute . ' is invalid.');
                }
            },],
            'new_added' => ['boolean', function ($attribute, $value, $fail) use ($input) {
                if ($input['new_added'] == 1 && ($input['most_relevant'] != 0 || $input['old_review'] != 0)) {
                    $fail('The ' . $attribute . ' is invalid.');
                }
            },],
            'old_review' => ['boolean', function ($attribute, $value, $fail) use ($input) {
                if ($input['old_review'] == 1 && ($input['new_added'] != 0 || $input['most_relevant'] != 0)) {
                    $fail('The ' . $attribute . ' is invalid.');
                }
            },],
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        $currentPage = $input['current_page'];
        $perPage =  $input['per_page'];
        $offset = (($currentPage - 1) * $perPage);
        $limit = $perPage;

        try {
            $apiData = [];
            $reviewData = $this->doctorInterface->getReviewList($input, $offset, $limit);

            $apiData =  $this->transformReviewList($reviewData, $offset, $limit, $currentPage);
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($apiData) ? trans('api/messages.success.MESSAGE_REVIEW_LIST')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Get review list');
            return $this->responseHelper->successWithPagination($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/getReviewList', $ex->getMessage());
            return $this->internalServerError();
        }
    }
    /**
     * Feed Like-Unlike
     **
     * @author Krishna Shah
     * @param  Request $request
     * @return JsonResponse
     */
    public function addFeedLike(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $validator = Validator::make($input, [
            'feed_id' => 'required',
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $feedData = $this->doctorInterface->addFeedLike($input, $loggedInUserId);

            $apiData = [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($feedData) ? trans('api/messages.success.MESSAGE_FEED_LIKE_DETAILS')
                : trans('api/messages.success.MESSAGE_NO_DATA_FOUND');
            addLog('Add feed like');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/addFeedLike', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Feed Comment
     **
     * @author Krishna Shah
     * @param  Request $request
     * @return JsonResponse
     */
    public function addFeedComment(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $validator = Validator::make($input, [
            'feed_id' => 'required',
            'description' => 'required|max:500'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $apiData = [];
            $feedData = $this->doctorInterface->addFeedComment($input, $loggedInUserId);

            $apiData =  [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = ($feedData) ? trans('api/messages.success.MESSAGE_FEED_COMMENT_DETAILS')
                : trans('api/messages.error.THERE_IS_SOME_ERROR');
            addLog('Add feed comment');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/addFeedComment', $ex->getMessage());
            return $this->internalServerError();
        }
    }

    /**
     * Feed Follows
     **
     * @author Krishna Shah
     * @param  Request $request
     * @return JsonResponse
     */
    public function followDoctor(Request $request): JsonResponse
    {
        $input = $request->toArray();
        $loggedInUserId = auth()->user()->id;

        $validator = Validator::make($input, [
            'doctor_id' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->error(
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $validator->errors()->first()
            );
        }

        try {
            $follow = $this->doctorInterface->followDoctor($input, $loggedInUserId);
            $apiData =  [];
            $apiStatus = Response::HTTP_OK;
            $apiMessage = trans('api/messages.success.MESSAGE_DOCTOR_FOLLOW', ['type' => $follow]);
            addLog('Follow doctor');
            return $this->responseHelper->success($apiStatus, $apiMessage, $apiData);
        } catch (Exception $ex) {
            addErrorLog('DoctorController/followDoctor', $ex->getMessage());
            return $this->internalServerError();
        }
    }
}
