<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MasterSubCategory;
use App\Models\MasterSubCategoryChild;
use Illuminate\Support\Facades\DB;
use AWS;

class ScriptController extends Controller
{

    public function copyData($id, $languageId)
    {
        $subCategory = MasterSubCategory::select(DB::raw('group_concat(id) as master_sub_category_id'))->where('master_category_id', $id)->first()->master_sub_category_id;

        $langCategory = MasterSubCategoryChild::select('id', 'label')->whereIn('master_sub_category_id', explode(',', $subCategory))->where('language_id', $languageId)->get()->toArray();

        foreach ($langCategory as $key => $value) {
            $saveCategory = new MasterSubCategory();
            $saveCategory->master_category_id = $id;
            $saveCategory->language_id = $languageId;
            $saveCategory->label = $value['label'];
            $saveCategory->order_no = $key + 1;
            $saveCategory->save();
        }
    }

    public function sendSms()
    {
        $sms = AWS::createClient('sns');

        $response = $sms->publish([
            'Message' => 'Testing From Local',
            'PhoneNumber' => '+14165677664',
            'MessageAttributes' => [
                'AWS.SNS.SMS.SMSType'  => [
                    'DataType'    => 'String',
                    'StringValue' => 'Transactional',
                ]
            ],
        ]);

        dd($response);
    }
}
