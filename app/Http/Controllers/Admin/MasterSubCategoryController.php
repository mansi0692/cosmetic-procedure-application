<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MasterCategory;
use App\Models\MasterSubCategory;
use App\Models\ModelHasRole;
use App\Models\Role;
use App\Models\User;
use App\Models\Language;
use App\Models\MasterSubCategoryChild;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session as FacadesSession;
use Illuminate\Support\Facades\Validator;

class MasterSubCategoryController extends Controller
{
    /**
     * __construct
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listMasterSubCategory($masterCategoryId, $isOther)
    {
        if ($masterCategoryId == 1 && $isOther == 1) {
            $masterSubCategory = MasterSubCategory::where('master_category_id', $masterCategoryId)
                ->where('is_approved', 0)->where('is_other', 1)->get();
        } else {
            $masterSubCategory = MasterSubCategory::where('master_category_id', $masterCategoryId)
                ->where('is_approved', 1)->get();
        }

        return view('category.list-master-sub-category', compact('masterSubCategory'));
    }

    public function addMasterSubCategory()
    {
        $languages = Language::get(['id', 'title']);
        return view('category.add-master-sub-category', compact('languages'));
    }

    public function saveMasterSubCategory(Request $request, $masterCategoryId, $isOther)
    {
        $input = $request->toArray();
        $category = $input['category'];

        if (in_array($masterCategoryId, [1, 5, 6])) {
            $validator = Validator::make($input, ['sub_category' => 'required'], ['sub_category.required' => ucwords($category) . ' field is required']);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }

            try {
                $masterSubCategory = new MasterSubCategory();
                $masterSubCategory->master_category_id = $masterCategoryId;
                $masterSubCategory->label = $input['sub_category'];
                $masterSubCategory->order_no = MasterSubCategory::where('master_category_id', $masterCategoryId)->max('order_no') + 1;
                $masterSubCategory->save();

                return redirect()->route('list-master-sub-category', ['masterCategoryId' => $masterCategoryId, 'isOther' => $isOther])->with('success', $category . ' saved successfully');
            } catch (\Throwable $th) {
                return redirect()->route('list-master-sub-category', ['masterCategoryId' => $masterCategoryId, 'isOther' => $isOther])->with('error', 'There is some error');
            }
        } else {
            $languages = Language::get(['id', 'title']);

            $messages = array();
            $rules = array();
            foreach ($languages as $language) {
                $rules['sub_category_' . $language->id] = 'required';
                $messages['sub_category_' . $language->id . '.required'] = ucwords($category) . ' (' . $language->title . ')' . ' field is required';
            }

            $validator = Validator::make($input, $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }

            // add english label in master sub category
            $masterSubCategory = new MasterSubCategory();
            $masterSubCategory->master_category_id = $masterCategoryId;
            $masterSubCategory->label = $input['sub_category_1']; // 1 = english
            $masterSubCategory->order_no = MasterSubCategory::where('master_category_id', $masterCategoryId)->max('order_no') + 1;
            $masterSubCategory->save();
            $masterSubCategoryId = $masterSubCategory->id;

            try {
                // add all language label in master sub category child
                foreach ($languages->pluck('id') as $languageId) {
                    $newRow = new MasterSubCategoryChild();
                    $newRow->master_sub_category_id = $masterSubCategoryId;
                    $newRow->language_id = $languageId;
                    $newRow->label = $input['sub_category_' . $languageId];
                    $newRow->save();
                }

                return redirect()->route('list-master-sub-category', ['masterCategoryId' => $masterCategoryId, 'isOther' => $isOther])->with('success', $category . ' saved successfully');
            } catch (\Throwable $th) {
                return redirect()->route('list-master-sub-category', ['masterCategoryId' => $masterCategoryId, 'isOther' => $isOther])->with('error', 'There is some error');
            }
        }
    }

    public function editMasterSubCategory($masterSubCategoryId)
    {
        $languages = Language::get(['id', 'title']);
        $masterSubCategory = MasterSubCategory::find($masterSubCategoryId);
        $masterSubCategoryChild = MasterSubCategoryChild::where('master_sub_category_id', $masterSubCategoryId)->get();

        return view('category.edit-master-sub-category', compact('languages', 'masterSubCategory', 'masterSubCategoryChild'));
    }

    public function updateMasterSubCategory(Request $request, $masterSubCategoryId, $masterCategoryId, $isOther)
    {
        $input = $request->toArray();
        $category = $input['category'];

        if (in_array($masterCategoryId, [1, 5, 6])) {
            $validator = Validator::make($input, ['sub_category' => 'required'], ['sub_category.required' => ucwords($category) . ' field is required']);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }

            try {
                MasterSubCategory::where('id', $masterSubCategoryId)->update(['label' => $input['sub_category']]);

                return redirect()->route('list-master-sub-category', ['masterCategoryId' => $masterCategoryId, 'isOther' => $isOther])->with('success', $category . ' updated successfully');
            } catch (\Throwable $th) {
                return redirect()->route('list-master-sub-category', ['masterCategoryId' => $masterCategoryId, 'isOther' => $isOther])->with('error', 'There is some error');
            }
        } else {
            $languages = Language::get(['id', 'title']);


            $messages = array();
            $rules = array();
            foreach ($languages as $language) {
                $rules['sub_category_' . $language->id] = 'required';
                $messages['sub_category_' . $language->id . '.required'] = ucwords($category) . ' (' . $language->title . ')' . ' field is required';
            }

            $validator = Validator::make($input, $rules, $messages);

            if ($validator->fails()) {
                return redirect()->back()->withInput()->withErrors($validator);
            }

            try {
                // update english label in master sub category so english translation in master_sub_categories and master_sub_category_child stays in sync
                MasterSubCategory::where('id', $masterSubCategoryId)->update(['label' => $input['sub_category_1']]); // 1 = english


                // update all language label in master sub category child
                foreach ($languages->pluck('id') as $languageId) {
                    $row = MasterSubCategoryChild::where('master_sub_category_id', $masterSubCategoryId)->where('language_id', $languageId)->first();

                    if ($row) {
                        $row->update(['label' => $input['sub_category_' . $languageId]]);
                    } else {
                        $newRow = new MasterSubCategoryChild();
                        $newRow->master_sub_category_id = $masterSubCategoryId;
                        $newRow->language_id = $languageId;
                        $newRow->label = $input['sub_category_' . $languageId];
                        $newRow->save();
                    }
                }
                return redirect()->route('list-master-sub-category', ['masterCategoryId' => $masterCategoryId, 'isOther' => $isOther])->with('success', $category . ' updated successfully');
            } catch (\Throwable $th) {
                return redirect()->route('list-master-sub-category', ['masterCategoryId' => $masterCategoryId, 'isOther' => $isOther])->with('error', 'There is some error');
            }
        }
    }

    public function deleteMasterSubCategory($masterSubCategoryId)
    {
        $deleteMasterSubCategory = MasterSubCategory::where('id', $masterSubCategoryId)->first();
        $mainCategory = config('constants.main_categories');
        foreach ($mainCategory as $key => $value) {
            if ($deleteMasterSubCategory->master_category_id == $key) {
                $label = $value;
            }
        }
        $deleteMasterSubCategory->forceDelete();
        return redirect()->back()->with('message', ucfirst($label) . ' deleted successfully');
    }

    public function changeMasterSubCategoryStatus($masterSubCategoryId, $status)
    {
        $masterSubCategoryStatus = MasterSubCategory::find($masterSubCategoryId);
        $mainCategory = config('constants.main_categories');
        foreach ($mainCategory as $key => $value) {
            if ($masterSubCategoryStatus->master_category_id == $key) {
                $label = $value;
            }
        }
        $masterSubCategoryStatus->status = $status == 'active' ? 0 : 1;
        $masterSubCategoryStatus->save();

        return redirect()->back()->with('success', ucfirst($label) . ' status changed successfully');
    }

    public function approveMasterSubCategory($masterSubCategoryId)
    {
        $masterSubCategory = MasterSubCategory::find($masterSubCategoryId);
        $mainCategory = config('constants.main_categories');
        foreach ($mainCategory as $key => $value) {
            if ($masterSubCategory->master_category_id == $key) {
                $label = $value;
            }
        }
        if ($masterSubCategory->is_approved == 1) {
            $masterSubCategory->is_approved = 0;
            $message = $label . " value disapproved successfully";
        } else {
            $masterSubCategory->is_approved = 1;
            $message = $label . " value approved successfully";
        }
        $masterSubCategory->save();
        return redirect()->back()->with('success', $message);
    }
}
