<?php
namespace App\Http\Controllers\Admin;

use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\ContactUsInterface;
use App\Interfaces\CustomMailInterface;
use App\Jobs\CustomMailJob;
use App\Mail\CustomMail;
use App\Models\CustomMailNotification;
use App\Models\DoctorInformation;
use App\Models\PatientInformation;
use App\Models\User;
use Illuminate\Http\Request;
use stdClass;
use Validator;
use App\Traits\RestExceptionHandlerTrait;

class ContactUsController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $contactUsInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $contactUsInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, ContactUsInterface $contactUsInterface)
    {
        $this->middleware('auth');
        $this->responseHelper = $responseHelper;
        $this->contactUsInterface = $contactUsInterface;
    }

    public function list() 
    {
        $contactUs = $this->contactUsInterface->list();
        return view('contact-us.list', compact('contactUs'));
    }

    public  function getContactUs($contactUsId) 
    {
        $contactUs = $this->contactUsInterface->getContactUs($contactUsId);
        return view('contact-us.reply', compact('contactUs'));
    }

    public function sendReply(Request $request, $contactUsId)
    {
        $input = $request->all();

        $validator = Validator::make($input, [
            'email' => 'required',
            'subject' => 'required',
            'emailcontent' => 'required'
        ]);
        
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $dataParams = new stdClass();
        $dataParams->email = $input['email'];
        $dataParams->subject = $input['subject'];
        $dataParams->message = $input['message'];
        $dataParams->emailcontent = $input['emailcontent'];

        $dataArray = $this->contactUsInterface->sendReply($dataParams, $contactUsId);
        return redirect('contact-us-list')->withSuccess(['message' => 'Reply send successfully']);
    }

    public function redirectBack()
    {
        return redirect('contact-us-list');
    }
}