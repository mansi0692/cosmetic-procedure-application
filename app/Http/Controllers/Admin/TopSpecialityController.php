<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TopSpeciality;
use App\Models\DoctorInformation;
use App\Models\Doctor;
use App\Models\MasterSubCategory;

class TopSpecialityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listTopSpeciality()
    {
        $topSpecialities = TopSpeciality::with('specialityData')->get();
        return view('top-speciality.list-top-speciality', compact('topSpecialities'));
    }

    public function addTopSpeciality()
    {
        $specialities = MasterSubCategory::where('master_category_id', 3)->where('is_approved', 1)->orderBy('label')->get();
        return view('top-speciality.add-top-speciality', compact('specialities'));
    }

    public function saveTopSpeciality(Request $request)
    {
        $specialityId = $request->speciality;

        if (TopSpeciality::where('speciality_id', $specialityId)->first()) {
            return redirect()->route('list-top-speciality')->with('success','Top Speciality Already Exists');
        } else {
            $topSpeciality = new TopSpeciality();
            $topSpeciality->speciality_id = $specialityId;
            $topSpeciality->save();
            return redirect()->route('list-top-speciality')->with('success','Top Speciality Created Successfully');
        }
    }

    public function editTopSpeciality($id)
    {
        $topSpeciality = TopSpeciality::with('specialityData')->where('id','=',$id)->first();
        $doctors = DoctorInformation::select('first_name','last_name','profile_image', 'user_id')->where(['specialty_id' => $topSpeciality->speciality_id, 'deleted_by' => null])->get();

        $unregiDocs = Doctor::where('is_registered', 0)->where('is_top_doctor', 1);
        $speciality = MasterSubCategory::find($topSpeciality->speciality_id);
        $specialityName = $speciality ? ($speciality->value ?? $speciality->label ?? '') : '';
        $unregiDocs = $unregiDocs->where('specialties', 'LIKE', "%" . $specialityName . "%")->get();
        $doctors = $doctors->concat($unregiDocs);

        return view('top-speciality.edit-top-speciality', compact(['doctors','topSpeciality']));
    }
    public function updateTopSpeciality($id, Request $request)
    {
        if(!empty($request->doctor_id))
        {
            $input = [];

            if (str_contains($request->doctor_id, 'Z')) { // 'Z' means that doctor is unregistered
                $input['unreg_doctor_id'] = substr($request->doctor_id, 0, -1);
                $input['doctor_id'] = NULL;
            } else {
                $input['doctor_id'] = $request->doctor_id;
                $input['unreg_doctor_id'] = NULL;
            }
            
            TopSpeciality::where('id', '=', $id)->update($input);
        }
        return redirect()->route('list-top-speciality')->with('success','Top Speciality Updated Successfully');
    }
}
