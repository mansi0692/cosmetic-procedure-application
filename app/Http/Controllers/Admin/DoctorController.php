<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\DoctorInterface;
use App\Models\City;
use App\Models\MasterCategory;
use App\Models\MasterSubCategory;
use App\Models\DoctorInformation;
use App\Models\State;
use App\Models\User;
use App\Services\EmailNotificationService;
use Illuminate\Support\Facades\Http;
use App\Traits\RestExceptionHandlerTrait;
use Illuminate\Support\Facades\Mail;
use stdClass;

class DoctorController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $doctorInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $doctorInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, DoctorInterface $doctorInterface, DoctorInformation $doctorInformation)
    {
        $this->middleware('auth');
        $this->responseHelper = $responseHelper;
        $this->doctorInterface = $doctorInterface;
        $this->doctorInformation = $doctorInformation;
    }

    public function listDoctor()
    {
        $doctorData = $this->doctorInterface->listDoctor();
        $doctors = $doctorData['doctors'];
        $doctorLikes = $doctorData['doctorLikes'];
        return view('doctor.list-doctor', compact(['doctors', 'doctorLikes']));
    }

    public function changeStatus($doctorId, $status)
    {
        $doctors = $this->doctorInterface->changeStatus($doctorId, $status);
        return redirect()->back()->with('success', 'Status Changed successfully');
    }

    public function deleteDoctor($doctorId)
    {
        $doctors = $this->doctorInterface->deleteDoctor($doctorId);
        return redirect()->back()->with('success', 'Doctor deleted successfully');
    }

    public function redirectAddDoctor()
    {
        $occuption = MasterSubCategory::where('master_category_id', '=', 2)->get();
        $specialty = MasterSubCategory::where('master_category_id', '=', 3)->get();
        $city = City::get();
        $state = State::get();
        return view('doctor.add-doctor', compact(['occuption', 'specialty', 'city', 'state']));
    }

    public function registerDoctor(Request $request)
    {
        $input = $request->toArray();

        $customMessages = [
            'required' => 'The :password must contain one alphabate and one numeric value'
        ];
        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'occupation' => 'required',
            'speciality' => 'required',
            'street_address' => 'required',
            'location_email' => 'required|email',
            'zip_code' => 'required|numeric',
            'city' => 'required',
            'npi_number' => 'required|numeric',
            'birth_date' => 'required',
            'state' => 'required',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            'password' => 'required|min:8|regex:/^.*(?=.{2,})(?=.*[a-z])(?=.*[0-9]).*$/|',
            'city_of_profession' => 'required',
            'state_of_office' => 'required',
        ], $customMessages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        
        $doctorParams = new stdClass();
        $doctorParams->first_name = $input['first_name'];
        $doctorParams->last_name = $input['last_name'];
        $doctorParams->occupation = $input['occupation'];
        $doctorParams->speciality = $input['speciality'];
        $doctorParams->street_address = $input['street_address'];
        $doctorParams->location_email = $input['location_email'];
        $doctorParams->zip_code = $input['zip_code'];
        $doctorParams->city = $input['city'];
        $doctorParams->office_city = $input['city_of_profession'];
        $doctorParams->office_state = $input['state_of_office'];
        $doctorParams->office_phone = isset($input['office_phone']) ? $input['office_phone'] : null;
        $doctorParams->office_fax = isset($input['office_fax']) ? $input['office_fax'] : null;
        $doctorParams->birth_date = date("Y-m-d", strtotime($input['birth_date']));
        $doctorParams->state = $input['state'];
        $doctorParams->email = $input['email'];
        $doctorParams->password = $input['password'];
        $doctorParams->npi_number = isset($input['npi_number']) ? $input['npi_number'] : null;
        $doctorParams->from_admin = 1;
        $doctorParams->latitude = NULL;
        $doctorParams->longitude = NULL;

        $doctors = $this->doctorInterface->registerDoctor($doctorParams);

        if ($doctors) {
            return redirect('list-doctor')->with('success', 'Doctor information saved successfully');
        }
        return redirect('list-doctor')->with('error', 'There is some error');
    }

    public function editDoctor($doctorId)
    {
        $doctors = $this->doctorInterface->editDoctor($doctorId);
        $doctorData = $doctors['doctorData'];
        $occuption = $doctors['occuption'];
        $specialty = $doctors['specialty'];
        $city = $doctors['cities'];
        $state = $doctors['states'];
        return view('doctor.edit-doctor', compact(['doctorData', 'occuption', 'specialty', 'city', 'state']));
    }

    public function redirectDoctorList()
    {
        return redirect('list-doctor');
    }

    public function updateDoctor($doctorId, Request $request)
    {
        $input = $request->toArray();
        $customMessages = [
            'required' => 'The :password must contain one alphabate and one numeric value'
        ];
        $validator = Validator::make($input, [
            'first_name' => 'required',
            'last_name' => 'required',
            'occupation' => 'required',
            'speciality' => 'required',
            'street_address' => 'required',
            'location_email' => 'required|email',
            'zip_code' => 'required|numeric',
            'city' => 'required',
            'npi_number' => 'required|numeric',
            'birth_date' => 'required',
            'state' => 'required',
            'email' => [
                'required', 'email',
                function ($attribute, $value, $fail) use ($doctorId) {
                    $useremail = User::find($doctorId);
                    if ($value != $useremail->email) {
                        $validateEmail = User::where('email', $value)->whereNull('deleted_at')->first();
                        if (isset($validateEmail)) {
                            $fail('This email is already taken.');
                        }
                    }
                }
            ],
            'password' => 'min:8|regex:/^.*(?=.{2,})(?=.*[a-z])(?=.*[0-9]).*$/|',
            'city_of_profession' => 'required',
            'state_of_office' => 'required',
        ], $customMessages);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $doctorParams = new stdClass();
        $doctorParams->first_name = $input['first_name'];
        $doctorParams->last_name = $input['last_name'];
        $doctorParams->occupation = $input['occupation'];
        $doctorParams->speciality = $input['speciality'];
        $doctorParams->street_address = $input['street_address'];
        $doctorParams->location_email = $input['location_email'];
        $doctorParams->zip_code = $input['zip_code'];
        $doctorParams->city = $input['city'];
        $doctorParams->office_phone = isset($input['office_phone']) ? $input['office_phone'] : null;
        $doctorParams->office_fax = isset($input['office_fax']) ? $input['office_fax'] : null;
        $doctorParams->birth_date = date("Y-m-d", strtotime($input['birth_date']));
        $doctorParams->state = $input['state'];
        $doctorParams->email = $input['email'];
        $doctorParams->password = $input['password'];
        $doctorParams->npi_number = isset($input['npi_number']) ? $input['npi_number'] : null;
        $doctorParams->city_of_profession = isset($input['city_of_profession']) ? $input['city_of_profession'] : null;
        $doctorParams->state_of_office = isset($input['state_of_office']) ? $input['state_of_office'] : null;

        $doctors = $this->doctorInterface->updateDoctor($doctorId, $doctorParams);
        if ($doctors) {
            return redirect('list-doctor')->with('success', 'Doctor information updated successfully');
        }
        return redirect('list-doctor')->with('error', 'There is some error');
    }

    public function viewDoctor($doctorId)
    {
        $doctors = $this->doctorInterface->editDoctor($doctorId);
        $doctorData = $doctors['doctorData'];
        $occuption = $doctors['occuption'];
        $specialty = $doctors['specialty'];
        $city = $doctors['cities'];
        $state = $doctors['states'];
        return view('doctor.view-doctor', compact(['doctorData', 'occuption', 'specialty', 'city', 'state']));
    }

    public function approveDoctor($doctorId)
    {
        $doctors = $this->doctorInterface->approveDoctor($doctorId);
        $doctor = $this->doctorInformation::where('user_id', $doctorId)->first();
        $doctorName = $doctor ? $doctor->first_name . " " . $doctor->last_name : '';

        $emailTemplateData = getEmailTemplateData('doctor_approve_notification');
        $resultMainArray = array("[doctorName]", "[doctorLoginUrl]", "[email]");
        $resultReplaceArray = array($doctorName, env('DOCTOR_LOGIN_URL') ?? '', $doctors->email);
        $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);

        $mailNotification = new EmailNotificationService();
        $content = $emailMiddleBody;
        $subject = $emailTemplateData->template_subject;
        $mailNotification->sendEmail($doctors->email, $content, $subject);

        return redirect()->back()->with('success', 'Doctor approved successfully');
    }

    public function likeDoctor($doctorId)
    {
        $doctors = $this->doctorInterface->likeDoctor($doctorId);
        if ($doctors) {
            return redirect()->back();
        } else {
            return redirect()->back()->with(['error' => 'You cannot feature more than ' . config('constants.doctor_feature_limit') . ' profile']);
        }
    }

    public function doctorDocuments($doctorId)
    {
        $doctorDocumemts = $this->doctorInterface->doctorDocuments($doctorId);
        $docArray = [];
        foreach ($doctorDocumemts['doctor_documemts'] as $data) {
            $tempArray = [];
            $tempArray['doctor_id'] = $data->doctor_id;
            $tempArray['file'] = $data->file;
            $tempArray['file_name'] = explode('.', $data->file)[0];
            $tempArray['type'] = explode('.', $data->file)[1];
            $tempArray['doc_url'] = getImage($data->file, $data->doctor_id);
            array_push($docArray, $tempArray);
        }
        $isDocumentVerified = $doctorDocumemts['is_document_verified'];
        return view('doctor.verify-doc', compact(['docArray', 'isDocumentVerified']));
    }

    public function verifyDoctor($doctorId)
    {
        $doctorDocumemts = $this->doctorInterface->verifyDoctor($doctorId);
        if ($doctorDocumemts) {
            return redirect('list-doctor')->withSuccess('Documents verified succesfully');
        }
        return redirect('list-doctor')->withErrors(trans('messages.errors.THERE_IS_SOME_ERROR'));
    }

    public function listDoctorReview()
    {
        $doctorData = $this->doctorInterface->listDoctor();
        $doctors = $doctorData['doctors'];
        return view('doctor-review.list-doctor', compact(['doctors']));
    }

    public function getDoctorRating($doctorId)
    {
        $doctorRating = $this->doctorInterface->getDoctorRating($doctorId);
        return view('doctor-review.doctor-rating', compact(['doctorRating']));
    }

    public function redirectDoctorRatingList()
    {
        return redirect('list-doctor-review');
    }
}
