<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MasterCategory;
use App\Models\ModelHasRole;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session as FacadesSession;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * __construct
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $category = MasterCategory::paginate(10);
        return view('category.list-category', compact('category'));
    }

    public function addCategory()
    {
        $mainCategory = MasterCategory::groupBy('type')->select('type')->get();
        return view('category.add-category', compact('mainCategory'));
    }

    public function saveCategory(Request $request)
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'main_category' => 'required',
            'sub_category' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $addCategory = new MasterCategory();
        $addCategory->type = $input['main_category'];
        $addCategory->label = $input['sub_category'];
        if ($addCategory->save()) {
            return redirect('list-category')->withSuccess(['message' => trans('messages.errors.CATEGORY_SAVED_SUCCESSFULLY')]);
        }
        return redirect('list-category')->withErrors(['message' => trans('messages.errors.THERE_IS_SOME_ERROR')]);
    }

    public function updateCategory(Request $request, $categoryId) 
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'main_category' => 'required',
            'sub_category' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $addCategory = MasterCategory::findOrFail($categoryId);
        $addCategory->type = $input['main_category'];
        $addCategory->label = $input['sub_category'];
        
        if ($addCategory->save()) {
            return redirect('list-category')->withSuccess(['message' => trans('messages.errors.CATEGORY_UPDATED_SUCCESSFULLY')]);
        }
        return redirect('list-category')->withErrors(['message' => trans('messages.errors.THERE_IS_SOME_ERROR')]);
    }

    public function editCategory($categoryId) 
    {
        $category = MasterCategory::find($categoryId);
        $mainCategory = MasterCategory::groupBy('type')->select('type')->get();
        
        return view('category.edit-category', compact('mainCategory', 'category'));
    }

    public function changeStatus($categoryId, $status)
    {
        $categoryStatus = MasterCategory::find($categoryId);
        if ($status == 'active') {
            $categoryStatus->status = 0;
        } else {
            $categoryStatus->status = 1;
        }
        $categoryStatus->save();
        return redirect('list-category');
    }

    public function deleteCategory($categoryId)
    {
        $deleteCategory = MasterCategory::where('id', $categoryId)->first();
        $deleteCategory->delete();
        $deleteCategory->save();
        return redirect('list-category');
    }

    public function redirectList() 
    {
        return redirect('list-category');
    }
}
