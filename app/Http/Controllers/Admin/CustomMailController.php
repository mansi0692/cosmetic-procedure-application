<?php
namespace App\Http\Controllers\Admin;

use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\CustomMailInterface;
use App\Jobs\CustomMailJob;
use App\Mail\CustomMail;
use App\Models\CustomMailNotification;
use App\Models\DoctorInformation;
use App\Models\PatientInformation;
use App\Models\User;
use Illuminate\Http\Request;
use stdClass;
use Validator;
use App\Traits\RestExceptionHandlerTrait;

class CustomMailController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $customMailInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $customMailInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, CustomMailInterface $customMailInterface)
    {
        $this->middleware('auth');
        $this->responseHelper = $responseHelper;
        $this->customMailInterface = $customMailInterface;
    }

    public function listCustomMail()
    {
        $dataArray = $this->customMailInterface->listCustomMail();
        return view('custom-mail-notification.list',compact('dataArray'));
    }

    public function getDoctorPatient() 
    {
        $dataArray = $this->customMailInterface->getDoctorPatient();
        return view('custom-mail-notification.send-mail',compact('dataArray'));
    }

    public function returnList()
    {
        return redirect('custom-mail-notification-list');
    }

    public function sendCustomMail(Request $request)
    {
        $input = $request->all();
       
        if ($input['send_to'] == 'specific_user') {
            $validator = Validator::make($input, [
                'send_to' => 'required',
                'email' => 'required',
                'subject' => 'required',
                'emailcontent' => 'required'
            ]);
        } else {
            $validator = Validator::make($input, [
                'send_to' => 'required',
                'subject' => 'required',
                'emailcontent' => 'required',
            ]);
        }
       
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $dataParams = new stdClass();
        $dataParams->send_to = $input['send_to'];
        $dataParams->email = $input['send_to'] == 'specific_user' ? $input['email'] : [];
        $dataParams->subject = $input['subject'];
        $dataParams->emailcontent = $input['emailcontent'];

        $dataArray = $this->customMailInterface->sendCustomMail($dataParams);
        if ($dataArray) {
            return redirect('custom-mail-notification-list')->withSuccess(['message' => trans('messages.errors.CATEGORY_UPDATED_SUCCESSFULLY')]);
        }
        return redirect('custom-mail-notification-list')->withErrors(['message' => trans('messages.errors.THERE_IS_SOME_ERROR')]);
    }

    public function viewCustomMail($customMailId)
    {
        $data = $this->customMailInterface->viewCustomMail($customMailId);
        $dataArray = $data['data'];
        $emailArray = $data['emails'];
        return view('custom-mail-notification.view',compact(['dataArray', 'emailArray']));
    }
}