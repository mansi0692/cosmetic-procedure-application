<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\DoctorPatientAdminInterface;
use App\Models\ModelHasRole;
use App\Models\User;
use App\Traits\RestExceptionHandlerTrait;
use Illuminate\Http\Request;
use stdClass;
use Validator;

class DoctorPatientAdminController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $doctorInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $doctorPatientAdminInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, DoctorPatientAdminInterface $doctorPatientAdminInterface)
    {
        $this->middleware('auth');
        $this->responseHelper = $responseHelper;
        $this->doctorPatientAdminInterface = $doctorPatientAdminInterface;
    }

    public function listDoctorAdmin()
    {
        $adminData = $this->doctorPatientAdminInterface->listDoctorAdmin();
        return view('doctor-patient.list', compact('adminData'));
    }

    public function listPatientAdmin()
    {
        $adminData = $this->doctorPatientAdminInterface->listPatientAdmin();
        return view('doctor-patient.list', compact('adminData'));
    }

    public function addDoctorPatientAdmin()
    {
        return view('doctor-patient.add');
    }

    public function saveDoctorPatientAdmin(Request $request)
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'admin' => 'required',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $adminData = new stdClass();
        $adminData->admin = $input['admin'];
        $adminData->email = $input['email'];
        $adminData->password = $input['password'];
        $saveAdmin = $this->doctorPatientAdminInterface->saveDoctorPatientAdmin($adminData);

        if ($saveAdmin) {
            if ($input['admin'] == 'doctor') {
                return redirect('list-doctor-admin')->with('success', trans('messages.success.DOCTOR_ADMIN_SAVED_SUCCESSFULLY'));
            } else if ($input['admin'] = "patient") {
                return redirect('list-doctor-admin')->with('success', trans('messages.success.PATIENT_ADMIN_SAVED_SUCCESSFULLY'));
            }
        }
        return redirect('list-doctor-admin')->with('error', 'There is some error');
    }

    public function editDoctorPatientAdmin($adminId)
    {
        $adminData = $this->doctorPatientAdminInterface->editDoctorPatientAdmin($adminId);
        return view('doctor-patient.edit', compact('adminData'));
    }

    public function updateDoctorPatientAdmin(Request $request, $adminId)
    {
        $input = $request->toArray();
        $validator = Validator::make($input, [
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $adminData = new stdClass();
        $adminData->password = $input['password'];
        $updateAdmin = $this->doctorPatientAdminInterface->updateDoctorPatientAdmin($adminData, $adminId);

        if ($updateAdmin) {
            $role = ModelHasRole::where('model_id', $adminId)->first();
            if ($role->role_id == config('constants.roles.doctor_admin')) {
                return redirect('list-doctor-admin')->with('success', trans('messages.success.DOCTOR_ADMIN_UPDATED_SUCCESSFULLY'));
            } else if ($role->role_id == config('constants.roles.patient_admin')) {
                return redirect('list-doctor-admin')->with('success', trans('messages.success.PATIENT_ADMIN_UPDATED_SUCCESSFULLY'));
            }
        }
        return redirect('list-doctor-admin')->with('error', 'There is some error');
    }

    public function returnAdminList()
    {
        return redirect('list-doctor-admin');
    }

    public function changeStatus($adminId, $status)
    {

        $admins = $this->doctorPatientAdminInterface->changeStatus($adminId, $status);
        return redirect()->back()->with('success', 'Status Changed successfully');
    }

    public function deleteAdmin($adminId)
    {
        $admins = $this->doctorPatientAdminInterface->deleteAdmin($adminId);
        return redirect()->back()->with('success', 'Admin deleted successfully');
    }
}
