<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\FAQInterface;
use App\Traits\RestExceptionHandlerTrait;
use Validator;

class FaqController extends Controller
{
    use RestExceptionHandlerTrait;


    /**
     * $faqInterface
     *
     * @var mixed
     */
    protected $faqInterface;

    /**
     * __construct
     *
     * @param FAQInterface $faqInterface
     */
    public function __construct(FAQInterface $faqInterface)
    {
        $this->middleware('auth');
        $this->faqInterface = $faqInterface;
    }

    /**
     * list
     *
     * @param int $faqCategoryId
     * @param int $languageId
     * 
     * @return mixed
     */
    public function list(int $faqCategoryId, int $languageId)
    {
        $faqs = $this->faqInterface->list($faqCategoryId, $languageId);
        return view('faq.list-faqs', compact('faqs'));
    }

    /**
     * create
     *
     * @param int $faqsId
     * 
     * @return mixed
     */
    public function create(int $faqsId)
    {
        return view('faq.add-faqs');
    }

    /**
     * changeStatus
     *
     * @param string $status
     * @param int $faqCategoryId
     * 
     * @return mixed
     */
    public function changeStatus(string $status, int $faqCategoryId)
    {
        $this->faqInterface->changeStatus($status, $faqCategoryId);
        return back()->with('success', 'Status marked as ' . $status);
    }

    /**
     * edit
     *
     * @param int $faqsId
     * @param int $faqCategoryId
     * @param int $languageId
     * 
     * @return mixed
     */
    public function edit(int $faqsId, int $faqCategoryId, int $languageId)
    {
        $faqData = $this->faqInterface->edit($faqsId, $faqCategoryId, $languageId);
        return view('faq.add-faqs', compact('faqData'));
    }

    /**
     * store
     *
     * @param Request $request
     * @param int $faqsId
     * @param int $languageId
     * 
     * @return mixed
     */
    public function store(Request $request, int $faqsId, int $languageId)
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $data = $this->faqInterface->store($input, $faqsId, $languageId);

        if ($data) {
            return redirect()->route('list-faqs', ['faqCategoryId' => $faqsId, 'languageId' => $languageId])
                ->with('success', ' saved successfully');
        }
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $faqsId
     * @param int $faqCategoryId
     * @param int $languageId
     * 
     * @return mixed
     */
    public function update(Request $request, int $faqsId, int $faqCategoryId, int $languageId)
    {
        $input = $request->toArray();
        
        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $data = $this->faqInterface->update($input, $faqsId, $faqCategoryId, $languageId);

        if ($data) {
            return redirect()->route('list-faqs', ['faqCategoryId' => $faqCategoryId, 'languageId' => $languageId])
                ->with('success', 'Saved successfully');
        }
    }

   
    /**
     * destroy
     *
     * @param int $faqsId
     * 
     * @return mixed
     */
    public function destroy(int $faqsId)
    {
        $data = $this->faqInterface->destroy($faqsId);

        if ($data) {
            return back()->with('success', 'Deleted successfully');
        }
        
        return back()->with('error', 'There is some error');
    }
}
