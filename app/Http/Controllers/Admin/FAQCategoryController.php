<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\FAQCategoryInterface;
use Validator;

class FAQCategoryController extends Controller
{
    /**
     * $faqCategoryInterface
     *
     * @var mixed
     */
    protected $faqCategoryInterface;

    /**
     * __construct
     *
     * @param FAQCategoryInterface $faqCategoryInterface
     */
    public function __construct(FAQCategoryInterface $faqCategoryInterface)
    {
        $this->middleware('auth');
        $this->faqCategoryInterface = $faqCategoryInterface;
    }

    /**
     * list
     *
     * @param int $languageId
     * 
     * @return mixed
     */
    public function list(int $languageId)
    {
        $faqCategoryData = $this->faqCategoryInterface->list($languageId);
        return view('faq.list-faq-category', compact('faqCategoryData'));
    }

    /**
     * edit
     *
     * @param int $categoryId
     * @param int $languageId
     * 
     * @return mixed
     */
    public function edit(int $categoryId, int $languageId)
    {
        $data = $this->faqCategoryInterface->edit($categoryId, $languageId);
        return view('faq.edit-faq-category', compact('data'));
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $faqCategoryId
     * @param int $languageId
     * 
     * @return mixed
     */
    public function update(Request $request, int $faqCategoryId, int $languageId)
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $data = $this->faqCategoryInterface->update($input, $faqCategoryId, $languageId);

        if ($data) {
            return redirect()->route('list-faq-category', ['languageId' => $languageId])
                ->with('success', 'Saved successfully');
        }
    }
}
