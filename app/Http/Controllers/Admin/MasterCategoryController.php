<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\MasterCategory;
use App\Models\ModelHasRole;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session as FacadesSession;
use Illuminate\Support\Facades\Validator;

class MasterCategoryController extends Controller
{
    /**
     * __construct
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->middleware('auth');
        $categoryType = MasterCategory::get();
        return view('category.list-master-category', compact('categoryType'));
    }

    public function redirectList($masterCategoryId, $isOther)
    {
        return redirect()->route('list-master-sub-category', ['masterCategoryId' => $masterCategoryId, 'isOther' => $isOther]);
    }

    public function getCityFromState($stateId)
    {
        $city = City::where('state_id', $stateId)->get();
        return Response::json($city);
    }
}
