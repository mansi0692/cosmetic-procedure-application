<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\EmailManagementInterface;
use App\Traits\RestExceptionHandlerTrait;
use Illuminate\Http\Request;
use Validator;

class EmailManagementController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $emailManagementInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $emailManagementInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, EmailManagementInterface $emailManagementInterface)
    {
        $this->middleware('auth');
        $this->responseHelper = $responseHelper;
        $this->emailManagementInterface = $emailManagementInterface;
    }

    public function list($languageId)
    {
        $dataArray = $this->emailManagementInterface->list($languageId);
        return view('email-management.list', compact('dataArray'));
    }

    public function changeStatus($emailTemplateId, $status)
    {
        $admins = $this->emailManagementInterface->changeStatus($emailTemplateId, $status);
        return redirect()->back()->with('success', 'Status Changed successfully');
    }

    public function editEmailTemplate($emailTemplateId)
    {
        $emailTemplate = $this->emailManagementInterface->editEmailTemplate($emailTemplateId);
        return view('email-management.edit', compact('emailTemplate'));
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $emailTemplateId
     *
     * @return mixed
     */
    public function update(Request $request, int $emailTemplateId, int $languageId)
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'subject' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $temp = $this->emailManagementInterface->update($input, $emailTemplateId);

        if ($temp)
            return redirect('list-email-template/' . $languageId)->with('success', 'Saved successfully');
    }
}
