<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ModelHasRole;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session as FacadesSession;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    /**
     * __construct
     * @return void
     */
    public function __construct()
    {
    }

    public function login()
    {
        if (!Auth::check()) {
            return view('login');
        } else {
            return redirect()->intended('dashboard');
        }
    }

    public function userLogin(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            if (Auth::user()->status) {
                if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3) {
                    return redirect()->back()->withErrors(['msg' => trans('messages.errors.NOT_ALLOWED_TO_ACCESS_PAGE')]);
                }
                return redirect()->intended('dashboard');
            } else {
                return redirect()->back()->withErrors(['msg' => 'Your account is not active']);
            }
        }
        return redirect()->back()->withErrors(['msg' => trans('messages.errors.INVALID_EMAIL_PASSWORD')]);
    }

    public function index()
    {
        if (Auth::check()) {
            $userRole = User::with('role')->where('id', Auth::user()->id)->first();
            $role = $userRole['role'][0]['name'];
            return view('dashboard', compact('role'));
        }

        return redirect("login")->withErrors(trans('messages.errors.NOT_ALLOWED_TO_ACCESS_PAGE'));
    }

    public function logout()
    {
        FacadesSession::flush();
        Auth::logout();

        return redirect()->route('login');
    }

    public function changepassword(Request $request)
    {
        $input = $request->all();
        $userId = Auth::user()->id;
        $validator = Validator::make($input, [
            'current_password' => [
                'required',
                function ($attribute, $value, $fail) use ($userId) {
                    $useremail = User::find($userId);
                    if (Hash::check($value, $useremail->password)) {
                    } else {
                        $fail('Current password is wrong');
                    }
                }
            ],
            'new_password' => 'required',
            'confirm_password' => 'required_with:new_password|same:new_password'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $changePassword = User::findOrFail(Auth::user()->id);
        if (Hash::check($request->current_password, $changePassword->password)) {
            $changePassword->password = Hash::make($request->new_password);
            $changePassword->save();
            return redirect()->back()->withSuccess(['msg' => trans('messages.success.PASSWORD_CHANGED')]);
        }
    }

    public function redirectToChangePassword(Request $request)
    {
        return view('changepassword');
    }
}
