<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\DoctorInterface;
use App\Interfaces\PatientInterface;
use App\Models\City;
use App\Models\MasterCategory;
use App\Models\PatientInformation;
use App\Models\State;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Http;
use App\Traits\RestExceptionHandlerTrait;
use Illuminate\Support\Facades\Auth;
use stdClass;

class PatientController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $patientInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $patientInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, PatientInterface $patientInterface)
    {
        $this->middleware('auth');
        $this->responseHelper = $responseHelper;
        $this->patientInterface = $patientInterface;
    }

    public function listPatient()
    {
        $patients = $this->patientInterface->listPatient();
        return view('patient.list-patient', compact('patients'));
    }

    public function changeStatus($patientId, $status)
    {
        $patients = $this->patientInterface->changeStatus($patientId, $status);
        return redirect()->back()->with('success', 'Status Changed successfully');
    }

    public function editPatient($patientId)
    {
        $patients = $this->patientInterface->editPatient($patientId);
        $patientData = $patients['patientData'];
        $country = $patients['countries'];
        $city = $patients['cities'];
        $state = $patients['states'];

        return view('patient.edit-patient', compact(['patientData', 'country', 'city', 'state']));
    }

    public function redirectPatientList()
    {
        return redirect('list-patient');
    }

    public function updatePatient($patientId, Request $request)
    {
        $input = $request->toArray();
        $loggedInUserId = Auth::user()->id;

        $validator = Validator::make($input, [
            'full_name' => 'required',
            'zipcode' => 'required',
            'city' => 'required',
            'state' => 'required',
            'country' => 'required',
            'email' => [
                'required', 'email',
                function ($attribute, $value, $fail) use ($patientId) {
                    $useremail = User::find($patientId);
                    if ($value != $useremail->email) {
                        $validateEmail = User::where('email', $value)->first();
                        if (isset($validateEmail)) {
                            $fail('This email is already taken.');
                        }
                    }
                }
            ],
            'username' => [
                'required',
                function ($attribute, $value, $fail) use ($patientId) {
                    $useremail = PatientInformation::where('user_id', $patientId)->first();
                    if ($value != $useremail->username) {
                        $validateEmail = PatientInformation::where('username', $value)->first();
                        if (isset($validateEmail)) {
                            $fail('This username is already taken.');
                        }
                    }
                }
            ],
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $patientParams = new stdClass();
        $patientParams->first_name = $input['full_name'];
        $patientParams->username = $input['username'];
        $patientParams->email = $input['email'];
        $patientParams->zipcode = $input['zipcode'];
        $patientParams->city = $input['city'];
        $patientParams->state = $input['state'];
        $patientParams->country = $input['country'];
        $patientParams->phone_number = $input['phone_number'];

        $patients = $this->patientInterface->updatePatient($patientId, $patientParams);
        if ($patients) {
            return redirect('list-patient')->with('success', 'Patient information updated successfully');
        }
        return redirect('list-patient')->with('error', 'There is some error');
    }

    public function viewPatient($patientId)
    {
        $patients = $this->patientInterface->editPatient($patientId);
        $patientData = $patients['patientData'];
        $country = $patients['countries'];
        $city = $patients['cities'];
        $state = $patients['states'];

        return view('patient.view-patient', compact(['patientData', 'country', 'city', 'state']));
    }

    public function listPendingPatinetCancleRequest()
    {
        $patientCancelRequest = $this->patientInterface->listPendingPatinetCancleRequest();
        return view('patient-cancel-request.list', compact('patientCancelRequest'));
    }
    public function listApprovedPatinetCancleRequest()
    {
        $patientCancelRequest = $this->patientInterface->listApprovedPatinetCancleRequest();
        return view('patient-cancel-request.list', compact('patientCancelRequest'));
    }

    public function viewPatientCancelRequest($patientId)
    {
        $patientData = $this->patientInterface->viewPatientCancelRequest($patientId);

        return view('patient-cancel-request.view', compact(['patientData']));
    }

    public function patientCancleRequestAppprove($patientId)
    {
        $patients = $this->patientInterface->patientCancleRequestAppprove($patientId);
        return redirect('patient-cancel-request-list-pending');
    }

    public function returnPatientCancelRequestList()
    {
        return redirect('patient-cancel-request-list-pending');
    }

    public function listPatientReview()
    {
        $patients = $this->patientInterface->listPatient();
        return view('patient-review.list-patient', compact(['patients']));
    }

    public function getPatientRating($patientId)
    {
        $patientRating = $this->patientInterface->getPatientRating($patientId);
        return view('patient-review.patient-rating', compact(['patientRating']));
    }

    public function redirectPatientRatingList()
    {
        return redirect('list-patient-review');
    }
}
