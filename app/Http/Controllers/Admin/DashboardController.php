<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DoctorInformation;
use App\Models\DoctorReview;
use App\Models\PatientInformation;

class DashboardController extends Controller
{
    /**
     * __construct
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $dashboardData = [];

        $patientCount = PatientInformation::leftJoin('users as user', 'user.id', 'patient_information.user_id')->count();
        $doctorCount = DoctorInformation::leftJoin('users as user', 'user.id', 'doctor_information.user_id')->count();
        $patientReviewCount = DoctorReview::groupBy('patient_id')->get()->count();
        $doctorReviewCount = DoctorReview::groupBy('doctor_id')->get()->count();
        $dashboardData['patientCount'] = $patientCount;
        $dashboardData['doctorCount'] = $doctorCount;
        $dashboardData['patientReviewCount'] = $patientReviewCount;
        $dashboardData['doctorReviewCount'] = $doctorReviewCount;
        return view('index', compact('dashboardData'));
    }
}
