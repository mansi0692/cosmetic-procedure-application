<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\CMSInterface;
use App\Interfaces\DoctorInviteInterface;
use App\Models\DoctorInvite;
use App\Models\Language;
use App\Traits\RestExceptionHandlerTrait;
use Ramsey\Uuid\Uuid;
use stdClass;

class CMSController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $cmsInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $cmsInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, CMSInterface $cmsInterface)
    {
        $this->middleware('auth');
        $this->responseHelper = $responseHelper;
        $this->cmsInterface = $cmsInterface;
    }

    public function list(int $languageId)
    {
        $dataArray = $this->cmsInterface->list($languageId);
        return view('cms-pages.list', compact('dataArray'));
    }
    public function addCMS()
    {
        $languages = Language::get(['id', 'title']);
        return view('cms-pages.add', compact('languages'));
    }

    public function saveCMS(Request $request)
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'language' => 'required',
            'title' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $dataParams = new stdClass();
        $dataParams->title = $input['title'];
        $dataParams->language_id = $input['language'];
        $dataParams->description = $input['description'];
        $dataParams->url = strtolower(str_replace(" ", "-", $input['title']));

        $saveCMS = $this->cmsInterface->saveCMS($dataParams);
        return redirect()->route('cms-list', ['languageId' => 1])->with('message', 'CMS page saved succcessfully');
    }

    public  function redirectBack()
    {
        return redirect()->route('cms-list', ['languageId' => 1]);
    }

    public function editCMS($cmsPageId)
    {
        $cms = $this->cmsInterface->editCMS($cmsPageId);
        return view('cms-pages.edit', compact('cms'));
    }

    public function updateCMS(Request $request, $cmsPageId) 
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'title' => 'required',
            'description' => 'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $dataParams = new stdClass();
        $dataParams->title = $input['title'];
        $dataParams->description = $input['description'];

        $saveCMS = $this->cmsInterface->updateCMS($dataParams, $cmsPageId);
        return redirect()->route('cms-list', ['languageId' => 1])->with('message', 'CMS page updated succcessfully');
    }

    public function changeStatus($cmsPageId, $status)
    {
        $cms = $this->cmsInterface->changeStatus($cmsPageId, $status);
        return redirect()->back()->with('message', 'Status Changed successfully');
    }

    public function deleteCMS($cmsPageId) 
    {
        $doctors = $this->cmsInterface->deleteCMS($cmsPageId);
        return redirect()->back()->with('message', 'CMS page deleted successfully');
    }
}
