<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\SmsManagementInterface;
use App\Traits\RestExceptionHandlerTrait;
use Illuminate\Http\Request;
use Validator;

class SmsManagementController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $smsManagementInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $smsManagementInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, SmsManagementInterface $smsManagementInterface)
    {
        $this->middleware('auth');
        $this->responseHelper = $responseHelper;
        $this->smsManagementInterface = $smsManagementInterface;
    }

    public function list($languageId)
    {
        $dataArray = $this->smsManagementInterface->list($languageId);
        return view('sms-management.list', compact('dataArray'));
    }

    public function changeStatus($smsTemplateId, $status)
    {
        $admins = $this->smsManagementInterface->changeStatus($smsTemplateId, $status);
        return redirect()->back()->with('success', 'Status Changed successfully');
    }

    public function editSmsTemplate($smsTemplateId)
    {
        $smsTemplate = $this->smsManagementInterface->editSmsTemplate($smsTemplateId);
        return view('sms-management.edit', compact('smsTemplate'));
    }

    /**
     * update
     *
     * @param Request $request
     * @param int $smsTemplateId
     *
     * @return mixed
     */
    public function update(Request $request, int $smsTemplateId, int $languageId)
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'message' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $temp = $this->smsManagementInterface->update($input, $smsTemplateId);

        if ($temp)
            return redirect('list-sms-template/' . $languageId)->with('success', 'Saved successfully');
    }
}
