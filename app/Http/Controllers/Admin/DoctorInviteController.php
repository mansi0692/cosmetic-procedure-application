<?php

namespace App\Http\Controllers\Admin;

use Validator;
use Illuminate\Http\Request;
use App\Helpers\Api\ResponseHelper;
use App\Http\Controllers\Controller;
use App\Interfaces\DoctorInviteInterface;
use App\Models\DoctorInvite;
use App\Models\User;
use App\Traits\RestExceptionHandlerTrait;
use Ramsey\Uuid\Uuid;

class DoctorInviteController extends Controller
{
    use RestExceptionHandlerTrait;
    /**
     * responseHelper
     *
     * @var mixed
     */
    private $responseHelper;

    protected $doctorInviteInterface;

    /**
     * __construct
     *
     * @param  mixed $responseHelper
     * @param  mixed $doctorInviteInterface
     * @return void
     */
    public function __construct(ResponseHelper $responseHelper, DoctorInviteInterface $doctorInviteInterface)
    {
        $this->middleware('auth');
        $this->responseHelper = $responseHelper;
        $this->doctorInviteInterface = $doctorInviteInterface;
    }

    public function list(Request $request, $tabNumber)
    {
        $dataArray = $this->doctorInviteInterface->list($tabNumber);

        return view('doctor-invite.list', compact('dataArray'));
    }
    public function doctorInvite()
    {
        return view('doctor-invite.send-invite');
    }

    public function sendInvitation(Request $request)
    {
        $input = $request->toArray();

        $validator = Validator::make($input, [
            'email' => [
                'required', 'email',
                function ($attribute, $value, $fail) {
                    $validateEmail = User::where('email', $value)->whereNull('deleted_at')->first();
                    if (isset($validateEmail)) {
                        $fail('This email is already registered.');
                    }
                }
            ]
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }
        $email = $input['email'];

        $inviteDoctor = $this->doctorInviteInterface->sendInvitation($email);
        if ($inviteDoctor) {
            return redirect('doctor-invite-list', ['tabNumber' => 0])->withSuccess(['message' => 'Email send succcessfully']);
        }
        return redirect('doctor-invite-list', ['tabNumber' => 0])->withErrors(['message' => trans('messages.errors.THERE_IS_SOME_ERROR')]);
    }
}
