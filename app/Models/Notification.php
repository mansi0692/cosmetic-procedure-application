<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    use HasFactory;

    protected $fillable = ['id', 'user_id', 'type', 'message', 'details', 'created_by', 'is_read', 'updated_by'];
}
