<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChatRequest extends Model
{
    protected $table='chat_request';

    protected $fillable=[
        'patient_id',
        'doctor_id',
        'status'
    ];

    use HasFactory;
}
