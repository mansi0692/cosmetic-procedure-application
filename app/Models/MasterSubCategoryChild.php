<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MasterSubCategoryChild extends Model
{
    use HasFactory;

    protected $table = 'master_sub_category_child';
    protected $fillable = ['master_sub_category_id', 'language_id', 'label'];
}
