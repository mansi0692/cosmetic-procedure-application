<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['from_user_id', 'to_user_id', 'message', 'file', 'deleted_by_patient', 'deleted_by_doctor'];
    protected $appends = ['send_at', 'original_file_name'];
    // public $timestamps = false;

    public function file(): Attribute
    {
        return new Attribute(
            get: fn ($value) => !empty($value) ? getImage($value, $this->from_user_id, true) : '',
        );
    }

    public function sendAt(): Attribute
    {
        return new Attribute(
            get: fn ($value, $attributes) => date('d-m-Y H:i:s', strtotime($attributes['created_at'])),
        );
    }

    public function originalFileName(): Attribute
    {
        return new Attribute(
            get: fn ($value, $attributes) => $attributes['file'],
        );
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'from_user_id');
    }
}
