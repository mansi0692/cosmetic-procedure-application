<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorFavorite extends Model
{
    use HasFactory, SoftDeletes;

    public $timestamps = true;
    
    protected $fillable = ['doctor_id', 'patient_id', 'doctor_favorite_group_id','liked_by_id','created_by', 'updated_by'];
}