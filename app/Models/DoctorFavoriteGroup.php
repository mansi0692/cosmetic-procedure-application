<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorFavoriteGroup extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'doctor_favorite_group';

    protected $fillable = ['name', 'created_by','updated_by'];
}
