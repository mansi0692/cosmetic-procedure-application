<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorFollow extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'doctor_follows';
    protected $fillable = ['id', 'doctor_id', 'follow_by', 'created_by', 'deleted_by'];
    protected $dates = ['deleted_at'];
}
