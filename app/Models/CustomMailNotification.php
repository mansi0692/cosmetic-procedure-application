<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomMailNotification extends Model
{
    use HasFactory;

    protected $tableName = "custom_mail_notifications";

    public $timestamps = false;

    protected $casts = [
        'send_to_id' => 'json',
    ];
}