<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Appointment extends Model
{
    use HasFactory;

    protected $fillable = ['status', 'declined_reason', 'updated_by'];

    protected $table = 'appointments';
    /**
     * Get the patient reschedule appointments
     *
     */
    public function rescheduleAppointmentByPatient()
    {
        return $this->hasOne(AppointmentReschedule::class, 'appointment_id', 'id')
            ->leftJoin('doctor_locations as dl', 'dl.id', 'appointment_reschedules.location_id')
            ->leftJoin('cities as ct', 'ct.id', 'dl.city_id')
            ->leftJoin('states as s', 's.id', 'dl.state_id')
            ->select('appointment_reschedules.id as appointment_reschedule_id', 'appointment_id', 'location_id','dl.address', 'dl.id','ct.name as city_name','s.name as state_name', 'appointment_date', 'start_time', 'end_time', 'appointment_reschedules.status', 'consultant_type', 'reason as consultancy_reason', 'declined_reason', 'requested_by',DB::raw("DATE_FORMAT(appointment_reschedules.created_at, '%d-%m-%Y %H:%i %p') as formatted_dob"))
            ->where('requested_by', 'patient')->latest('appointment_reschedules.created_at');

        // return $this->hasOne(AppointmentReschedule::class, 'appointment_id', 'id')
        //     ->leftJoin('doctor_locations as dl', 'dl.id', 'appointment_reschedules.location_id')
        //     ->leftJoin('cities as ct', 'ct.id', 'dl.city_id')
        //     ->leftJoin('states as s', 's.id', 'dl.state_id')
        //     ->select('appointment_reschedules.id as appointment_reschedule_id', 'appointment_id', 'location_id', 'dl.id','ct.name as city_name','s.name as state_name', 'appointment_date', 'start_time', 'end_time', 'appointment_reschedules.status', 'consultant_type', 'reason as consultancy_reason', 'declined_reason', 'requested_by',DB::raw("DATE_FORMAT(appointment_reschedules.created_at, '%d-%m-%Y %H:%i %p') as formatted_dob"))
        //     ->selectRaw('DATE_FORMAT(appointment_reschedules.created_at,"%Y-%m-%d")')
        //     ->where('requested_by', 'patient')->latest('appointment_reschedules.created_at');
    }

    /**
     * Get the rescheduled appointments
     *
     */
    public function rescheduleAppointmentByDoctor()
    {
        return $this->hasOne(AppointmentReschedule::class, 'appointment_id', 'id')
            ->leftJoin('doctor_locations as dl', 'dl.id', 'appointment_reschedules.location_id')
            ->leftJoin('cities as ct', 'ct.id', 'dl.city_id')
            ->leftJoin('states as s', 's.id', 'dl.state_id')
            ->select('appointment_reschedules.id as appointment_reschedule_id', 'appointment_id', 'location_id', 'dl.address','dl.id','ct.name as city_name','s.name as state_name', 'appointment_date', 'start_time', 'end_time', 'appointment_reschedules.status', 'consultant_type', 'reason as consultancy_reason', 'declined_reason', 'requested_by',DB::raw("DATE_FORMAT(appointment_reschedules.created_at, '%d-%m-%Y %H:%i %p') as formatted_dob"))
            ->where('requested_by', 'doctor')->latest('appointment_reschedules.created_at');
    }

    public function review()
    {
        return $this->hasOne(DoctorReview::class, 'booking_id', 'booking_id')
            ->leftJoin('patient_information as pt', 'pt.user_id', 'doctor_reviews.patient_id')
            ->select(['booking_id','patient_id', 'rating', 'doctor_id', 'bedside_manner', 'answered_questions', 'after_care', 'time_spent', 'responsiveness', 'courtesy', 'payment_process', 'wait_times', 'review', 'doctor_reviews.created_at as review_created_at','pt.full_name as fullname', 'pt.profile_image', 'pt.username']);
    }
    public function getAllReview()
    {
        return $this->hasMany(DoctorReview::class, 'booking_id', 'booking_id')
            ->select(['booking_id', 'rating', 'doctor_id', 'bedside_manner', 'answered_questions', 'after_care', 'time_spent', 'responsiveness', 'courtesy', 'payment_process', 'wait_times', 'review', 'created_at as review_created_at']);
    }

    public function isMultipleBooking()
    {
        return $this->hasMany(Appointment::class,'booking_id','booking_id');
    }
}
