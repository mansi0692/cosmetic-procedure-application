<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

class NewsFeed extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['id', 'title', 'description', 'file','poster_image','created_by', 'updated_by'];
    protected $dates = ['deleted_at'];

    protected $appends = [
        'like_count', 'comment_count'
    ];

    /**
     * createdAt
     *
     * @return Attribute
     */
    public function createdAt(): Attribute
    {
      return new Attribute(
          get: fn ($value) => (new Carbon($value))->diffForHumans(),
      );
    }

    public function likes()
    {
        return $this->hasMany(FeedLike::class, 'feed_id', 'id');
    }

    public function follows()
    {
        return $this->hasMany(DoctorFollow::class, 'doctor_id', 'created_by');
    }

    public function getLikeCountAttribute()
    {
        return $this->hasMany(FeedLike::class, 'feed_id', 'id')->get()->count();
    }

    public function getCommentCountAttribute()
    {
        return $this->hasMany(FeedComment::class, 'feed_id', 'id')->get()->count();
    }

    public function comments()
    {
        return $this->hasMany('App\Models\FeedComment', 'feed_id', 'id')
            ->leftjoin('users', 'users.id', 'feed_comments.created_by')
            ->leftjoin('doctor_information', 'doctor_information.user_id', 'users.id')
            ->leftjoin('patient_information', 'patient_information.user_id', 'users.id')
            ->select([
                'feed_comments.id as comment_id', 'feed_comments.feed_id','feed_comments.created_by', 'feed_comments.description as description', 'feed_comments.created_at as comment_created_at',
                'users.role_id', 'doctor_information.first_name as dr_first_name', 'doctor_information.last_name as dr_last_name', 'doctor_information.profile_image as dr_profile_image',
                'patient_information.full_name as patient_full_name', 'patient_information.profile_image as patient_profile_image','comment_by'
            ]);
    }

    public function isAuthUserLikedPost()
    {
        return $this->likes()->where('like_by',  auth()->id())->exists();
    }

    public function isAuthUserFollowedPost()
    {
        return $this->follows()->where('follow_by',  auth()->id())->exists();
    }
}
