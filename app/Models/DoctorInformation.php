<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorInformation extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'doctor_information';

    public function review()
    {
        return $this->hasMany(DoctorReview::class,'doctor_id','user_id')
            ->select(['booking_id', 'doctor_id','rating', 'bedside_manner', 'answered_questions', 'after_care', 'time_spent', 'responsiveness', 'courtesy', 'payment_process', 'wait_times', 'review', 'created_at as review_created_at']);
    }
    
    public function createdAt(): Attribute
    {
      return new Attribute(
          get: fn ($value) => (new Carbon($value))->format('Y-m-d h:i:s'),
      );
    }
}
