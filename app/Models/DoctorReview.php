<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DoctorReview extends Model
{
    use HasFactory;

    protected $appends = [
        'average_rating',
    ];

    public function getAverageRatingAttribute()
    {
        return (round(($this->bedside_manner + $this->answered_questions + $this->after_care + $this->time_spent + $this->responsiveness + $this->courtesy + $this->payment_process + $this->wait_times) / 8));
    }
}
