<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FeedLike extends Model
{
    use HasFactory,SoftDeletes;

    protected $table = 'feed_likes';
    protected $fillable = ['id','feed_id','like_by','created_by','deleted_by'];
    protected $dates = ['deleted_at'];
}
