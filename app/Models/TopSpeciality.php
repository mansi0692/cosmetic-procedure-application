<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Casts\Attribute;
use App\Models\DoctorInformation;
use App\Models\Doctor;

class TopSpeciality extends Model
{
    use HasFactory;

    protected $appends = ['reg_doctor', 'unreg_doctor'];

    public function specialityData() {
        return $this->hasOne(MasterSubCategory::class, 'id', 'speciality_id')->select('id', 'label');
    }

    public function regDoctor(): Attribute
    {
        return new Attribute(
            get: fn ($value, $attributes) => DoctorInformation::where('user_id', $this->doctor_id)->first(DB::raw('CONCAT(first_name, \' \', last_name) as name')),
        );
    }

    public function unregDoctor(): Attribute
    {
        return new Attribute(
            get: fn ($value, $attributes) => Doctor::where('id', $this->unreg_doctor_id)->first(DB::raw('CONCAT(first_name, \' \', last_name) as name')),
        );
    }
}
