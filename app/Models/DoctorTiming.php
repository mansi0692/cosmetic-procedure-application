<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorTiming extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['id','doctor_id','location_id','day','start_time','end_time','is_available','created_by'];
}
