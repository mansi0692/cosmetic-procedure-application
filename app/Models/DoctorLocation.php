<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class DoctorLocation extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'doctor_id',
        'address',
        'city_id',
        'suite',
        'state_id',
        'zipcode',
        'coordinates',
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_by',
    ];
}
