<?php

namespace App\Providers;

use App\Jobs\CustomMailJob;
use App\Models\User;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Support\Facades\Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_ENV') !== 'local') {
            $this->app['request']->server->set('HTTPS', true);
        }


        Schema::defaultStringLength(191);
        Paginator::useBootstrap();

        view()->composer([
            'partials.header',
            'index'
        ], function ($view) {
            $userRole = User::with('role')->where('id', Auth::user()->id)->first();
            $role = $userRole['role'][0]['name'];
            view()->share('role', $role);
        });

        Queue::after(function (JobProcessed $event) {
            return $event;
        });
    }
}
