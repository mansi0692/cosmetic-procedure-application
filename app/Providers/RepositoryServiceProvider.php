<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $modules = [
            'Auth',
            'MasterData',
            'User',
            'Doctor',
            'Patient',
            'DoctorPatientAdmin',
            'CustomMail',
            'DoctorInvite',
            'ContactUs',
            'CMS',
            'EmailManagement',
            'SmsManagement',
            'Calendar',
            'Message',
            'FAQ',
            'FAQCategory',
            'Notification'
        ];

        foreach ($modules as $module) {
            $this->app->bind(
                "App\Interfaces\\{$module}Interface",
                "App\Repositories\\{$module}Repository"
            );
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
