<?php

namespace App\Traits;

use App\Models\Notification;
use App\Models\UserDeviceToken;
use Aws\Sns\Exception\SnsException;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

trait NotificationTrait
{
    public function sendPushNotification(string $notificationTitle = '', array $notificationDetails, int $userId, string $appointmentId = '', string $type = '')
    {
        $userDeviceToken = UserDeviceToken::where('user_id', $userId)->latest()->first();
        $unreadNotificationsCount = Notification::where(['user_id' => $userId, 'is_read' => 0])->get()->count();

        $data = [
            'id' => $appointmentId ?? null,
            "type" => "Notification",
            "action" => "openAppointmentDetail",
            "channel" => "appointmentStatusChannel",
            'notification_type' => $type,
            'created_at' => Carbon::now()->diffForHumans(),
            'unread_notification_count' => !empty($unreadNotificationsCount) ? $unreadNotificationsCount : 0,
        ];
        
        $notificationMessage = str_replace(
            array('{{doctorName}}', '{{patientName}}', '{{appointmentStatus}}', '{{appointmentTiming}}'),
            array($notificationDetails['doctorName'] ?? NULL, $notificationDetails['patientName'] ?? NULL, array_key_exists('appointmentStatus', $notificationDetails) ? trans('status.' . $notificationDetails['appointmentStatus']) : NULL, $notificationDetails['appointmentTiming'] ?? NULL),
            getNotificationTemplateData($notificationDetails['templateName'])
        );

        $notificationData = [
            'id' => $appointmentId,
            'title' => getNotificationTemplateData($notificationDetails['notificationTitle']),
            'body' => $notificationMessage,
            'notification_type' => $type,
            'sound' => 'default',
            "action" => "openAppointmentDetail",
            "channel" => "appointmentStatusChannel"
        ];

        $notification = Notification::create([
            'user_id' => $userId,
            'message' => json_encode($notificationData),
            'details' => json_encode($notificationDetails),
            'type' => $notificationTitle,
            'is_read' => 0,
            'created_by' => Auth::user()->id ?? 0
        ]);

        if (!empty($userDeviceToken)) {

            $endPointArn = [
                'EndpointArn' => $userDeviceToken->arn
            ];

        }

        try {
            if (!empty($userDeviceToken) && !empty(Auth::user()->id)) {

                $sns = App::make('aws')->createClient('sns');
                $endpointAtt = $sns->getEndpointAttributes($endPointArn);

                if ($endpointAtt != 'failed' && $endpointAtt['Attributes']['Enabled']) {
                    if ($userDeviceToken->platform == 'android') {

                        $fcmPayload = json_encode(
                            [
                                'notification' => $notificationData,
                                'data' => $data
                            ]
                        );

                        $message = json_encode([
                            'default' => $notificationMessage,
                            'GCM' => $fcmPayload
                        ]);

                        $response = $sns->publish([
                            'TargetArn' => $userDeviceToken->arn,
                            'Message' => $message,
                            'MessageStructure' => 'json'
                        ]);

                        $metadata = $response->get('@metadata');

                        if ($metadata['statusCode'] == 200) {
                            return true;
                        }
                        return false;
                    }
                }
            }
        } catch (SnsException $e) {
            Log::info($e->getMessage());
        }
    }
}
