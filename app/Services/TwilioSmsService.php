<?php

namespace App\Services;

use Twilio\Rest\Client;
use Twilio\Jwt\ClientToken;

class TwilioSmsService
{
    public function sendSms($phoneNumber, $otp)
    {
        $accountSid = config('services.twilio.TWILIO_ACCOUNT_SID');
        $authToken  = config('services.twilio.TWILIO_AUTH_TOKEN');
        $appSid     = config('services.twilio.TWILIO_APP_SID');

        $client = new Client($accountSid, $authToken);

        try {
            $client->messages->create(
                // the number you'd like to send the message to
                 $phoneNumber,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => '+14422394707',
                    // the body of the text message you'd like to send
                    'body' => 'Your one time password for authentication is ' . $otp
                )
            );
        } catch (Exception $e) {
            echo "Error: " . $e->getMessage();
        }
    }
}
