<?php

namespace App\Services;

use AWS;
use App\Models\User;

class SmsNotificationService
{
    public function sendSms($phoneNumber, $message)
    {
        if (!empty($phoneNumber) && !empty($message)) {
            $sms = AWS::createClient('sns');
    
            $response = $sms->publish([
                'Message' => $message,
                'PhoneNumber' => config('constants.country_code') . $phoneNumber,
                'MessageAttributes' => [
                    'AWS.SNS.SMS.SMSType'  => [
                        'DataType'    => 'String',
                        'StringValue' => 'Transactional',
                    ]
                ],
            ]);
        }
    }
}
