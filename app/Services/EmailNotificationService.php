<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Mail;


class EmailNotificationService
{
    public function sendEmail($email, $content, $subject)
    {
        $detail = ['subject' => $subject, 'email' => $email];
        $data = User::where('email', $email)->first();
        $logoURL = !empty($data) && $data->role_id != config('constants.roles.doctor') ? ($data->role_id != config('constants.roles.patient') ? 'site-logo.png' : 'patient-logo.png') : 'yume_logo.png';

        return Mail::send('mail', ['content' => $content, 'logoURL' => $logoURL], function ($message) use ($detail) {
            $message->to($detail['email']);
            $message->subject($detail['subject']);
        });
    }

    public function sendEmails($toEmails, $content, $subject, $ccEmails)
    {
        $logoURL = 'site-logo.png';

        return Mail::send('mail', ['content' => $content, 'logoURL' => $logoURL], function ($message) use ($toEmails, $subject, $ccEmails) {
            $message->to($toEmails)->cc($ccEmails)->subject($subject);
        });
    }
}
