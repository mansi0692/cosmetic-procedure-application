<?php

namespace App\Interfaces;


interface CustomMailInterface
{
    /**
     * listCustomMail
     *
     * @return void
     */
    public function listCustomMail();
    /**
     * getDoctorPatient
     *
     * @return void
     */
    public function getDoctorPatient();
    /**
     * sendCustomMail
     *
     * @param  mixed $dataParams
     * @return void
     */
    public function sendCustomMail($dataParams);
    /**
     * viewCustomMail
     *
     * @param  mixed $customMailId
     * @return void
     */
    public function viewCustomMail(string $customMailId);
}
