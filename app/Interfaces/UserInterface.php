<?php

namespace App\Interfaces;

interface UserInterface
{
    /**
     * findUser
     *
     * @param  mixed $userName
     * @param  mixed $roleId
     * @return void
     */
    public function findUser(string $userName,int $roleId);

    /**
     * verifyUser
     *
     * @param  mixed $userId
     * @return void
     */
    public function verifyUser(int $userId);
}
