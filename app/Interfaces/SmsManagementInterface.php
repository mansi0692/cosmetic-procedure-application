<?php

namespace App\Interfaces;


interface SmsManagementInterface
{
    /**
     * list
     *
     * @return void
     */
    public function list(int $languageId);
    /**
     * changeStatus
     *
     * @param  mixed $smsTemplateId
     * @param  mixed $status
     * @return void
     */
    public function changeStatus(int $smsTemplateId, string $status);
    /**
     * editSmsTemplate
     *
     * @param  mixed $smsTemplateId
     * @return void
     */
    public function editSmsTemplate(int $smsTemplateId);

    /**
     * update
     *
     * @param int $smsTemplateId
     * 
     * @return mixed
     */
    public function update(array $input, int $smsTemplateId);
}
