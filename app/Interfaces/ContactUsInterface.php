<?php

namespace App\Interfaces;


interface ContactUsInterface
{
    /**
     * list
     *
     * @return void
     */
    public function list();
    /**
     * getContactUs
     *
     * @param  mixed $contactUsId
     * @return void
     */
    public  function getContactUs(int $contactUsId);
    /**
     * sendReply
     *
     * @param  mixed $dataParams
     * @param  mixed $contactUsId
     * @return void
     */
    public function sendReply($dataParams,int $contactUsId);
}
