<?php

namespace App\Interfaces;


interface DoctorInterface
{
    /**
     * searchDoctorProfile
     *
     * @return void
     */
    public function searchDoctorProfile(
        int $offset,
        int $limit,
        int $currentPage,
        string $firstName,
        string $lastName,
        $state,
        int $npiNumber,
        int $doctorSearch
    );

    /**
     * findDoctorWithNpiNumber
     *
     * @param  mixed $npiNumber
     * @return void
     */
    public function findDoctorWithNpiNumber(int $npiNumber);

    /**
     * saveProfileInformation
     *
     * @param  mixed $input
     * @param  mixed $userId
     * @return void
     */
    public function saveProfileInformation(array $input, int $userId);

    /**
     * getDoctorProfileInformation
     *
     * @param  mixed $userId
     * @return void
     */
    public function getDoctorProfileInformation($userId, string $doctorData,int $patientId);

    /**
     * saveOverviewInformation
     *
     * @param  mixed $input
     * @param  mixed $userId
     * @return void
     */
    public function saveOverviewInformation(array $input, int $userId);

    /**
     * deleteProfileInformation
     *
     * @param  mixed $input
     * @param  mixed $userId
     * @return void
     */
    public function deleteProfileInformation(array $input, int $userId);

    /**
     * checkDataExists
     *
     * @param  mixed $input
     * @param  mixed $userId
     * @return void
     */
    public function checkDataExists(array $input, int $userId);

    /**
     * saveDoctorLocationData
     *
     * @param  mixed $input
     * @param  mixed $userId
     * @return void
     */
    public function saveDoctorLocationData(array $input, int $userId);

    /**
     * updateDoctorLocationData
     *
     * @param  mixed $input
     * @param  mixed $userId
     * @return void
     */
    public function updateDoctorLocationData(array $input, int $doctorLocationId);

    /**
     * getDoctorsAppointmentList
     *
     * @param  mixed $data
     * @param  integer $offset
     * @param  integer $limit
     * @param  integer $currentPage
     * @return void
     */

    public function getDoctorsAppointmentList(array $data, int $offset, int $limit, int $currentPage);

    /**
     * uploadPhotosVideos
     *
     * @param  mixed $input
     * @param  mixed $userId
     * @return void
     */
    /**
     * uploadPhotosVideos
     *
     * @param  mixed $input
     * @param  mixed $userId
     * @return void
     */
    public function uploadPhotosVideos(array $input, int $userId);
    /**
     * listDoctor
     *
     * @return void
     */
    public function listDoctor();
    /**
     * changeStatus
     *
     * @param  mixed $doctorId
     * @param  mixed $status
     * @return void
     */
    public function changeStatus(int $doctorId,$status);
    /**
     * deleteDoctor
     *
     * @param  mixed $doctorId
     * @return void
     */
    public function deleteDoctor(int $doctorId);
    /**
     * registerDoctor
     *
     * @param  mixed $doctorParams
     * @return void
     */
    public function registerDoctor($doctorParams);
    /**
     * editDoctor
     *
     * @param  mixed $doctorId
     * @return void
     */
    public function editDoctor(int $doctorId);
    /**
     * updateDoctor
     *
     * @param  mixed $doctorId
     * @param  mixed $doctorParams
     * @return void
     */
    public function updateDoctor(int $doctorId, $doctorParams);
    /**
     * approveDoctor
     *
     * @param  mixed $doctorId
     * @return void
     */
    public function approveDoctor(int $doctorId);
    /**
     * likeDoctor
     *
     * @param  mixed $doctorId
     * @return void
     */
    public function likeDoctor(int $doctorId);
    /**
     * doctorDocuments
     *
     * @param  mixed $doctorId
     * @return void
     */
    public function doctorDocuments(int $doctorId);
    /**
     * verifyDoctor
     *
     * @param  mixed $doctorId
     * @return void
     */
    public function verifyDoctor(int $doctorId);
    /**
     * getDoctorRating
     *
     * @param  mixed $doctorId
     * @return void
     */
    public function getDoctorRating(int $doctorId);

    /**
     * Create doctor favorite group
     *
     * @param array $input,
     * @param integer $patientId
     * @return void
     */
    public function addFavoriteGroup(array $input, int $patientId);

    /**
     * update doctor favorite group
     *
     * @param array $input,
     * @param integer $patientId
     * @return void
     */
    public function updateFavoriteGroup(array $input, int $patientId);

    /**
     * Delete doctor favorite group
     *
     * @param array $input
     * @return void
     */
    public function deleteFavoriteGroup(array $input);

    /**
     * Favorite doctor list
     *
     * @param integer $patientId
     * @return void
     */
    public function favoriteGroupList(int $patientId);

    /**
     * Add to favorite doctor
     *
     * @param array $input,
     * @param integer $patientId
     * @return void
     */
    public function doctorAddToFavorite(array $input, int $patient);

    /**
     * Remove doctor from favorite list
     *
     * @param array $input,
     * @param integer $patientId
     * @return void
     */
    public function doctorRemoveFromFavorite(array $input, int $patientId);

    /**
     * Favorite doctor list with group
     *
     * @param array $input,
     * @param integer $patientId
     * @return void
     */
    public function favoriteDoctorList(array $input, int $patientId);

    /**
     * Move favorite doctor to new group
     *
     * @param array $input,
     * @param integer $patientId
     * @return void
     */
    public function doctorMoveToNewGroup(array $input, int $patientId);

    /**
     * Upload registration form
     *
     * @param integer $doctorId
     * @param integer $filename
     * @return void
     */
    public function uploadRegistrationDoc(int $doctorId, $filename);

    /**
     * Get doctor appoinment slots
     *
     * @param integer $doctorId
     * @return void
     */
    public function getAppoinmentSlot(int $doctorId);

    /**
     * Get doctor appointment booking form
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function getDoctorAppointmentBookingForm(int $doctorId);

    /**
     * Appointment doctor action
     *
     * @param array $input
     * @param integer $loggedInUserId
     * @return void
     */
    public function doctorAppointmentAction(array $input, int $loggedInUserId);

    /**
     * Appointment joint action
     *
     * @param array $input
     * @param integer $loggedInUserId
     * @return void
     */
    public function joinAppointmentAction(array $input, int $loggedInUserId);

    /**
     * Save Feed
     *
     * @param array $input
     * @param integer $loggedInUserId
     * @return void
     */
    public function saveFeed(array $input, string $posterImage,int $loggedInUserId);

    /**
     * findFeedFromId
     *
     * @param  integer $feedId
     * @param  integer $loggedInUserId
     * @return void
     */
    public function findFeedFromId(int $feedId, int $loggedInUserId);
    /**
     * Update Feed
     *
     * @param array $input
     * @param integer $loggedInUserId
     * @return void
     */
    public function updateFeed(array $input, string $posterImage, int $loggedInUserId);

    /**
     * Delete Feed
     *
     * @param integer $feedId
     * @param integer $loggedInUserId
     * @return void
     */
    public function deleteFeed(int $feedId, int $loggedInUserId);

    /**
     * Feed List and Details
     *
     * @param array $input
     * @param  integer $offset
     * @param  integer $limit
     * @param  integer $currentPage
     * @return void
     */
    public function getFeedListData(array $input, int $offset, int $limit, int $currentPage);

    /**
     * Feed Like and Unlike
     *
     * @param array $input
     * @param  integer $loggedInUserId
     * @return void
     */
    public function addFeedLike(array $input, int $loggedInUserId);

    /**
     * Feed Comment
     *
     * @param array $input
     * @param  integer $loggedInUserId
     * @return void
     */
    public function addFeedComment(array $input, int $loggedInUserId);

    /**
     * Feed Follows
     *
     * @param array $input
     * @param  integer $loggedInUserId
     * @return void
     */
    public function followDoctor(array $input, int $loggedInUserId);

    /**
     * Doctor dashboard
     **
     * @param  Request $request
     * @return JsonResponse
     */
    public function doctorDashboard(int $doctorId);
    /**
     * getReviewList
     *
     * @param  mixed $input
     * @return void
     */
    public function getReviewList(array $input,int $offset, int $limit);
}
