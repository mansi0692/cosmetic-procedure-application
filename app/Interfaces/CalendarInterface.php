<?php

namespace App\Interfaces;


interface CalendarInterface
{
    /**
     * blockDoctorSlot
     *
     * @return void
     */
    public function blockDoctorSlot(array $bookingRequestData,int $doctorId);

    /**
     * manageWorkingTime
     *
     * @return void
     */
    public function manageWorkingTime(array $doctorWorkTimingRequestData,int $doctorId);

    /**
     * getWorkingTime
     *
     * @return void
     */
    public function getWorkingTime(array $doctorSlotData,int $doctorId);

    /**
     * getDoctorAppointment
     *
     * @return void
     */
    public function getDoctorAppointment(array $doctorAppointmentData,int $doctorId);

    /**
     * get Doctor Information
     *
     * @return void
     */
    public function getDoctorLocation(int $doctorId,int $locationId);
}
