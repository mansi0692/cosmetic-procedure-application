<?php

namespace App\Interfaces;

use App\Models\Appointment;

interface PatientInterface
{
    /**
     * listPatient
     *
     * @return void
     */
    public function listPatient();
    /**
     * changeStatus
     *
     * @param  mixed $patientId
     * @param  mixed $status
     * @return void
     */
    public function changeStatus($patientId, $status);
    /**
     * editPatient
     *
     * @param  mixed $patientId
     * @return void
     */
    public function editPatient($patientId);
    /**
     * updatePatient
     *
     * @param  mixed $patientId
     * @param  mixed $patientParams
     * @return void
     */
    public function updatePatient($patientId, $patientParams);
    /**
     * contactUsPatient
     *
     * @param  mixed $userId
     * @param  mixed $subject
     * @param  mixed $message
     * @param  mixed $email
     * @return void
     */
    public function contactUsPatient(int $userId, string $subject, string $message, string $email);
    /**
     * cancelPatientAccount
     *
     * @param  mixed $userId
     * @param  mixed $reason
     * @return void
     */
    public function cancelPatientAccount(int $userId, string $reason);
    /**
     * listPendingPatinetCancleRequest
     *
     * @return void
     */
    public function listPendingPatinetCancleRequest();
    /**
     * listApprovedPatinetCancleRequest
     *
     * @return void
     */
    public function listApprovedPatinetCancleRequest();
    /**
     * patientCancleRequestAppprove
     *
     * @param  mixed $patientId
     * @return void
     */
    public function patientCancleRequestAppprove(int $patientId);
    /**
     * getPatientRating
     *
     * @param  mixed $patientId
     * @return void
     */
    public function getPatientRating(int $patientId);

    /**
     * No appointment open
     *
     * @param  array $dataParams
     * @return void
     */
    public function noAppointmentSchedule(array $dataParams);

    /**
     * Get appoinment patient list
     *
     * @param  int $loggedInUserId
     * @return void
     */
    public function getAppoinmentPatientList(int $loggedInUserId);

    /**
     * Add other patient details
     *
     * @param  array $dataParams
     * @return Appointment
     */
    public function addOtherPatientDetails(array $dataParams);

    /**
     * Update other patient details
     *
     * @param  array $dataParams
     * @return Appointment
     */
    public function updateOtherPatientDetails(array $dataParams);

    /**
     * Book appoinment
     *
     * @param  array $dataParams
     * @return void
     */
    public function bookAppoinment(array $dataParams);

    /**
     * Patient bookings
     *
     * @param  integer $patientId
     * @return void
     */
    public function patientBookings(string $status, int $patientId, int $offset, int $limit, int $currentPage);

    /**
     * Add doctor review
     **
     * @param  Request $request
     * @return void
     */
    public function addDoctorReview(array $dataParams);

    /**
     * Get doctor review
     **
     * @param  Request $request
     * @return void
     */
    public function getDoctorReview(string $bookingId);

    /**
     * Patient Reschedule Bookings
     *
     * @param  array $patientRescheduleBookingData,integer $loginUserId
     * @return void
     */
    public function patientRescheduleBookings(array $patientRescheduleBookingData, int $loginUserId);

    /**
     * Accept/reject rescheduled request
     *
     * @param  array $bookingData,integer $userId,boolean $isReschedule
     * @return void
     */
    public function rescheduleBookingStatus(array $bookingData, int $userId);

    /**
     * Cancel appointment
     *
     * @param  array $appointmentDetails,integer $userId
     * @return void
     */
    public function cancelBooking(array $appointmentDetails, int $userId);

    /**
     * Book appoinment new
     *
     * @param  array $dataParams
     * @return void
     */
    public function bookAppoinmentNew(array $dataParams);

    /**
     * Join wait list
     *
     * @param  collection $dataParams
     * @return void
     */
    public function joinWaitList($dataParams);

    /**
     * Feed List and Details
     *
     * @param array $input
     * @param  integer $offset
     * @param  integer $limit
     * @param  integer $currentPage
     * @return void
     */
    public function getFeedListData(array $input, int $offset, int $limit, int $currentPage);
}
