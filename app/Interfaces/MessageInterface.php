<?php

namespace App\Interfaces;


interface MessageInterface
{
    /**
     * store
     *
     * @param  mixed $input
     * @return void
     */
    public function store(array $input);

    /**
     * getMessages
     *
     * @param  mixed $loggedInUserId
     * @param  mixed $fromUserId
     * @return void
     */
    public function getMessages(int $loggedInUserId, int $fromUserId);

    /**
     * sendChatRequest
     *
     * @param  mixed $input
     * @return void
     */
    public function sendChatRequest(array $input);

    /**
     * chatRequestList
     *
     * @param  mixed $loggedInUserId
     * @return void
     */
    public function chatRequestList(int $loggedInUserId);

    /**
     * deleteChat
     *
     * @param  int $patientId, $doctorId
     * @return void
     */
    public function deleteChat(int $patientId, int $doctorId);

    /**
     * readAllMessages
     *
     * @param  mixed $loggedInUserId
     * @param  mixed $fromUserId
     * @return void
     */
    public function readAllMessages(int $loggedInUserId, int $fromUserId);
}
