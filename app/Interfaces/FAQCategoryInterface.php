<?php

namespace App\Interfaces;


interface FAQCategoryInterface
{
    /**
     * list
     *
     * @param int $languageId
     *
     * @return mixed
     */
    public function list(int $languageId);

    /**
     * edit
     *
     * @param int $categoryId
     * @param int $languageId
     *
     * @return mixed
     */
    public function edit(int $categoryId, int $languageId);

    /**
     * update
     *
     * @param array $input
     * @param int $categoryId
     * @param int $languageId
     *
     * @return mixed
     */
    public function update(array $input, int $categoryId, int $languageId);

    /**
     * getAllCategories
     *
     * @return void
     */
    public function getAllCategories();
}
