<?php

namespace App\Interfaces;


interface DoctorInviteInterface
{
    /**
     * list
     *
     * @return void
     */
    public function list($tabNumber);
    /**
     * sendInvitation
     *
     * @param  mixed $email
     * @return void
     */
    public function sendInvitation(string $email);

    public function requestAdminToInviteDoctor(int $doctorId);
}
