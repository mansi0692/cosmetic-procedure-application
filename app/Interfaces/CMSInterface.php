<?php

namespace App\Interfaces;


interface CMSInterface
{
    /**
     * list
     *
     * @return void
     */
    public function list(int $languageId);
    /**
     * saveCMS
     *
     * @param  mixed $dataParams
     * @return void
     */
    public function saveCMS($dataParams);
    /**
     * editCMS
     *
     * @param  mixed $cmsPageId
     * @return void
     */
    public function editCMS(int $cmsPageId);
    /**
     * updateCMS
     *
     * @param  mixed $dataParams
     * @param  mixed $cmsPageId
     * @return void
     */
    public function updateCMS($dataParams,int $cmsPageId);
    /**
     * changeStatus
     *
     * @param  mixed $cmsPageId
     * @param  mixed $status
     * @return void
     */
    public function changeStatus(int $cmsPageId,string $status);
    /**
     * deleteCMS
     *
     * @param  mixed $cmsPageId
     * @return void
     */
    public function deleteCMS(int $cmsPageId);
}
