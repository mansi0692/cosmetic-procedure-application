<?php

namespace App\Interfaces;

interface MasterDataInterface
{
    /**
     * getAllCountries
     *
     * @return void
     */
    public function getAllCountries();

    /**
     * getCitiesFromState
     *
     * @param  mixed $stateId
     * @return void
     */
    public function getCitiesFromState(int $stateId);

    /**
     * getAllStates
     *
     * @return void
     */
    public function getAllStates();

    /**
     * getCityStateId
     *
     * @return void
     */
    public function getCityStateId($cityName, $stateName);

    /**
     * getCategoriesFromType
     *
     * @param  mixed $type
     * @return void
     */
    public function getCategoriesFromType($type);

    /**
     * getSymptoms
     *
     * @param  mixed $type
     * @return void
     */
    public function getSymptoms($specialtyId);
}
