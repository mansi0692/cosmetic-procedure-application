<?php

namespace App\Interfaces;


interface EmailManagementInterface
{
    /**
     * list
     *
     * @return void
     */
    public function list(int $languageId);
    /**
     * changeStatus
     *
     * @param  mixed $emailTemplateId
     * @param  mixed $status
     * @return void
     */
    public function changeStatus(int $emailTemplateId, string $status);
    /**
     * editEmailTemplate
     *
     * @param  mixed $emailTemplateId
     * @return void
     */
    public function editEmailTemplate(int $emailTemplateId);

    /**
     * update
     *
     * @param int $emailTemplateId
     * 
     * @return mixed
     */
    public function update(array $input, int $emailTemplateId);
}
