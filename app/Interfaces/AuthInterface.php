<?php

namespace App\Interfaces;

interface AuthInterface
{
    /**
     * registerPatient
     *
     * @param  mixed $input
     * @return void
     */
    public function registerPatient(array $input, array $deviceData);

    /**
     * sendOtpToUser
     *
     * @param  mixed $userId
     * @return void
     */
    public function sendOtpToUser(object $user,bool $isPhoneNumber);

    /**
     * verifyOtp
     *
     * @param  mixed $userId
     * @param  mixed $otp
     * @return void
     */
    public function verifyOtp(int $userId,int $otp);

    /**
     * lastOtpSentTime
     *
     * @param  mixed $userId
     * @return void
     */
    public function lastOtpSentTime(int $userId);

    /**
     * resetOtpCount
     *
     * @param  mixed $userId
     * @return void
     */
    public function resetOtpCount(int $userId);

    /**
     * login
     *
     * @param  mixed $emailOrPhone
     * @param  mixed $password
     * @return void
     */
    public function login(string $emailOrPhone,string $password,int $roleId, array $deviceData);

    /**
     * updateNewPassword
     **
     * @param  mixed $userId
     * @param  mixed $newPassword
     * @return void
     */
    public function updateNewPassword(int $userId,string $newPassword);

    /**
     * getPatientInformation
     *
     * @param  mixed $userId
     * @return void
     */
    public function getPatientInformation(int $userId, array $deviceData);

    /**
     * getDoctorInformation
     *
     * @param  mixed $userId
     * @return void
     */
    public function getDoctorInformation(int $userId);

    /**
     * checkUserExistByEmailOrToken
     *
     * @param  mixed $email
     * @param  mixed $token
     * @return void
     */
    public function checkUserExistByEmailOrToken(string $email,string $token);

    /**
     * logout
     *
     * @param array $input
     * @param int $loggedInUserId
     * 
     * @return mixed
     */
    public function logout(array $input,int $loggedInUserId);
}
