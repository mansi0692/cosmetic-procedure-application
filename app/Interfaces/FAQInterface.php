<?php

namespace App\Interfaces;


interface FAQInterface
{
    /**
     * list
     *
     * @return void
     */
    public function list(int $faqCategoryId, int $languageId);

    /**
     * changeStatus
     *
     * @param string $status
     * @param int $faqCategoryId
     * 
     * @return mixed
     */
    public function changeStatus(string $status, int $faqCategoryId);

    /**
     * edit
     *
     * @param int $faqsId
     * @param int $faqCategoryId
     * @param int $languageId
     * 
     * @return mixed
     */
    public function edit(int $faqsId, int $faqCategoryId, int $languageId);

    /**
     * store
     *
     * @param array $input
     * @param int $faqsId
     * @param int $languageId
     * 
     * @return mixed
     */
    public function store(array $input, int $faqsId, int $languageId);

    /**
     * update
     *
     * @param array $input
     * @param int $faqsId
     * @param int $faqCategoryId
     * @param int $languageId
     * 
     * @return mixed
     */
    public function update(array $input, int $faqsId, int $faqCategoryId, int $languageId);
    
    /**
     * destroy
     *
     * @param int $faqsId
     * 
     * @return mixed
     */
    public function destroy(int $faqsId);
    
    /**
     * getFAQList
     *
     * @param int $faqCategoryId
     * 
     * @return mixed
     */
    public function getFAQList(int $faqCategoryId);
}
