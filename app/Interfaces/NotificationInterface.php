<?php

namespace App\Interfaces;


interface NotificationInterface
{    
    /**
     * getAllNotifications
     *
     * @param int $faqCategoryId
     * 
     * @return mixed
     */
    public function getAllNotifications(int $faqCategoryId);
    
    /**
     * updateNotificationStatus
     *
     * @param array $input
     * @param int $loggedInUserId
     * 
     * @return mixed
     */
    public function updateNotificationStatus(array $input,int $loggedInUserId);
}
