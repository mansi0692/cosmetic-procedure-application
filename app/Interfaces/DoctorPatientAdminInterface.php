<?php

namespace App\Interfaces;


interface DoctorPatientAdminInterface
{
    /**
     * listDoctorAdmin
     *
     * @return void
     */
    public function listDoctorAdmin();
    /**
     * listPatientAdmin
     *
     * @return void
     */
    public function listPatientAdmin();
    /**
     * saveDoctorPatientAdmin
     *
     * @param  mixed $adminData
     * @return void
     */
    public function saveDoctorPatientAdmin($adminData);
    /**
     * editDoctorPatientAdmin
     *
     * @param  mixed $adminId
     * @return void
     */
    public function editDoctorPatientAdmin(int $adminId);
    /**
     * updateDoctorPatientAdmin
     *
     * @param  mixed $adminData
     * @param  mixed $adminId
     * @return void
     */
    public function updateDoctorPatientAdmin($adminData,int $adminId);
    /**
     * changeStatus
     *
     * @param  mixed $adminId
     * @param  mixed $status
     * @return void
     */
    public function changeStatus(int $adminId,string $status);
    /**
     * deleteAdmin
     *
     * @param  mixed $adminId
     * @return void
     */
    public function deleteAdmin(int $adminId);
}
