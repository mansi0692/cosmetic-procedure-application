<?php

use App\Models\EmailTemplate;
use App\Models\SmsTemplate;
use App\Models\NotificationTemplate;
use App\Models\Language;
use App\Models\LogActivity;
use App\Models\ErrorLog;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;

/**
 * generateRandomSixDigitNumber
 *
 * @return string
 */
function generateRandomSixDigitNumber(): string
{
    return sprintf("%06d", mt_rand(1, 999999));
}

/**
 * getDoctorAndPatientRoleIds
 *
 * @return array
 */
function getDoctorAndPatientRoleIds(): array
{
    return [
        config('constants.roles.doctor'),
        config('constants.roles.patient')
    ];
}
function uploadImage($image, $userId, $type, $forChat = false)
{
    $name = $image->getClientOriginalName(); //file.jpg
    $file_name = pathinfo($name, PATHINFO_FILENAME); // file
    $extension = strtolower(pathinfo($name, PATHINFO_EXTENSION)); // jpg
    $imageName = $file_name . '_' . time() . '.' . $extension;

    if (config('app.env') == 'local' || config('app.env') == 'staging') {
        $image->storeAs('public/uploads', $imageName);
    } else {
        $folderPath = config('constants.s3.uploadUrl') . $userId . '/';
        $filePath = $forChat ? $folderPath . 'chat/' . $imageName : $folderPath . $imageName;

        Storage::disk('s3')->put($filePath, file_get_contents($image), 'public');
    }
    return $imageName;
}

function getImage($file, $userId = '', $forChat = false)
{
    if (config('app.env') == 'local' || config('app.env') == 'staging') {
        return Storage::exists('public/uploads/' . $file) ? asset('storage/uploads/' . $file) : '';
    } else {
        $folderPath = config('constants.get_file_path') . $userId . '/';
        $filePath =  $forChat ? $folderPath . 'chat/' . $file : $folderPath . $file;
        // return Storage::disk('s3')->url($path);

        return $filePath;
    }
}

function getDayName($requestDay)
{
    $doctorDays = config('constants.days');
    $dayName = array_search($requestDay, $doctorDays);
    return $dayName;
}

function getFullDay($requestDay)
{
    $doctorDays = config('constants.days');
    $dayName = $doctorDays[$requestDay];
    return $dayName;
}

function randomSecure($min, $max)
{
    $range = $max - $min;
    if ($range < 1) return $min;
    $log = ceil(log($range, 2));
    $bytes = (int) ($log / 8) + 1;
    $bits = (int) $log + 1;
    $filter = (int) (1 << $bits) - 1;
    do {
        $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
        $rnd = $rnd & $filter;
    } while ($rnd > $range);
    return $min + $rnd;
}

function getBookingId($length)
{
    $token = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet .= "0123456789";
    $max = strlen($codeAlphabet);

    for ($i = 0; $i < $length; $i++) {
        $token .= $codeAlphabet[randomSecure(0, $max - 1)];
    }

    return "#" . $token;
}

function getTimeAgo($carbonObject)
{
    return str_ireplace(
        [' seconds', ' second', ' minutes', ' minute', ' hours', ' hour', ' days', ' day', ' weeks', ' week'],
        ['s', 's', 'm', 'm', 'h', 'h', 'd', 'd', 'w', 'w'],
        $carbonObject->diffForHumans()
    );
}

function date_compare($element1, $element2)
{
    $datetime1 = strtotime($element1['most_recent_message_date']);
    $datetime2 = strtotime($element2['most_recent_message_date']);
    return $datetime2 - $datetime1;
}

function getEmailTemplateData($templateName)
{
    $languageId = getLanguageId(app()->getLocale());
    return EmailTemplate::where([
        'language_id' => $languageId,
        'template_name' => $templateName,
        'status' => 1
    ])->first();
}

function getSmsTemplateData($templateName)
{
    $languageId = getLanguageId(app()->getLocale());
    $smsTemplate = SmsTemplate::where([
        'language_id' => $languageId,
        'template_name' => $templateName,
        'status' => 1
    ])->first();

    return $smsTemplate ? $smsTemplate->value('message') : '';
}

function getNotificationTemplateData($templateName)
{
    $languageId = getLanguageId(app()->getLocale());
    $notificationTemplate = NotificationTemplate::where([
        'language_id' => $languageId,
        'template_name' => $templateName,
        'status' => 1
    ])->first();

    return $notificationTemplate ? $notificationTemplate->message : '';
}

function getLanguageId($locale)
{
    $language = Language::where('code', $locale)->first();
    if (!empty($language)) {
        return $language->id;
    }

    return 1; //Default English Language
}

function getLanguage($id)
{
    return Language::where('id', $id)->first()->title;
}

/**
 * transformWorkingTimeSlots
 *
 * @author Heli Patel
 * @param  string $interval, string $start_time,string $end_time,Object $workTimingData
 * @return array
 */
function getTimeSlot(Object $workTimingDataArray, $doctorBlockSlotArray): array
{
    $daysArray = [];
    $start_time = Null;
    $end_time = Null;
    $data = [];

    foreach ($workTimingDataArray as $day => $workTimingData) {

        $workTimeArray = [];
        foreach ($workTimingData as $key => $workTiming) {

            $appointmentDuration = !empty($workTiming->appointment_duration) ? $workTiming->appointment_duration : Null;
            $start_time = !empty($workTiming->start_time) ? Carbon::parse($workTiming->start_time)->format('H:i') : Null;
            $end_time = !empty($workTiming->end_time) ? Carbon::parse($workTiming->end_time)->format('H:i') : Null;

            $start = new DateTime($start_time);
            $end = new DateTime($end_time);
            $startTime = $start->format('H:i');
            $endTime = $end->format('H:i');
            $time = [];
            $i = 0;

            while (strtotime($startTime) <= strtotime($endTime)) {
                $start = $startTime;
                $end = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                $startTime = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                $i++;

                if (!empty($doctorBlockSlotArray)) {

                    foreach ($doctorBlockSlotArray as $blockslot) {

                        if (strtotime($startTime) <= strtotime($endTime) && $start == $blockslot['start_time'] && $end == $blockslot['end_time'] && !in_array($workTiming->doctor_id, $data)) {
                            $data[] = $workTiming->doctor_id;
                        }
                    }
                }

                if (strtotime($startTime) <= strtotime($endTime) && !in_array($workTiming->doctor_id, $data)) {

                    if (!empty($data)) {
                        array_push($data, $workTiming->doctor_id);
                    } else {
                        $data[] = $workTiming->doctor_id;
                    }
                }
            }
        }
    }
    return $data;
}

function addLog($subject)
{
    $log = array();
    $log['subject'] = $subject;
    $log['url'] = Request::fullUrl();
    $log['method'] = Request::method();
    $log['ip'] = Request::ip();
    $log['agent'] = Request::header('user-agent');
    if (auth()->check()) { $log['user_id'] = auth()->user()->id; }

    LogActivity::create($log);
}

function addErrorLog($fnName, $errMsg)
{
    $log = array();
    if (auth()->check()) { $log['user_id'] = auth()->user()->id; }
    $log['function'] = $fnName;
    $log['error_message'] = $errMsg;
    $log['url'] = Request::fullUrl();
    $log['ip'] = Request::ip();
    $log['agent'] = Request::header('user-agent');
    
    ErrorLog::create($log);
}
