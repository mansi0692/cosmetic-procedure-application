<?php

namespace App\Repositories;

use App\Events\PushNotification;
use Carbon\Carbon;
use App\Models\City;
use App\Models\Doctor;
use App\Models\DoctorInformation;
use App\Models\Appointment;
use App\Models\DoctorLocation;
use App\Models\DoctorMedia;
use App\Models\MasterCategory;
use App\Models\MasterSubCategory;
use App\Models\ModelHasRole;
use App\Models\PatientInformation;
use App\Models\Role;
use App\Models\User;
use App\Models\State;
use App\Models\NewsFeed;
use App\Interfaces\DoctorInterface;
use App\Models\AppointmentPatientDetail;
use App\Models\DoctorBlockSlot;
use App\Models\DoctorFavorite;
use App\Models\DoctorFavoriteGroup;
use App\Models\DoctorOverview;
use App\Models\DoctorReview;
use App\Models\DoctorTiming;
use App\Models\FeedComment;
use App\Models\DoctorFollow;
use App\Models\FeedLike;
use App\Models\Notification;
use App\Models\TopSpeciality;
use App\Services\EmailNotificationService;
use App\Services\SmsNotificationService;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use stdClass;
use App\Traits\NotificationTrait;
use Carbon\CarbonPeriod;

class DoctorRepository implements DoctorInterface
{
    use NotificationTrait;

    /**
     * doctorTiming
     *
     * @var mixed
     */
    protected $doctorTiming;
    /**
     * city
     *
     * @var mixed
     */
    protected $city;
    /**
     * doctor
     *
     * @var mixed
     */
    protected $doctor;
    /**
     * doctorInformation
     *
     * @var mixed
     */
    protected $doctorInformation;
    /**
     * appointment
     *
     * @var mixed
     */
    protected $appointment;
    /**
     * doctorLocation
     *
     * @var mixed
     */
    protected $doctorLocation;
    /**
     * doctorMedia
     *
     * @var mixed
     */
    protected $doctorMedia;
    /**
     * masterCategory
     *
     * @var mixed
     */
    protected $masterCategory;
    /**
     * masterSubCategory
     *
     * @var mixed
     */
    protected $masterSubCategory;
    /**
     * modelHasRole
     *
     * @var mixed
     */
    /**
     * modelHasRole
     *
     * @var mixed
     */
    protected $modelHasRole;
    /**
     * patientInformation
     *
     * @var mixed
     */
    protected $patientInformation;
    /**
     * role
     *
     * @var mixed
     */
    protected $role;
    /**
     * user
     *
     * @var mixed
     */
    protected $user;
    /**
     * state
     *
     * @var mixed
     */
    protected $state;
    /**
     * newsFeed
     *
     * @var mixed
     */
    protected $newsFeed;
    /**
     * appointmentPatientDetail
     *
     * @var mixed
     */
    protected $appointmentPatientDetail;
    /**
     * doctorBlockSlot
     *
     * @var mixed
     */
    protected $doctorBlockSlot;
    /**
     * doctorFavorite
     *
     * @var mixed
     */
    protected $doctorFavorite;
    /**
     * doctorFavoriteGroup
     *
     * @var mixed
     */
    protected $doctorFavoriteGroup;
    /**
     * doctorOverview
     *
     * @var mixed
     */
    protected $doctorOverview;
    /**
     * doctorReview
     *
     * @var mixed
     */
    protected $doctorReview;
    /**
     * feedComment
     *
     * @var mixed
     */
    protected $feedComment;
    /**
     * doctorFollow
     *
     * @var mixed
     */
    protected $doctorFollow;
    /**
     * feedLike
     *
     * @var mixed
     */
    protected $feedLike;
    protected $notification;

    public function __construct(
        DoctorTiming $doctorTiming,
        City $city,
        Doctor $doctor,
        DoctorInformation $doctorInformation,
        Appointment $appointment,
        DoctorLocation $doctorLocation,
        DoctorMedia $doctorMedia,
        MasterCategory $masterCategory,
        MasterSubCategory $masterSubCategory,
        ModelHasRole $modelHasRole,
        PatientInformation $patientInformation,
        Role $role,
        User $user,
        State $state,
        NewsFeed $newsFeed,
        AppointmentPatientDetail $appointmentPatientDetail,
        DoctorBlockSlot $doctorBlockSlot,
        DoctorFavorite $doctorFavorite,
        DoctorFavoriteGroup $doctorFavoriteGroup,
        DoctorOverview $doctorOverview,
        DoctorReview $doctorReview,
        FeedComment $feedComment,
        DoctorFollow $doctorFollow,
        FeedLike $feedLike,
        Notification $notification,
        SmsNotificationService $smsNotification,
        TopSpeciality $topSpeciality,
    ) {
        $this->doctorTiming = $doctorTiming;
        $this->city = $city;
        $this->doctor = $doctor;
        $this->doctorInformation = $doctorInformation;
        $this->appointment = $appointment;
        $this->doctorLocation = $doctorLocation;
        $this->doctorMedia = $doctorMedia;
        $this->masterCategory = $masterCategory;
        $this->masterSubCategory = $masterSubCategory;
        $this->modelHasRole = $modelHasRole;
        $this->patientInformation = $patientInformation;
        $this->role = $role;
        $this->user = $user;
        $this->state = $state;
        $this->newsFeed = $newsFeed;
        $this->appointmentPatientDetail = $appointmentPatientDetail;
        $this->doctorBlockSlot = $doctorBlockSlot;
        $this->doctorFavorite = $doctorFavorite;
        $this->doctorFavoriteGroup = $doctorFavoriteGroup;
        $this->doctorOverview = $doctorOverview;
        $this->doctorReview = $doctorReview;
        $this->feedComment = $feedComment;
        $this->doctorFollow = $doctorFollow;
        $this->feedLike = $feedLike;
        $this->notification = $notification;
        $this->smsNotification = $smsNotification;
        $this->topSpeciality = $topSpeciality;
    }

    /**
     * searchDoctorProfile
     * @param  integer $offset
     * @param  integer $limit
     * @param  integer $limit
     * @param string $firstName
     * @param string $lastName
     * @param string $state
     * @param integer $npiNumber
     * @param integer $id
     * @return Collection
     */
    public function searchDoctorProfile(
        $offset,
        $limit,
        $currentPage,
        $firstName,
        $lastName,
        $state,
        $npiNumber,
        $doctorSearch
    ) {
        if ($doctorSearch) {
            $doctors = $this->doctor::where('npi_number', $npiNumber)->first();
            $doctorInformation = $this->doctorInformation::where('npi_number', $npiNumber)->first();
            if (isset($doctorInformation)) {
                $doctors->profile_image = $doctorInformation->profile_image;
            }
            return !empty($doctors) ? $doctors->toArray() : [];
        }
        $doctors = $this->doctor::select('*');

        if (!empty($firstName)) {
            $doctors = $doctors->where(DB::raw('lower(first_name)'), "LIKE", "%" . strtolower($firstName) . "%");
        }
        if (!empty($lastName)) {
            $doctors = $doctors->where(DB::raw('lower(last_name)'), "LIKE", "%" . strtolower($lastName) . "%");
        }
        if (!empty($npiNumber)) {
            $doctors = $doctors->where('npi_number', 'LIKE', '%' . $npiNumber . '%');
        }
        if (!empty($state) && $state != "all") {
            $findState = $this->state::where('name', $state)->first();
            $doctors = $doctors->where('state', $findState->code);
        }

        $totalCount = $doctors->get()->count();
        $doctors = $doctors->skip($offset)->take($limit);
        $doctors = $doctors->get();

        $states = $this->state::get();
        $i = 0;
        foreach ($doctors as $doctorData) {
            foreach ($states as $stateData) {
                if ($stateData['code'] == $doctorData['state']) {
                    $doctors[$i]['state'] = $stateData['name'];
                    $doctors[$i]['state_code'] = $stateData['code'];
                    $doctors[$i]['profile_image'] = '';
                    break;
                }
            }
            $i++;
        }
        $paginator = new Paginator($doctors, $totalCount, $limit, $currentPage);
        return $paginator;
    }

    /**
     * findDoctorWithNpiNumber
     * @param  integer $npiNumber
     * @return Collection
     */
    public function findDoctorWithNpiNumber($npiNumber)
    {
        $doctorInformation = $this->doctorInformation::join('users as user', 'user.id', 'doctor_information.user_id')
            ->where('doctor_information.npi_number', $npiNumber)
            ->select('doctor_information.*', 'user.email as email', DB::raw('DATE_FORMAT(doctor_information.created_at, "%d %b %Y") as joining_date'))->first();

        if (isset($doctorInformation)) {
            return $doctorInformation->toArray();
        } else {
            return [];
        }
    }

    /**
     * listDoctor
     * @return array
     */
    public function listDoctor(): array
    {
        $doctorData = [];
        $doctors = $this->doctorInformation::leftJoin('users as user', 'user.id', 'doctor_information.user_id')
            ->leftJoin('master_sub_categories as master_occ', 'master_occ.id', 'doctor_information.occupation_id')
            ->leftJoin('master_sub_categories as master_spe', 'master_spe.id', 'doctor_information.specialty_id')
            ->select(
                'user.email as email',
                'user.id as id',
                'doctor_information.first_name as first_name',
                'doctor_information.last_name as last_name',
                'doctor_information.is_featured as is_featured',
                'doctor_information.is_document_verified as is_document_verified',
                'master_occ.label as occupation',
                'master_spe.label as speciality',
                'user.status as status',
                'user.is_verified as is_verified'
            )
            ->orderBy('user.created_at', 'DESC')
            ->get();

        $doctorFavorites = $this->doctorFavorite::where('patient_id', Auth::user()->id)->get();
        $likeArray = [];
        if (isset($doctorFavorites)) {
            foreach ($doctorFavorites as $favorite) {
                array_push($likeArray, $favorite->doctor_id);
            }
        }
        $doctorData['doctors'] = $doctors;
        $doctorData['doctorLikes'] = $likeArray;
        return $doctorData;
    }

    /**
     * changeStatus
     * @return bool
     */
    public function changeStatus($doctorId, $status): bool
    {
        $userStatus = $this->user::find($doctorId);
        $doctorName = $this->doctorInformation::where('user_id', $doctorId)->first();

        if ($status == 'active') {
            $userStatus->status = 0;
            $userStatusString = "Inactive";
        } else {
            $userStatus->status = 1;
            $userStatusString = "Active";
        }

        $emailTemplateData = getEmailTemplateData('account_status_change_notification');
        $resultMainArray = array("[status]", "[first_name]");
        $resultReplaceArray = array(ucfirst($userStatusString), $doctorName->first_name);
        $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);

        $mailNotification = new EmailNotificationService();
        $content = $emailMiddleBody;
        $subject = $emailTemplateData->template_subject;
        $mailNotification->sendEmail($userStatus->email, $content, $subject);
        return $userStatus->save();
    }

    /**
     * deleteDoctor
     * @return bool
     */
    public function deleteDoctor($doctorId): bool
    {
        $deleteDoctor = $this->doctorInformation::where('user_id', $doctorId)->first();
        $deleteDoctor->deleted_by = Auth::user()->id;
        $deleteDoctor->delete();

        $user = $this->user::find($doctorId);
        $user->deleted_by = Auth::user()->id;
        $user->delete();
        $user->save();
        return $deleteDoctor->save();
    }

    public function registerDoctor($doctorParams): int
    {
        return DB::transaction(
            function () use ($doctorParams) {
                $usertableInfo = $this->user;
                $role = $this->role::where('id', config('constants.roles.doctor'))->first();
                $usertableInfo->email = $doctorParams->email;
                $usertableInfo->role_id = $role->id;
                $usertableInfo->password = Hash::make($doctorParams->password);
                $usertableInfo->role_id = config('constants.roles.doctor');
                $usertableInfo->save();

                $modelHasRole = $this->modelHasRole;
                $modelHasRole->role_id = $role->id;
                $modelHasRole->model_id = $usertableInfo->id;
                $modelHasRole->save();

                $doctor = $this->doctor::where('npi_number', $doctorParams->npi_number)->first();

                if ($doctor) {
                    $doctorInformation = $this->doctorInformation;
                    $doctorInformation->user_id = $usertableInfo->id;
                    $doctorInformation->npi_number = $doctor->npi_number;
                    $doctorInformation->state_id = $doctorParams->state;
                    $doctorInformation->city_id = $doctorParams->city;
                    $doctorInformation->date_of_birth = $doctorParams->birth_date;                    
                    $doctorInformation->first_name = $doctor->first_name;
                    $doctorInformation->last_name = $doctor->last_name;
                    $doctorInformation->gender = ($doctor->gender == 'M') ? 1 : 2;
                    $doctorInformation->specialty_id = $doctor->specialties[0] ? $this->masterSubCategory::where('label', $doctor->specialties[0])->first()->id ?? NULL : NULL;
                    $doctorInformation->in_person = 1;
                    $doctorInformation->video_consultancy = 1;
                    $doctorInformation->created_by = $usertableInfo->id;
                    $doctorInformation->save();

                    $doctor->is_registered = 1;
                    $doctor->save();
                } else {
                    $doctorInformation = $this->doctorInformation;
                    $doctorInformation->user_id = $usertableInfo->id;
                    $doctorInformation->first_name = $doctorParams->first_name;
                    $doctorInformation->last_name = $doctorParams->last_name;
                    $doctorInformation->npi_number = $doctorParams->npi_number;
                    $doctorInformation->occupation_id = $this->masterSubCategory::where('label', $doctorParams->occupation)->first()->id ?? NULL;
                    $doctorInformation->specialty_id = $this->masterSubCategory::where('label', $doctorParams->speciality)->first()->id ?? NULL;
                    $doctorInformation->office_phone_number = $doctorParams->office_phone;
                    $doctorInformation->office_fax = $doctorParams->office_fax;
                    $doctorInformation->date_of_birth = $doctorParams->birth_date;
                    $doctorInformation->state_id = $doctorParams->state;
                    $doctorInformation->city_id = $doctorParams->city;
                    $doctorInformation->in_person = 1;
                    $doctorInformation->video_consultancy = 1;
                    $doctorInformation->created_by = $usertableInfo->id;
                    $doctorInformation->state_of_office = $doctorParams->office_state;
                    $doctorInformation->city_of_profession = $doctorParams->office_city;
                    $doctorInformation->save();

                    $doctorLocation = $this->doctorLocation;
                    $doctorLocation->doctor_id = $usertableInfo->id;
                    $doctorLocation->address = $doctorParams->street_address;
                    $doctorLocation->city_id = $doctorParams->office_city;
                    $doctorLocation->location_email = $doctorParams->location_email;
                    $doctorLocation->state_id = $doctorParams->office_state;
                    $doctorLocation->zipcode = $doctorParams->zip_code;
                    $doctorLocation->latitude = $doctorParams->latitude;
                    $doctorLocation->longitude = $doctorParams->longitude;
                    $doctorLocation->save();
                }
                
                if ($usertableInfo->save() && $modelHasRole->save() && $doctorInformation->save()) {
                    $emailTemplateData = getEmailTemplateData('doctor_registration');
                    $resultMainArray = array("[doctorName]");
                    $resultReplaceArray = array($doctorInformation->first_name . " " . $doctorInformation->last_name);
                    $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);
                    
                    $mailNotification = new EmailNotificationService();
                    $mailNotification->sendEmail($doctorParams->email, $emailMiddleBody, $emailTemplateData->template_subject);

                    return 1;
                } else {
                    return 0;
                }
            }
        );
    }

    public function saveProfileInformation($input, $userId)
    {
        return $this->doctorInformation::where('user_id', $userId)->update($input);
    }

    public function getDoctorProfileInformation($userId, $doctorData = '', $patientId = null)
    {
        $doctorInformationFields = [
            'users.id as user_id', 'doctor_information.id as doctor_id', 'users.is_verified', 'doctor_information.is_document_verified',
            'users.email', 'users.country_code', 'users.phone_number', 'doctor_information.first_name', 'doctor_information.last_name', 'doctor_information.gender', 'in_person', 'video_consultancy',
            'doctor_information.profile_image', 'occupation_id', 'specialty_id', 'symptom_id', 'dl.office_phone_number', 'master_sub_categories.label as occupation_name', 'm.label as speciality_name', 'dl.office_fax',
            'date_of_birth', 'doctor_information.state_id', 'about', 'dl.appointment_duration', 'is_featured', 'procedure_id', 'states.name as state_name', 'dl.address', 'doc.name_prefix', 'doctor_information.registration_form','doctor_information.medical_school as medical_school', 'doctor_information.grad_year as grad_year',
        ];

        if (!empty($doctorData['current_coordinates'])) {
            $currentCoordinates = explode(",", $doctorData['current_coordinates']);

            array_push($doctorInformationFields, DB::raw("3959 * acos(cos(radians(" . $currentCoordinates[0] . "))
            * cos(radians(dl.latitude))
            * cos(radians(dl.longitude) - radians(" . $currentCoordinates[1] . "))
            * sin(radians(dl.latitude))) AS distance"));
        }

        $doctorReviewData = $this->doctorReview::where('doctor_id', $userId);
        $doctorAllReviewData = $doctorReviewData->get();
        $doctorReviewDataCount = $doctorReviewData->get()->count();

        $doctorLatestReviewData = $doctorReviewData->first();

        if (!empty($patientId))
            $doctorFavoriteGroupdata = $this->doctorFavorite::where('doctor_id', $userId)->where('created_by', $patientId)->first();

        $profileInformation = $this->user::join('doctor_information', 'users.id', '=', 'doctor_information.user_id')
            ->leftJoin('states', 'states.id', '=', 'doctor_information.state_id')
            ->leftJoin('master_sub_categories', 'doctor_information.occupation_id', '=', 'master_sub_categories.id')
            ->leftJoin('master_sub_categories as m', 'doctor_information.specialty_id', '=', 'm.id')
            ->leftJoin('doctor_locations as dl', 'dl.doctor_id', 'doctor_information.user_id')
            ->leftJoin('doctors as doc', 'doc.npi_number', 'doctor_information.npi_number')
            ->select($doctorInformationFields)
            ->where('users.id', $userId);

        if (!empty($doctorData['location_id']))
            $profileInformation = $profileInformation->where('dl.id', $doctorData['location_id'])->first();
        else
            $profileInformation = $profileInformation->first();

        if (!is_null($profileInformation)) {
            $doctorId = $profileInformation['doctor_id'];

            $profileInformation->is_favorite = !empty($doctorFavoriteGroupdata) ? 1 : 0;
            $profileInformation['symptom'] = $this->getSymptoms($profileInformation['symptom_id']);
            $profileInformation['procedure'] = $this->getProcedure($profileInformation['procedure_id']);
            $profileInformation->overview = $this->getOverViewInformation($userId);
            $profileInformation->location = $this->getLocations($userId);
            $profileInformation->media = $this->getMedias($userId);
            $profileInformation->review = $doctorAllReviewData;
            $profileInformation->latest_review = $doctorLatestReviewData;
            $profileInformation->reviewCount = $doctorReviewDataCount;
            $profileInformation->document = $this->getDocuments($userId);
        }
        return $profileInformation;
    }

    /**
     * Save doctor location
     *
     * @param stdClass $doctorLocationData, $userId
     * @return int
     */
    public function saveDoctorLocationData(array $doctorLocationData, $userId)
    {
        return DB::transaction(
            function () use ($doctorLocationData, $userId) {
                // $getCoordinatesData = explode(",", $doctorLocationData['coordinates']);

                $doctorLocation = $this->doctorLocation;
                $doctorLocation->doctor_id = $userId;
                $doctorLocation->location_email = $doctorLocationData['email'];
                $doctorLocation->address = $doctorLocationData['address'];
                $doctorLocation->office_phone_number = $doctorLocationData['office_phone_number'];
                $doctorLocation->office_fax = $doctorLocationData['office_fax'];
                $doctorLocation->city_id = $doctorLocationData['city'];
                $doctorLocation->state_id = $doctorLocationData['state'] ?? NULL;
                $doctorLocation->latitude = $doctorLocationData['latitude'];
                $doctorLocation->longitude = $doctorLocationData['longitude'];
                $doctorLocation->zipcode = $doctorLocationData['zipcode'];
                $doctorLocation->created_by = $userId;
                $doctorLocationSaveData = $doctorLocation->save();

                return $doctorLocationSaveData;
            }
        );
    }

    /**
     * Update doctor location
     *
     * @param stdClass $doctorLocationData, $userId
     * @return int
     */
    public function updateDoctorLocationData(array $doctorLocationData, $doctorLocationId)
    {
        return DB::transaction(
            function () use ($doctorLocationData, $doctorLocationId) {
                // if (isset($doctorLocationData['coordinates'])) {
                // $getCoordinatesData = explode(",", $doctorLocationData['coordinates']);
                // unset($doctorLocationData['coordinates']);
                // }

                $doctorLocationData['longitude'] = $doctorLocationData['longitude'];
                $doctorLocationData['latitude'] = $doctorLocationData['latitude'];
                $doctorLocationData['location_email'] = $doctorLocationData['location_email'];
                $doctorLocationData['city_id'] = $doctorLocationData['city_id'];
                $doctorLocationData['state_id'] = $doctorLocationData['state_id'];
                $doctorLocationData['address'] = $doctorLocationData['address'];
                $doctorLocationData['zipcode'] = $doctorLocationData['zipcode'];
                $doctorLocationData['doctor_id'] = auth()->user()->id;
                $doctorLocationData['updated_by'] = auth()->user()->id;
                $doctorLocationData['updated_at'] = Carbon::now();
                $doctorLocationData['office_phone_number'] = $doctorLocationData['office_phone_number'];
                $doctorLocationData['office_fax'] = $doctorLocationData['office_fax'];
                return $this->doctorLocation::where('id', $doctorLocationId)->update($doctorLocationData);
            }
        );
    }

    public function getOverViewInformation($doctorId)
    {
        return $this->doctorOverview::select('doctor_overviews.id', 'doctor_id', 'doctor_overviews.type', 'other', 'master_data_id', 'master_sub_categories.label as other_category', 'master_sub_categories.is_approved')
            ->leftJoin('master_sub_categories', 'doctor_overviews.master_data_id', '=', 'master_sub_categories.id')
            ->where('doctor_id', $doctorId)->get();
    }

    public function getLocations($doctorId, $doctorLocationId = Null)
    {
        $doctorLocation = $this->doctorLocation::select('id', 'doctor_id', 'location_email as email', 'address', 'city_id', 'state_id', 'zipcode', 'latitude', 'longitude', 'office_phone_number', 'office_fax', 'created_at')->where('doctor_id', $doctorId);
        if (!empty($doctorLocationId)) {
            $doctorLocation = $doctorLocation->where('id', $doctorLocationId)->orderBy('created_at', 'DESC')->get();
        } else {
            $doctorLocation = $doctorLocation->orderBy('created_at', 'DESC')->get();
        }
        return $doctorLocation;
    }

    public function getMedias($doctorId)
    {
        return $this->doctorMedia::select('id', 'doctor_id', 'file', 'type', 'upload_type', 'order_no', 'description')->where('doctor_id', $doctorId)->orderBy('order_no', 'ASC')->get();
    }

    public function getDoctorsList($doctorData, $offset, $limit, $currentPage)
    {
        if (isset($doctorData['symptom']) && !empty($doctorData['symptom']))
            $symptomData = implode("|", $doctorData['symptom']);

        $fields = [
            'doctor_information.*',
            'do1.master_data_id',
            'ct.name', 'dl.address',
            'dl.zipcode',
            'c.id',
            'dl.id as doctor_location_id', 'users.email',
            'users.phone_number', 'users.country_code',
            'c.label as specialty_name',
            // 'ma_occu.label as occupation_name', 
            'df.doctor_favorite_group_id',
            'dt.day', 'dt.start_time', 'dt.end_time', 'dl.appointment_duration', 'dt.id'
        ];

        if (!empty($doctorData['current_coordinates'])) {
            $currentCoordinates = explode(",", $doctorData['current_coordinates']);

            $distanceField = [
                DB::raw("3959 * acos(cos(radians(" . $currentCoordinates[0] . "))
                    * cos(radians(dl.latitude))
                    * cos(radians(dl.longitude) - radians(" . $currentCoordinates[1] . "))
                    + sin(radians(" . $currentCoordinates[0] . "))
                    * sin(radians(dl.latitude))) AS distance")
            ];
            $fields =  array_merge($fields, $distanceField);
        }

        $locationIds = '';

        if (isset($doctorData['location']) && !empty($doctorData['location'])) {
            $doctorLocations = $this->doctorLocation::whereRaw("UPPER(s.name) LIKE '%" . strtoupper($doctorData['location']) . "%'")
                ->orWhereRaw("UPPER(ct.name) LIKE '%" . strtoupper($doctorData['location']) . "%'")
                ->Join('cities as ct', 'ct.id', 'doctor_locations.city_id')
                ->Join('states as s', 's.id', 'doctor_locations.state_id')
                ->select(DB::raw('group_concat(doctor_locations.id) as location_id'))
                ->first();

            $locationIds = !is_null($doctorLocations) ? $doctorLocations->location_id : '';
        }

        $doctorInformations = $this->doctorInformation::select($fields)
            ->leftJoin('master_sub_categories as c', 'c.id', 'doctor_information.specialty_id')
            ->leftJoin('users', 'users.id', 'doctor_information.user_id')
            ->Join('doctor_locations as dl', 'dl.doctor_id', 'doctor_information.user_id')
            ->leftJoin('doctor_overviews as do1', function($join){
                $join->on('do1.doctor_id', '=', 'doctor_information.user_id')->where('do1.deleted_by', '=', null);
            })
            ->leftJoin('doctor_timings as dt', 'doctor_information.user_id', 'dt.doctor_id')
            ->leftJoin('doctor_favorites as df', function($join){
                $join->on('df.doctor_id', '=', 'doctor_information.user_id')->where('df.deleted_at', '=', null);
            })
            ->Join('cities as ct', 'ct.id', 'dl.city_id')
            ->Join('states as s', 's.id', 'dl.state_id');

        $doctorInformations = $doctorInformations->where('dl.deleted_by', null);
        if ($doctorData['is_featured_listing'] == 1) {
            $doctorInformations = $doctorInformations->where(['is_featured' => 1]);
        }

        if (!empty($doctorData['is_favorite']) && isset($doctorData['patient_id']) && !empty($doctorData['patient_id'])) {
            $doctorInformations = $doctorInformations->where('df.patient_id', $doctorData['patient_id']);
        }

        if (isset($doctorData['specialty']) && !empty($doctorData['specialty'])) {
            $doctorInformations = $doctorInformations->where('doctor_information.specialty_id', $doctorData['specialty']);
        }

        if (isset($doctorData['gender']) && !empty($doctorData['gender'])) {
            $doctorInformations = $doctorInformations->where('doctor_information.gender', $doctorData['gender']);
        }

        if (isset($doctorData['doctor_name']) && !empty($doctorData['doctor_name'])) {
            $fullName = explode(' ', $doctorData['doctor_name']);

            $doctorInformations = $doctorInformations->where(function ($query) use ($fullName) {
                $query->when(!empty($fullName[1]) && $fullName[1], function ($query) use ($fullName) {
                    $query->where(DB::raw('lower(first_name)'), "LIKE", "%" . strtolower($fullName[0]) . "%")
                        ->orWhere(DB::raw('lower(last_name)'), "LIKE", "%" . strtolower($fullName[1]) . "%")
                        ->orWhere(DB::raw('lower(first_name)'), "LIKE", "%" . strtolower($fullName[1]) . "%")
                        ->orWhere(DB::raw('lower(last_name)'), "LIKE", "%" . strtolower($fullName[0]) . "%");
                })->when($fullName[0], function ($query) use ($fullName) {
                    $query->where(DB::raw('lower(first_name)'), "LIKE", "%" . strtolower($fullName[0]) . "%")
                        ->orWhere(DB::raw('lower(last_name)'), "LIKE", "%" . strtolower($fullName[0]) . "%");
                });
            });
        }

        if (!empty($doctorData['start_date']) || !empty($doctorData['end_date'])) {

            $startDate = $doctorData['start_date'];
            $endDate = $doctorData['end_date'];

            if (!empty($startDate)) {
                $dbDayName = [];

                // Create day name array
                if (!empty($endDate)) {
                    $ranges = CarbonPeriod::create($startDate, $endDate)->toArray();

                    foreach ($ranges as $day) {
                        $dayName = strtolower($day->format('l'));
                        $dbDayName[] = getDayName(strtolower($dayName));
                    }

                    $dbDayName = array_unique($dbDayName);
                } else {
                    $dayName = Carbon::createFromFormat('Y-m-d', $startDate)->format('l');
                    $dbDayName = [getDayName(strtolower($dayName))];
                }
            }

            $doctorAppointments = $this->appointment::where('status', '!=', config('constants.appointment_status.3'))
                ->where('status', '!=', config('constants.appointment_status.11'))
                ->where('status', '!=', config('constants.appointment_status.9'))
                ->where(function ($query) use ($startDate, $endDate) {
                    if (!empty($endDate)) {
                        $query->whereBetween('appointment_date', [$startDate, $endDate]);
                    } else if (!empty($startDate) && empty($endDate)) {
                        $query->whereDate('appointment_date', '=', $startDate);
                    }
                })
                ->whereNotNull(['status', 'start_time', 'end_time'])
                ->get();

            $doctorWorkingTimeData = $this->doctorTiming::Join('doctor_locations as dl', 'dl.id', 'doctor_timings.location_id')
                ->select('doctor_timings.id', 'doctor_timings.doctor_id', 'start_time', 'end_time', 'day', 'dl.appointment_duration')
                ->orderBy('doctor_timings.start_time', 'ASC')
                ->whereIn('doctor_timings.day', $dbDayName)
                ->get()->groupBy('day');

            $doctorBlockSlot = $this->doctorBlockSlot::where(function ($query) use ($startDate, $endDate) {
                if (!empty($endDate)) {
                    $query->whereBetween('date', [$startDate, $endDate]);
                } else if (!empty($startDate) && empty($endDate)) {
                    $query->whereDate('date', '=', $startDate);
                }
            })->get();

            $doctorBlockSlotArray = [];

            foreach ($doctorBlockSlot as $blockSlot) {
                $blockSlot = [
                    'date' => !empty($blockSlot->date) ? date('d-m-Y', strtotime($blockSlot->date)) : Null,
                    'start_time' => !empty($blockSlot->start) ? date('H:i A', strtotime($blockSlot->start)) : Null,
                    'end_time' => !empty($blockSlot->end) ? date('H:i A', strtotime($blockSlot->end)) : Null,
                ];
                array_push($doctorBlockSlotArray, $blockSlot);
            }

            foreach ($doctorAppointments as $book_slot) {
                $bookSlotArray = [
                    'date' => !empty($book_slot->appointment_date) ? date('d-m-Y', strtotime($book_slot->appointment_date)) : Null,
                    'start_time' => !empty($book_slot->start_time) ? date('H:i', strtotime($book_slot->start_time)) : Null,
                    'start_period' => !empty($book_slot->start_time) ? date('A', strtotime($book_slot->start_time)) : Null,
                    'end_time' => !empty($book_slot->end_time) ? date('H:i', strtotime($book_slot->end_time)) : Null,
                    'end_period' => !empty($book_slot->end_time) ? date('A', strtotime($book_slot->end_time)) : Null,
                    'is_appointment' => true,
                    'status' => 0
                ];

                if ($book_slot->status != config('constants.appointment_status.1') && !empty($book_slot->status))
                    $bookSlotArray['status'] = 1;

                array_push($doctorBlockSlotArray, $bookSlotArray);
            }

            $workingTimeArray = getTimeSlot($doctorWorkingTimeData, $doctorBlockSlotArray);

            $doctorInformations = $doctorInformations->whereIn('doctor_information.user_id', $workingTimeArray);
        }

        if (isset($doctorData['in_person']) && ($doctorData['in_person'] != "") && ($doctorData['in_person'] > 0)) {
            $doctorInformations = $doctorInformations->where('doctor_information.in_person', $doctorData['in_person']);
        }

        if (isset($doctorData['video_consultancy']) && ($doctorData['video_consultancy'] != "") && ($doctorData['video_consultancy'] > 0)) {
            $doctorInformations = $doctorInformations->where('doctor_information.video_consultancy', $doctorData['video_consultancy']);
        }

        if (isset($doctorData['symptom']) && !empty($doctorData['symptom'])) {
            $doctorInformations = $doctorInformations->where("symptom_id", "REGEXP", "(^|,)($symptomData)(,|$)");
        }

        if (isset($doctorData['location']) && !empty($doctorData['location'])) {
            $doctorInformations = $doctorInformations->whereIn('dl.id', explode(",", $locationIds));
        }

        if (isset($doctorData['language']) && !empty($doctorData['language'])) {
            $doctorInformations = $doctorInformations->whereIn('do1.master_data_id', $doctorData['language']);
        }

        $totalDoctorswithDistance = $doctorInformations->groupBy('doctor_information.id')->get();

        if (!empty($doctorData['current_coordinates']) && !empty($doctorData['distance'])) {
            $doctorInformations = $doctorInformations->having('distance', '<', $doctorData['distance']);
        }

        $totalDoctorsWithoutLanguage = $doctorInformations->groupBy('doctor_information.id')->get();

        $totalDoctors = $doctorInformations->groupBy('doctor_information.id')->get();
        $regiDocsCount = $totalDoctors->count();
        $doctorIds = [];
        if (isset($doctorData['patient_id']) && !empty($doctorData['patient_id'])) {

            $doctorIds = $this->doctorFavorite::where('patient_id', $doctorData['patient_id'])
                ->pluck('doctor_id')->toArray();
        }

        $doctorInformations = $doctorInformations->skip($offset)->take($limit)->groupBy('doctor_information.id');

        if (!empty($doctorIds)) {
            $doctorIds = implode(",", $doctorIds);
            $doctorInformations = $doctorInformations->orderByRaw("CASE WHEN df.doctor_id IN (" . $doctorIds . ") then -1  Else dl.id END ASC,df.doctor_id")->get();
        } else {
            $doctorInformations = $doctorInformations->orderBy('doctor_information.id', 'DESC')->get();
        }

        $masterDataIds = $doctorDistanceArray = [];

        $allDistance = $this->masterSubCategory::select('master_sub_categories.id', 'master_category_id', 'label', 'value')
            ->where(['status' => config('constants.status.active'), 'master_category_id' => 6])->get()->toArray();
        if (!$totalDoctors->isEmpty())
            $doctorDistanceArray = array_filter(array_column(!empty($doctorData['language']) ? $totalDoctors->toArray() : $totalDoctorswithDistance->toArray(), 'distance'));

        $distanceWithCount = $this->getDistanceWithCount($doctorDistanceArray, $allDistance);

        foreach ($doctorInformations as $information) {
            if (!is_null($doctorInformations)) {
                if (isset($doctorData['symptom']) && !empty($doctorData['symptom']))
                    $information['symptom'] = $this->getSymptoms($symptomData);

                $doctorReviewData = $this->doctorReview::where('doctor_id', $information->user_id)->first();

                $information->latest_review = $doctorReviewData;

                $information->is_favorite = 0;

                if (isset($doctorData['patient_id']) && !empty($doctorData['patient_id'])) {
                    $doctorFavoriteGroupdata = $this->doctorFavorite::where([
                        'patient_id' => $doctorData['patient_id'],
                        'doctor_id' => $information->user_id
                    ])->first();

                    $information->is_favorite = !empty($doctorFavoriteGroupdata) ? 1 : 0;
                }

                $information->overview = $this->getOverViewInformation($information->user_id);
                $information->location = $this->getLocations($information->user_id, $information->doctor_location_id);
            }
        }

        foreach ($totalDoctorsWithoutLanguage as $doctors) {
            $languages =  $this->doctorOverview::select('master_data_id')
                ->where('doctor_id', $doctors->user_id)
                ->where('type', 'language')->get();
            if (!$languages->isEmpty())
                $masterDataIds[] = array_column($languages->toArray(), 'master_data_id');
        }
        $masterDataIds = call_user_func_array('array_merge', $masterDataIds);

        $allLanguages = $this->masterSubCategory::select(['master_sub_categories.id', 'master_category_id', 'label', 'value'])
            ->where([
                'status' => config('constants.status.active'),
                'is_approved' => 1,
                'master_category_id' => 5
            ])
            ->get()->toArray();

        array_walk($allLanguages, function (&$v, $k) use ($masterDataIds) {
            $languageId = $v['id'];
            $total = array_filter($masterDataIds, function ($value) use ($languageId) {
                return $value == $languageId;
            });
            $v['count'] = count($total);
        });
        $languagesWithCount = $allLanguages;

        // unregistered doctors
        $unregiDocsCount = 0;
        $unregiCount = 0;
        $currPageCount = $doctorInformations->count(); // current page registered doctor count
        
        if (empty($doctorData['is_favorite'])) {
            $unregiDocs = $this->doctor::where('is_registered', 0);

            if (isset($doctorData['location']) && !empty($doctorData['location'])) {
                $unregiDocs = $unregiDocs->where('city', 'LIKE', "%" . $doctorData['location'] . "%");
            }

            if (isset($doctorData['specialty']) && !empty($doctorData['specialty'])) {
                $speciality = $this->masterSubCategory::find($doctorData['specialty'])->value ?? $this->masterSubCategory::find($doctorData['specialty'])->label ?? '';
                $unregiDocs = $unregiDocs->where('specialties', 'LIKE', "%" . $speciality . "%");
            }
    
            if (isset($doctorData['gender']) && !empty($doctorData['gender'])) {
                if (in_array($doctorData['gender'], array(1, 2))) {
                    $genderArray = array('1' => 'M', '2' => 'F');
                    $unregiDocs = $unregiDocs->where('gender', $genderArray[$doctorData['gender']]);
                }
            }

            if (isset($doctorData['doctor_name']) && !empty($doctorData['doctor_name'])) {
                $fullName = explode(' ', $doctorData['doctor_name']);
    
                $unregiDocs = $unregiDocs->where(function ($query) use ($fullName) {
                    $query->when(!empty($fullName[1]) && $fullName[1], function ($query) use ($fullName) {
                        $query->where(DB::raw('lower(first_name)'), "LIKE", "%" . strtolower($fullName[0]) . "%")
                            ->orWhere(DB::raw('lower(last_name)'), "LIKE", "%" . strtolower($fullName[1]) . "%")
                            ->orWhere(DB::raw('lower(first_name)'), "LIKE", "%" . strtolower($fullName[1]) . "%")
                            ->orWhere(DB::raw('lower(last_name)'), "LIKE", "%" . strtolower($fullName[0]) . "%");
                    })->when($fullName[0], function ($query) use ($fullName) {
                        $query->where(DB::raw('lower(first_name)'), "LIKE", "%" . strtolower($fullName[0]) . "%")
                            ->orWhere(DB::raw('lower(last_name)'), "LIKE", "%" . strtolower($fullName[0]) . "%");
                    });
                });
            }

            $unregiDocsCount = $unregiDocs->count();
            if ($currPageCount < 10) { // if registered doctors are less than 10 then add unregistered doctors
                $unregiDocs = $unregiDocs->skip($doctorData['prev_doctor_count'])->take($limit - $currPageCount)->orderBy('is_top_doctor', 'DESC')->get(['id', 'npi_number', 'first_name', 'last_name', 'profile_image', 'specialties', 'address', 'city', 'state', 'zipcode']);
                $doctorInformations = $doctorInformations->concat($unregiDocs);
                $unregiCount = $limit - $currPageCount;
            }
        }
        //

        $doctorInformations['prev_doctor_count'] = $doctorData['prev_doctor_count']+$unregiCount;
        $doctorInformations['categories'] = array_merge($languagesWithCount, $distanceWithCount);
        $doctorInformations['totalRecord'] = $regiDocsCount + $unregiDocsCount;

        return $doctorInformations;
    }

    public function getTopDoctorsList($doctorData, $offset, $limit, $currentPage)
    {
        $doctors = $this->doctor::where('is_registered', 0)
            ->where('is_top_doctor', 1)
            ->where('city', 'LIKE', "%New York%");
            
        if (isset($doctorData['specialty']) && !empty($doctorData['specialty'])) {
            $speciality = $this->masterSubCategory::find($doctorData['specialty']);
            $specialityName = $speciality ? ($speciality->value ?? $speciality->label ?? '') : '';
            $doctors = $doctors->where('specialties', 'LIKE', "%" . $specialityName . "%");
        }
        
        $doctorsCount = $doctors->count();
        $doctors = $doctors->skip($offset)
            ->take($limit)
            ->get(['id', 'npi_number', 'first_name', 'last_name', 'profile_image', 'specialties', 'address', 'city', 'state', 'zipcode']);
        
        $doctors['totalRecord'] = $doctorsCount;
        
        return $doctors;
    }

    public function doctorsAndFeeds($doctorData, $offset, $limit, $currentPage)
    {
        $fields = [
            'doctor_information.*',
            'do1.master_data_id',
            'ct.name', 'dl.address',
            'dl.zipcode',
            'c.id',
            'dl.id as doctor_location_id', 'users.email',
            'users.phone_number', 'users.country_code',
            'c.label as specialty_name',
            // 'ma_occu.label as occupation_name', 
            'df.doctor_favorite_group_id',
            'dt.day', 'dt.start_time', 'dt.end_time', 'dl.appointment_duration', 'dt.id'
        ];
        
        $locationIds = '';

        if (isset($doctorData['location']) && !empty($doctorData['location'])) {
            $doctorLocations = $this->doctorLocation::whereRaw("UPPER(s.name) LIKE '%" . strtoupper($doctorData['location']) . "%'")
                ->orWhereRaw("UPPER(ct.name) LIKE '%" . strtoupper($doctorData['location']) . "%'")
                ->Join('cities as ct', 'ct.id', 'doctor_locations.city_id')
                ->Join('states as s', 's.id', 'doctor_locations.state_id')
                ->select(DB::raw('group_concat(doctor_locations.id) as location_id'))
                ->first();

            $locationIds = !is_null($doctorLocations) ? $doctorLocations->location_id : '';
        }

        $doctorInformations = $this->doctorInformation::select($fields)
            ->leftJoin('master_sub_categories as c', 'c.id', 'doctor_information.specialty_id')
            ->leftJoin('users', 'users.id', 'doctor_information.user_id')
            ->Join('doctor_locations as dl', 'dl.doctor_id', 'doctor_information.user_id')
            ->leftJoin('doctor_overviews as do1', 'do1.doctor_id', 'doctor_information.user_id')
            ->join('doctor_timings as dt', 'doctor_information.user_id', 'dt.doctor_id')
            ->leftJoin('doctor_favorites as df', 'df.doctor_id', 'doctor_information.user_id')
            ->Join('cities as ct', 'ct.id', 'dl.city_id')
            ->Join('states as s', 's.id', 'dl.state_id');

        if (isset($doctorData['location']) && !empty($doctorData['location'])) {
            $doctorInformations = $doctorInformations->whereIn('dl.id', explode(",", $locationIds));
        }

        $doctorInformations = $doctorInformations->groupBy('doctor_information.id');
        $totalDoctors = $doctorInformations->get();
        $totalCount = $totalDoctors->count();

        $doctorIds = $doctorInformations->pluck('user_id');
        $doctorInformations = $doctorInformations->skip($offset)->take($limit)->orderBy('doctor_information.id', 'DESC')->get();

        foreach ($doctorInformations as $information) {
            if (!is_null($doctorInformations)) {
                $doctorReviewData = $this->doctorReview::where('doctor_id', $information->user_id)->first();

                $information->latest_review = $doctorReviewData;
                $information->is_favorite = 0;
                $information->overview = $this->getOverViewInformation($information->user_id);
                $information->location = $this->getLocations($information->user_id, $information->doctor_location_id);
            }
        }

        $doctorInformations['totalRecord'] = $totalCount;
        $doctorInformations['doctorIds'] = $doctorIds;

        return $doctorInformations;
    }

    public function getDocuments($doctorId)
    {
        return $this->doctorMedia::select('id', 'doctor_id', 'file', 'type', 'order_no')->where([
            'doctor_id' => $doctorId,
            'type' => 'document',
        ])->get();
    }

    public function saveOverviewInformation($input, $userId)
    {
        return DB::transaction(
            function () use ($input, $userId) {

                $updateData = [
                    'about' => $input['about'] ?? '',
                    'updated_at' => Carbon::now(),
                    'updated_by' => $userId,
                ];
                $this->doctorInformation::where('user_id', $userId)->update($updateData);

                unset($input['about']);
                if (!empty($input)) {
                    foreach ($input as $key => $overviewArray) {
                        foreach ($overviewArray as $overviewValue) {
                            if ($overviewValue['id'] > 0) {
                                $doctorOverView = $this->doctorOverview::find($overviewValue['id']);
                                $doctorOverView->updated_by = $userId;
                                $doctorOverView->updated_at = Carbon::now();
                            } else {
                                $doctorOverView = new DoctorOverview();
                                $doctorOverView->created_by = $userId;
                                $doctorOverView->created_at = Carbon::now();
                            }

                            $otherValue = '';
                            if (isset($overviewValue['category_id'])) {
                                if ((empty($overviewValue['category_id']) || $overviewValue['category_id'] == 0) && isset($overviewValue['other']) && !empty($overviewValue['other'])) {

                                    $checkCategoryExists = $this->masterCategory::join('master_sub_categories as sc', 'master_categories.id', 'sc.master_category_id')
                                        ->where('master_categories.name', $key)
                                        ->where('sc.label', $overviewValue['other'])
                                        ->where('sc.status', config('constants.status.active'))
                                        ->first();

                                    if (!is_null($checkCategoryExists)) {
                                        $overviewValue['value'] = $checkCategoryExists->id;
                                    } else {
                                        $lastCategory = $this->masterCategory::join('master_sub_categories as sc', 'sc.master_category_id', 'master_categories.id')
                                            ->where('master_categories.name', $key)
                                            ->orderBy('sc.order_no', 'desc')->first();

                                        if (!is_null($lastCategory)) {
                                            $orderNo = $lastCategory->order_no + 1;
                                        } else {
                                            $orderNo = 1;
                                        }

                                        $masterSubCategory = $this->masterSubCategory;
                                        $masterSubCategory->label = $overviewValue['other'];
                                        $masterSubCategory->master_category_id = $lastCategory->master_category_id;
                                        $masterSubCategory->order_no = $orderNo;
                                        $masterSubCategory->status = config('constants.status.active');
                                        $masterSubCategory->is_approved = 0;
                                        $masterSubCategory->is_other = 1;
                                        if ($masterSubCategory->save()) {
                                            $overviewValue['value'] = $masterSubCategory->id;
                                        }
                                    }
                                } else {
                                    $overviewValue['value'] = $overviewValue['category_id'];
                                }
                            } else {
                                $otherValue = $overviewValue['value'];
                                $overviewValue['master_data_id'] = null;
                            }

                            if ($doctorOverView) {
                                $doctorOverView->doctor_id = $userId;
                                $doctorOverView->master_data_id = $overviewValue['value'];
                                $doctorOverView->type = $key;
                                $doctorOverView->other = $otherValue;
                                $doctorOverView->save();
                            }
                        }
                    }
                }

                return true;
            }
        );
    }

    public function checkDataExists($input, $userId)
    {
        $id = $input['id'];
        $type = $input['type'];
        if ($type == 'overview') {
            return $this->doctorOverview::where('id', $id)->where('doctor_id', $userId)->exists();
        } elseif ($type == 'location') {
            return $this->doctorLocation::where('id', $id)->where('doctor_id', $userId)->exists();
        } elseif ($type == 'media') {
            return $this->doctorMedia::where('id', $id)->where('doctor_id', $userId)->exists();
        } elseif ($type == 'document') {
            return $this->doctorMedia::where('id', $id)->where('doctor_id', $userId)->exists();
        }
        return false;
    }

    public function deleteProfileInformation($input, $userId)
    {
        return DB::transaction(
            function () use ($input, $userId) {
                $id = $input['id'];
                $type = $input['type'];
                if ($type == 'overview') {
                    $overview = $this->doctorOverview::find($id);
                    $overview->deleted_by = $userId;
                    if ($overview->save()) {
                        if ($overview->type == 'certification') {
                            $this->masterSubCategory::where(['id' => $overview->master_data_id, 'is_other' => 1, 'is_approved' => 0])->delete();
                        }

                        return $overview->delete();
                    }
                } elseif ($type == 'location') {
                    $location = $this->doctorLocation::find($id);
                    $location->deleted_by = $userId;
                    if ($location->save())
                        return $location->delete();
                } elseif ($type == 'media') {
                    $media = $this->doctorMedia::find($id);
                    $media->deleted_by = $userId;
                    if ($media->save())
                        return $media->delete();
                } elseif ($type == 'document') {
                    $media = $this->doctorMedia::find($id);
                    $media->deleted_by = $userId;
                    if ($media->save())
                        return $media->delete();
                }
                return false;
            }
        );
    }

    /**
     * upload photos & videos
     *
     * @param stdClass $doctorMedia, $userId
     * @return int
     */
    public function uploadPhotosVideos(array $doctorMediaArray, $userId)
    {
        foreach ($doctorMediaArray as $data) {

            $getLastInsertedRecord = $this->doctorMedia::where('doctor_id', $userId)->orderBy('order_no', 'DESC')->first();
            $currentOrderNo =  is_null($getLastInsertedRecord) ? 1 : $getLastInsertedRecord->order_no + 1;

            $doctorMedia = new DoctorMedia();
            $doctorMedia->doctor_id = $userId;
            $doctorMedia->file = $data[0];
            $doctorMedia->type = $data[1];
            $doctorMedia->upload_type = $data[2] ?? 'media';
            $doctorMedia->order_no = (isset($data[2]) && $data[2] == 'proof') ? 0 : $currentOrderNo;
            $doctorMedia->created_by = $userId;
            $doctorMediaSaveData = $doctorMedia->save();
        }

        return $doctorMediaSaveData;
    }


    /**
     * Update Order Number
     *
     * @param stdClass $doctorMedia, $userId
     * @return int
     */
    public function updateMediaOrder(array $doctorMediaArray, $userId)
    {
        $updateCurrentId = $this->doctorMedia::where('id', $doctorMediaArray['doctor_media_id'])->update([
            'order_no' => $doctorMediaArray['new_order_no'],
            'updated_by' => $userId
        ]);

        $updateNewId = $this->doctorMedia::where('id', $doctorMediaArray['new_doctor_media_id'])->update([
            'order_no' => $doctorMediaArray['order_no'],
            'updated_by' => $userId
        ]);

        return ($updateCurrentId && $updateNewId) ?? false;
    }

    /**
     * Update Media description
     *
     * @param stdClass $doctorMedia, $userId
     * @return int
     */
    public function updateMediaDescription(array $doctorMediaArray, $userId)
    {
        $updateDescription = $this->doctorMedia::where('id', $doctorMediaArray['doctor_media_id'])->update([
            'description' => $doctorMediaArray['description'] ?? '',
            'updated_by' => $userId
        ]);

        return ($updateDescription) ?? false;
    }

    public function editDoctor($doctorId)
    {
        $allData = [];
        $doctorData = new stdClass();
        $user = $this->user::find($doctorId);
        $doctorData->email = $user->email;

        $doctorInformation = $this->doctorInformation::where('user_id', $doctorId)->first();
        $occupation = $this->masterSubCategory::find($doctorInformation->occupation_id);
        $speciality = $this->masterSubCategory::find($doctorInformation->specialty_id);
        $doctorData->first_name = isset($doctorInformation->first_name) ? $doctorInformation->first_name : "";
        $doctorData->last_name = isset($doctorInformation->last_name) ? $doctorInformation->last_name : "";
        $doctorData->profile_image = isset($doctorInformation->profile_image) ? $doctorInformation->profile_image : "";
        $doctorData->occupation_id = isset($occupation->id) ? $occupation->id : "";
        $doctorData->occupation_name = isset($occupation->label) ? $occupation->label : "";
        $doctorData->speciality_id = isset($speciality->id) ? $speciality->id : "";
        $doctorData->speciality_name = isset($speciality->label) ? $speciality->label : "";
        $doctorData->office_phone = isset($doctorInformation->office_phone_number) ? $doctorInformation->office_phone_number : "";
        $doctorData->office_fax = isset($doctorInformation->office_fax) ? $doctorInformation->office_fax : "";
        $doctorData->date_of_birth = date("m/d/Y", strtotime($doctorInformation->date_of_birth));
        $doctorData->state_of_office = isset($doctorInformation->state_of_office) ? $doctorInformation->state_of_office : "";
        $doctorData->state_id = isset($doctorInformation->state_id) ? $doctorInformation->state_id : "";
        $doctorData->npi_number = isset($doctorInformation->npi_number) ? $doctorInformation->npi_number : "";

        $doctorLocation = $this->doctorLocation::where('doctor_id', $doctorId)->first();
        $doctorData->street_address = isset($doctorLocation->address) ? $doctorLocation->address : "";
        
        $doctorData->zipcode = isset($doctorLocation->zipcode) ? $doctorLocation->zipcode : "";
        $doctorData->location_email = isset($doctorLocation->location_email) ? $doctorLocation->location_email : "";
        $doctorData->city_id = isset($doctorInformation->city_id) ? $doctorInformation->city_id : "";
        $doctorData->city_of_profession = isset($doctorInformation->city_of_profession) ? $doctorInformation->city_of_profession : "";

        $allData['doctorData'] = $doctorData;
        $allData['states'] = $this->state::get();
        $allData['cities'] = $this->city::get();
        $allData['occuption'] = $this->masterSubCategory::where('master_category_id', '=', 2)->get();
        $allData['specialty'] = $this->masterSubCategory::where('master_category_id', '=', 3)->get();

        return $allData;
    }


    public function updateDoctor($doctorId, $doctorParams)
    {
        $usertableInfo = $this->user::find($doctorId);
        $usertableInfo->email = $doctorParams->email;
        if (isset($doctorParams->password) && !empty($doctorParams->password) && $doctorParams->password != '') {
            $usertableInfo->password = Hash::make($doctorParams->password);
        }
        $usertableInfo->save();

        // $role = $this->role::where('id', config('constants.roles.doctor'))->first();
        // $modelHasRole = $this->modelHasRole;
        // $modelHasRole->role_id = $role->id;
        // $modelHasRole->model_id = $usertableInfo->id;
        // $modelHasRole->save();

        $doctorInformation = $this->doctorInformation::where('user_id', $doctorId)->first();
        $doctorInformation->user_id = $usertableInfo->id;
        $doctorInformation->first_name = $doctorParams->first_name;
        $doctorInformation->last_name = $doctorParams->last_name;
        $doctorInformation->npi_number = $doctorParams->npi_number;
        $doctorInformation->occupation_id = $doctorParams->occupation;
        $doctorInformation->specialty_id = $doctorParams->speciality;
        $doctorInformation->office_phone_number = $doctorParams->office_phone;
        $doctorInformation->office_fax = $doctorParams->office_fax;
        $doctorInformation->date_of_birth = $doctorParams->birth_date;
        $doctorInformation->state_id = $doctorParams->state;
        $doctorInformation->state_of_office = $doctorParams->state_of_office;
        $doctorInformation->city_of_profession = $doctorParams->city;
        $doctorInformation->save();

        $doctorLocation = $this->doctorLocation::where('doctor_id', $doctorId)->first();
        $doctorLocation->doctor_id = $usertableInfo->id;
        $doctorLocation->address = $doctorParams->street_address;
        $doctorLocation->city_id = $doctorParams->city_of_profession;
        $doctorLocation->location_email = $doctorParams->location_email;
        $doctorLocation->state_id = $doctorParams->state;
        $doctorLocation->zipcode = $doctorParams->zip_code;
        $doctorLocation->save();

        if ($usertableInfo->save() && $doctorInformation->save() && $doctorLocation->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function approveDoctor($doctorId)
    {
        $doctors = $this->user::find($doctorId);
        $doctors->is_verified = 1;
        $doctors->save();
        return $doctors;
    }

    public function likeDoctor($doctorId)
    {
        $findDotorFeaturedCount = $this->doctorInformation::where('is_featured', 1)->count();

        $findDotorFeatured = $this->doctorInformation::where('user_id', $doctorId)->first();
        if ($findDotorFeatured->is_featured) {
            $findDotorFeatured->is_featured = 0;
        } else {
            if ($findDotorFeaturedCount >= config('constants.doctor_feature_limit')) {
                return 0;
            }
            $findDotorFeatured->is_featured = 1;
        }
        return $findDotorFeatured->save();
    }

    public function doctorDocuments($doctorId)
    {
        $doctorDataArray = [];
        $isDocumentVerified = $this->doctorInformation::where('user_id', $doctorId)->first();
        $doctorDocumemts = $this->doctorMedia::where('doctor_id', $doctorId)->get();
        $doctorDataArray['is_document_verified'] = $isDocumentVerified->is_document_verified;
        $doctorDataArray['doctor_documemts'] = $doctorDocumemts;
        return $doctorDataArray;
    }

    public function verifyDoctor($doctorId)
    {
        $verifyDoctor = $this->doctorInformation::where('user_id', $doctorId)->first();
        $verifyDoctor->is_document_verified = 1;
        return $verifyDoctor->save();
    }

    public function featuredDoctorList(array $data)
    {
        $fields = [
            'doctor_information.*', 'dl.address', 'c.id as master_data_id', 'dl.zipcode', 'dl.id as doctor_location_id', 'users.email', 'users.phone_number', 'users.country_code', 'c.label as specialty_name', 'ma_occu.label as occupation_name', 'df.doctor_favorite_group_id'
        ];

        $doctorInformations = $this->doctorInformation::with('review')->select($fields)
            ->leftJoin('master_sub_categories as c', 'c.id', 'doctor_information.specialty_id')
            ->leftJoin('users', 'users.id', 'doctor_information.user_id')
            ->leftJoin('master_sub_categories as ma_occu', 'doctor_information.occupation_id', '=', 'ma_occu.id')
            ->Join('doctor_locations as dl', 'dl.doctor_id', 'doctor_information.user_id')
            ->leftJoin('doctor_overviews as do1', 'do1.doctor_id', 'doctor_information.user_id')
            ->leftJoin('master_sub_categories as ma_language', 'ma_language.master_category_id', 'do1.master_data_id')
            ->leftJoin('doctor_overviews as do', 'do.master_data_id', 'c.id')
            ->leftJoin('doctor_favorites as df', 'df.doctor_id', 'doctor_information.user_id')
            ->Join('cities as ct', 'ct.id', 'dl.city_id')
            ->Join('states as s', 's.id', 'dl.state_id')
            ->select($fields)
            ->where([
                'is_featured' => 1,
                'dl.deleted_by' => null
            ])
            ->groupBy('doctor_information.id');

        if (isset($doctorData['symptom'])) {
            foreach ($doctorData['symptom'] as $symptom) {
                $doctorInformations = $doctorInformations->orWhereRaw("find_in_set('" . $symptom . "',symptom_id)");
            }
        }
        $doctorInformations = $doctorInformations->groupBy('doctor_information.id')
            ->orderBy('doctor_information.id', 'DESC')->get();

        foreach ($doctorInformations as $information) {
            if (!is_null($doctorInformations)) {
                $doctorFavoriteGroupdata = $this->doctorFavorite::where('doctor_id', $information->user_id)->first();

                $doctorReviewData = $this->doctorReview::where('doctor_id', $information->user_id)->first();

                if (!empty($data['patient_id']))
                    $information->is_read = $this->notification::where(['user_id' => $data['patient_id'], 'is_read' => 0])->get()->count();

                $information->latest_review = $doctorReviewData;
                $information['symptom'] = $this->getSymptoms($information->symptom_id);
                $information->is_favorite = !empty($doctorFavoriteGroupdata) ? 1 : 0;
                $information->overview = $this->getOverViewInformation($information->user_id);
                $information->location = $this->getLocations($information->user_id, $information->doctor_location_id);
            }
        }

        return $doctorInformations;
    }

    public function topSpecialitiesDoctorList()
    {
        $topDoctors = $this->doctor::where('is_top_doctor', 1)->get();
        foreach ($topDoctors as $doctor) {
            $doctor->profile_image = !empty($doctor->profile_image) ? config('constants.get_file_path') . 'top-doctors/' . $doctor->profile_image : NULL;
        }
        $topDoctors = $topDoctors->toArray();

        return $topDoctors;
    }

    public function getSymptoms($symptomId)
    {
        if (empty($symptomId))
            return [];

        return  $this->masterSubCategory::select('id', 'label')->whereIn('id', explode(",", $symptomId))->get()->toArray();
    }

    public function getProcedure($procedureId)
    {
        if (empty($procedureId))
            return [];

        return  $this->masterSubCategory::select('id', 'label')->whereIn('id', explode(",", $procedureId))->get()->toArray();
    }

    public function getDoctorRating($doctorId)
    {
        $doctorRating = $this->patientInformation::leftJoin('users as user', 'user.id', 'patient_information.user_id')
            ->leftJoin('doctor_reviews as dr', 'dr.patient_id', 'patient_information.user_id')
            ->where('dr.doctor_id', $doctorId)
            ->select(
                'user.email as email',
                'patient_information.full_name as full_name',
                'dr.rating as rating',
                'dr.bedside_manner as bedside_manner',
                'dr.answered_questions as answered_questions',
                'dr.after_care as after_care',
                'dr.time_spent as time_spent',
                'dr.responsiveness as responsiveness',
                'dr.courtesy as courtesy',
                'dr.payment_process as payment_process',
                'dr.wait_times as wait_times',
                'dr.review as review'
            )
            ->get();

        $doctorRating = $doctorRating->toArray();

        $headerArray = [];
        $valueArray = [];
        $dataArray = [];

        foreach ($doctorRating as $key => $data) {
            $headerArray = str_replace('_', ' ', array_keys($data));
            break;
        }

        foreach ($doctorRating as $key => $data) {
            $tempArray = [];
            $tempArray['email'] = $data['email'];
            $tempArray['full_name'] = $data['full_name'];
            $tempArray['rating'] = $data['rating'];
            $tempArray['bedside_manner'] = $data['bedside_manner'];
            $tempArray['answered_questions'] = $data['answered_questions'];
            $tempArray['after_care'] = $data['after_care'];
            $tempArray['time_spent'] = $data['time_spent'];
            $tempArray['responsiveness'] = $data['responsiveness'];
            $tempArray['courtesy'] = $data['courtesy'];
            $tempArray['payment_process'] = $data['payment_process'];
            $tempArray['wait_times'] = $data['wait_times'];
            $tempArray['review'] = $data['review'];

            array_push($valueArray, $tempArray);
        }
        $dataArray['headers'] = $headerArray;
        $dataArray['data'] = $valueArray;

        return $dataArray;
    }

    public function getDistanceWithCount($distanceArray, $allDistance)
    {
        array_walk($allDistance, function (&$v, $k) use ($distanceArray) {
            $distance = $v['value'];
            $gtn = array_filter($distanceArray, function ($value) use ($distance) {
                return $value < $distance;
            });
            $v['count'] = count($gtn);
        });
        return $allDistance;
    }

    public function getSearchFilterQuery($fields)
    {
        $doctorInformations = $this->doctorInformation::select($fields)
            ->leftJoin('master_sub_categories as c', 'c.id', 'doctor_information.specialty_id')
            ->leftJoin('users', 'users.id', 'doctor_information.user_id')
            ->leftJoin('master_sub_categories as ma_occu', 'doctor_information.occupation_id', '=', 'ma_occu.id')
            ->leftJoin('doctor_locations as dl', 'dl.doctor_id', 'doctor_information.user_id')
            ->leftJoin('doctor_overviews as do1', 'do1.doctor_id', 'doctor_information.user_id')
            ->leftJoin('master_sub_categories as ma_language', 'ma_language.master_category_id', 'do1.master_data_id')
            ->leftJoin('doctor_overviews as do', 'do.master_data_id', 'c.id')
            ->leftJoin('cities as ct', 'ct.id', 'dl.city_id')
            ->leftJoin('states as s', 's.id', 'dl.state_id');

        return $doctorInformations;
    }

    public function getAllDistanceData($doctorInformations)
    {
        $otherDistanceData =  $doctorInformations->having('distance', '>', '0.5')->groupBy('dl.id')->orderBy('dl.id', 'DESC')->get();
        return $otherDistanceData;
    }

    public function getAllLanguagesData($doctorData)
    {
        $otherLanguagesData = $doctorData->having('distance', '>', '0.5')
            ->where('do1.type', "language")
            ->groupBy('dl.id')->orderBy('dl.id', 'DESC')->get();
        return $otherLanguagesData;
    }

    public function compareDoctor($doctorId)
    {
        $doctorInformationFields = [
            'users.id as user_id', 'doctor_information.id as doctor_id', 'users.is_verified', 'doctor_information.is_document_verified',
            'users.email', 'users.email', 'users.country_code', 'users.phone_number', 'doctor_information.first_name', 'doctor_information.last_name', 'doctor_information.gender', 'in_person', 'video_consultancy',
            'doctor_information.profile_image', 'occupation_id', 'specialty_id', 'symptom_id', 'dl.office_phone_number', 'master_sub_categories.label as occupation_name', 'm.label as specialty_name', 'dl.office_fax', 'date_of_birth', 'doctor_information.state_id', 'about', 'dl.appointment_duration', 'is_featured', 'doc.name_prefix', 'dl.address'
        ];

        $profileInformation = $this->user::join('doctor_information', 'users.id', '=', 'doctor_information.user_id')
            ->leftJoin('master_sub_categories', 'doctor_information.occupation_id', '=', 'master_sub_categories.id')
            ->leftJoin('master_sub_categories as m', 'doctor_information.specialty_id', '=', 'm.id')
            ->leftJoin('doctor_locations as dl', 'dl.doctor_id', 'doctor_information.user_id')
            ->leftJoin('doctors as doc', 'doc.npi_number', 'doctor_information.npi_number')
            ->select($doctorInformationFields)
            ->whereIn('doctor_information.user_id', $doctorId)->groupBy('doctor_information.user_id')->orderBy('doctor_information.user_id', 'DESC')->get();

        if (!empty($profileInformation)) {
            foreach ($profileInformation as $information) {
                $information->overview = $this->getOverViewInformation($information->user_id);
                $information->location = $this->getLocations($information->user_id, $information->doctor_location_id);
                $information->media = $this->getMedias($information->user_id);
                $information->document = $this->getDocuments($information->user_id);
            }
        }
        return $profileInformation;
    }

    /**
     * Create doctor favorite group
     *
     * @param $input, $patientId
     * @return Array
     */
    public function addFavoriteGroup($input, $patientId)
    {
        $doctorFavorite = $this->doctorFavoriteGroup;
        $data = $doctorFavorite->updateOrCreate([
            'name' => $input['name'],
            'created_by' => $patientId,
        ], [
            'name' => $input['name'],
            'created_by' => $patientId,
            'updated_by' => $patientId,
            'updated_at' => Carbon::now(),
        ]);

        $favoriteGroupData = [
            'id' => $data['id'],
            'name' => $data['name'],
        ];
        return $favoriteGroupData;
    }

    /**
     * update doctor favorite group
     *
     * @param $input, $patientId
     * @return Array
     */
    public function updateFavoriteGroup($input, $patientId)
    {
        $favGroupData = $this->doctorFavoriteGroup::findOrFail($input['doctor_favorite_group_id']);
        $favGroupData->name = $input['name'];
        $favGroupData->updated_by = $patientId;
        $favGroupData->updated_at = Carbon::now();
        $saveFavGroup = $favGroupData->save();

        if ($saveFavGroup) {
            $favoriteGroupData = [
                'id' =>  $favGroupData->id,
                'name' =>  $favGroupData->name,
            ];
            return $favoriteGroupData;
        }
        return false;
    }

    /**
     * Delete doctor favorite group
     *
     * @param $input
     * @return boolean
     */
    public function deleteFavoriteGroup($input)
    {
        $deleteFavGroupDoctor = $this->doctorFavorite::where('doctor_favorite_group_id', $input['doctor_favorite_group_id']);
        if ($deleteFavGroupDoctor)
            $deleteFavGroupDoctor->delete();

        $deleteFavGroup =  $this->doctorFavoriteGroup::where('id', $input['doctor_favorite_group_id'])->delete();
        return $deleteFavGroup;
    }

    /**
     * Favorite doctor list
     *
     * @param $patientId
     * @return Array
     */
    public function favoriteGroupList($patientId)
    {
        return $this->doctorFavoriteGroup::where('created_by', $patientId)->select('id', 'name')->get()->toArray();
    }

    /**
     * Add to favorite doctor
     *
     * @param $input, $patientId
     * @return Array
     */
    public function doctorAddToFavorite($input, $patientId)
    {
        $doctorFavorite = $this->doctorFavorite;
        $data = $doctorFavorite->updateOrCreate([
            'doctor_id' => $input['doctor_id'],
            'patient_id' => $patientId,
            'doctor_favorite_group_id' => $input['doctor_favorite_group_id'],
        ], [
            'doctor_id' => $input['doctor_id'],
            'patient_id' => $patientId,
            'doctor_favorite_group_id' => $input['doctor_favorite_group_id'],
            'created_by' => $patientId,
            'updated_by' => $patientId,
            'updated_at' => Carbon::now(),
        ]);

        $favoriteDoctorData = [
            'id' =>   $data['id'],
            'doctor_id' =>   $data['doctor_id'],
            'patient_id' =>   $data['patient_id'],
            'doctor_favorite_group_id' =>   $data['doctor_favorite_group_id'],
        ];
        return $favoriteDoctorData;
    }

    /**
     * Move favorite doctor to new group
     *
     * @param $input, $patientId
     * @return Array
     */
    public function doctorMoveToNewGroup($input, $patientId)
    {
        $doctorFavorite = $this->doctorFavorite::findOrFail($input['doctor_favorites_id']);
        $doctorFavorite->doctor_favorite_group_id = $input['new_doctor_favorite_group_id'];
        $doctorFavorite->updated_by = $patientId;
        $doctorFavorite->updated_at = Carbon::now();
        $doctorFavorite->save();

        $favoriteDoctorData = [
            'id' =>   $doctorFavorite->id,
            'doctor_id' =>   $doctorFavorite->doctor_id,
            'patient_id' =>  $doctorFavorite->patient_id,
            'doctor_favorite_group_id' =>   $doctorFavorite->doctor_favorite_group_id,
        ];
        return $favoriteDoctorData;
    }

    /**
     * Remove doctor from favorite list
     *
     * @param $input,  $patientId
     * @return boolean
     */
    public function doctorRemoveFromFavorite($input,  $patientId)
    {
        return $this->doctorFavorite::where([
            'doctor_id' => $input['doctor_id'],
            'doctor_favorite_group_id' => $input['doctor_favorite_group_id'],
            'patient_id' =>  $patientId
        ])->delete();
    }

    /**
     * Favorite doctor list with group
     *
     * @param $input,  $patientId
     * @return stdClass
     */
    public function favoriteDoctorList($input,  $patientId): array
    {
        $currentCoordinates = explode(",", $input['current_coordinates']);
        $fields = [
            'doctor_information.*', 'do1.master_data_id', 'ct.name', 'dl.address', 'dl.zipcode', 'c.id',
            'dl.id as doctor_location_id', 'users.email', 'users.phone_number', 'users.country_code', 'c.label as specialty_name',
            'ma_occu.label as occupation_name', 'dfg.name as group_name', 'df.doctor_id as doctor_id', 'df.patient_id as patient_id', 'df.id',
            DB::raw("3959 * acos(cos(radians(" . $currentCoordinates[0] . "))
                * cos(radians(dl.latitude))
                * cos(radians(dl.longitude) - radians(" . $currentCoordinates[1] . "))
                + sin(radians(" . $currentCoordinates[0] . "))
                * sin(radians(dl.latitude))) AS distance"),
            'users.status as status',
            'users.is_verified as is_verified', 'df.doctor_favorite_group_id'
        ];


        $doctorInformation = $this->doctorInformation::with('review')->leftJoin('users', 'users.id', 'doctor_information.user_id')
            ->leftJoin('doctor_favorites as df', 'df.doctor_id', 'doctor_information.user_id')
            ->leftJoin('doctor_favorite_group as dfg', 'dfg.id', 'df.doctor_favorite_group_id')
            ->leftJoin('master_sub_categories as ma_occu', 'doctor_information.occupation_id', '=', 'ma_occu.id')
            ->leftJoin('master_sub_categories as c', 'c.id', 'doctor_information.specialty_id')
            ->leftJoin('doctor_locations as dl', 'dl.doctor_id', 'doctor_information.user_id')
            ->leftJoin('doctor_overviews as do1', 'do1.doctor_id', 'doctor_information.user_id')
            ->leftJoin('doctor_overviews as do', 'do.master_data_id', 'c.id')
            ->leftJoin('cities as ct', 'ct.id', 'dl.city_id')
            ->leftJoin('states as s', 's.id', 'dl.state_id')
            ->select($fields)
            ->where([
                'df.patient_id' =>  $patientId,
                'df.deleted_at' => Null,
                'dfg.deleted_at' => Null
            ])
            ->groupBy('df.id')->orderBy('df.id', 'DESC')->get()->groupBy('group_name');

        $favoriteGroupIds = [];

        foreach ($doctorInformation as $info) {
            foreach ($info as $array) {
                $favoriteGroupIds[] = $array['doctor_favorite_group_id'];
            }
        }

        $doctorOtherFavGroups = $this->doctorFavoriteGroup::join('doctor_favorites as df', 'df.doctor_favorite_group_id', "!=", 'doctor_favorite_group.id')
            ->where([
                'doctor_favorite_group.created_by' => $patientId,
            ])
            ->whereNotIn('doctor_favorite_group.id', $favoriteGroupIds)
            ->select('doctor_favorite_group.id as doctor_favorite_group_id', 'name')
            ->get()->groupBy('name')->toArray();

        $data = [];
        $data['doctor_information'] = $doctorInformation;
        $data['other_favorite_group'] = $doctorOtherFavGroups;

        return $data;
    }

    /**
     * Get doctor slots
     *
     * @param $doctorData
     * @return DoctorTiming
     */
    public function getAppoinmentSlot($doctorData)
    {
        $doctorWorkingTime = [];

        // doctor working time data get
        $doctorWorkingTimeData = $this->doctorTiming::where(['doctor_timings.doctor_id' => $doctorData['doctor_id']]);
        $doctorBlockSlot = $this->doctorBlockSlot::where(['doctor_id' => $doctorData['doctor_id']])->whereDate('date', '>=', Carbon::today());
        $doctorAppointments = $this->appointment::with(['rescheduleAppointmentByDoctor', 'rescheduleAppointmentByPatient'])
            ->where(['doctor_id' => $doctorData['doctor_id']])
            ->where('status', '!=', config('constants.appointment_status.3'))
            ->where('status', '!=', config('constants.appointment_status.11'))
            ->where('status', '!=', config('constants.appointment_status.9'))
            ->whereDate('appointment_date', '>=', Carbon::today())
            ->whereNotNull(['status', 'start_time', 'end_time'])
            ->get();

        if (!empty($doctorData['location_id'])) {

            $doctorWorkingTimeData = $doctorWorkingTimeData->where(['location_id' => $doctorData['location_id']])
                ->leftJoin('doctor_locations as dl', 'dl.id', 'doctor_timings.location_id')
                ->select('doctor_timings.id', 'start_time', 'end_time', 'day', 'dl.appointment_duration')->orderBy('doctor_timings.start_time', 'ASC')
                ->get()->groupBy('day');

            $doctorBlockSlot = $doctorBlockSlot->where(['location_id' => $doctorData['location_id']])->get();
        } else {

            $doctorWorkingTimeData = $doctorWorkingTimeData->leftJoin('doctor_locations as dl', 'dl.id', 'doctor_timings.location_id')
                ->select('doctor_timings.id', 'start_time', 'end_time', 'day', 'dl.appointment_duration')->orderBy('doctor_timings.start_time', 'ASC')
                ->get()->groupBy('day');

            $doctorBlockSlot = $doctorBlockSlot->get();
        }

        $doctorRegistrationForm = $this->doctorInformation::where('user_id', $doctorData['doctor_id'])->first();

        if (isset($doctorRegistrationForm->registration_form)) {
            $doctorWorkingTime['registration_form'] = getImage($doctorRegistrationForm->registration_form);
        }

        $doctorWorkingTime['work_timing_data'] = $doctorWorkingTimeData;
        $doctorWorkingTime['doctor_id'] = $doctorData['doctor_id'];
        $doctorWorkingTime['location_id'] = $doctorData['location_id'] ?? Null;
        $doctorWorkingTime['block_slot'] = $doctorBlockSlot;
        $doctorWorkingTime['book_slot'] = $doctorAppointments;
        $doctorWorkingTime['video_consultancy'] = $doctorRegistrationForm->video_consultancy ?? Null;
        $doctorWorkingTime['in_person'] = $doctorRegistrationForm->in_person ?? Null;

        return $doctorWorkingTime;
    }

    /**
     * Upload registration form
     *
     * @param integer $doctorId
     * @param integer $filename
     * @return DoctorInformation
     */
    public function uploadRegistrationDoc($doctorId, $filename): DoctorInformation
    {
        $uploadRegistrationDoc = $this->doctorInformation::where('user_id', $doctorId)->first();
        $uploadRegistrationDoc->registration_form = $filename;
        $uploadRegistrationDoc->save();

        return $uploadRegistrationDoc;
    }

    /**
     * Appointment list from doctor-side
     *
     * @param  integer $offset
     * @param  integer $limit
     * @param  integer $currentPage
     * @param  array   $appointmentData
     * @return Collection
     */
    public function getDoctorsAppointmentList($appointmentData, $offset = '', $limit = '', $currentPage = '')
    {
        $appointmentInformationFields = [
            'appointments.id', 'appointments.booking_id', 'appointments.declined_reason', 'appointments.consultant_type', 'appointments.consultation_url', 'apd.patient_id', 'appointments.doctor_id', 'appointments.location_id', 'patient_information.profile_image as patient_profile_image',
            'appointments.reason', 'appointments.status', 'apd.patient_name', 'apd.patient_age', 'apd.patient_phone_number', 'apd.patient_address', 'apd.patient_state_id', 'appointments.created_at as appointment_created_on',
            'apd.patient_city_id', 'apd.patient_country_id', 'appointments.registration_form', 'appointments.appointment_date', 's.name as patient_state_name', 'ct.name as patient_city_name', 'con.name as patient_country_name', 'doctor_locations.address as location_address',
            'di.first_name', 'di.last_name', 'di.profile_image', 'di.specialty_id', 'c.label as specialty_name', 'appointments.start_time', 'appointments.end_time', 'doc.name_prefix', 'appointments.additional_note', 'chat_request.status as chat_request_status'
        ];

        $appointmentInformation = $this->appointment::leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
            ->leftJoin('patient_information', 'apd.patient_id', 'patient_information.user_id')
            ->leftJoin('countries as con', 'con.id', 'apd.patient_country_id')
            ->leftJoin('cities as ct', 'ct.id', 'apd.patient_city_id')
            ->leftJoin('states as s', 's.id', 'apd.patient_state_id')
            ->leftJoin('doctor_locations', 'doctor_locations.id', 'appointments.location_id')
            ->leftJoin('doctor_information as di', 'di.user_id', 'appointments.doctor_id')
            ->leftJoin('master_sub_categories as c', 'c.id', 'di.specialty_id')
            ->leftJoin('doctors as doc', 'doc.npi_number', 'di.npi_number')
            ->leftJoin('chat_request', function ($join) {
                $join->on('chat_request.doctor_id', '=', 'appointments.doctor_id');
                $join->on('chat_request.patient_id', '=', 'apd.patient_id');
            })
            ->with(['review', 'getAllReview', 'rescheduleAppointmentByDoctor', 'rescheduleAppointmentByPatient'])
            ->orderBy('appointments.created_at', 'DESC'); // Order by appointment created date

        $appointmentInformationCount = $appointmentInformation;

        if (!empty($appointmentData['status'])) {
            switch ($appointmentData['status']) {
                case "new":
                    $status = [config('constants.appointment_status.0')];
                    break;
                case "today":
                    $status = [config('constants.appointment_status.1')];
                    break;
                case "upcoming":
                    $status = [config('constants.appointment_status.1')];
                    break;
                case "completed":
                    $status = [config('constants.appointment_status.2')];
                    break;
                case "cancelled":
                    $status = [config('constants.appointment_status.3')];
                    break;
                case "reschedule_requested":
                    $status = [config('constants.appointment_status.4')];
                    break;
                case "wait_list":
                    $status = [config('constants.appointment_status.5')];
                    break;
                case "no_appointment_schedule":
                    $status = [config('constants.appointment_status.6')];
                    break;
                default:
                    $status = '';
            }
            if ($appointmentData['status'] == config('constants.appointment_status.4')) {
                array_push($appointmentInformationFields, 'appointment_reschedules.appointment_date as new_slot_time');
                array_push($appointmentInformationFields, 'appointments.reschedule_id as appointment_reschedules_id');
                array_push($appointmentInformationFields, 'appointment_reschedules.location_id as appointment_reschedules_location_id');
                array_push($appointmentInformationFields, 'appointment_reschedules.consultant_type as appointment_reschedules_consultant_type');
                array_push($appointmentInformationFields, 'appointment_reschedules.requested_by as appointment_reschedules_requested_by');

                $appointmentInformation = $appointmentInformation->leftJoin('appointment_reschedules', 'appointment_reschedules.appointment_id', 'appointments.id');
            }

            if ($appointmentData['status'] == 'today') {
                $appointmentInformation = $appointmentInformation->whereDate('appointments.appointment_date', Carbon::today());
            }
            if ($appointmentData['status'] == 'upcoming') {
                $appointmentInformation = $appointmentInformation->whereDate('appointments.appointment_date', '>', Carbon::today());
            }
        }
        $appointmentInformation = $appointmentInformation->select($appointmentInformationFields);

        if (Auth::user()->role_id == 3)
            $appointmentInformation = $appointmentInformation->where('apd.patient_id', Auth::user()->id);

        if (Auth::user()->role_id == 2)
            $appointmentInformation = $appointmentInformation->where('appointments.doctor_id', Auth::user()->id);

        if (!empty($appointmentData['status'])) {
            $appointmentInformation = $appointmentInformation->whereIn('appointments.status', $status);
        }
        if (!empty($appointmentData['search_name'])) {
            $appointmentInformation = $appointmentInformation->where(DB::raw('lower(apd.patient_name)'), "LIKE", "%" . strtolower($appointmentData['search_name']) . "%");
        }
        if (!empty($appointmentData['appointment_id'])) {


            $appointmentInformation = $appointmentInformation->where('appointments.id', $appointmentData['appointment_id'])->first();
        } else {
            $totalCount = $appointmentInformation->get()->count();
            if (!empty($limit) && !empty($offset)) {
                $appointmentInformation = $appointmentInformation->skip($offset + 1)->take($limit);
            }

            $appointmentInformation = $appointmentInformation->groupBy('appointments.id')->get()->toArray();


            $appointmentInformation['appointment_information'] = (count($appointmentInformation) > 0) ? $appointmentInformation : [];
            $appointmentInformation['totalRecord'] = $totalCount;
            $appointmentInformation['statusCount'] = $this->getDoctorsAppointmentStatusCount($appointmentData['search_name'], $appointmentData['status']);
            $appointmentInformation['is_read'] = $this->notification::where(['user_id' => Auth::user()->id, 'is_read' => 0])->get()->count();
        }

        return $appointmentInformation;
    }

    /**
     * Get doctor appointment booking form
     *
     * @param  integer $doctorId
     * @return Collection
     */
    public function getDoctorAppointmentBookingForm(int $doctorId)
    {
        $doctorInformation = $this->doctorInformation::where('user_id', $doctorId)->first();
        $data = [];
        if (isset($doctorInformation->registration_form)) {
            $data['registration_form'] = getImage($doctorInformation->registration_form, $doctorId);
        }
        return $data;
    }

    public function getDoctorsAppointmentStatusCount($search_name, $status)
    {
        $statusCount = [];

        $appointmentInformation = $this->appointment::select('appointments.status', 'appointments.appointment_date', 'appointments.id')
            ->leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
            ->with('rescheduleAppointmentByPatient', 'rescheduleAppointmentByDoctor');

        if (Auth::user()->role_id == 3)
            $appointmentInformation = $appointmentInformation->where('apd.patient_id', Auth::user()->id);

        if (Auth::user()->role_id == 2)
            $appointmentInformation = $appointmentInformation->where('appointments.doctor_id', Auth::user()->id);

        if (!empty($search_name)) {
            $appointmentInformation = $appointmentInformation->where(DB::raw('lower(apd.patient_name)'), "LIKE", "%" . strtolower($search_name) . "%");
        }
        $appointmentInformation = $appointmentInformation->get()->toArray();

        $statusCount['newCount']  = count(array_filter($appointmentInformation, function ($item) {
            return ($item['status'] == config('constants.appointment_status.0'));
        }));

        $statusCount['todayCount'] = count(array_filter($appointmentInformation, function ($item) {
            $today = Carbon::today()->format('Y-m-d');
            $date1 = Carbon::parse($item['appointment_date'])->format('Y-m-d');
            return (($item['status'] == config('constants.appointment_status.1')) && ($date1 == $today));
        }));

        $statusCount['upcomingCount'] =  count(array_filter($appointmentInformation, function ($item) {
            $today = Carbon::today()->format('Y-m-d');
            $date1 = Carbon::parse($item['appointment_date'])->format('Y-m-d');
            return (($item['status'] == config('constants.appointment_status.1')) && ($date1 > $today));
        }));

        $statusCount['cancelledCount'] =  count(array_filter($appointmentInformation, function ($item) {
            return ($item['status'] == config('constants.appointment_status.3'));
        }));

        $statusCount['completedCount'] =  count(array_filter($appointmentInformation, function ($item) {
            return ($item['status'] == config('constants.appointment_status.2'));
        }));

        $statusCount['rescheduleCount'] =  count(array_filter($appointmentInformation, function ($item) {
            return ($item['status'] == config('constants.appointment_status.4'));
            // if (Auth::user()->role_id == 3) {
            //     return ($item['status'] == config('constants.appointment_status.4') && !empty($item['reschedule_appointment_by_doctor']));
            // } else if (Auth::user()->role_id == 2) {
            //     return ($item['status'] == config('constants.appointment_status.4') && !empty($item['reschedule_appointment_by_patient']));
            // }
        }));

        $statusCount['waitlistCount'] =  count(array_filter($appointmentInformation, function ($item) {
            return ($item['status'] == config('constants.appointment_status.5'));
        }));

        $statusCount['offlineCount'] =  count(array_filter($appointmentInformation, function ($item) {
            return ($item['status'] == config('constants.appointment_status.6'));
        }));

        return $statusCount;
    }

    /**
     * Get doctor locations
     *
     * @param  array $data
     * @return Collection
     */
    public function getDoctorLocation($data): Collection
    {
        $doctorLocation = $this->doctorLocation::select('doctor_locations.id', 'address', 'ct.name as city_name', 's.name as state_name', 'zipcode', 'latitude', 'longitude', 'office_phone_number', 'office_fax', 'appointment_duration')
            ->leftJoin('cities as ct', 'ct.id', 'doctor_locations.city_id')
            ->leftJoin('states as s', 's.id', 'doctor_locations.state_id')
            ->where('doctor_id', $data['doctor_id'])
            ->orderBy('doctor_locations.created_at', 'DESC')->get();
        return $doctorLocation;
    }

    public function doctorAppointmentAction($appointmentData, $loggedInUserId)
    {
        $appointment = $this->appointment::where(['appointments.id' => $appointmentData['appointment_id']])
            ->leftjoin('appointment_patient_details', 'appointments.appointment_patient_id', 'appointment_patient_details.id')
            ->first();
        $otherAppointments = $this->appointment::where(['booking_id' => $appointment->booking_id])->get();

        if (!empty($otherAppointments)) {
            foreach ($otherAppointments as $appo) {
                if (($appo->appointment_date == $appointment->appointment_date) && ($appo->start_time == $appointment->start_time) && ($appointment->id != $appo->id) && ($appointmentData['status'] != config('constants.appointment_status.7'))) {
                    $appo->status = config('constants.appointment_status.11');
                    $appo->save();
                }
            }
        }

        if (isset($appointmentData['consultancy_type']))
            $consultancyType = ($appointmentData['consultancy_type'] == 'in_person') ? 'in person' : 'video';

        $appointmentInformation = [
            'status' => $appointmentData['status'],
            'declined_reason' =>  $appointmentData['declined_reason'] ?? '',
            'consultation_url' =>  $appointmentData['consultancy_url'] ?? '',
        ];

        if ($appointment->status == 'no_appointment_schedule' && isset($appointmentData['appointment_date']) && !empty($appointmentData['appointment_date'])) {
            $startTime = $appointmentData['slot_time'];
            $endTime = $appointmentData['slot_end_time'] ?? '';
            if (isset($appointmentData['location_id']) && !empty($appointmentData['location_id']) && $appointmentData['consultancy_type'] == 'in_person') {
                $doctorLocationData = $this->doctorLocation::where('id', $appointmentData['location_id'])->first();
                $appointmentDuration = !empty($doctorLocationData) ? $doctorLocationData->appointment_duration : 0;
                $endTime = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
            }

            $appointmentInformation['appointment_date'] = date("Y-m-d H:i:s", strtotime($appointmentData['appointment_date'] . " " . $appointmentData['slot_time']));
            $appointmentInformation['start_time'] = $startTime;
            $appointmentInformation['end_time'] = $endTime;
            $appointmentInformation['consultant_type'] = $consultancyType;
            $appointmentInformation['location_id'] = $appointmentData['location_id'] ?? '';
        }
        $updateAppointment = $this->appointment::where(['appointments.id' => $appointmentData['appointment_id']])->update($appointmentInformation);

        //email
        if ($updateAppointment && !empty($appointment)) {
            $providerInfo = $this->doctorInformation::where('user_id', auth()->user()->id)
                ->leftjoin('users', 'doctor_information.user_id', 'users.id')
                ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
                ->select('doctor_information.specialty_id', 'doctor_information.*', 'master_sub_categories.label')
                ->first();

            $patientInfo = $this->patientInformation::where('user_id', $appointment->patient_id)
                ->join('users', 'patient_information.user_id', 'users.id')->first();

            $year = date("Y");
            $day = date('D', strtotime($appointment->appointment_date));
            $date = date('jS M,Y', strtotime($appointment->appointment_date));
            $time = date("H:i", strtotime($appointment->start_time));
            $providerLocation = $this->doctorLocation::where('doctor_id', auth()->user()->id)->first();
            $location = !empty($providerLocation) ? $this->city::find($providerLocation->city_id)->name : '';
            $suffix = !empty($providerInfo) ? $providerInfo->suffix : '';
            $doctorImage = !empty($providerInfo) && $providerInfo->profile_image != '' ? getImage($providerInfo->profile_image, $providerInfo->user_id) : asset('/assets/images/default_profile.jpg');
            $patientImage = !empty($patientInfo) && $patientInfo->profile_image != '' ? getImage($patientInfo->profile_image, $patientInfo->user_id) : asset('/assets/images/default_profile.jpg');
            $patientFirstName = !empty($patientInfo) && !empty(explode(' ', $patientInfo->full_name)[0]) ? explode(' ', $patientInfo->full_name)[0] : '';
            $patientLastName = !empty($patientInfo) && !empty(explode(' ', $patientInfo->full_name)[1]) ? explode(' ', $patientInfo->full_name)[1] : '';
            $providerFirstName = !empty($providerInfo) ? 'Dr.' . $providerInfo->first_name : '';
            $providerLastName = !empty($providerInfo) ?  $providerInfo->last_name : '';

            $siteURL = env('FRONT_APP_URL');
            $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
            $contactusURL = env('FRONT_APP_URL') . 'patient/home';
            $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
            $findDoctorURL = env('FRONT_APP_URL') . 'patient/doctor-list';
            $imgURL = asset('/assets/images') . '/';
            $providerSEOURL = env('FRONT_APP_URL') . 'patient/doctor-profile/' . auth()->user()->id . '/' . $providerInfo->location_id;

            $resultMainArray = array(
                "{{siteUrl}}",
                "{{contactUsUrl}}",
                "{{privacyPolicyUrl}}",
                "{{termsUrl}}",
                "{{patientFindDoctorUrl}}",
                "{{providerSeoUrl}}",
                "{{patientFirstName}}",
                "{{patientShortLastName}}",
                "{{appointmentDayName}}",
                "{{appointmentMonthDate}}",
                "{{appointmentTime}}",
                "{{providerFirstName}}",
                "{{providerLastName}}",
                "{{providerSuffix}}",
                "{{providerSpecialty}}",
                "{{providerLocationName}}",
                "{{providerAddress}}",
                "{{providerPhoneNumberFormatted}}",
                "{{patientProfilePhoto}}",
                "{{providerProfilePhoto}}",
                "{{imgUrl}}",
                "{{currentYear}}",
            );

            $resultReplaceArray = array(
                $siteURL,
                $contactusURL,
                $privacyURL,
                $termsURL,
                $findDoctorURL,
                $providerSEOURL,
                $patientFirstName,
                $patientLastName,
                $day,
                $date,
                $time,
                $providerFirstName,
                $providerLastName,
                $suffix,
                $providerInfo->label,
                $location,
                $providerLocation->address ?? '',
                $providerLocation->office_phone_number ?? '',
                $patientImage,
                $doctorImage,
                $imgURL,
                $year,
            );

            if ($appointment['consultant_type'] == 'video' && $appointmentData['status'] == 'cancelled') {
                $emailTemplateData = getEmailTemplateData('appointment_cancelled_by_provider_video');
            } else if ($appointment['consultant_type'] == 'in person' && $appointmentData['status'] == 'cancelled') {
                $emailTemplateData = getEmailTemplateData('appointment_cancelled_by_provider');
            } else if ($appointmentData['consultancy_type'] == 'in_person' && $appointmentData['status'] == 'confirmed') {
                $emailTemplateData = getEmailTemplateData('appointment_confirmed');
            } else if ($appointmentData['consultancy_type'] == 'video_consultancy' && $appointmentData['status'] == 'confirmed') {
                $emailTemplateData = getEmailTemplateData('appointment_confirmed_video');
            }

            $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
            $emailData['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
            $emailData['content'] = $emailMiddleBody;
            $emailData['email'] = !empty($patientInfo) ? $patientInfo->email : "";
            //push-notification
            if (!empty($updateAppointment)) {
                $doctorDetails = $this->doctorInformation->where('user_id', $appointment->doctor_id)->first();
                $appointmentPatientDetails = $this->appointmentPatientDetail->where('id', $appointment->appointment_patient_id)->first();
                $appointmentTiming = date("M d", strtotime($appointment->appointment_date)) . ' at ' . date("H A", strtotime($appointment->start_time));

                if ($doctorDetails)
                    $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                $notificationTitle = 'Appointment Update';

                $notiDocDetails = $notiPatDetails = array();

                $notiDocDetails['templateName'] = 'appointment_update_doctor';
                $notiDocDetails['patientName'] = $appointmentPatientDetails->patient_name;

                $notiPatDetails['templateName'] = 'appointment_update_patient';
                $notiPatDetails['doctorName'] = $doctorName;

                $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_update_title';
                $notiDocDetails['appointmentStatus'] = $notiPatDetails['appointmentStatus'] = $appointmentData['status'];
                $notiDocDetails['appointmentTiming'] = $notiPatDetails['appointmentTiming'] = $appointmentTiming;

                if ($appointmentData['status'] == 'cancelled') {
                    $notificationTitle = 'Appointment Cancelled';
                    $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_cancelled_title';
                }

                if ($appointment->consultant_type == 'video') {
                    $notiDocDetails['templateName'] = 'appointment_update_doctor_video';
                    $notiPatDetails['templateName'] = 'appointment_update_patient_video';
                }

                $this->sendPushNotification($notificationTitle, $notiDocDetails, $appointment->doctor_id, $appointmentData['appointment_id'], $appointmentData['status']);
                $this->sendPushNotification($notificationTitle, $notiPatDetails, $appointmentPatientDetails->patient_id, $appointmentData['appointment_id'], $appointmentData['status']);
                $user = User::find($appointmentPatientDetails->patient_id);
                broadcast(new PushNotification($user))->toOthers();

                // SMS
                if ($appointment['allow_text_messages'] == 1) {
                    if ($appointmentData['status'] == 'cancelled') {
                        $smsNotification = new SmsNotificationService();

                        $smsNotification->sendSms(
                            $appointmentPatientDetails->patient_phone_number ?? NULL,
                            str_replace(
                                array("{{doctorName}}", "{{appointmentTiming}}"),
                                array($doctorName, $appointmentTiming),
                                getSmsTemplateData('appointment_cancelled_by_provider')
                            )
                        );
                    }

                    if ($appointmentData['status'] == 'confirmed') {
                        $smsNotification = new SmsNotificationService();

                        $smsNotification->sendSms(
                            $appointmentPatientDetails->patient_phone_number ?? NULL,
                            str_replace(
                                array("{{doctorName}}", "{{appointmentTiming}}"),
                                array($doctorName, $appointmentTiming),
                                getSmsTemplateData('appointment_confirmed')
                            )
                        );
                    }
                }
            }

            return $emailData;
            // return $updateAppointment;
        }
    }

    public function joinAppointmentAction($appointmentData, $loggedInUserId)
    {
        $appointment = $this->appointment::where(['appointments.id' => $appointmentData['appointment_id']])->first();

        if (Auth::user()->role_id == 3) {
            $jointField = 'appointment_patient_details.joint_by_patient';
        }
        if (Auth::user()->role_id == 2) {
            $jointField = 'appointment_patient_details.joint_by_doctor';
        }

        $appointmentData = $this->appointmentPatientDetail::where(['appointment_patient_details.id' => $appointment->appointment_patient_id])
            ->update([
                $jointField => 1
            ]);

        return $appointmentData;
    }
    /**
     * Doctor dashboard
     **
     * @param  int $doctorId
     * @return Collection
     */
    public function doctorDashboard(int $doctorId)
    {
        $countData = [];
        $appointmentData = $this->appointment::where('doctor_id', $doctorId)->get();

        $waitingListCount = count(array_filter($appointmentData->toArray(), function ($item) {
            return $item['status'] == 'waiting';
        }));

        $totalCount = count(array_filter($appointmentData->toArray(), function ($item) {
            return $item['status'] == 'completed';
        }));

        $offlineRequestsCount = count(array_filter($appointmentData->toArray(), function ($item) {
            return $item['status'] == 'no_appointment_schedule';
        }));

        $newBookingCount = count(array_filter($appointmentData->toArray(), function ($item) {
            return $item['appointment_date'] > today()->format('Y-m-d H:i:s');
        }));

        $startDate = Carbon::now();
        $endDate = today()->format('Y-m-d') . " 23:59:59";

        // $recentReviews = $this->doctorReview::leftJoin('patient_information as pt', 'pt.user_id', 'doctor_reviews.patient_id')
        //     ->where('doctor_id', $doctorId)->orderBy('doctor_reviews.created_at', 'DESC')
        //     ->leftJoin('doctor_information as di', 'di.user_id', 'doctor_reviews.doctor_id')
        //     ->select('doctor_reviews.*', 'pt.full_name as fullname', 'pt.profile_image', 'pt.user_id')
        //     ->get();

        $recentReviews = $this->doctorReview::join('appointments', 'appointments.booking_id', 'doctor_reviews.booking_id')
            ->join('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
            ->leftJoin('patient_information as pt', 'apd.patient_id', 'pt.user_id')
            ->select('doctor_reviews.*', 'apd.patient_name', 'pt.profile_image')
            ->where('doctor_reviews.doctor_id', $doctorId)
            ->groupBy('doctor_reviews.id')
            ->orderBy('doctor_reviews.created_at', 'DESC')
            ->take(5)
            ->get();

        $recentReviewArray = [];
        foreach ($recentReviews as $data) {
            $tempArray = [];
            $tempArray['id'] = $data->id;
            $tempArray['full_name'] = $data->patient_name;
            $tempArray['profile_image'] = !empty($data->profile_image) ? getImage($data->profile_image, $data->patient_id) : '';
            $tempArray['average_rating']  = $data->rating;
            $tempArray['time_ago'] = $data->created_at->diffForHumans();
            array_push($recentReviewArray, $tempArray);
        }

        $todayBookings = $this->appointment::where([
            ['appointments.doctor_id', $doctorId],
            ['appointments.appointment_date', ">=", $startDate],
            ['appointments.appointment_date', "<=", $endDate],
        ])->leftJoin('doctor_information as di', 'di.user_id', 'appointments.doctor_id')
            ->leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
            ->leftJoin('patient_information as pt', 'apd.patient_id', 'pt.user_id')
            ->select('appointments.*', 'pt.profile_image', 'apd.patient_name as patient_name', 'pt.user_id')
            ->where('status', config('constants.appointment_status.1'))
            ->orderBy('appointments.created_at', 'DESC')
            ->get();

        foreach ($todayBookings as $todayBooking) {
            $todayBooking->profile_image = !empty($todayBooking->profile_image) ? getImage($todayBooking->profile_image, $todayBooking->user_id) : '';
        }

        $newAppointments = $this->appointment::leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
            ->leftJoin('doctor_information as di', 'di.user_id', 'appointments.doctor_id')
            ->leftJoin('patient_information as pt', 'apd.patient_id', 'pt.user_id')
            ->select('appointments.id', 'apd.patient_name as patient_name', 'appointments.appointment_date', 'appointments.consultant_type as consultant_type', 'appointments.reason as reason', 'pt.profile_image', 'pt.user_id')
            ->where([
                ['appointments.doctor_id', $doctorId],
                ['appointments.status', 'confirmation_pending']
            ])
            ->orderBy('appointments.created_at', 'DESC')
            ->get();

        foreach ($newAppointments as $newAppointment) {
            $newAppointment->profile_image = !empty($newAppointment->profile_image) ? getImage($newAppointment->profile_image, $newAppointment->user_id) : '';
        }

        $countData['waiting_list_count'] = $waitingListCount;
        $countData['total_count'] = $totalCount;
        $countData['offline_appointment_count'] = $offlineRequestsCount;
        $countData['new_appintment_count'] = $newAppointments->count();
        $countData['today_booking_count'] = $todayBookings->count();
        $countData['today_booking'] = $todayBookings->take(5);
        $countData['recent_review'] = $recentReviewArray;
        $countData['new_appointment'] = $newAppointments->take(5);

        return $countData;
    }

    public function saveFeed($feedData, $posterImage = '', $loggedInUserId)
    {
        $doctorFeedData = $this->newsFeed::create(
            [
                'title' => $feedData['title'] ?? "",
                'description' => $feedData['description'] ?? "",
                'file' => $feedData['file'] ?? "",
                'poster_image' => $posterImage,
                'created_by' => $loggedInUserId,
            ]
        );

        $doctorFeedData = $doctorFeedData->select('id', 'title', 'description', 'status', 'file', 'poster_image')->where('id', $doctorFeedData->id)->first();
        return $doctorFeedData;
    }

    public function findFeedFromId($id, $loggedInUserId)
    {
        return $this->newsFeed::where('id', $id)->where('created_by', $loggedInUserId)->exists();
    }

    public function updateFeed($feedData, $posterImage = '', $loggedInUserId)
    {

        $updateData = [
            'title' => $feedData['title'],
            'description' => $feedData['description'],
            'updated_by' => $loggedInUserId
        ];
        if (isset($feedData['file']) && !empty($feedData['file'])) {
            $updateData['file'] = $feedData['file'];
            $updateData['poster_image'] = $posterImage ?? '';
        }
        $this->newsFeed::where('id', $feedData['id'])->update($updateData);
        $doctorFeedData = $this->newsFeed::select('id', 'title', 'description', 'status', 'file', 'poster_image')->where('id', $feedData['id'])->first();

        return $doctorFeedData;
    }

    public function deleteFeed($id, $loggedInUserId)
    {
        $doctorFeedData = $this->newsFeed::find($id);
        $doctorFeedData->deleted_by = $loggedInUserId;
        if ($doctorFeedData->save())
            $doctorFeedData->delete();

        return $doctorFeedData;
    }


    public function getFeedListData($feedData, $offset = '', $limit = '', $currentPage = '')
    {
        $feedInformationData = [];
        $feedInformationFields = [
            'news_feeds.id', 'news_feeds.title', 'news_feeds.description', 'news_feeds.file', 'news_feeds.poster_image',
            'news_feeds.created_at', 'news_feeds.status', 'news_feeds.created_by', 'di.profile_image', 'di.first_name', 'di.last_name', 'di.user_id'
        ];

        $feedInformation = $this->newsFeed::with('comments')->leftJoin('doctor_information as di', 'di.user_id', 'news_feeds.created_by')->where('news_feeds.status', 1);

        // if (!empty(Auth::user()->id)) {
        //     $feedInformation = $this->newsFeed::with('comments')->leftJoin('doctor_information as di', 'di.user_id', 'news_feeds.created_by')->where('news_feeds.status', 1)->where('news_feeds.created_by', Auth::user()->id);
        // }
        if (!empty(Auth::user()->id)) {

            $doctorIds = $this->newsFeed::leftJoin('doctor_follows', 'doctor_follows.doctor_id', 'news_feeds.created_by')->where('doctor_follows.follow_by', Auth::user()->id)
                ->pluck('doctor_follows.doctor_id')->toArray();
        }
        if (!empty($doctorIds)) {
            $doctorIds = implode(",", array_unique($doctorIds));
            // $feedInformation = $feedInformation->orderByRaw("CASE WHEN news_feeds.created_by IN (" . $doctorIds . ")then -1  Else news_feeds.created_by END ASC,news_feeds.created_by");
            $feedInformation = $feedInformation->orderByRaw("CASE WHEN news_feeds.created_by IN (" . $doctorIds . ")then news_feeds.created_at END DESC");
        }

        // for doctors-feeds-list api
        if (isset($feedData['doctor_ids']) && count($feedData['doctor_ids'])) {
            $feedInformation = $feedInformation->whereIn('news_feeds.created_by', $feedData['doctor_ids']);
        }

        if (!empty($feedData['feed_id'])) {
            $feedInformationData = $feedInformation->where('news_feeds.id', $feedData['feed_id'])->select($feedInformationFields)->first();
        } else {
            if (isset($feedData['doctor_id']) && !empty($feedData['doctor_id'])) {
                $feedInformation = $feedInformation->where('news_feeds.created_by', $feedData['doctor_id'])->orderBy('news_feeds.created_at', 'DESC');
            }
            $totalCount = $feedInformation->select($feedInformationFields)->get()->count();
            $feedInformation = $feedInformation->skip($offset)->take($limit)->orderBy('news_feeds.created_at', 'DESC')->get();
            $feedInformationData['feed_information'] = (count($feedInformation) > 0) ? $feedInformation : [];
            $feedInformationData['totalRecord'] = $totalCount;
        }
        return $feedInformationData;
    }

    public function addFeedLike($feedInfo, $loggedInUserId)
    {
        $doctorFeedLikeData = $this->feedLike::where('feed_id', $feedInfo['feed_id'])->where('like_by', $loggedInUserId)->first();
        if ($doctorFeedLikeData) {
            $doctorFeedLikeData->forceDelete();
        } else {
            $doctorFeedLikeData = $this->feedLike::create([
                'feed_id' => $feedInfo['feed_id'],
                'like_by' => $loggedInUserId,
                'created_by' => $loggedInUserId,
            ]);
        }
        return $doctorFeedLikeData;
    }

    public function addFeedComment($feedInfo, $loggedInUserId)
    {
        $doctorFeedCommentData = $this->feedComment::Create(
            [
                'feed_id' => $feedInfo['feed_id'] ?? "",
                'description' => $feedInfo['description'] ?? "",
                'comment_by' => $loggedInUserId,
                'created_by' => $loggedInUserId,
                'updated_by' => $loggedInUserId
            ]
        );
        return $doctorFeedCommentData;
    }

    public function followDoctor($feedInfo, $loggedInUserId)
    {
        $doctorFollowData = $this->doctorFollow::where('doctor_id', $feedInfo['doctor_id'])->where('follow_by', $loggedInUserId)->first();
        $type = '';
        if ($doctorFollowData) {
            $doctorFollowData->forceDelete();
            $type = 'unfollowed';
        } else {
            $doctorFollowData = $this->doctorFollow::create([
                'doctor_id' => $feedInfo['doctor_id'],
                'follow_by' => $loggedInUserId,
                'created_by' => $loggedInUserId
            ]);
            $type = ' followed';
        }
        return $type;
    }
    /**
     * getReviewList
     *
     * @param  mixed $input
     * @return void
     */
    public function getReviewList(array $input, int $offset, int $limit,)
    {
        $reviewInformation = [];
        $reviewInformationFields = [
            'doctor_reviews.*', 'pt.full_name as fullname', 'pt.profile_image', 'pt.username'
        ];
        $rating = 0;
        $doctorId = $input['doctor_id'];

        $data = DoctorReview::when(
            ($input['most_relevant']),
            function ($query) {
                $query->orderBy('doctor_reviews.rating', 'DESC');
            }
        )->when(
            ($input['new_added']),
            function ($query) {
                $query->orderBy('doctor_reviews.created_at', 'DESC');
            }
        )->when(
            ($input['old_review']),
            function ($query) {
                $query->orderBy('doctor_reviews.created_at', 'ASC');
            }
        )->leftJoin('patient_information as pt', 'pt.user_id', 'doctor_reviews.patient_id')
            ->where('doctor_id', $doctorId)
            ->select($reviewInformationFields)
            ->orderBy('doctor_reviews.created_at', 'DESC');

        $allReviews = $data->get();
        $data = $data->skip($offset)->take($limit)->get();

        if (!empty($data)) {
            foreach ($allReviews as $val) {
                $rating = $rating + $val['rating'];
            }
            $rating = sizeOf($allReviews) != 0 ? $rating / sizeOf($allReviews) : 0;
            $reviewInformation['reviewInformation'] = $data;
            $reviewInformation['allReviews'] = $allReviews;
            $reviewInformation['rating'] = $rating;
        }
        $reviewInformation['totaldata'] = count($allReviews);

        return $reviewInformation;
    }
}
