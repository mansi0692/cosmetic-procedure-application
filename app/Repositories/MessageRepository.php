<?php

namespace App\Repositories;

use App\Events\MessageSent;
use App\Events\RequestAccepted;
use App\Interfaces\MessageInterface;
use App\Models\ChatRequest;
use App\Models\DoctorInformation;
use App\Models\Message;
use App\Models\PatientInformation;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Services\EmailNotificationService;

class MessageRepository implements MessageInterface
{

    /**
     * message
     *
     * @var mixed
     */
    protected $message;

    /**
     * __construct
     *
     * @param  mixed $message
     * @return void
     */
    public function __construct(Message $message)
    {
        $this->message = $message;
    }

    /**
     * store
     *
     * @param  mixed $input
     * @return void
     */
    public function store(array $input)
    {
        $fileName = '';
        $fileType = "text";
        if (isset($input['file']) && !empty($input['file'])) {
            $fileName = uploadImage($input['file'], auth()->user()->id, '', true);

            $video = ['mp4', 'mkv', 'webm'];
            $image = ['png', 'jpg', 'jpeg', 'png', 'svg'];
            $doc = ['doc', 'docx', 'pdf'];

            $fileExt = explode('.', $fileName);
            $ext = end($fileExt);
            if (in_array($ext, $image)) {
                $fileType = "image";
            } elseif (in_array($ext, $video)) {
                $fileType = "video";
            } elseif (in_array($ext, $doc)) {
                $fileType = "document";
            }
        }

        $message = new Message();
        $message->from_user_id = auth()->user()->id;
        $message->to_user_id = $input['to_user_id'];
        $message->message = $input['message'] ?? '';
        $message->file = $fileName;
        $message->message_type = $fileType;

        if ($message->save()) {

            $broadcastMessage = $this->message::where('id', $message->id)->with('user')->first()->toArray();

            broadcast(new MessageSent($broadcastMessage))->toOthers();

            $lastMessage = $this->getLastSentMessage($message->id);
            return $lastMessage;
        }
    }

    /**
     * getMessages
     *
     * @param  mixed $loggedInUserId
     * @param  mixed $fromUserId
     * @return void
     */
    public function getMessages(int $loggedInUserId, int $fromUserId): array
    {
        $roleId = auth()->user()->role_id;
        $messages = Message::with('user')
            ->where(function ($query) use ($fromUserId, $loggedInUserId) {
                return $query->where('from_user_id', $fromUserId)->where('to_user_id', $loggedInUserId);
            })
            ->orWhere(function ($query) use ($fromUserId, $loggedInUserId) {
                return $query->where('from_user_id', $loggedInUserId)->where('to_user_id', $fromUserId);
            })
            ->orderBy('messages.created_at', 'asc')->get()
            ->map(function ($elem) use ($loggedInUserId) {
                $elem['user_id'] = $elem['user']['id'];
                $elem['role_id'] = $elem['user']['role_id'];
                $elem['sent_by_me'] = $elem['from_user_id'] == $loggedInUserId ? 1 : 0;
                unset($elem['user']);
                return $elem;
            });

        if ($roleId == 3) {
            $messages = $messages->where('deleted_by_patient', '!=', $loggedInUserId);
        }

        if ($roleId == 2) {
            $messages = $messages->where('deleted_by_doctor', '!=', $loggedInUserId);
        }

        $messages = $messages->toArray();

        if ($messages) {
            foreach ($messages as $key => $value) {

                if ($value['role_id'] == config('constants.roles.doctor')) {
                    $temp = DoctorInformation::select('first_name', 'last_name', 'profile_image')->where('user_id', $value['user_id'])->get()->toArray();
                    $messages[$key]['user'] = $temp[0]['first_name'] . ' ' . $temp[0]['last_name'];
                    $messages[$key]['profile_image'] = $temp[0]['profile_image'];
                }

                if ($value['role_id'] == config('constants.roles.patient')) {
                    $temp = PatientInformation::select('full_name', 'profile_image')->where('user_id', $value['user_id'])->get()->toArray();
                    $messages[$key]['user'] = $temp[0]['full_name'];
                    $messages[$key]['profile_image'] = $temp[0]['profile_image'];
                }

                Message::where('id', $value['id'])->update([
                    'is_read' => 1,
                ]);
            }
        }

        return $messages;
    }

    /**
     * sendChatRequest
     *
     * @param  mixed $input
     * @return object
     */
    public function sendChatRequest(array $input): object
    {
        $data = [
            'patient_id' => $input['patient_id'],
            'doctor_id' => $input['doctor_id'],
            'deleted_at' => NULL
        ];

        $chatRequest = ChatRequest::updateOrCreate($data, ['status' => $input['status']]);

        if ($input['status'] == 1) {
            $info = [
                'patient_id' => $input['patient_id'],
                'doctor_id' => $input['doctor_id'],
            ];
            broadcast(new RequestAccepted($info));
        }

        return $chatRequest;
    }

    /**
     * deleteChat
     *
     * @param $patientId, $doctorId
     * @return object
     */
    public function deleteChat(int $patientId, int $doctorId): bool
    {
        $userId = auth()->user()->id;
        $roleId = auth()->user()->role_id;

        $drText = '';
        $doctorText = '';
        $patientText = '';
        
        $patientFullName = PatientInformation::where('user_id', $patientId)->first()->full_name ?? '';
        $doctor = DoctorInformation::where('user_id', $doctorId)->first();
        $doctorFullName = $doctor ? $doctor->first_name . ' ' . $doctor->last_name : '';

        if ($roleId == 2) { // doctor
            $toEmail = User::find($patientId)->email;
            $senderName = $doctorFullName;
            $recevierName = $patientFullName;
            $doctorText = trans('extra.doctor');
        }
        if ($roleId == 3) { // patient
            $toEmail = User::find($doctorId)->email;
            $senderName = $patientFullName;
            $recevierName = $doctorFullName;
            $drText = trans('extra.Dr');
            $patientText = trans('extra.patient');
        }

        $files = Message::whereIn('from_user_id', [$patientId, $doctorId])->whereIn('to_user_id', [$doctorId, $patientId])->where('message_type', '!=', 'text')->pluck('file');

        if (count($files)) {
            foreach ($files as $file) {
                $filePathPatient = config('constants.s3.uploadUrl') . $patientId . '/chat/' . $file;
                $filePathDoctor = config('constants.s3.uploadUrl') . $doctorId . '/chat/' . $file;

                if (Storage::disk('s3')->exists($filePathPatient)) {
                    Storage::disk('s3')->delete($filePathPatient);
                }
                if (Storage::disk('s3')->exists($filePathDoctor)) {
                    Storage::disk('s3')->delete($filePathDoctor);
                }
            }
        }

        Message::whereIn('from_user_id', [$patientId, $doctorId])->whereIn('to_user_id', [$doctorId, $patientId])->forceDelete();

        $emailTemplateData = getEmailTemplateData('delete_chat');
        $subject = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
        $content = !empty($emailTemplateData) ? str_replace(
            array('[dr]', '[receiver_name]', '[doctor]', '[patient]', '[sender_name]'),
            array($drText, $recevierName, $doctorText, $patientText, $senderName),
            $emailTemplateData->template_body)
        : '';

        $mailNotification = new EmailNotificationService();
        $mailNotification->sendEmail($toEmail, $content, $subject);

        return true;
    }

    /**
     * chatRequestList
     *
     * @param  mixed $loggedInUserId
     * @return array
     */
    public function chatRequestList(int $loggedInUserId): array
    {
        $userId = $loggedInUserId;
        $roleId = auth()->user()->role_id;

        $selectFeildPatient = [
            'pi.full_name as patient_name', 'pi.profile_image', 'chat_request.status as chat_request_status', 'chat_request.patient_id'
        ];

        $selectFeildDoctor = [
            DB::raw('CONCAT(di.first_name, " ", di.last_name) AS doctor_name'), 'di.profile_image', 'chat_request.status as chat_request_status', 'chat_request.doctor_id'
        ];

        $messages = Message::where('deleted_at', NULL);

        $messages = $messages->whereIn('to_user_id', [$userId])->orWhereIn('from_user_id', [$userId]);

        if ($roleId == 3) {
            $messages = $messages->get()->map(function ($elem) use ($userId) {
                if ($elem['deleted_by_patient'] != $userId)
                    return $elem;
            });
            // $messages = $messages->where('deleted_by_patient', '!=', $loggedInUserId)->orWhereNull('deleted_by_patient');
        }

        if ($roleId == 2) {
            $messages = $messages->get()->map(function ($elem) use ($userId) {
                if ($elem['deleted_by_doctor'] != $userId)
                    return $elem;
            });
        }

        $messageData = array_filter($messages->toArray());

        $unreadMessageCount = $messages->where('is_read', 0)->toArray();

        if (auth()->user()->role_id == 2) {
            $chatRequestList = ChatRequest::where('doctor_id', $userId)
                ->leftjoin('patient_information as pi', 'chat_request.patient_id', 'pi.user_id')
                ->select($selectFeildPatient)
                ->get()
                ->toArray();
        } else if (auth()->user()->role_id == 3) {
            $chatRequestList = ChatRequest::where('patient_id', $userId)
                ->leftjoin('doctor_information as di', 'di.user_id', 'chat_request.doctor_id')
                ->select($selectFeildDoctor)
                ->get()
                ->toArray();
        }

        $data['messages'] = $messageData;
        $data['chatRequestList'] = $chatRequestList;

        return $data;
    }

    /**
     * getLastSentMessage
     *
     * @param  mixed $messageId
     * @return void
     */
    public function getLastSentMessage($messageId)
    {
        $loggedInUserId = auth()->user()->id;
        $roleId = auth()->user()->role_id;

        $messages = Message::with('user')->where('id', $messageId)->get()
            ->map(function ($elem) {
                $elem['user_id'] = $elem['user']['id'];
                $elem['role_id'] = $elem['user']['role_id'];
                $elem['sent_by_me'] = 1;
                unset($elem['user']);
                return $elem;
            });

        if ($roleId == 3) {
            $messages = $messages->where('deleted_by_patient', '!=', $loggedInUserId);
        }

        if ($roleId == 2) {
            $messages = $messages->where('deleted_by_doctor', '!=', $loggedInUserId);
        }

        $messages = $messages->toArray();

        if ($messages) {
            foreach ($messages as $key => $value) {

                if ($value['role_id'] == config('constants.roles.doctor')) {
                    $temp = DoctorInformation::select('first_name', 'last_name', 'profile_image')->where('user_id', $value['user_id'])->get()->toArray();
                    $messages[$key]['user'] = $temp[0]['first_name'] . ' ' . $temp[0]['last_name'];
                    $messages[$key]['profile_image'] = $temp[0]['profile_image'];
                }

                if ($value['role_id'] == config('constants.roles.patient')) {
                    $temp = PatientInformation::select('full_name', 'profile_image')->where('user_id', $value['user_id'])->get()->toArray();
                    $messages[$key]['user'] = $temp[0]['full_name'];
                    $messages[$key]['profile_image'] = $temp[0]['profile_image'];
                }
            }
        }

        return $messages;
    }

    /**
     * readAllMessages
     *
     * @param  mixed $loggedInUserId
     * @param  mixed $fromUserId
     * @return void
     */
    public function readAllMessages(int $loggedInUserId, int $fromUserId)
    {
        $roleId = auth()->user()->role_id;

        $messages = Message::where(function ($query) use ($fromUserId, $loggedInUserId) {
            return $query->where('from_user_id', $fromUserId)->where('to_user_id', $loggedInUserId)->where('is_read', 0);
        })->orWhere(function ($query) use ($fromUserId, $loggedInUserId) {
            return $query->where('from_user_id', $loggedInUserId)->where('to_user_id', $fromUserId)->where('is_read', 0);
        });

        if ($roleId == 3) {
            $messages = $messages->where('deleted_by_patient', '!=', $loggedInUserId);
        }

        if ($roleId == 2) {
            $messages = $messages->where('deleted_by_doctor', '!=', $loggedInUserId);
        }

        $updateMessages = $messages->update(['is_read' => 1]);
    }
}
