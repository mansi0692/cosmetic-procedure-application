<?php

namespace App\Repositories;

use App\Interfaces\FAQInterface;
use App\Models\FAQ;
use App\Models\FaqCategory;
use Illuminate\Support\Facades\Auth;

class FAQRepository implements FAQInterface
{
    /**
     * $faq
     *
     * @var mixed
     */
    protected $faq;
    /**
     * $faqCategory
     *
     * @var mixed
     */
    protected $faqCategory;

    /**
     * __construct
     *
     * @param FAQ $faq
     * @param FaqCategory $faqCategory
     */
    public function __construct(FAQ $faq, FaqCategory $faqCategory)
    {
        $this->faq = $faq;
        $this->faqCategory = $faqCategory;
    }

    /**
     * list
     *
     * @param int $faqCategoryId
     * @param int $languageId
     * 
     * @return object
     */
    public function list(int $faqCategoryId, int $languageId)
    {
        $data['faqs'] = $this->faq::where(['faq_category_id' => $faqCategoryId, 'language_id' => $languageId])->get();
        $data['categories'] = $this->faqCategory::where(['id' => $faqCategoryId, 'language_id' => $languageId])->get();
        return $data;
    }

    /**
     * changeStatus
     *
     * @param string $status
     * @param int $faqCategoryId
     * 
     * @return int
     */
    public function changeStatus(string $status, int $faqCategoryId): int
    {
        $faq = $this->faq::find($faqCategoryId);

        if ($status == 'active') {
            $faq->status = 1;
        } else {
            $faq->status = 0;
        }
        $faq->updated_by = auth()->user()->id;

        return $faq->save();
    }

    /**
     * edit
     *
     * @param int $faqsId
     * @param int $faqCategoryId
     * @param int $languageId
     * 
     * @return object
     */
    public function edit(int $faqsId, int $faqCategoryId, int $languageId): object
    {
        return $this->faq::find($faqsId);
    }

    /**
     * store
     *
     * @param array $input
     * @param int $faqsId
     * @param int $languageId
     * 
     * @return int
     */
    public function store(array $input, int $faqsId, int $languageId): int
    {
        $faq = $this->faq;
        $faq->faq_category_id = $faqsId;
        $faq->language_id = $languageId;
        $faq->title = $input['title'];
        $faq->description = $input['description'];
        return $faq->save();
    }

    /**
     * update
     *
     * @param array $input
     * @param int $faqsId
     * @param int $faqCategoryId
     * @param int $languageId
     * 
     * @return int
     */
    public function update(array $input, int $faqsId, int $faqCategoryId, int $languageId): int
    {
        $faq = $this->faq::find($faqsId);
        $faq->faq_category_id = $faqCategoryId;
        $faq->language_id = $languageId;
        $faq->title = $input['title'];
        $faq->description = $input['description'];
        $faq->updated_by = auth()->user()->id;
        return $faq->save();
    }

    /**
     * destroy
     *
     * @param int $faqsId
     * 
     * @return int
     */
    public function destroy(int $faqsId): int
    {
        $faqData = $this->faq::find($faqsId);
        $faqData->deleted_by = auth()->user()->id;
        $faqData->save();

        return $faqData->delete();
    }

    /**
     * getFAQList
     *
     * @param int $faqCategoryId
     * 
     * @return object
     */
    public function getFAQList(int $faqCategoryId): object
    {
        $languageId = getLanguageId(app()->getLocale());
        $faqCategory = $this->faqCategory::where(['parent_id' => $faqCategoryId, 'language_id' => $languageId])->get()->toArray();
        return $this->faq::where([
            'faq_category_id' => $faqCategory[0]['id'],
            'status' => 1,
            'language_id' => $languageId
        ])->get();
    }
}
