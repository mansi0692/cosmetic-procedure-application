<?php

namespace App\Repositories;

use App\Interfaces\ContactUsInterface;
use App\Models\ContactUsDetail;
use App\Models\DoctorInformation;
use App\Models\PatientInformation;
use App\Services\EmailNotificationService;
use Illuminate\Support\Facades\Auth;

class ContactUsRepository implements ContactUsInterface
{
    /**
     * contactUsDetail
     *
     * @var mixed
     */
    protected $contactUsDetail;
    /**
     * doctorInformation
     *
     * @var mixed
     */
    protected $doctorInformation;
    /**
     * patientInformation
     *
     * @var mixed
     */
    protected $patientInformation;

    public function __construct(ContactUsDetail $contactUsDetail, DoctorInformation $doctorInformation, PatientInformation $patientInformation)
    {
        $this->contactUsDetail = $contactUsDetail;
        $this->doctorInformation = $doctorInformation;
        $this->patientInformation = $patientInformation;
    }
    public function list()
    {
        $role = Auth::user()->role_id;

        $contactUs = $this->contactUsDetail::select(
            'contact_us_details.id as id',
            'contact_us_details.subject as subject',
            'contact_us_details.message as message',
            'contact_us_details.email as email',
            'contact_us_details.is_replied as is_replied',
            'contact_us_details.reply_message as reply_message'
        );

        if ($role == config('constants.roles.super_admin')) {
            $contactUs = $contactUs;
        } else if ($role == config('constants.roles.doctor_admin')) {
            $contactUs = $this->contactUsDetail::join('doctor_information as di', 'di.user_id', 'contact_us_details.user_id');
        } else if ($role == config('constants.roles.patient_admin')) {
            $contactUs = $contactUs->join('patient_information as pi', 'pi.user_id', 'contact_us_details.user_id');
        }

        return $contactUs->orderBy('created_at', 'DESC')->get();
    }

    public  function getContactUs($contactUsId)
    {
        return $this->contactUsDetail::find($contactUsId);
    }

    public function sendReply($dataParams, $contactUsId)
    {
        $contactUs = $this->contactUsDetail::find($contactUsId);

        $firstName = "";
        $doctor = $this->doctorInformation::where('user_id', $contactUs->user_id)->first();
        if (isset($doctor)) {
            $firstName = $doctor->first_name;
        }
        $patient = $this->patientInformation::where('user_id', $contactUs->user_id)->first();
        if (isset($patient) && !empty($doctor->full_name)) {
            $firstName = $doctor->full_name;
        }

        $emailTemplateData = getEmailTemplateData('contact_us_reply_notification');
        $resultMainArray = array("[first_name]", "[message]", "[content]");
        $resultReplaceArray = array($firstName, $dataParams->message, $dataParams->emailcontent);
        $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);

        $mailNotification = new EmailNotificationService();
        $content = $emailMiddleBody;
        $subject = $emailTemplateData->template_subject;
        $mailNotification->sendEmail($dataParams->email, $content, $subject);

        $contactUs->is_replied = 1;
        $contactUs->reply_message = $dataParams->emailcontent;
        return $contactUs->save();
    }
}
