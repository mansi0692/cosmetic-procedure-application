<?php

namespace App\Repositories;

use App\Interfaces\DoctorPatientAdminInterface;
use App\Models\ModelHasRole;
use App\Models\User;
use App\Services\EmailNotificationService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DoctorPatientAdminRepository implements DoctorPatientAdminInterface
{
    /**
     * modelHasRole
     *
     * @var mixed
     */
    protected $modelHasRole;
    /**
     * user
     *
     * @var mixed
     */
    protected $user;
    
    public function __construct(ModelHasRole $modelHasRole, User $user)
    {
        $this->modelHasRole = $modelHasRole;
        $this->user = $user;
    }
    public function listDoctorAdmin()
    {
        $doctorAdmins = $this->user::leftJoin('model_has_roles as mhr', 'mhr.model_id', 'users.id')
            ->where('mhr.role_id', config('constants.roles.doctor_admin'))
            ->select('users.id as id', 'users.status as status', 'users.email as email')
            ->get();

        return $doctorAdmins;
    }

    public function listPatientAdmin()
    {
        $patientAdmins = $this->user::leftJoin('model_has_roles as mhr', 'mhr.model_id', 'users.id')
            ->where('mhr.role_id', config('constants.roles.patient_admin'))
            ->select('users.id as id', 'users.status as status', 'users.email as email')
            ->get();

        return $patientAdmins;
    }

    public function saveDoctorPatientAdmin($adminData)
    {
        $user = $this->user;
        $user->email = $adminData->email;
        $user->password = Hash::make($adminData->password);

        if ($adminData->admin == 'doctor') {
            $user->role_id = config('constants.roles.doctor_admin');
        } else if ($adminData->admin == 'patient') {
            $user->role_id = config('constants.roles.patient_admin');
        }

        $user->save();

        $modelHasRole = $this->modelHasRole;
        $modelHasRole->model_id = $user->id;
        $subject = "";
        $content = [];
        if ($adminData->admin == 'doctor') {
            $subject = "Yume doctor admin invitation";
            $modelHasRole->role_id = config('constants.roles.doctor_admin');
        } else if ($adminData->admin == 'patient') {
            $subject = "Yume doctor patient invitation";
            $modelHasRole->role_id = config('constants.roles.patient_admin');
        }

        $emailTemplateData = getEmailTemplateData('admin_create_notification');
        $resultMainArray = array("[adminLoginUrl]");
        $resultReplaceArray = array(env('ADMIN_LOGIN_URL') ?? '');
        $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);
        $emailMiddleBody = $emailTemplateData->template_body;

        $mailNotification = new EmailNotificationService();
        $content = $emailMiddleBody;
        $mailNotification->sendEmail($user->email, $content, $subject);

        $modelHasRole->save();

        if ($user->save() && $modelHasRole->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function editDoctorPatientAdmin($adminId)
    {
        $adminData = $this->user::leftJoin('model_has_roles as mhr', 'mhr.model_id', 'users.id')
            ->select('users.email as email', 'mhr.role_id as role_id')
            ->where('users.id', $adminId)
            ->first();

        return $adminData;
    }

    public function updateDoctorPatientAdmin($adminData, $adminId)
    {
        $user = $this->user::find($adminId);
        $user->password = Hash::make($adminData->password);

        $user->save();

        if ($user->save()) {
            return 1;
        } else {
            return 0;
        }
    }

    public function changeStatus($adminId, $status)
    {
        $userStatus = $this->user::find($adminId);
        if ($status == 'active') {
            $userStatus->status = 0;
        } else {
            $userStatus->status = 1;
        }
        return $userStatus->save();
    }

    public function deleteAdmin($adminId)
    {
        $deleteAdmin = $this->user::find($adminId);
        $deleteAdmin->deleted_by = Auth::user()->id;
        $deleteAdmin->delete();

        $deleteRole = $this->modelHasRole::where('model_id', $adminId)->delete();
        return $deleteAdmin->save();
    }
}
