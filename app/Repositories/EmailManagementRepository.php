<?php

namespace App\Repositories;

use App\Interfaces\EmailManagementInterface;
use App\Models\EmailTemplate;
use App\Models\Language;

class EmailManagementRepository implements EmailManagementInterface
{
    /**
     * emailTemplate
     *
     * @var mixed
     */
    protected $emailTemplate;
    /**
     * $language
     *
     * @var mixed
     */
    protected $language;

    public function __construct(EmailTemplate $emailTemplate, Language $language)
    {
        $this->emailTemplate = $emailTemplate;
        $this->language = $language;
    }
    public function list($languageId)
    {
        $emailTemplate = $this->emailTemplate::where('language_id', $languageId)->get();
        $language = $this->language->get();
        return compact('emailTemplate', 'language');
    }

    public function changeStatus($emailTemplateId, $status)
    {
        $emailTemplate = $this->emailTemplate::find($emailTemplateId);
        if ($status == 'active') {
            $emailTemplate->status = 0;
        } else {
            $emailTemplate->status = 1;
        }
        return $emailTemplate->save();
    }

    public function editEmailTemplate($emailTemplateId)
    {
        return $this->emailTemplate::find($emailTemplateId);
    }

    /**
     * update
     *
     * @param mixed $input
     * @param mixed $emailTemplateId
     * 
     * @return int
     */
    public function update($input, $emailTemplateId): int
    {
        return $this->emailTemplate::find($emailTemplateId)
            ->update([
                'template_subject' => $input['subject'],
                'template_body' => $input['description'],
            ]);
    }
}
