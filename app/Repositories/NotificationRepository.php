<?php

namespace App\Repositories;

use App\Interfaces\NotificationInterface;
use App\Models\Notification;

class NotificationRepository implements NotificationInterface
{

    /**
     * $notification
     *
     * @var mixed
     */
    protected $notification;

    /**
     * __construct
     *
     * @param Notification $notification
     */
    public function __construct(Notification $notification)
    {
        $this->notification = $notification;
    }

    /**
     * getAllNotifications
     *
     * @param  int $userId
     * @return array $notifications
     */
    public function getAllNotifications($userId)
    {
        $notifications = Notification::where('user_id', $userId)->orderBy('id', 'DESC');
        $updateNotifications = $notifications->update([
            'is_read' => 1
        ]);

        $notifications = $notifications->get();

        foreach ($notifications as $notification) {
            if ($notification->details) {
                $notificationDetails = json_decode($notification->details);
                $notificationMessage = json_decode($notification->message);
            
                $notificationMessage->body = str_replace(
                    array('{{doctorName}}', '{{patientName}}', '{{appointmentStatus}}', '{{appointmentTiming}}'),
                    array($notificationDetails->doctorName ?? NULL, $notificationDetails->patientName ?? NULL, !empty($notificationDetails->appointmentStatus) ? trans('status.' . $notificationDetails->appointmentStatus) : NULL, $notificationDetails->appointmentTiming ?? NULL),
                    getNotificationTemplateData($notificationDetails->templateName)
                );
                
                if (isset($notificationDetails->notificationTitle)) {
                    $notificationMessage->title = getNotificationTemplateData($notificationDetails->notificationTitle);
                }

                $notification->message = json_encode($notificationMessage);
            }
        }

        return $notifications;
    }

    /**
     * updateNotificationStatus
     *
     * @param array $input
     * @param int $loggedInUserId
     * 
     * @return Bool
     */
    public function updateNotificationStatus(array $input, int $loggedInUserId): Bool
    {

        $updateData = $this->notification::find($input['notification_id']);

        if (!empty($updateData))
            return $updateData->update([
                'is_read' => 1,
                'updated_by' => $loggedInUserId
            ]);

        return false;
    }
}