<?php

namespace App\Repositories;

use App\Events\PushNotification;
use App\Interfaces\AuthInterface;
use App\Models\PatientInformation;
use App\Models\User;
use App\Models\UserDeviceToken;
use App\Models\UserOtp;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use App\Services\EmailNotificationService;
use App\Services\TwilioSmsService;
use Illuminate\Support\Facades\App;
use AWS;

class AuthRepository implements AuthInterface
{
    /**
     * patientInformation
     *
     * @var mixed
     */
    protected $patientInformation;
    /**
     * user
     *
     * @var mixed
     */
    protected $user;
    /**
     * userOtp
     *
     * @var mixed
     */
    protected $userOtp;
    /**
     * $userDeviceToken
     *
     * @var mixed
     */
    protected $userDeviceToken;

    public function __construct(PatientInformation $patientInformation, User $user, UserOtp $userOtp, UserDeviceToken $userDeviceToken)
    {
        $this->patientInformation = $patientInformation;
        $this->user = $user;
        $this->userOtp = $userOtp;
        $this->userDeviceToken = $userDeviceToken;
    }

    public function login($emailOrPhone, $password, $roleId, $deviceData)
    {
        /*if (is_numeric($emailOrPhone)) {
            $country_code = config('constants.country_code');
            $phoneNumber = preg_replace("/^\+?{$country_code}/", '', $emailOrPhone);
            $emailOrPhone = config('constants.country_code') . $phoneNumber;
        }*/

        $user = $this->user::where('role_id', $roleId)->where('email', $emailOrPhone)->orWhere('phone_number', $emailOrPhone)->first();
        // broadcast(new PushNotification($user))->toOthers();

        $userId = Null;

        if (!empty($user))
            $userId = $user->id;

        if (!empty($deviceData))
            $this->getDeviceToken($deviceData, $userId);

        return $user;
    }

    /**
     * logout
     *
     * @param array $input
     * @param int $loggedInUserId
     * 
     * @return Bool
     */
    public function logout(array $input, int $loggedInUserId):Bool
    {
        $data = $this->userDeviceToken::where(['user_id' => $loggedInUserId, 'device_token' => $input['device_token']]);

        if ($data->get()) {
            return $data->delete();
        }

        return false;
    }

    public function getDeviceToken($deviceData, $userId)
    {
        if (!empty($deviceData['device_token']) && !empty($deviceData['platform']) && !empty($userId)) {
            $deviceToken = UserDeviceToken::whereDeviceToken($deviceData['device_token'])->where('user_id', $userId)->first();

            if ($deviceToken == null) {
                $platformApplicationArn = '';

                if (!empty($deviceData['platform'])) {
                    $platformApplicationArn = env('ANDROID_APPLICATION_ARN');
                }

                $client = App::make('aws')->createClient('sns');
                $result = $client->createPlatformEndpoint(array(
                    'PlatformApplicationArn' => $platformApplicationArn,
                    'Token' => $deviceData['device_token'],
                ));

                $endPointArn = isset($result['EndpointArn']) ? $result['EndpointArn'] : '';
                $deviceToken = new UserDeviceToken();
                $deviceToken->platform = $deviceData['platform'];
                $deviceToken->device_token = $deviceData['device_token'];
                $deviceToken->arn = $endPointArn;
            }
            $deviceToken->user_id = $userId;
            $deviceToken->save();
        }

        return true;
    }


    /**
     * registerPatient
     *
     * @param  mixed $input
     * @return mixed
     */
    public function registerPatient($input, $deviceData): mixed
    {
        return DB::transaction(
            function () use ($input, $deviceData) {
                $user = $this->user;
                $user->email = $input['email'];
                $user->country_code = isset($input['country_code']) ? $input['country_code'] : config('constants.country_code');
                $user->phone_number = $input['phone_number'];
                $user->role_id = config('constants.roles.patient');
                $user->password = Hash::make($input['password']);
                $user->status = config('constants.status.active');
                $user->social_account_type = isset($input['social_account_type']) ? $input['social_account_type'] : null;
                $user->social_account_token = isset($input['social_account_token']) ? $input['social_account_token'] : null;
                $user->is_verified = isset($input['social_account_token']) ? 1 : 0;

                if ($user->save()) {
                    $patientInformation = $this->patientInformation::create(
                        [
                            'user_id' => $user->id,
                            'full_name' => $input['full_name'],
                            'username' => $input['username'],
                            'country_id' => $input['country'],
                            'city_id' => $input['city'],
                            'zipcode' => $input['zip_code'],
                            'state_id' => $input['state'] ?? NULL,
                            'created_by' => $user->id
                        ]
                    );

                    if (!empty($input['phone_number']))
                        $this->sendOtpToUser($user);

                    if (!empty($deviceData))
                        $this->getDeviceToken($deviceData, $user->id);

                    return $this->getPatientInformation($user->id);
                }
            }
        );
    }

    /**
     * getPatientInformation
     *
     * @param  mixed $userId
     * @return mixed
     */
    public function getPatientInformation($userId, $deviceData = []): mixed
    {

        if (!empty($deviceData))
            $this->getDeviceToken($deviceData, $userId);

        return $this->user::join('patient_information', 'patient_information.user_id', '=', 'users.id')
            ->select(
                'users.id',
                'users.role_id',
                'patient_information.full_name',
                'patient_information.username',
                'users.email',
                'users.phone_number',
                'gender',
                'profile_image',
                'address',
                'country_id',
                'city_id',
                'state_id',
                'zipcode',
                'password'
            )
            ->where('users.id', $userId)->where('role_id', config('constants.roles.patient'))->first();
    }

    /**
     * sendOtpToUser
     *
     * @param  mixed $user
     * @return int
     */
    public function sendOtpToUser($user, $isPhoneNumber = true): int
    {
        $otp = generateRandomSixDigitNumber();
        if ($isPhoneNumber) {
            // $twilioService = new TwilioSmsService();
            // $twilioService->sendSms($user->country_code . $user->phone_number, $otp);

            // Sms using aws sns
            $sms = AWS::createClient('sns');

            $response = $sms->publish([
                'Message' => 'Yume App - Your one time password for authentication is ' . $otp,
                'PhoneNumber' => $user->country_code . $user->phone_number,
                'MessageAttributes' => [
                    'AWS.SNS.SMS.SMSType'  => [
                        'DataType'    => 'String',
                        'StringValue' => 'Transactional',
                    ]
                ],
            ]);

            // $status = $response->get('@metadata');
            // $statusCode = $status['statusCode'];
        } else {
            $mailNotification = new EmailNotificationService();
            $content = 'Your one time password for authentication is ' . $otp;
            $subject = 'OTP Verification';
            $mailNotification->sendEmail($user->email, $content, $subject);
        }

        $userOtpCount = $this->user::where('id', $user->id)->first()->otp_count;
        $otpCount = $userOtpCount + 1;

        $this->user::where('id', $user->id)->update(['otp_count' => $otpCount, 'updated_at' => now()]);

        $this->userOtp::updateOrInsert(['user_id' => $user->id], ['user_id' => $user->id, 'otp' => $otp, 'created_at' => now()]);

        return $otpCount;
    }

    public function verifyOtp($userId, $otp)
    {
        return $this->userOtp::where('user_id', $userId)->latest()->first();
    }

    public function lastOtpSentTime($userId)
    {
        return $this->userOtp::where('user_id', $userId)->first()->created_at;
    }

    public function resetOtpCount($userId)
    {
        return $this->user::where('id', $userId)->update(['otp_count' => 0, 'updated_at' => now()]);
    }

    /**
     * updateNewPassword
     *
     * @param  mixed $userId
     * @param  mixed $newPassword
     * @return void
     */
    public function updateNewPassword($userId, $newPassword)
    {
        return $this->user::where('id', $userId)->update(['password' => bcrypt($newPassword), 'otp_count' => 0, 'updated_at' => now()]);
    }

    /**
     * getDoctorInformation
     *
     * @param  mixed $userId
     * @return mixed
     */
    public function getDoctorInformation($userId): mixed
    {
        return $this->user::join('doctor_information', 'doctor_information.user_id', '=', 'users.id')
            ->select(
                'users.id',
                'users.role_id',
                'first_name',
                'last_name',
                'users.email',
                'users.phone_number',
                'gender',
                'profile_image',
                'occupation_id',
                'specialty_id',
                'office_phone_number',
                'ext',
                'office_fax',
                'medical_school',
                'grad_year',
                'date_of_birth',
                'state_id',
                'about',
                'appointment_duration',
                'is_featured'
            )
            ->where('users.id', $userId)->where('role_id', config('constants.roles.doctor'))->first();
    }

    public function checkUserExistByEmailOrToken($email, $token)
    {
        if (!empty($email)) {
            return $this->user::where('email', $email)->first();
        }
        return $this->user::where('social_account_token', $token)->first();
    }
}
