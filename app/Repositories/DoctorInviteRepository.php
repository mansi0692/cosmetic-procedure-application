<?php

namespace App\Repositories;

use App\Interfaces\DoctorInviteInterface;
use App\Models\DoctorInvite;
use App\Models\User;
use App\Models\Doctor;
use App\Services\EmailNotificationService;

class DoctorInviteRepository implements DoctorInviteInterface
{
    /**
     * doctorInvite
     *
     * @var mixed
     */
    protected $doctorInvite;
    /**
     * user
     *
     * @var mixed
     */
    protected $user;
    public function __construct(DoctorInvite $doctorInvite, Doctor $doctors, User $user)
    {
        $this->doctorInvite = $doctorInvite;
        $this->doctors = $doctors;
        $this->user = $user;
    }
    public function list($tabNumber)
    {
        if ($tabNumber == 0) {
            $doctorInviteList = $this->doctorInvite::where('doctor_id', 0)->get();
    
            if (isset($doctorInviteList)) {
                foreach ($doctorInviteList as $data) {
                    if ($this->user::where('email', $data->email)->first()) {
                        $data->is_registered = 1;
                    } else {
                        $data->is_registered = 0;
                    }
                }
            }
        }

        if ($tabNumber == 1) {
            $doctorInviteList = $this->doctors::whereIn('id', $this->doctorInvite::where('doctor_id', '>', 0)->groupBy('doctor_id')->pluck('doctor_id'))->get();
        }
        
        return $doctorInviteList ?? [];
    }
    /**
     * sendInvitation
     *
     * @param mixed $email
     * 
     * @return Bool
     */
    public function sendInvitation($email):Bool
    {
        $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
        $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
        $registrationURL = env('FRONT_APP_URL') . 'doctor/search';
        $iosURL = env('IOS_APP_URL');
        $androidURL = env('ANDROID_APP_URL');

        $resultMainArray = array(
            '{{email}}',
            "{{privacyURL}}",
            "{{termsUrl}}",
            "{{registrationUrl}}",
            "{{iosURL}}",
            "{{androidURL}}",
        );
        $resultReplaceArray = array(
            $email,
            $privacyURL,
            $termsURL,
            $registrationURL,
            $iosURL,
            $androidURL,
        );
    
        $emailTemplateData = getEmailTemplateData('provider_invite');
        $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';

        $mailNotification = new EmailNotificationService();
        $content = $emailMiddleBody;
        $subject = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
        $mail = $mailNotification->sendEmail($email, $content, $subject);

        $doctorInvite = $this->doctorInvite;

        if ($mail) {
            $doctorInvite->email = $email;
            $doctorInvite->created_by = auth()->user()->id;
        }

        return $doctorInvite->save();
    }

    public function requestAdminToInviteDoctor($doctorId):Bool
    {
        $doctorData = $this->doctors::find($doctorId);

        $emailTemplateData = getEmailTemplateData('request_admin_to_invite_doctor');

        $subject = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
        $content = !empty($emailTemplateData) && !empty($doctorData) ? str_replace(
            array('[npiNumber]', '[doctorName]', '[specialty]'),
            array($doctorData->npi_number, $doctorData->first_name . ' ' . $doctorData->last_name, $doctorData->specialties[0] ?? ''),
            $emailTemplateData->template_body)
        : '';

        $superAdmin = User::where('role_id', config('constants.roles.super_admin'))->first();

        if ($superAdmin) {
            $mailNotification = new EmailNotificationService();
            $mailNotification->sendEmail($superAdmin->email, $content, $subject);
        }

        $doctorInvite = $this->doctorInvite;
        $doctorInvite->doctor_id = $doctorId;

        return $doctorInvite->save();
    }
}
