<?php

namespace App\Repositories;

use App\Interfaces\CustomMailInterface;
use App\Jobs\CustomMailJob;
use App\Models\CustomMailNotification;
use App\Models\DoctorInformation;
use App\Models\PatientInformation;
use App\Models\User;
use stdClass;

class CustomMailRepository implements CustomMailInterface
{
    /**
     * customMailNotification
     *
     * @var mixed
     */
    protected $customMailNotification;
    /**
     * doctorInformation
     *
     * @var mixed
     */
    protected $doctorInformation;
    /**
     * patientInformation
     *
     * @var mixed
     */
    protected $patientInformation;
    /**
     * user
     *
     * @var mixed
     */
    protected $user;
    public function __construct(CustomMailNotification $customMailNotification, DoctorInformation $doctorInformation, PatientInformation $patientInformation, User $user)
    {
        $this->customMailNotification = $customMailNotification;
        $this->doctorInformation = $doctorInformation;
        $this->patientInformation = $patientInformation;
        $this->user = $user;
    }
    public function listCustomMail()
    {
        return $this->customMailNotification::select('id', 'send_to', 'subject', 'status')->get();
    }
    public function getDoctorPatient()
    {
        $dataArray = [];
        $doctors = $this->doctorInformation::leftJoin('users as user', 'user.id', 'doctor_information.user_id')
            ->select(
                'user.email as email',
                'user.id as id',
                'doctor_information.first_name as first_name',
                'doctor_information.last_name as last_name',
            )
            ->where('user.status', 1)
            ->get();

        $doctorInfo = new stdClass();
        $doctorInfo->id = 1;
        $doctorInfo->text = "Doctors";
        $doctorInfo->children = [];
        foreach ($doctors as $data) {
            $tempArray = [];
            $tempArray['id'] = $data->id;
            $tempArray['text'] = $data->first_name . " " . $data->last_name . "(" . $data->email . ")";
            array_push($doctorInfo->children, $tempArray);
        }
        array_push($dataArray, $doctorInfo);

        $patients = $this->patientInformation::leftJoin('users as user', 'user.id', 'patient_information.user_id')
            ->select(
                'user.email as email',
                'user.id as id',
                'patient_information.full_name as full_name',
            )
            ->where('user.status', 1)
            ->get();

        $patientInfo = new stdClass();
        $patientInfo->id = 1;
        $patientInfo->text = "Patients";
        $patientInfo->children = [];
        foreach ($patients as $data) {
            $tempArray = [];
            $tempArray['id'] = $data->id;
            $tempArray['text'] = $data->full_name . "(" . $data->email . ")";
            array_push($patientInfo->children, $tempArray);
        }
        array_push($dataArray, $patientInfo);

        return $dataArray;
    }

    public function sendCustomMail($dataParams)
    {
        $sendEmailIdArray = [];
        if ($dataParams->send_to == 'specific_user') {
            foreach ($dataParams->email as $data) {
                $email = $this->user::find($data);
                if (isset($email)) {
                    dispatch(new CustomMailJob($email->email, $dataParams->emailcontent, $dataParams->subject));
                    array_push($sendEmailIdArray, $email->id);
                }
            }
        } else {
            $doctors = $this->doctorInformation::leftJoin('users as user', 'user.id', 'doctor_information.user_id')
                ->select(
                    'user.email as email',
                    'user.id as id',
                    'doctor_information.first_name as first_name',
                    'doctor_information.last_name as last_name',
                )
                ->where('user.status', 1)
                ->get();


            foreach ($doctors as $data) {
                $email = $this->user::find($data->id);
                if (isset($email)) {
                    dispatch(new CustomMailJob($email->email, $dataParams->emailcontent, $dataParams->subject));
                    array_push($sendEmailIdArray, $email->id);
                }
            }
            $patients = $this->patientInformation::leftJoin('users as user', 'user.id', 'patient_information.user_id')
                ->select(
                    'user.email as email',
                    'user.id as id',
                    'patient_information.full_name as full_name',
                )
                ->where('user.status', 1)
                ->get();

            foreach ($patients as $data) {
                $email = $this->user::find($data->id);
                if (isset($email)) {
                    dispatch(new CustomMailJob($email->email, $dataParams->emailcontent, $dataParams->subject));
                    array_push($sendEmailIdArray, $email->id);
                }
            }
        }


        $customMail = $this->customMailNotification;
        $customMail->send_to = $dataParams->send_to;
        $customMail->send_to_id = $sendEmailIdArray;
        $customMail->subject = $dataParams->subject;
        $customMail->content = $dataParams->emailcontent;
        $customMail->status = 1;
        return $customMail->save();
    }


    public function viewCustomMail($customMailId)
    {
        $customMailNotification = $this->customMailNotification::find($customMailId);
        $dataArray = [];
        $dataArray['emails'] = [];
        if (isset($customMailNotification) && sizeof($customMailNotification->send_to_id)) {
            foreach ($customMailNotification->send_to_id as $sendToId) {
                $email = $this->user::find($sendToId);
                if (isset($email)) {
                    array_push($dataArray['emails'], $email->email);
                }
            }
        }
        $dataArray['data'] = $customMailNotification;

        return $dataArray;
    }
}
