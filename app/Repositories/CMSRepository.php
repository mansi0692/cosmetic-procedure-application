<?php

namespace App\Repositories;

use App\Interfaces\CMSInterface;
use App\Models\CmsPage;
use App\Models\Language;
use Illuminate\Support\Facades\Auth;

class CMSRepository implements CMSInterface
{
    /**
     * cmsPage
     *
     * @var mixed
     */
    protected $cmsPage;
    public function __construct(Language $language, CmsPage $cmsPage)
    {
        $this->language = $language;
        $this->cmsPage = $cmsPage;
    }
    public function list($languageId): array
    {
        $cmsPages = $this->cmsPage->where('language_id', $languageId)->get();
        $languages = $this->language->get();
        return compact('cmsPages', 'languages');
    }

    public function saveCMS($dataParams)
    {
        $cms = $this->cmsPage;
        $cms->title = $dataParams->title;
        $cms->language_id = $dataParams->language_id;
        $cms->description = $dataParams->description;
        $cms->url = $dataParams->url;

        return $cms->save();
    }

    public function editCMS($cmsPageId)
    {
        return $this->cmsPage::find($cmsPageId);
    }

    public function updateCMS($dataParams, $cmsPageId)
    {
        $cms = $this->cmsPage::find($cmsPageId);
        $cms->title = $dataParams->title;
        $cms->description = $dataParams->description;

        return $cms->save();
    }

    public function changeStatus($doctorId, $status): bool
    {
        $changeStatus = $this->cmsPage::find($doctorId);
        if ($status == 'active') {
            $changeStatus->status = 0;
        } else {
            $changeStatus->status = 1;
        }
        return $changeStatus->save();
    }

    public function deleteCMS($cmsPageId)
    {
        $deleteCMS = $this->cmsPage::find($cmsPageId);
        $deleteCMS->deleted_by = Auth::user()->id;
        $deleteCMS->delete();
        return $deleteCMS->save();
    }

    /**
     * Get cms content by url
     **
     * @author Heli Patel
     * @param  mixed $cmsUrl
     * @return $cmsContent
     */
    public function getCmsContent($cmsUrl)
    {
        $languageId = getLanguageId(app()->getLocale());
        $cmsContent = $this->cmsPage::select('id', 'title', 'description', 'url', 'status')->where(['url' => $cmsUrl, 'status' => 1])->where('language_id', $languageId)->first();
        return $cmsContent;
    }
}
