<?php

namespace App\Repositories;

use App\Interfaces\SmsManagementInterface;
use App\Models\SmsTemplate;
use App\Models\Language;

class SmsManagementRepository implements SmsManagementInterface
{
    /**
     * smsTemplate
     *
     * @var mixed
     */
    protected $smsTemplate;
    /**
     * $language
     *
     * @var mixed
     */
    protected $language;

    public function __construct(SmsTemplate $smsTemplate, Language $language)
    {
        $this->smsTemplate = $smsTemplate;
        $this->language = $language;
    }
    public function list($languageId)
    {
        $smsTemplate = $this->smsTemplate::where('language_id', $languageId)->get();
        $language = $this->language->get();
        return compact('smsTemplate', 'language');
    }

    public function changeStatus($smsTemplateId, $status)
    {
        $smsTemplate = $this->smsTemplate::find($smsTemplateId);
        if ($status == 'active') {
            $smsTemplate->status = 0;
        } else {
            $smsTemplate->status = 1;
        }
        return $smsTemplate->save();
    }

    public function editSmsTemplate($smsTemplateId)
    {
        return $this->smsTemplate::find($smsTemplateId);
    }

    /**
     * update
     *
     * @param mixed $input
     * @param mixed $smsTemplateId
     * 
     * @return int
     */
    public function update($input, $smsTemplateId): int
    {
        return $this->smsTemplate::find($smsTemplateId)
            ->update([
                'message' => $input['message'],
            ]);
    }
}
