<?php

namespace App\Repositories;

use App\Events\PushNotification;
use App\Interfaces\PatientInterface;
use App\Models\Appointment;
use App\Models\AppointmentReschedule;
use App\Models\AppointmentPatientDetail;
use App\Models\City;
use App\Models\ContactUsDetail;
use App\Models\Country;
use App\Models\DoctorInformation;
use App\Models\DoctorLocation;
use App\Models\DoctorReview;
use App\Models\MasterSubCategory;
use App\Models\PatientInformation;
use App\Models\State;
use App\Models\User;
use App\Models\NewsFeed;
use App\Models\ChatRequest;
use App\Models\CustomMailNotification;
use App\Models\DoctorFavorite;
use App\Models\DoctorFollow;
use App\Models\FeedComment;
use App\Models\FeedLike;
use App\Models\Message;
use App\Models\Notification;
use App\Models\Setting;
use App\Models\Testimonial;
use App\Models\UserOtp;
use App\Models\UserDeviceToken;
use App\Models\DoctorFavoriteGroup;
use App\Services\EmailNotificationService;
use App\Services\SmsNotificationService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use stdClass;
use App\Traits\NotificationTrait;

class PatientRepository implements PatientInterface
{
    use NotificationTrait;

    /**
     * appoinment
     *
     * @var mixed
     */
    protected $appointment;
    /**
     * masterSubCategory
     *
     * @var mixed
     */
    protected $masterSubCategory;
    /**
     * appointmentReschedule
     *
     * @var mixed
     */
    protected $appointmentReschedule;
    /**
     * appointmentPatientDetail
     *
     * @var mixed
     */
    protected $appointmentPatientDetail;
    /**
     * city
     *
     * @var mixed
     */
    protected $city;
    /**
     * contactUsDetail
     *
     * @var mixed
     */
    protected $contactUsDetail;
    /**
     * country
     *
     * @var mixed
     */
    protected $country;
    /**
     * DoctorInformation
     *
     * @var mixed
     */
    protected $doctorInformation;
    /**
     * doctorLocation
     *
     * @var mixed
     */
    protected $doctorLocation;
    /**
     * doctorReview
     *
     * @var mixed
     */
    protected $doctorReview;
    /**
     * patientInformation
     *
     * @var mixed
     */
    protected $patientInformation;
    /**
     * state
     *
     * @var mixed
     */
    protected $state;
    /**
     * user
     *
     * @var mixed
     */
    protected $user;
    /**
     * newsFeed
     *
     * @var mixed
     */
    protected $newsFeed;

    /**
     * mailNotification
     *
     * @var mixed
     */
    protected $mailNotification;

    public function __construct(
        Appointment $appointment,
        MasterSubCategory $masterSubCategory,
        AppointmentReschedule $appointmentReschedule,
        AppointmentPatientDetail $appointmentPatientDetail,
        City $city,
        ContactUsDetail $contactUsDetail,
        Country $country,
        DoctorInformation $doctorInformation,
        DoctorLocation $doctorLocation,
        DoctorReview $doctorReview,
        PatientInformation $patientInformation,
        State $state,
        User $user,
        NewsFeed $newsFeed,
        EmailNotificationService $mailNotification,
        SmsNotificationService $smsNotification
    ) {
        $this->appointment = $appointment;
        $this->masterSubCategory = $masterSubCategory;
        $this->appointmentReschedule = $appointmentReschedule;
        $this->appointmentPatientDetail = $appointmentPatientDetail;
        $this->city = $city;
        $this->contactUsDetail = $contactUsDetail;
        $this->country = $country;
        $this->doctorInformation = $doctorInformation;
        $this->doctorLocation = $doctorLocation;
        $this->doctorReview = $doctorReview;
        $this->patientInformation = $patientInformation;
        $this->state = $state;
        $this->user = $user;
        $this->newsFeed = $newsFeed;
        $this->mailNotification = $mailNotification;
        $this->smsNotification = $smsNotification;
    }

    public function listPatient()
    {
        $patients = $this->patientInformation::leftJoin('users as user', 'user.id', 'patient_information.user_id')
            ->leftJoin('countries as country', 'country.id', 'patient_information.country_id')
            ->leftJoin('cities as city', 'city.id', 'patient_information.city_id')
            ->leftJoin('states as state', 'state.id', 'patient_information.state_id')
            ->select(
                'user.email as email',
                'user.id as id',
                'user.status as status',
                'patient_information.full_name as full_name',
                'patient_information.gender as gender',
                'patient_information.address as address',
                'patient_information.zipcode as zipcode',
                'country.id as country_id',
                'country.name as country_name',
                'city.id as city_id',
                'city.name as city_name',
                'state.id as state_id',
                'state.name as state_name'
            )
            ->get();
        return $patients;
    }

    public function changeStatus($patientId, $status)
    {
        $userStatus = $this->user::find($patientId);
        $patientName = $this->patientInformation::where('user_id', $patientId)->first();

        if (!empty($userStatus->email)) {
            $emailTemplateData = getEmailTemplateData('account_status_change_notification');
            $resultMainArray = array("[status]", "[first_name]");
            $resultReplaceArray = array(ucfirst($status), $patientName->full_name);
            $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);

            $mailNotification = new EmailNotificationService();
            $content = $emailMiddleBody;
            $subject = $emailTemplateData->template_subject;
            $mailNotification->sendEmail($userStatus->email, $content, $subject);
        }
        if ($status == 'active') {
            $userStatus->status = 0;
        } else {
            $userStatus->status = 1;
        }
        return $userStatus->save();
    }

    public function editPatient($patientId)
    {
        $allData = [];
        $patientData = new stdClass();
        $user = $this->user::find($patientId);
        $patientData->email = $user->email;
        $patientData->phone_number = $user->phone_number;

        $patientInformation = $this->patientInformation::where('user_id', $patientId)->first();
        $patientData->full_name = isset($patientInformation->full_name) ? $patientInformation->full_name : "";
        $patientData->profile_image = isset($patientInformation->profile_image) ? $patientInformation->profile_image : "";
        $patientData->username = isset($patientInformation->username) ? $patientInformation->username : "";
        $patientData->gender = isset($patientInformation->gender) ? $patientInformation->gender : "";
        $patientData->address = isset($patientInformation->address) ? $patientInformation->address : "";
        $patientData->zipcode = isset($patientInformation->zipcode) ? $patientInformation->zipcode : "";

        if (isset($patientInformation->city_id)) {
            $city = $this->city::find($patientInformation->city_id);
            $patientData->city_id = isset($city) ? $city->id : "";
            $patientData->city_name = isset($city) ? $city->name : "";
        }

        if (isset($patientInformation->state_id)) {
            $state = $this->state::find($patientInformation->state_id);
            $patientData->state_id = isset($state) ? $state->id : "";
            $patientData->state_name = isset($state) ? $state->name : "";
        }

        if (isset($patientInformation->state_id)) {
            $country = $this->country::find($patientInformation->country_id);
            $patientData->country_id = $country->id;
            $patientData->country_name = $country->name;
        }

        $patientData->cancel_request = isset($patientInformation->cancel_request) ? $patientInformation->cancel_request : "";
        $patientData->cancel_reason = isset($patientInformation->cancel_reason) ? $patientInformation->cancel_reason : "";

        $allData['patientData'] = $patientData;
        $allData['states'] = State::get();
        $allData['cities'] = $this->city::get();
        $allData['countries'] = $this->country::get();

        return $allData;
    }

    public function updatePatient($patientId, $patientParams)
    {
        $usertableInfo = $this->user::find($patientId);
        $usertableInfo->email = $patientParams->email;
        $usertableInfo->phone_number = $patientParams->phone_number;
        // $usertableInfo->password = Hash::make($patientParams->password);
        $usertableInfo->save();

        $patientInformation = $this->patientInformation::where('user_id', $patientId)->first();
        $patientInformation->full_name = $patientParams->first_name;
        $patientInformation->username = $patientParams->username;
        $patientInformation->country_id = $patientParams->country;
        $patientInformation->state_id = $patientParams->state;
        $patientInformation->city_id = $patientParams->city;
        $patientInformation->zipcode = $patientParams->zipcode;
        $patientInformation->save();

        if ($usertableInfo->save() && $patientInformation->save()) {
            $data = [
                'full_name' => $patientInformation->full_name,
                'phone_number' => $usertableInfo->phone_number,
                'email' => $usertableInfo->email,
                'country_id' => $patientInformation->country_id,
                'username' => $patientInformation->username,
                'city_id' => $patientInformation->city_id,
                'state_id' => $patientInformation->state_id,
                'zipcode' => $patientInformation->zipcode,
                'address' => $patientInformation->address,
                'profile_image' => !empty($patientInformation->profile_image) ? getImage($patientInformation->profile_image, $patientId) : ''
            ];

            return $data;
        } else {
            return 0;
        }
    }

    public function updatePatientProfile($patientId, $patientParams)
    {
        return DB::transaction(
            function () use ($patientId, $patientParams) {
                $usertableInfo = $this->user::find($patientId);
                $usertableInfo->email = $patientParams['email'];
                $usertableInfo->phone_number = $patientParams['phone_number'] ?? '';
                $usertableInfo->save();

                $patientInformation = $this->patientInformation::where('user_id', $patientId)->first();
                $patientInformation->full_name =  !empty($patientParams['full_name']) ? $patientParams['full_name'] : $patientInformation->full_name;
                $patientInformation->username = $patientParams['username'] ? $patientParams['username'] : $patientInformation->username;
                $patientInformation->profile_image = !empty($patientParams['profile_image']) ? $patientParams['profile_image'] : $patientInformation->profile_image;
                $patientInformation->address = $patientParams['address'] ? $patientParams['address'] : $patientInformation->address;
                $patientInformation->country_id  = $patientParams['country_id'] ? $patientParams['country_id'] : $patientInformation->country_id;
                $patientInformation->state_id  = $patientParams['state_id'] ? $patientParams['state_id'] : $patientInformation->state_id;
                $patientInformation->city_id  = $patientParams['city_id'] ? $patientParams['city_id'] : $patientInformation->city_id;
                $patientInformation->zipcode = $patientParams['zipcode'] ? $patientParams['zipcode'] : $patientInformation->zipcode;
                $patientInformation->save();

                if ($usertableInfo->save() && $patientInformation->save()) {
                    $data = [
                        'full_name' => $patientInformation->full_name,
                        'phone_number' => $usertableInfo->phone_number,
                        'email' => $usertableInfo->email,
                        'country_id' => $patientInformation->country_id,
                        'username' => $patientInformation->username,
                        'city_id' => $patientInformation->city_id,
                        'state_id' => $patientInformation->state_id,
                        'zipcode' => $patientInformation->zipcode,
                        'address' => $patientInformation->address,
                        'profile_image' => !empty($patientInformation->profile_image) ? getImage($patientInformation->profile_image, $patientId) : ''
                    ];

                    return $data;
                } else {
                    return 0;
                }
            }
        );
    }

    public function listPendingPatinetCancleRequest()
    {
        $patientPandingCancelRequest = $this->patientInformation::leftJoin('users as user', 'user.id', 'patient_information.user_id')
            ->where('patient_information.cancel_request', 0)
            ->select(
                'user.email as email',
                'user.id as id',
                'patient_information.full_name as full_name',
                'patient_information.cancel_request as cancel_request'
            )
            ->get();
        return $patientPandingCancelRequest;
    }
    public function listApprovedPatinetCancleRequest()
    {
        $patientApprovedCancelRequest = $this->patientInformation::leftJoin('users as user', 'user.id', 'patient_information.user_id')
            ->where('patient_information.cancel_request', 1)
            ->select(
                'user.email as email',
                'user.id as id',
                'patient_information.full_name as full_name',
                'patient_information.cancel_request as cancel_request'
            )
            ->withTrashed()
            ->get();
        return $patientApprovedCancelRequest;
    }

    public function patientCancleRequestAppprove($patientId)
    {
        try {
            $patient = $this->patientInformation::where('user_id', $patientId)->first();
            $patientFullName = $patient ? $patient->full_name : '';
            $user = $this->user::find($patientId);
            $userEmail = $user ? $user->email : '';

            $appointmentPatientIds = AppointmentPatientDetail::where('patient_id', $patientId)->pluck('id');
            AppointmentPatientDetail::whereIn('id', $appointmentPatientIds)->forceDelete();

            $appointmentIds = Appointment::whereIn('appointment_patient_id', $appointmentPatientIds)->pluck('id');
            AppointmentReschedule::whereIn('appointment_id', $appointmentIds)->forceDelete();
            Appointment::whereIn('id', $appointmentIds)->forceDelete();

            ChatRequest::where('patient_id', $patientId)->forceDelete();
            ContactUsDetail::where('user_id', $patientId)->forceDelete();

            $doctorFavoriteGroupIds = DoctorFavoriteGroup::where('created_by', $patientId)->pluck('id');
            DoctorFavoriteGroup::whereIn('id', $doctorFavoriteGroupIds)->forceDelete();

            DoctorFavorite::where('patient_id', $patientId)->orWhereIn('doctor_favorite_group_id', $doctorFavoriteGroupIds)->forceDelete();
            DoctorFollow::where('follow_by', $patientId)->orWhere('created_by', $patientId)->forceDelete();
            DoctorReview::where('patient_id', $patientId)->forceDelete();

            $newsFeedIds = NewsFeed::where('created_by', $patientId)->pluck('id');
            NewsFeed::whereIn('id', $newsFeedIds)->forceDelete();

            FeedComment::whereIn('feed_id', $newsFeedIds)->orWhere('comment_by', $patientId)->orWhere('created_by', $patientId)->forceDelete();
            FeedLike::whereIn('feed_id', $newsFeedIds)->orWhere('like_by', $patientId)->orWhere('created_by', $patientId)->forceDelete();
            Message::where('from_user_id', $patientId)->orWhere('to_user_id', $patientId)->forceDelete();
            Notification::where('user_id', $patientId)->forceDelete();
            PatientInformation::where('user_id', $patientId)->forceDelete();
            DB::table('personal_access_tokens')->where('tokenable_id', $patientId)->delete();
            Setting::where('user_id', $patientId)->forceDelete();
            Testimonial::where('created_by', $patientId)->forceDelete();
            UserDeviceToken::where('user_id', $patientId)->forceDelete();
            UserOtp::where('user_id', $patientId)->forceDelete();
            User::where('id', $patientId)->forceDelete();

            $customMailNotifications = CustomMailNotification::where('send_to_id', 'LIKE', '%' . $patientId . '%')->get();
            foreach ($customMailNotifications as $row) {
                $sendToId = $row->send_to_id;
                $key = array_search($patientId, $sendToId);
                unset($sendToId[$key]);

                CustomMailNotification::where('id', $row->id)->update([
                    'send_to_id' => $sendToId
                ]);
            }

            // delete s3 folder
            $folderPath = config('constants.s3.uploadUrl') . $patientId;
            if (Storage::disk('s3')->exists($folderPath)) {
                Storage::disk('s3')->deleteDirectory($folderPath);
            }

            $emailTemplateData = getEmailTemplateData('cancel_request_approve_notification');
            $resultMainArray = array("[first_name]");
            $resultReplaceArray = array($patientFullName);
            $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);

            $mailNotification = new EmailNotificationService();
            $content = $emailMiddleBody;
            $subject = $emailTemplateData->template_subject;
            if ($userEmail) {
                $mailNotification->sendEmail($userEmail, $content, $subject);
            }
            
            return 1;
        } catch (\Throwable $th) {
            info($th);
            return 0;
        }
    }

    public function viewPatientCancelRequest($patientId)
    {
        $patientData = new stdClass();
        $user = $this->user::where('id', $patientId)->withTrashed()->first();
        $patientData->email = $user->email;

        $patientInformation = $this->patientInformation::where('user_id', $patientId)->withTrashed()->first();
        $patientData->full_name = $patientInformation->full_name;
        $patientData->gender  = $patientInformation->gender;
        $patientData->address = $patientInformation->address;
        $patientData->zipcode = $patientInformation->zipcode;
        $patientData->cancel_request = $patientInformation->cancel_request;
        $patientData->cancel_reason = $patientInformation->cancel_reason;

        return $patientData;
    }

    public function getPatientRating($patientId)
    {
        $doctorRating = $this->doctorInformation::leftJoin('users as user', 'user.id', 'doctor_information.user_id')
            ->leftJoin('doctor_reviews as dr', 'dr.doctor_id', 'doctor_information.user_id')
            ->where('dr.patient_id', $patientId)
            ->select(
                'user.email as email',
                'doctor_information.first_name as first_name',
                'doctor_information.last_name as last_name',
                'dr.rating as rating',
                'dr.bedside_manner as bedside_manner',
                'dr.answered_questions as answered_questions',
                'dr.after_care as after_care',
                'dr.time_spent as time_spent',
                'dr.responsiveness as responsiveness',
                'dr.courtesy as courtesy',
                'dr.payment_process as payment_process',
                'dr.wait_times as wait_times',
                'dr.review as review'
            )
            ->get();

        $doctorRating = $doctorRating->toArray();

        $headerArray = [];
        $valueArray = [];
        $dataArray = [];

        foreach ($doctorRating as $key => $data) {
            $headerArray = str_replace('_', ' ', array_keys($data));
            break;
        }

        foreach ($doctorRating as $key => $data) {
            $tempArray = [];
            $tempArray['email'] = $data['email'];
            $tempArray['first_name'] = $data['first_name'];
            $tempArray['last_name'] = $data['last_name'];
            $tempArray['rating'] = $data['rating'];
            $tempArray['bedside_manner'] = $data['bedside_manner'];
            $tempArray['answered_questions'] = $data['answered_questions'];
            $tempArray['after_care'] = $data['after_care'];
            $tempArray['time_spent'] = $data['time_spent'];
            $tempArray['responsiveness'] = $data['responsiveness'];
            $tempArray['courtesy'] = $data['courtesy'];
            $tempArray['payment_process'] = $data['payment_process'];
            $tempArray['wait_times'] = $data['wait_times'];
            $tempArray['review'] = $data['review'];

            array_push($valueArray, $tempArray);
        }
        $dataArray['headers'] = $headerArray;
        $dataArray['data'] = $valueArray;

        return $dataArray;
    }

    /**
     * contactUsPatient
     *
     * @param  mixed $userId
     * @param  mixed $subject
     * @param  mixed $message
     * @param  mixed $email
     * @return ContactUsDetail
     */
    public function contactUsPatient($userId, $subject, $message, $email): ContactUsDetail
    {
        $userEmail = $this->user::find($userId);

        $toEmails = [];
        $ccEmails = [];
        $superAdminEmails = $this->user::where('role_id', config('constants.roles.super_admin'))->get();
        if (isset($superAdminEmails)) {
            foreach ($superAdminEmails as $adminEmail) {
                array_push($toEmails, $adminEmail->email);
            }
        }

        $patientName = $this->patientInformation::where('user_id', $userId)->first();
        $adminData = array(
            'patient_name' => $patientName->full_name
        );
        $patientAdminEmails = $this->user::where('role_id', config('constants.roles.patient_admin'))->get();
        if (isset($patientAdminEmails)) {
            foreach ($patientAdminEmails as $patientEmail) {
                array_push($ccEmails, $patientEmail->email);
            }
        }

        // to admin
        $emailTemplateData = getEmailTemplateData('contact_us_admin_notification');
        $resultMainArray = array("[patientName]", "[adminLoginUrl]");
        $resultReplaceArray = array($patientName->full_name, env('ADMIN_LOGIN_URL') ?? '');
        $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);
        
        $mailNotification = new EmailNotificationService();
        $mailNotification->sendEmails($toEmails, $emailMiddleBody, $emailTemplateData->template_subject, $ccEmails);

        // to user
        $emailTemplateData = getEmailTemplateData('contact_us_email');
        $resultMainArray = array("[patientName]", "[messages]");
        $resultReplaceArray = array($patientName->full_name, $message);
        $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);
        
        $mailNotification = new EmailNotificationService();
        $mailNotification->sendEmail($userEmail->email, $emailMiddleBody, $subject);

        $contactUsInformation = $this->contactUsDetail;
        $contactUsInformation->user_id = $userId;
        $contactUsInformation->subject = $subject;
        $contactUsInformation->message = $message;
        $contactUsInformation->email = $email;
        $contactUsInformation->save();

        return $contactUsInformation;
    }

    /**
     * cancelPatientAccount
     *
     * @param  mixed $userId
     * @param  mixed $reason
     * @return PatientInformation
     */
    public function cancelPatientAccount($userId, $reason): PatientInformation
    {
        $patient = $this->patientInformation::where('user_id', $userId)->first();
        $patientEmail = $this->user::find($userId);

        // to user
        $emailTemplateData = getEmailTemplateData('patient_cancel_request_patient');
        $resultMainArray = array("[patientName]");
        $resultReplaceArray = array($patient->full_name);
        $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);
        
        $mailNotification = new EmailNotificationService();
        $mailNotification->sendEmail($patientEmail->email, $emailMiddleBody, $emailTemplateData->template_subject);

        $toEmails = [];
        $ccEmails = [];
        $superAdminEmails = $this->user::where('role_id', config('constants.roles.super_admin'))->get();
        if (isset($superAdminEmails)) {
            foreach ($superAdminEmails as $email) {
                array_push($toEmails, $email->email);
            }
        }

        $patientName = $this->patientInformation::where('user_id', $userId)->first();
        $patientAdminEmails = $this->user::where('role_id', config('constants.roles.patient_admin'))->get();
        if (isset($patientAdminEmails)) {
            foreach ($patientAdminEmails as $email) {
                array_push($ccEmails, $email->email);
            }
        }

        // to admin
        $emailTemplateData = getEmailTemplateData('patient_cancel_request_admin');
        $resultMainArray = array("[patientName]");
        $resultReplaceArray = array($patientName->full_name);
        $emailMiddleBody = str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body);
        
        $mailNotification = new EmailNotificationService();
        $mailNotification->sendEmails($toEmails, $emailMiddleBody, $emailTemplateData->template_subject, $ccEmails);

        $patient->cancel_request = config('constants.patient_cancel_request.PENDING');
        $patient->cancel_reason = $reason;
        $patient->save();
        return $patient;
    }

    /**
     * Get patient profile information
     *
     * @param  $userId
     * @return $patientInformation
     */
    public function getPatientProfileInformation($userId)
    {
        $patientInformation = $this->patientInformation::leftJoin('users as user', 'user.id', 'patient_information.user_id')
            ->leftJoin('countries as country', 'country.id', 'patient_information.country_id')
            ->leftJoin('cities as city', 'city.id', 'patient_information.city_id')
            ->leftJoin('states as state', 'state.id', 'patient_information.state_id')
            ->select(
                'user.email as email',
                'user.id as id',
                'user.status as status',
                'user.phone_number',
                'patient_information.full_name as full_name',
                'patient_information.username',
                'patient_information.profile_image',
                'patient_information.gender as gender',
                'patient_information.address as address',
                'patient_information.zipcode as zipcode',
                'country.id as country_id',
                'country.name as country_name',
                'city.id as city_id',
                'city.name as city_name',
                'state.id as state_id',
                'state.name as state_name'
            )
            ->where('user_id', $userId)
            ->first();

        return $patientInformation;
    }

    /**
     * No appointment open
     *
     * @param  array $dataParams
     * @return Collection
     */
    public function noAppointmentSchedule(array $dataParams): Appointment
    {
        return DB::transaction(
            function () use ($dataParams) {

                $appointmentPatientDetail = $this->appointmentPatientDetail;
                $appointmentPatientDetail->created_by = $dataParams['created_by'];
                $appointmentPatientDetail->patient_id = $dataParams['created_by'];
                $appointmentPatientDetail->patient_name =  $dataParams['patient_name'];
                $appointmentPatientDetail->patient_age =  $dataParams['age'] ?? Null;
                $appointmentPatientDetail->patient_phone_number =  $dataParams['phone_number'] ?? Null;
                $appointmentPatientDetail->patient_address =  $dataParams['address'] ?? Null;
                $appointmentPatientDetail->appointment_for = $dataParams['appointment_for'];
                $appointmentPatientDetail->is_offline = 1;
                $appointmentPatientDetail->save();

                $patientDetailId = $appointmentPatientDetail->id;
                $this->appointment->doctor_id = $dataParams['doctor_id'];
                $this->appointment->appointment_patient_id = $patientDetailId;
                $this->appointment->booking_id = $dataParams['booking_id'];
                $this->appointment->consultant_type = $dataParams['consultancy_type'];
                $this->appointment->reason = $dataParams['consultancy_reason'];
                $this->appointment->status = config('constants.appointment_status.6');
                $this->appointment->additional_note = $dataParams['additional_information'];
                $this->appointment->location_id = $dataParams['location_id'] ?? NULL;
                $this->appointment->save();

                if (!empty($this->appointment)) {
                    $doctorDetails = $this->doctorInformation->where('user_id',  $dataParams['doctor_id'])->first();
                    $appoinmentPatientDetails = $this->appointmentPatientDetail::where('id', $this->appointment->appointment_patient_id)->first();
                    $appointmentTiming = date("M d", strtotime($this->appointment->appointment_date)) . ' at ' . date("H A", strtotime($this->appointment->start_time));

                    if ($doctorDetails)
                        $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                    $notificationTitle = 'Appointment Booking';

                    $notiDocDetails = array();
                    $notiDocDetails['templateName'] = 'appointment_booking_doctor';
                    $notiDocDetails['patientName'] = $appoinmentPatientDetails->patient_name;
                    $notiDocDetails['notificationTitle'] = 'appointment_booking_title';
                    $notiDocDetails['appointmentTiming'] = $appointmentTiming;

                    $this->sendPushNotification($notificationTitle, $notiDocDetails,  $dataParams['doctor_id'], $this->appointment->id, config('constants.appointment_status.6'));
                    $user = User::find($appoinmentPatientDetails->patient_id);
                    broadcast(new PushNotification($user))->toOthers();
                }

                return $this->appointment;
            }
        );
    }

    public function joinWaitList($dataParams)
    {
        return DB::transaction(
            function () use ($dataParams) {
                if ($dataParams->patient_id) {
                    $appointmentPatientDetail = $this->appointmentPatientDetail::find($dataParams->patient_id);
                    $appointmentPatientDetail->updated_by = $dataParams->created_by;
                    $appointmentPatientDetail->updated_at = Carbon::now();
                } else {
                    $appointmentPatientDetail = $this->appointmentPatientDetail;
                    $appointmentPatientDetail->created_by = $dataParams->created_by;
                }

                $appointmentPatientDetail->patient_id = $dataParams->user_id;
                $appointmentPatientDetail->patient_name = $dataParams->patient_name;
                $appointmentPatientDetail->patient_phone_number = $dataParams->phone_number;
                $appointmentPatientDetail->patient_age = $dataParams->age;
                $appointmentPatientDetail->appointment_for = $dataParams->appointment_for;
                $appointmentPatientDetail->patient_address = $dataParams->address;
                $appointmentPatientDetail->patient_state_id = $dataParams->state_id;
                $appointmentPatientDetail->patient_city_id = $dataParams->city_id;
                $appointmentPatientDetail->patient_country_id = $dataParams->country_id;
                $appointmentPatientDetail->save();

                $startTime = date('H:i', strtotime($dataParams->preferred_time));


                if (!empty($dataParams->location_id)) {
                    $doctorLocationData = $this->doctorLocation::where('id', $dataParams->location_id)->first();
                    $appointmentDuration = !empty($doctorLocationData) ? $doctorLocationData->appointment_duration : 0;
                    $endTime = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                }
                foreach ($dataParams->doctor_id as $doctorId) {
                    $appointments = $this->appointment;
                    $appointments->appointment_patient_id = $appointmentPatientDetail->id;
                    $appointments->doctor_id = $doctorId;
                    $appointments->location_id = $dataParams->location_id;
                    $appointments->status = config('constants.appointment_status.5');
                    $appointments->appointment_date = $dataParams->appointment_date;
                    $appointments->start_time = $startTime ?? Null;
                    $appointments->end_time = $endTime ?? Null;
                    $appointments->consultant_type = $dataParams->consultancy_type;
                    $appointments->reason = $dataParams->consultancy_reason;
                    $appointments->allow_text_messages = 0;
                    $updateAppointment = $appointments->save();
                }

                //email
                if ($updateAppointment) {
                    $providerInfo = $this->doctorInformation::whereIn('user_id', $dataParams->doctor_id)
                        ->leftjoin('users', 'doctor_information.user_id', 'users.id')
                        ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
                        ->select('doctor_information.specialty_id', 'doctor_information.*', 'users.email', 'master_sub_categories.label')
                        ->get();

                    $patientInfo = $this->patientInformation::where('user_id', $dataParams->user_id)
                        ->leftjoin('users', 'patient_information.user_id', 'users.id')
                        ->first();

                    $year = date("Y");
                    $day = date('D', strtotime($dataParams->appointment_date));
                    $date = date('jS M,Y', strtotime($dataParams->appointment_date));
                    $time = date("H:i", strtotime($dataParams->preferred_time));
                    $patientImage = !empty($patientInfo) && $patientInfo->profile_image != '' ? getImage($patientInfo->patientInfo, $patientInfo->user_id) : asset('/assets/images/default_profile.jpg');
                    $patientName = !empty($patientInfo) ? $patientInfo->full_name : '';
                    $patientPhoneNumber = auth()->user()->phone_number ?? '';
                    $reason = $dataParams->consultancy_reason ?? '';

                    $siteURL = env('FRONT_APP_URL');
                    $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
                    $contactusURL = env('FRONT_APP_URL') . 'patient/home';
                    $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
                    $findDoctorURL = env('FRONT_APP_URL') . 'patient/doctor-list';
                    $imgURL = asset('/assets/images') . '/';
                    $providerConfirmAppointmentUrl = env('FRONT_APP_URL') . 'doctor/appointments';

                    foreach ($providerInfo as $key => $doctor) {
                        if (!empty($doctor)) {
                            $providerLocation = $this->doctorLocation::where('doctor_id', $doctor->user_id)->first();
                            $location = $this->city::find($providerLocation->city_id);
                            $suffix = $doctor->suffix;
                            $doctorImage = $doctor->profile_image != '' ? getImage($doctor->profile_image, $doctor->user_id) : asset('/assets/images/default_profile.jpg');

                            $resultMainArray = array(
                                "{{providerConfirmAppointmentUrl}}",
                                "{{providerCancelAppointmentUrl}}",
                                "{{siteUrl}}",
                                "{{contactUsUrl}}",
                                "{{privacyPolicyUrl}}",
                                "{{termsUrl}}",
                                "{{patientFindDoctorUrl}}",
                                "{{patientSignInUrl}}",
                                "{{patientName}}",
                                "{{patientPhoneNumber}}",
                                "{{reasonForConsultancy}}",
                                "{{appointmentDayName}}",
                                "{{appointmentMonthDate}}",
                                "{{appointmentTime}}",
                                "{{providerFirstName}}",
                                "{{providerLastName}}",
                                "{{providerSuffix}}",
                                "{{providerSpecialty}}",
                                "{{providerLocationName}}",
                                "{{providerAddress}}",
                                "{{providerPhoneNumberFormatted}}",
                                "{{patientProfilePhoto}}",
                                "{{providerProfilePhoto}}",
                                "{{imgUrl}}",
                                "{{currentYear}}",
                            );

                            $resultReplaceArray = array(
                                $providerConfirmAppointmentUrl,
                                $providerConfirmAppointmentUrl,
                                $siteURL,
                                $contactusURL,
                                $privacyURL,
                                $termsURL,
                                $findDoctorURL,
                                $findDoctorURL,
                                $patientName,
                                $patientPhoneNumber ?? '',
                                $reason,
                                $day,
                                $date,
                                $time,
                                $doctor->first_name ?? '',
                                $doctor->last_name ?? '',
                                $suffix,
                                $doctor->label ?? '',
                                $location->name ?? '',
                                $providerLocation[$key]->address ?? '',
                                $providerLocation[$key]->office_phone_number ?? '',
                                $patientImage,
                                $doctorImage,
                                $imgURL,
                                $year,
                            );

                            if ($dataParams->consultancy_type == 'video') {
                                $emailTemplateData = getEmailTemplateData('wait_list_video_pending_to_dr');
                            }

                            if ($dataParams->consultancy_type == 'in person') {
                                $emailTemplateData = getEmailTemplateData('wait_list_pending_to_dr');
                            }

                            $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                            $temp[$key]['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                            $temp[$key]['content'] = $emailMiddleBody;
                            $temp[$key]['email'] = $doctor->email;
                        }
                    }
                    $appointments['email_data'] = isset($temp) ? $temp : '';
                }

                if (!empty($appointments)) {
                    $doctorDetails = $this->doctorInformation->where('user_id',  $doctorId)->first();
                    $appoinmentPatientDetails = $this->appointmentPatientDetail::where('id', $appointments->appointment_patient_id)->first();
                    $appointmentTiming = date("M d", strtotime($appointments->appointment_date)) . ' at ' . date("H A", strtotime($appointments->start_time));

                    if ($doctorDetails)
                        $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                    $notificationTitle = 'Appointment Booking';

                    $notiDocDetails = array();
                    $notiDocDetails['templateName'] = 'appointment_booking_doctor';
                    $notiDocDetails['patientName'] = $appoinmentPatientDetails->patient_name;
                    $notiDocDetails['notificationTitle'] = 'appointment_booking_title';
                    $notiDocDetails['appointmentTiming'] = $appointmentTiming;

                    $this->sendPushNotification($notificationTitle, $notiDocDetails,  $doctorId, $appointments->id, config('constants.appointment_status.5'));
                    $user = User::find($appoinmentPatientDetails->patient_id);
                    broadcast(new PushNotification($user))->toOthers();

                    if ($appointments->allow_text_messages == 1) {
                        $smsNotification = new SmsNotificationService();

                        $smsNotification->sendSms(
                            $appoinmentPatientDetails->patient_phone_number ?? NULL,
                            str_replace(
                                array("{{doctorName}}", "{{appointmentTiming}}"),
                                array($doctorName, $appointmentTiming),
                                getSmsTemplateData('appointment_wait_list')
                            )
                        );
                    }
                }

                return $appointments;
            }
        );
    }

    public function randomSecure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min;
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1;
        $bits = (int) $log + 1;
        $filter = (int) (1 << $bits) - 1;
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter;
        } while ($rnd > $range);
        return $min + $rnd;
    }

    public function getBookingId($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet .= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->randomSecure(0, $max - 1)];
        }

        return "#" . $token;
    }

    /**
     * Get appoinment patinet list
     *
     * @param  int $loggedInUserId
     * @return Array
     */
    public function getAppoinmentPatientList($loggedInUserId): array
    {
        $dataArray = [];
        $selectionFiledsAppoinment = [
            'appointment_patient_details.id as id',
            'appointment_patient_details.patient_id as patient_id',
            'appointment_patient_details.patient_name as patient_name',
            'appointment_patient_details.patient_phone_number as patient_phone_number',
            'appointment_patient_details.patient_address as patient_address',
            'appointment_patient_details.patient_age as patient_age',
            'appointment_patient_details.appointment_for as appointment_for',
            'city.id as city_id',
            'city.name as city_name',
            'state.id as state_id',
            'state.name as state_name',
            'country.id as country_id',
            'country.name as country_name'
        ];

        $appointmentPatientLists = $this->appointmentPatientDetail::leftJoin('cities as city', 'city.id', 'appointment_patient_details.patient_city_id')
            ->leftJoin('states as state', 'state.id', 'appointment_patient_details.patient_state_id')
            ->leftJoin('countries as country', 'country.id', 'appointment_patient_details.patient_country_id')
            ->where('appointment_patient_details.patient_id', $loggedInUserId)
            ->where('appointment_patient_details.is_offline', 0)
            ->whereNull('appointment_patient_details.deleted_at')
            ->select($selectionFiledsAppoinment)
            ->get();

        $selfRecord = array_filter($appointmentPatientLists->toArray(), function ($item) use ($loggedInUserId) {
            if ($item['appointment_for'] == 'self') {
                return $item;
            }
        });
        $age = null;
        $address = null;
        $additionalNotes = null;
        if (!empty($selfRecord)) {
            foreach ($selfRecord as $item) {
                $dataArray['patient_list'] = [];
                $tempArray = [];
                $tempArray['id'] = $item['id'];
                $tempArray['appointment_for'] = 'self';
                $tempArray['patient_name'] = $item['patient_name'];
                $tempArray['patient_address'] = $item['patient_address'];
                $tempArray['patient_phone_number'] = $item['patient_phone_number'];
                $tempArray['city_id'] = $item['city_id'];
                $tempArray['city_name'] = $item['city_name'];
                $tempArray['state_id'] = $item['state_id'];
                $tempArray['state_name'] = $item['state_name'];
                $tempArray['country_id'] = $item['country_id'];
                $tempArray['country_name'] = $item['country_name'];
                $tempArray['patient_age'] = $item['patient_age'];
                $age = $item['patient_age'];
                $address = $item['patient_address'];
                array_push($dataArray['patient_list'], $tempArray);
            }
        } else {
            $selectionFiledsSelf = [
                'patient_information.id as id',
                'patient_information.full_name as patient_name',
                'patient_information.address as patient_address',
                'user.phone_number as patient_phone_number',
                'city.id as city_id',
                'city.name as city_name',
                'state.id as state_id',
                'state.name as state_name',
                'country.id as country_id',
                'country.name as country_name'
            ];
            $selfRecord = $this->patientInformation::where('user_id', $loggedInUserId)
                ->leftJoin('cities as city', 'city.id', 'patient_information.city_id')
                ->leftJoin('states as state', 'state.id', 'patient_information.state_id')
                ->leftJoin('countries as country', 'country.id', 'patient_information.country_id')
                ->leftJoin('users as user', 'user.id', 'patient_information.user_id')
                ->select($selectionFiledsSelf)
                ->first();
            $dataArray['patient_list'] = [];
            $tempArray = [];
            $tempArray['id'] = 0;
            $tempArray['appointment_for'] = 'self';
            $tempArray['patient_name'] = $selfRecord->patient_name;
            $tempArray['patient_address'] = $selfRecord->patient_address;
            $tempArray['patient_phone_number'] = $selfRecord->patient_phone_number;
            $tempArray['city_id'] = $selfRecord->city_id;
            $tempArray['city_name'] = $selfRecord->city_name;
            $tempArray['state_id'] = $selfRecord->state_id;
            $tempArray['state_name'] = $selfRecord->state_name;
            $tempArray['country_id'] = $selfRecord->country_id;
            $tempArray['country_name'] = $selfRecord->country_name;
            $tempArray['patient_address'] = $address;
            $tempArray['patient_age'] = $age;
            array_push($dataArray['patient_list'], $tempArray);
        }

        foreach ($appointmentPatientLists as $data) {
            $tempArray = [];
            $tempArray['id'] = $data['id'];
            $tempArray['appointment_for'] = $data->appointment_for;
            $tempArray['patient_name'] = $data['patient_name'];
            $tempArray['patient_address'] = $data['patient_address'];
            $tempArray['patient_phone_number'] = $data['patient_phone_number'];
            $tempArray['patient_age'] = $data['patient_age'];
            $tempArray['city_id'] = $data['city_id'];
            $tempArray['city_name'] = $data['city_name'];
            $tempArray['state_id'] = $data['state_id'];
            $tempArray['state_name'] = $data['state_name'];
            $tempArray['country_id'] = $data['country_id'];
            $tempArray['country_name'] = $data['country_name'];
            if ($data['appointment_for'] != 'self') {
                array_push($dataArray['patient_list'], $tempArray);
            }
        }
        return $dataArray;
    }

    /**
     * Add other patient details
     *
     * @param  array $dataParams
     * @return Collection
     */
    public function addOtherPatientDetails($dataParams): array
    {
        $appoinmentPatientDetails = $this->appointmentPatientDetail;
        $appoinmentPatientDetails->patient_id = $dataParams['patient_id'];
        $appoinmentPatientDetails->appointment_for = 'someone else';
        $appoinmentPatientDetails->patient_name = $dataParams['full_name'];
        $appoinmentPatientDetails->patient_age = $dataParams['age'];
        $appoinmentPatientDetails->patient_phone_number = $dataParams['mobile_number'];
        $appoinmentPatientDetails->patient_address = $dataParams['address'];
        $appoinmentPatientDetails->patient_state_id = $dataParams['state'];
        $appoinmentPatientDetails->patient_city_id = $dataParams['city'];
        $appoinmentPatientDetails->patient_country_id = 1;
        $appoinmentPatientDetails->save();

        $tempArray = [];
        $tempArray['id'] = $appoinmentPatientDetails->id;
        $tempArray['appointment_for'] = $appoinmentPatientDetails->appointment_for;
        $tempArray['patient_name'] = $appoinmentPatientDetails->patient_name;
        $tempArray['patient_address'] = $appoinmentPatientDetails->patient_address;
        $tempArray['patient_phone_number'] = $appoinmentPatientDetails->patient_phone_number;
        $tempArray['patient_age'] = $appoinmentPatientDetails->patient_age;
        $tempArray['city_id'] = $appoinmentPatientDetails->patient_city_id;
        $tempArray['state_id'] = $appoinmentPatientDetails->patient_state_id;
        $tempArray['country_id'] = $appoinmentPatientDetails->patient_country_id;

        return $tempArray;
    }

    /**
     * Update other patient details
     *
     * @param  array $dataParams
     * @return Collection
     */
    public function updateOtherPatientDetails($dataParams): array
    {
        $appoinmentPatientDetails = $this->appointmentPatientDetail::find($dataParams['id']);
        if (!isset($appoinmentPatientDetails)) {
            $appoinmentPatientDetails = $this->appointmentPatientDetail;
        }
        $appoinmentPatientDetails->patient_id = $dataParams['patient_id'];
        $appoinmentPatientDetails->appointment_for = $dataParams['booking_for'];
        $appoinmentPatientDetails->patient_name = $dataParams['full_name'];
        $appoinmentPatientDetails->patient_age = $dataParams['age'];
        $appoinmentPatientDetails->patient_phone_number = $dataParams['mobile_number'];
        $appoinmentPatientDetails->patient_address = $dataParams['address'];
        $appoinmentPatientDetails->patient_state_id = $dataParams['state'];
        $appoinmentPatientDetails->patient_city_id = $dataParams['city'];
        $appoinmentPatientDetails->save();

        $tempArray = [];
        $tempArray['id'] = $appoinmentPatientDetails->id;
        $tempArray['appointment_for'] = $appoinmentPatientDetails->appointment_for;
        $tempArray['patient_name'] = $appoinmentPatientDetails->patient_name;
        $tempArray['patient_address'] = $appoinmentPatientDetails->patient_address;
        $tempArray['patient_phone_number'] = $appoinmentPatientDetails->patient_phone_number;
        $tempArray['patient_age'] = $appoinmentPatientDetails->patient_age;
        $tempArray['city_id'] = $appoinmentPatientDetails->patient_city_id;
        $tempArray['state_id'] = $appoinmentPatientDetails->patient_state_id;
        $tempArray['country_id'] = $appoinmentPatientDetails->patient_country_id;

        return $tempArray;
    }

    /**
     * Book appoinment
     *
     * @param  array $dataParams
     * @return Collection
     */
    public function bookAppoinment(array $dataParams): Appointment
    {
        $findSelfRecord = $this->appointmentPatientDetail::find($dataParams['id']);

        if (!isset($findSelfRecord)) {
            $findFromPatientInformation = $this->patientInformation::find($dataParams['id']);
            $appoinmentPatientDetails = $this->appointmentPatientDetail;
            $appoinmentPatientDetails->patient_id = $dataParams['patient_id'];
            $appoinmentPatientDetails->appointment_for = "self";
            $appoinmentPatientDetails->patient_name = $findFromPatientInformation->full_name;
            $appoinmentPatientDetails->patient_address = $findFromPatientInformation->address;
            $appoinmentPatientDetails->patient_state_id = $findFromPatientInformation->state_id;
            $appoinmentPatientDetails->patient_city_id = $findFromPatientInformation->city_id;
            $appoinmentPatientDetails->patient_country_id = $findFromPatientInformation->country_id;
            $appoinmentPatientDetails->save();
            $dataParams['id'] = $appoinmentPatientDetails->id;
        }

        $bookingId = $this->getBookingId(9);

        if (!empty($dataParams['booking_details'])) {

            foreach ($dataParams['booking_details'] as $bookingDetails) {

                if (!empty($bookingDetails)) {
                    $consultantType = $bookingDetails['consultancy_type'] == 'in_person' ? "in person" : 'video';
                    $appointmentDate = date("Y-m-d H:i:s", strtotime($bookingDetails['slot_date'] . " " . $bookingDetails['slot_time']));
                    $startTime = date("H:i", strtotime($bookingDetails['slot_time']));
                    $doctorLocationData = $this->doctorLocation::where('id', $bookingDetails['location'])->first();
                    $appointmentDuration = !empty($doctorLocationData) ? $doctorLocationData->appointment_duration : 0;
                    $endTime = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                }

                $appoinment = new Appointment();
                $appoinment->appointment_patient_id = $dataParams['id'] ?? Null;
                $appoinment->booking_id = $bookingId ?? '1126gfg';
                $appoinment->doctor_id = $bookingDetails['doctor_id'] ?? Null;
                $appoinment->consultant_type = $consultantType ?? Null;
                $appoinment->appointment_date = $appointmentDate ?? Null;
                $appoinment->reason = $dataParams['consultancy_reason'] ?? '';
                $appoinment->location_id = $bookingDetails['location'] ?? Null;
                $appoinment->start_time = $startTime ?? Null;
                $appoinment->end_time = $endTime ?? Null;
                $appoinment->additional_note = $dataParams['additional_note'] ?? '';
                $appoinment->status = config('constants.appointment_status.0');
                $appoinment->registration_form = '';
                $appoinment->allow_text_messages = $dataParams['allow_text_messages'] ?? Null;
                $appoinment->save();

                if (!empty($dataParams['registration_form'])) {
                    foreach ($dataParams['registration_form'] as $registrationForm) {
                        if ($registrationForm['doctor_id'] == $bookingDetails['doctor_id'])
                            $appoinment->registration_form = $registrationForm['file'];

                        $appoinment->save();
                    }
                }

                if (!empty($appoinment)) {
                    $doctorDetails = $this->doctorInformation->where('user_id',  $bookingDetails['doctor_id'])->first();
                    $appoinmentPatientDetails = $this->appointmentPatientDetail::where('id', $appoinment->appointment_patient_id)->first();
                    $appointmentTiming = date("M d", strtotime($appoinment->appointment_date)) . ' at ' . date("H A", strtotime($appoinment->start_time));

                    if ($doctorDetails)
                        $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                    $notificationTitle = 'Appointment Booking';

                    $notiDocDetails = $notiPatDetails = array();

                    $notiDocDetails['templateName'] = 'appointment_booking_doctor';
                    $notiDocDetails['patientName'] = $appoinmentPatientDetails->patient_name;

                    $notiPatDetails['templateName'] = 'appointment_booking_patient';
                    $notiPatDetails['doctorName'] = $doctorName;

                    $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_booking_title';
                    $notiDocDetails['appointmentTiming'] = $notiPatDetails['appointmentTiming'] = $appointmentTiming;

                    $this->sendPushNotification($notificationTitle, $notiDocDetails, $bookingDetails['doctor_id'], $appoinment->id, config('constants.appointment_status.0'));
                    $this->sendPushNotification($notificationTitle, $notiPatDetails, $appoinmentPatientDetails->patient_id, $appoinment->id, config('constants.appointment_status.0'));
                    $user = User::find($appoinmentPatientDetails->patient_id);
                    broadcast(new PushNotification($user))->toOthers();

                    // SMS
                    if ($appoinment->allow_text_messages == 1) {
                        $smsNotification = new SmsNotificationService();

                        $smsNotification->sendSms(
                            $appoinmentPatientDetails->patient_phone_number ?? NULL,
                            str_replace(
                                array("{{doctorName}}", "{{appointmentTiming}}"),
                                array($doctorName, $appointmentTiming),
                                getSmsTemplateData('appointment_pending')
                            )
                        );
                    }
                }
            }
        }

        return $appoinment;
    }

    /**
     * Patient Bookings
     *
     * @param  string $status,
     * @param integer $patientId
     * @return Collection
     */
    public function patientBookings(string $status, int $patientId, int $offset, int $limit, int $currentPage)
    {
        $doctorAppointmentArray = [];

        $bookingCondition = '>=';

        if ($status && $status == 'past-bookings')
            $bookingCondition = '<=';

        $start_date = Carbon::now()->toDateTimeString();
        $doctorInformationFields = [
            'dl.id as doctor_location_id', 'users.id as user_id', 'di.id as doctor_id', 'users.is_verified', 'di.is_document_verified',
            'users.email', 'users.email', 'users.country_code', 'users.phone_number', 'di.first_name', 'di.last_name', 'di.gender', 'in_person', 'video_consultancy', 'occupation_id', 'specialty_id', 'symptom_id', 'dl.office_phone_number', 'master_sub_categories.label as occupation_name', 'm.label as specialty_name', 'dl.ext', 'dl.office_fax', 'medical_school',
            'grad_year', 'date_of_birth', 'di.state_id', 'about', 'dl.appointment_duration', 'is_featured', 'grad_year', 'doc.name_prefix', 'appointments.*', 'dl.address', 'dl.zipcode', 'dl.suite', 'di.profile_image',
            'appointments.id as appointment_id', 'apd.patient_id', 'appointments.consultation_url', 'apd.patient_name'
        ];

        $getPatientAppointment = $this->appointment::with('rescheduleAppointmentByPatient', 'rescheduleAppointmentByDoctor', 'review', 'isMultipleBooking')->where([
            'apd.patient_id' => $patientId
        ])
            ->leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
            ->leftJoin('appointment_reschedules as ar', 'ar.appointment_id', 'appointments.id')
            ->leftJoin('doctor_information as di', 'di.user_id', 'appointments.doctor_id')
            ->leftJoin('master_sub_categories', 'di.occupation_id', '=', 'master_sub_categories.id')
            ->leftJoin('master_sub_categories as m', 'di.specialty_id', '=', 'm.id')
            ->leftJoin('doctors as doc', 'doc.npi_number', 'di.npi_number')
            ->leftJoin('users', 'users.id', '=', 'di.user_id')
            ->leftJoin('doctor_locations as dl', 'dl.doctor_id', 'di.user_id')
            ->select($doctorInformationFields);

        $pastBookingsCount = $this->appointment::leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')->where(['apd.patient_id' => $patientId])->where('appointment_date', '<', $start_date)->whereIn('appointments.status', [config('constants.appointment_status.2'), config('constants.appointment_status.3'), config('constants.appointment_status.9')])->groupBy('appointments.id')->get()->count();
        $upcomingBookingsCount = $this->appointment::leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')->where(['apd.patient_id' => $patientId])->where('appointment_date', '>=', $start_date)->where('appointments.status', '!=', config('constants.appointment_status.3'))->groupBy('appointments.id')->get()->count();
        $noScheduleBookingCount = $this->appointment::leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')->where(['apd.patient_id' => $patientId])->whereNull('appointments.appointment_date')->where('appointments.status', '=', config('constants.appointment_status.6'))->groupBy('appointments.id')->get()->count();

        if ($status && $status == 'upcoming-bookings')
            $getPatientAppointment = $getPatientAppointment->where('appointments.appointment_date', $bookingCondition, $start_date)->whereNotIn('appointments.status', [config('constants.appointment_status.2'), config('constants.appointment_status.3'), config('constants.appointment_status.9'), config('constants.appointment_status.11')])->where('apd.patient_id', Auth::user()->id);

        if ($status && $status == 'past-bookings')
            $getPatientAppointment = $getPatientAppointment->where('appointments.appointment_date', $bookingCondition, $start_date)->whereIn('appointments.status', [config('constants.appointment_status.2'), config('constants.appointment_status.3'), config('constants.appointment_status.9'), config('constants.appointment_status.11')]);

        if ($status && $status == 'no-appointment-schedule')
            $getPatientAppointment = $getPatientAppointment->whereNull('appointments.appointment_date')->where('appointments.status', '=', config('constants.appointment_status.6'))->where('apd.patient_id', Auth::user()->id);

        $totalCount = $getPatientAppointment->groupBy('appointments.id')->get()->count();
        // $isMultipleBooking= $totalCount > 1 ? 1 : 0;
        $getPatientAppointment = $getPatientAppointment->skip($offset)->take($limit);
        $getPatientAppointment = $getPatientAppointment->groupBy('appointments.id')->orderBy('appointments.id', 'DESC')->get();

        $doctorAppointmentArray = [];
        // $doctorAppointmentArray['is_multiple_booking'] = $isMultipleBooking;
        $doctorAppointmentArray['patient_id'] = $patientId;
        $doctorAppointmentArray['appointment_data'] = $getPatientAppointment;
        $doctorAppointmentArray['totalRecord'] = $totalCount;
        $doctorAppointmentArray['past_bookings'] = $pastBookingsCount;
        $doctorAppointmentArray['upcoming_bookings'] = $upcomingBookingsCount;
        $doctorAppointmentArray['no_appointment_schedule'] = $noScheduleBookingCount;

        return $doctorAppointmentArray;
    }

    /**
     * Add doctor review
     **
     * @param  Request $request
     * @return DoctorReview
     */
    public function addDoctorReview($dataParams): DoctorReview
    {
        $findBookingId = $this->appointment::where('booking_id', $dataParams['booking_id'])->first();

        $doctorReview = DoctorReview::where('booking_id', $dataParams['booking_id'])->first();

        $rating = ($dataParams['bedside_manner'] + $dataParams['answered_questions'] + $dataParams['after_care']
            + $dataParams['time_spent'] + $dataParams['responsiveness'] + $dataParams['courtesy']
            + $dataParams['payment_process'] + $dataParams['wait_times']) / 8;

        if (isset($doctorReview)) {
            $doctorReview->doctor_id = $findBookingId->doctor_id;
            $doctorReview->patient_id = $dataParams['patient_id'];
            $doctorReview->booking_id = $dataParams['booking_id'];
            $doctorReview->rating = $rating;
            $doctorReview->bedside_manner = $dataParams['bedside_manner'];
            $doctorReview->answered_questions = $dataParams['answered_questions'];
            $doctorReview->after_care = $dataParams['after_care'];
            $doctorReview->time_spent = $dataParams['time_spent'];
            $doctorReview->responsiveness = $dataParams['responsiveness'];
            $doctorReview->courtesy = $dataParams['courtesy'];
            $doctorReview->payment_process = $dataParams['payment_process'];
            $doctorReview->wait_times = $dataParams['wait_times'];
            $doctorReview->review = $dataParams['review'];
            $doctorReview->created_by = $dataParams['patient_id'];
            $doctorReview->save();
        } else {
            $doctorReview = $this->doctorReview;
            $doctorReview->doctor_id = $findBookingId->doctor_id;
            $doctorReview->patient_id = $dataParams['patient_id'];
            $doctorReview->booking_id = $dataParams['booking_id'];
            $doctorReview->rating = $rating;
            $doctorReview->bedside_manner = $dataParams['bedside_manner'];
            $doctorReview->answered_questions = $dataParams['answered_questions'];
            $doctorReview->after_care = $dataParams['after_care'];
            $doctorReview->time_spent = $dataParams['time_spent'];
            $doctorReview->responsiveness = $dataParams['responsiveness'];
            $doctorReview->courtesy = $dataParams['courtesy'];
            $doctorReview->payment_process = $dataParams['payment_process'];
            $doctorReview->wait_times = $dataParams['wait_times'];
            $doctorReview->review = $dataParams['review'];
            $doctorReview->created_by = $dataParams['patient_id'];
            $doctorReview->save();
        }

        return $doctorReview;
    }

    /**
     * Get doctor review
     **
     * @param  string $bookingId
     * @return DoctorReview
     */
    public function getDoctorReview(string $bookingId): mixed
    {
        $doctorReview = $this->doctorReview::where('booking_id', $bookingId)->first();
        return $doctorReview;
    }

    /**
     * Patient Reschedule Bookings
     *
     * @param array $patientRescheduleBookingData,int $loginUserId
     * @return stdClass
     */
    public function patientRescheduleBookings(array $patientRescheduleBookingData, int $loginUserId): array
    {
        return DB::transaction(
            function () use ($patientRescheduleBookingData, $loginUserId) {
                $doctorAppointmentArray = [];
                if (!empty($patientRescheduleBookingData['location_id']))
                    $doctorLocationData = $this->doctorLocation::where('id', $patientRescheduleBookingData['location_id'])->first();

                $appointmentDuration = !empty($doctorLocationData) ? $doctorLocationData->appointment_duration : 0;
                $startTime = date('H:i', strtotime($patientRescheduleBookingData['slot_time']));
                $endTime = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));

                $appointmentDate = date("Y-m-d H:i:s", strtotime($patientRescheduleBookingData['appointment_date'] . " " . $patientRescheduleBookingData['slot_time']));
                if (Auth::user()->role_id == 3) {
                    $appointmentDetails = $this->appointment::where(['appointments.id' => $patientRescheduleBookingData['appointment_id'], 'apd.patient_id' => $loginUserId])
                        ->leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
                        ->first();
                } elseif (Auth::user()->role_id == 2) {
                    $appointmentDetails = $this->appointment::where(['id' => $patientRescheduleBookingData['appointment_id'], 'doctor_id' => $loginUserId])->first();
                }

                if ($appointmentDetails) {

                    // Create reschedule appointment data for patient
                    // $appoinmentReschedule = new AppointmentReschedule();
                    $appoinmentReschedule = $this->appointmentReschedule;
                    $appoinmentReschedule->appointment_id = $patientRescheduleBookingData['appointment_id'];
                    $appoinmentReschedule->appointment_date = $appointmentDate;
                    $appoinmentReschedule->start_time = $startTime;
                    $appoinmentReschedule->end_time = $endTime;
                    $appoinmentReschedule->location_id = $patientRescheduleBookingData['location_id'] ?? Null;
                    $appoinmentReschedule->consultant_type = $patientRescheduleBookingData['consultancy_type'];
                    $appoinmentReschedule->reason = $patientRescheduleBookingData['consultancy_reason'];
                    $appoinmentReschedule->created_by = $loginUserId;
                    if (Auth::user()->role_id == 3) {
                        $appoinmentReschedule->requested_by = 'patient';
                    } elseif (Auth::user()->role_id == 2) {
                        $appoinmentReschedule->requested_by = 'doctor';
                    }
                    $appoinmentReschedule->status = config('constants.appointment_status.10');
                    $updateAppointment = $appoinmentReschedule->save();

                    // Update status in appointments


                    $appointmentData = $this->appointment::where('id', $patientRescheduleBookingData['appointment_id']);
                    $patientRescheduleBookingDataUpdate = $appointmentData->update([
                        'status' => config('constants.appointment_status.4'),
                        'reschedule_id' => $appoinmentReschedule->id
                    ]);

                    // $this->appointment::where('id', $patientRescheduleBookingData['appointment_id'])->update([
                    //     'status' => 'reschedule_requested',
                    //     'reschedule_id' => $appoinmentReschedule->id
                    // ]);

                    $appointment = $appointmentData->first();

                    if (!empty($appointment)) {
                        $doctorDetails = $this->doctorInformation->where('user_id', $appointment->doctor_id)->first();
                        $appointmentPatientDetails = $this->appointmentPatientDetail->where('id', $appointment->appointment_patient_id)->first();
                        $appointmentTiming = date("M d", strtotime($appointment->appointment_date)) . ' at ' . date("H A", strtotime($appointment->start_time));

                        if ($doctorDetails)
                            $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                        $notificationTitle = 'Appointment Update';

                        $notiDocDetails = $notiPatDetails = array();
    
                        $notiDocDetails['templateName'] = 'appointment_reschedule_request_doctor';
                        $notiDocDetails['patientName'] = $appointmentPatientDetails->patient_name;
    
                        $notiPatDetails['templateName'] = 'appointment_reschedule_request_patient';
                        $notiPatDetails['doctorName'] = $doctorName;
    
                        $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_update_title';
                        $notiDocDetails['appointmentStatus'] = $notiPatDetails['appointmentStatus'] = $appointment->status;
                        $notiDocDetails['appointmentTiming'] = $notiPatDetails['appointmentTiming'] = $appointmentTiming;

                        if ($appointment->status == 'cancelled') {
                            $notificationTitle = 'Appointment Cancelled';
                            $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_cancelled_title';
                        }

                        if ($appointment->consultant_type == 'video') {
                            $notiDocDetails['templateName'] = 'appointment_update_doctor_video';
                            $notiPatDetails['templateName'] = 'appointment_update_patient_video';
                        }

                        $this->sendPushNotification($notificationTitle, $notiDocDetails, $appointment->doctor_id, $appointment->id, config('constants.appointment_status_notification.reschedule_requested'));
                        $this->sendPushNotification($notificationTitle, $notiPatDetails, $appointmentPatientDetails->patient_id, $appointment->id, config('constants.appointment_status_notification.reschedule_requested'));
                        $user = User::find($appointmentPatientDetails->patient_id);
                        broadcast(new PushNotification($user))->toOthers();

                        // SMS
                        if ($appointment->allow_text_messages == 1) {
                            $smsNotification = new SmsNotificationService();

                            $smsNotification->sendSms(
                                $appointmentPatientDetails->patient_phone_number ?? NULL,
                                str_replace(
                                    array("{{doctorName}}", "{{appointmentTiming}}"),
                                    array($doctorName, $appointmentTiming),
                                    getSmsTemplateData('appointment_reschedule')
                                )
                            );
                        }
                    }

                    $doctorInformationFields = [
                        'dl.id as doctor_location_id', 'users.id as user_id', 'di.id as doctor_id', 'users.is_verified', 'di.is_document_verified',
                        'users.email', 'users.email', 'users.country_code', 'users.phone_number', 'di.first_name', 'di.last_name', 'di.gender', 'in_person', 'video_consultancy', 'occupation_id', 'specialty_id', 'symptom_id', 'dl.office_phone_number', 'master_sub_categories.label as occupation_name', 'm.label as specialty_name', 'dl.ext', 'dl.office_fax', 'medical_school',
                        'grad_year', 'date_of_birth', 'di.state_id', 'about', 'dl.appointment_duration', 'is_featured', 'grad_year', 'doc.name_prefix', 'appointments.*', 'dl.address', 'dl.zipcode', 'dl.suite', 'di.profile_image',
                        'appointments.id as appointment_id', 'apd.patient_id', 'appointments.consultation_url'
                    ];

                    $getPatientAppointment = $this->appointment::with('rescheduleAppointmentByPatient', 'rescheduleAppointmentByDoctor')
                        ->leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
                        ->leftJoin('appointment_reschedules', 'appointments.id', 'appointment_reschedules.appointment_id')
                        ->leftJoin('doctor_information as di', 'di.user_id', 'appointments.doctor_id')
                        ->leftJoin('master_sub_categories', 'di.occupation_id', '=', 'master_sub_categories.id')
                        ->leftJoin('master_sub_categories as m', 'di.specialty_id', '=', 'm.id')
                        ->leftJoin('doctors as doc', 'doc.npi_number', 'di.npi_number')
                        ->leftJoin('users', 'users.id', '=', 'di.user_id')
                        ->leftJoin('doctor_locations as dl', 'dl.doctor_id', 'di.user_id')
                        ->select($doctorInformationFields);
                    if (Auth::user()->role_id == 3) {
                        $getPatientAppointment = $getPatientAppointment->where([
                            'apd.patient_id' => $loginUserId,
                            'appointments.id' => $patientRescheduleBookingData['appointment_id']
                        ]);
                    } elseif (Auth::user()->role_id == 2) {
                        $getPatientAppointment = $getPatientAppointment->where([
                            'appointments.doctor_id' => $loginUserId,
                            'appointments.id' => $patientRescheduleBookingData['appointment_id']
                        ]);
                        $appoinmentReschedule->requested_by = 'doctor';
                    }

                    $getPatientAppointment = $getPatientAppointment->where('appointments.id', $patientRescheduleBookingData['appointment_id'])
                        ->groupBy('appointment_reschedules.appointment_id')->orderBy('appointment_reschedules.appointment_id', 'DESC')->get();

                    $doctorAppointmentArray = [];

                    //email
                    if ($updateAppointment) {
                        $providerInfo = $this->doctorInformation::where('user_id', $appointment->doctor_id)
                            ->leftjoin('users', 'doctor_information.user_id', 'users.id')
                            ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
                            ->select('doctor_information.specialty_id', 'doctor_information.*', 'users.email', 'master_sub_categories.label')
                            ->first();

                        $patientInfo = $this->patientInformation::where('user_id', $getPatientAppointment[0]->patient_id)
                            ->leftjoin('users', 'patient_information.user_id', 'users.id')
                            ->first();

                        $year = date("Y");
                        $day = date('D', strtotime($patientRescheduleBookingData['appointment_date']));
                        $date = date('jS M,Y', strtotime($patientRescheduleBookingData['appointment_date']));
                        $time = date("H:i", strtotime($patientRescheduleBookingData['slot_time']));
                        $providerLocation = $this->doctorLocation::where('doctor_id', $appointment->doctor_id)->first();
                        $location = !empty($providerLocation) ? $this->city::find($providerLocation->city_id)->name : '';
                        $suffix = !empty($providerLocation) ? $providerInfo->suffix : '';
                        $doctorImage = !empty($providerInfo) && $providerInfo->profile_image != '' ? getImage($providerInfo->profile_image, $providerInfo->user_id) : asset('/assets/images') . '/default_profile.jpg';
                        $patientImage = !empty($patientInfo) && $patientInfo->profile_image != '' ? getImage($patientInfo->profile_image, $patientInfo->user_id) : asset('/assets/images') . '/default_profile.jpg';
                        $patientFirstName = !empty($patientInfo) && !empty(explode(' ', $patientInfo->full_name)[0]) ? explode(' ', $patientInfo->full_name)[0] : '';
                        $patientLastName = !empty($patientInfo) && !empty(explode(' ', $patientInfo->full_name)[1]) ? explode(' ', $patientInfo->full_name)[1] : '';
                        $providerFirstName = !empty($providerInfo) ? 'Dr.' . $providerInfo->first_name  : '';
                        $providerLastName = !empty($providerInfo) ? $providerInfo->last_name  : '';

                        $siteURL = env('FRONT_APP_URL');
                        $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
                        $contactusURL = env('FRONT_APP_URL') . 'patient/home';
                        $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
                        $findDoctorURL = env('FRONT_APP_URL') . 'patient/doctor-list';
                        $imgURL = asset('/assets/images') . '/';
                        $providerConfirmAppointmentUrl = env('FRONT_APP_URL') . 'doctor/appointments';

                        $resultMainArray = array(
                            "{{providerConfirmAppointmentUrl}}",
                            "{{providerCancelAppointmentUrl}}",
                            "{{siteUrl}}",
                            "{{contactUsUrl}}",
                            "{{privacyPolicyUrl}}",
                            "{{termsUrl}}",
                            "{{patientFindDoctorUrl}}",
                            "{{patientSignInUrl}}",
                            "{{patientFirstName}}",
                            "{{patientShortLastName}}",
                            "{{appointmentDayName}}",
                            "{{appointmentMonthDate}}",
                            "{{appointmentTime}}",
                            "{{providerFirstName}}",
                            "{{providerLastName}}",
                            "{{providerSuffix}}",
                            "{{providerSpecialty}}",
                            "{{providerLocationName}}",
                            "{{providerAddress}}",
                            "{{providerPhoneNumberFormatted}}",
                            "{{patientProfilePhoto}}",
                            "{{providerProfilePhoto}}",
                            "{{imgUrl}}",
                            "{{currentYear}}",
                        );

                        $resultReplaceArray = array(
                            $providerConfirmAppointmentUrl,
                            $providerConfirmAppointmentUrl,
                            $siteURL,
                            $contactusURL,
                            $privacyURL,
                            $termsURL,
                            $findDoctorURL,
                            $findDoctorURL,
                            $patientFirstName,
                            $patientLastName,
                            $day,
                            $date,
                            $time,
                            $providerFirstName,
                            $providerLastName,
                            $suffix,
                            $providerInfo->label ?? '',
                            $location,
                            $providerLocation->address ?? '',
                            $providerLocation->office_phone_number ?? '',
                            $patientImage,
                            $doctorImage,
                            $imgURL,
                            $year,
                        );
                    }
                    if (Auth::user()->role_id == 3) {
                        $doctorAppointmentArray['patient_id'] = $loginUserId;

                        //email
                        $emailTemplateData = getEmailTemplateData('appointment_reschedule_to_dr');
                        $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                        $doctorAppointmentArray['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                        $doctorAppointmentArray['content'] = $emailMiddleBody;
                        $doctorAppointmentArray['email'] = !empty($providerInfo) ? $providerInfo->email : '';
                    } elseif (Auth::user()->role_id == 2) {
                        $doctorAppointmentArray['doctor_id'] = $loginUserId;

                        //email
                        if ($patientRescheduleBookingData['consultancy_type'] == 'in_person') {
                            $emailTemplateData = getEmailTemplateData('appointment_reschedule');
                        }

                        if ($patientRescheduleBookingData['consultancy_type'] == 'video_consultancy') {
                            $emailTemplateData = getEmailTemplateData('appointment_reschedule_video');
                        }

                        $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                        $doctorAppointmentArray['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                        $doctorAppointmentArray['content'] = $emailMiddleBody;
                        $doctorAppointmentArray['email'] = !empty($patientInfo) ? $patientInfo->email : '';
                    }
                    $doctorAppointmentArray['appointment_data'] = $getPatientAppointment;

                    return $doctorAppointmentArray;
                }

                return [];
            }
        );
    }

    /**
     * Accept/reject rescheduled request
     *
     * @param  int $doctorId
     * @return Array
     */
    public function rescheduleBookingStatus(array $bookingData, $userId)
    {
        $appointmentRescheduleData = $this->appointmentReschedule
            // ->leftJoin('appointments', 'appointments.id', 'appointment_reschedules.appointment_id')
            ->where([
                'appointment_id' => $bookingData['appointment_id'],
                'appointment_reschedules.id' => $bookingData['appointment_reschedule_id']
            ]);

        $appointmentRescheduleUpdate = $appointmentRescheduleData->update([
            'appointment_reschedules.status' => $bookingData['status'],
            'appointment_reschedules.declined_reason' => $bookingData['declined_reason'] ?? '',
            "appointment_reschedules.updated_by" => $userId
        ]);

        $appointmentRescheduleData = $appointmentRescheduleData->first();

        if ($appointmentRescheduleUpdate) {
            $appointmentData = $this->appointment::join('appointment_reschedules', 'appointments.id', 'appointment_reschedules.appointment_id')
                ->join('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
                ->where([
                    'appointments.id' => $bookingData['appointment_id'],
                ]);

            if ($bookingData['status'] == "accepted") {
                $updateAppointment = $appointmentData->update([
                    'appointments.status' => config('constants.appointment_status.1'),
                    'appointments.location_id' => $appointmentRescheduleData->location_id,
                    'appointments.appointment_date' => $appointmentRescheduleData->appointment_date,
                    'appointments.start_time' => $appointmentRescheduleData->start_time,
                    'appointments.end_time' => $appointmentRescheduleData->end_time,
                    'appointments.consultant_type' => $appointmentRescheduleData->consultant_type == 'in_person' ? "in person" : 'video',
                    'appointments.reason' => $appointmentRescheduleData->reason,
                    'appointments.declined_reason' => $appointmentRescheduleData->declined_reason,
                    "appointments.consultation_url" => $bookingData['consultancy_url'] ?? ''
                ]);

                // Email
                $appointmentData = $appointmentData->get();

                if ($updateAppointment) {

                    $appointment = $appointmentData->first();

                    if (!empty($appointment)) {
                        $doctorDetails = $this->doctorInformation->where('user_id', $appointment->doctor_id)->first();
                        $appointmentPatientDetails = $this->appointmentPatientDetail->where('id', $appointment->appointment_patient_id)->first();
                        $appointmentTiming = date("M d", strtotime($appointment->appointment_date)) . ' at ' . date("H A", strtotime($appointment->start_time));

                        if ($doctorDetails)
                            $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                        $notificationTitle = 'Appointment Update';

                        $notiDocDetails = $notiPatDetails = array();
    
                        $notiDocDetails['templateName'] = 'appointment_reschedule_request_doctor';
                        $notiDocDetails['patientName'] = $appointmentPatientDetails->patient_name;
    
                        $notiPatDetails['templateName'] = 'appointment_reschedule_request_patient';
                        $notiPatDetails['doctorName'] = $doctorName;
    
                        $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_update_title';
                        $notiDocDetails['appointmentStatus'] = $notiPatDetails['appointmentStatus'] = $appointment->status;
                        $notiDocDetails['appointmentTiming'] = $notiPatDetails['appointmentTiming'] = $appointmentTiming;

                        if ($appointment->status == 'cancelled') {
                            $notificationTitle = 'Appointment Cancelled';
                            $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_cancelled_title';
                        }

                        if ($appointment->consultant_type == 'video') {
                            $notiDocDetails['templateName'] = 'appointment_update_doctor_video';
                            $notiPatDetails['templateName'] = 'appointment_update_patient_video';
                        }

                        $this->sendPushNotification($notificationTitle, $notiDocDetails, $appointment->doctor_id, $appointment->id, config('constants.appointment_status_notification.reschedule_requested'));
                        $this->sendPushNotification($notificationTitle, $notiPatDetails, $appointmentPatientDetails->patient_id, $appointment->id, config('constants.appointment_status_notification.reschedule_requested'));
                        $user = User::find($appointmentPatientDetails->patient_id);
                        broadcast(new PushNotification($user))->toOthers();

                        // SMS
                        if ($appointment->allow_text_messages == 1) {
                            $smsNotification = new SmsNotificationService();

                            $smsNotification->sendSms(
                                $appointmentPatientDetails->patient_phone_number ?? NULL,
                                str_replace(
                                    array("{{doctorName}}", "{{appointmentTiming}}"),
                                    array($doctorName, $appointmentTiming),
                                    getSmsTemplateData('appointment_reschedule')
                                )
                            );
                        }
                    }

                    $providerInfo = $this->doctorInformation::where('user_id', $bookingData['doctor_id'])
                        ->leftjoin('users', 'doctor_information.user_id', 'users.id')
                        ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
                        ->select('doctor_information.specialty_id', 'doctor_information.*', 'users.email', 'master_sub_categories.label')
                        ->first();

                    $patientInfo = $this->patientInformation::where('user_id', $appointmentData[0]->patient_id)
                        ->leftjoin('users', 'patient_information.user_id', 'users.id')
                        ->first();

                    $year = date("Y");
                    $day = date('D', strtotime($appointmentData[0]->appointment_date));
                    $date = date('jS M,Y', strtotime($appointmentData[0]->appointment_date));
                    $time = date("H:i", strtotime($appointmentData[0]->start_time));
                    $providerLocation = $this->doctorLocation::where('doctor_id', $bookingData['doctor_id'])->first();
                    $location = !empty($providerLocation) ? $this->city::find($providerLocation->city_id)->name : '';
                    $suffix = $providerInfo->suffix ?? '';
                    $doctorImage = !empty($providerInfo) && $providerInfo->profile_image != '' ? getImage($providerInfo->profile_image, $providerInfo->user_id) : asset('/assets/images/default_profile.jpg');
                    $patientImage = !empty($patientInfo) && $patientInfo->profile_image != '' ? getImage($patientInfo->profile_image, $patientInfo->user_id) : asset('/assets/images/default_profile.jpg');
                    $patientFirstName = !empty($patientInfo) && !empty(explode(' ', $patientInfo->full_name)[0]) ? explode(' ', $patientInfo->full_name)[0] : '';
                    $patientLastName = !empty($patientInfo) && !empty(explode(' ', $patientInfo->full_name)[1]) ? explode(' ', $patientInfo->full_name)[1] : '';
                    $providerFirstName = !empty($providerInfo) ? 'Dr.' . $providerInfo->first_name : '';
                    $providerLastName = !empty($providerInfo) ? $providerInfo->last_name : '';

                    $siteURL = env('FRONT_APP_URL');
                    $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
                    $contactusURL = env('FRONT_APP_URL') . 'patient/home';
                    $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
                    $findDoctorURL = env('FRONT_APP_URL') . 'patient/doctor-list';
                    $imgURL = asset('/assets/images') . '/';

                    $resultMainArray = array(
                        "{{siteUrl}}",
                        "{{contactUsUrl}}",
                        "{{privacyPolicyUrl}}",
                        "{{termsUrl}}",
                        "{{patientFindDoctorUrl}}",
                        "{{providerSeoUrl}}",
                        "{{patientFirstName}}",
                        "{{patientShortLastName}}",
                        "{{appointmentDayName}}",
                        "{{appointmentMonthDate}}",
                        "{{appointmentTime}}",
                        "{{providerFirstName}}",
                        "{{providerLastName}}",
                        "{{providerSuffix}}",
                        "{{providerSpecialty}}",
                        "{{providerLocationName}}",
                        "{{providerAddress}}",
                        "{{providerPhoneNumberFormatted}}",
                        "{{patientProfilePhoto}}",
                        "{{providerProfilePhoto}}",
                        "{{imgUrl}}",
                        "{{currentYear}}",
                    );

                    $resultReplaceArray = array(
                        $siteURL,
                        $contactusURL,
                        $privacyURL,
                        $termsURL,
                        $findDoctorURL,
                        $findDoctorURL,
                        $patientFirstName,
                        $patientLastName,
                        $day,
                        $date,
                        $time,
                        $providerFirstName,
                        $providerLastName,
                        $suffix,
                        $providerInfo->label ?? '',
                        $location,
                        $providerLocation->address ?? '',
                        $providerLocation->office_phone_number ?? '',
                        $patientImage,
                        $doctorImage,
                        $imgURL,
                        $year,
                    );

                    if ($bookingData['consultancy_type'] == 'in_person') {
                        $emailTemplateData = getEmailTemplateData('appointment_reschedule_confirmed_to_patient');
                    }

                    if ($bookingData['consultancy_type'] == 'video_consultancy') {
                        $emailTemplateData = getEmailTemplateData('appointment_reschedule_confirmed_video_to_patient');
                    }

                    $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                    $emailData['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                    $emailData['content'] = $emailMiddleBody;
                    $emailData['email'] = $patientInfo->email ?? '';

                    return $emailData;
                }
            } else {
                $updateAppointment = $appointmentData->update([
                    'appointments.status' => config('constants.appointment_status.3'),
                    "appointments.consultation_url" => $bookingData['consultancy_url'] ?? ''
                ]);

                if ($updateAppointment) {

                    $appointment = $appointmentData->first();

                    if (!empty($appointment)) {
                        $doctorDetails = $this->doctorInformation->where('user_id', $appointment->doctor_id)->first();
                        $appointmentPatientDetails = $this->appointmentPatientDetail->where('id', $appointment->appointment_patient_id)->first();
                        $appointmentTiming = date("M d", strtotime($appointment->appointment_date)) . ' at ' . date("H A", strtotime($appointment->start_time));

                        if ($doctorDetails)
                            $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                        $notificationTitle = 'Appointment Update';

                        $notiDocDetails = $notiPatDetails = array();
    
                        $notiDocDetails['templateName'] = 'appointment_update_doctor';
                        $notiDocDetails['patientName'] = $appointmentPatientDetails->patient_name;
    
                        $notiPatDetails['templateName'] = 'appointment_update_patient';
                        $notiPatDetails['doctorName'] = $doctorName;
    
                        $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_update_title';
                        $notiDocDetails['appointmentStatus'] = $notiPatDetails['appointmentStatus'] = $appointment->status;
                        $notiDocDetails['appointmentTiming'] = $notiPatDetails['appointmentTiming'] = $appointmentTiming;

                        if ($appointment->status == 'cancelled') {
                            $notificationTitle = 'Appointment Cancelled';
                            $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_cancelled_title';
                        }

                        if ($appointment->consultant_type == 'video') {
                            $notiDocDetails['templateName'] = 'appointment_update_doctor_video';
                            $notiPatDetails['templateName'] = 'appointment_update_patient_video';
                        }

                        $this->sendPushNotification($notificationTitle, $notiDocDetails, $appointment->doctor_id, $appointment->id, config('constants.appointment_status_notification.rejected'));
                        $this->sendPushNotification($notificationTitle, $notiPatDetails, $appointmentPatientDetails->patient_id, $appointment->id, config('constants.appointment_status_notification.rejected'));
                        $user = User::find($appointmentPatientDetails->patient_id);
                        broadcast(new PushNotification($user))->toOthers();
                    }
                }
            }
        }

        return $appointmentRescheduleUpdate;
    }

    /**
     * cancelBooking
     *
     * @param array $appointmentDetails
     * @param int $userId
     *
     * @return array
     */
    public function cancelBooking(array $appointmentDetails, int $userId): array
    {
        $appointment = $this->appointment::where(['appointments.id' => $appointmentDetails['appointment_id']])
            ->leftjoin('appointment_patient_details', 'appointments.appointment_patient_id', 'appointment_patient_details.id')
            ->first();

        $updateAppointment = $this->appointment::where('id', $appointmentDetails['appointment_id'])
            ->update([
                'status' => config('constants.appointment_status.3'),
                'declined_reason' => $appointmentDetails['declined_reason'],
                // 'updated_by' => $userId
            ]);

        if ($updateAppointment && !empty($appointment)) {
            $providerInfo = $this->doctorInformation::where('user_id', $appointment->doctor_id)
                ->leftjoin('users', 'doctor_information.user_id', 'users.id')
                ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
                ->select('doctor_information.specialty_id', 'doctor_information.*', 'users.email', 'master_sub_categories.label')
                ->first();

            $patientInfo = $this->patientInformation::where('user_id', $appointment->patient_id)->first();

            $year = date("Y");
            $day = date('D', strtotime($appointment->appointment_date));
            $date = date('jS M,Y', strtotime($appointment->appointment_date));
            $time = date("H:i", strtotime($appointment->start_time));
            $providerLocation = $this->doctorLocation::where('doctor_id', $appointment->doctor_id)->first();
            $location = !empty($providerLocation) ? $this->city::find($providerLocation->city_id)->name : '';
            $suffix = !empty($providerInfo) ? $providerInfo->suffix : '';
            $doctorImage = !empty($providerInfo) && $providerInfo->profile_image != '' ? getImage($providerInfo->profile_image, $providerInfo->user_id) : asset('/assets/images/default_profile.jpg');
            $patientImage = !empty($patientInfo) && $patientInfo->profile_image != '' ? getImage($patientInfo->profile_image, $patientInfo->user_id) : asset('/assets/images/default_profile.jpg');
            $reason = $appointmentDetails['declined_reason'];
            $patientFirstName = !empty($patientInfo) && !empty(explode(' ', $patientInfo->full_name)[0]) ? explode(' ', $patientInfo->full_name)[0] : '';
            $patientLastName = !empty($patientInfo) && !empty(explode(' ', $patientInfo->full_name)[1]) ? explode(' ', $patientInfo->full_name)[1] : '';
            $providerFirstName = !empty($providerInfo) ? 'Dr.' . $providerInfo->first_name : '';
            $providerLastName = !empty($providerInfo) ? $providerInfo->last_name : '';

            $siteURL = env('FRONT_APP_URL');
            $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
            $contactusURL = env('FRONT_APP_URL') . 'patient/home';
            $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
            $findDoctorURL = env('FRONT_APP_URL') . 'patient/doctor-list';
            $imgURL = asset('/assets/images') . '/';

            $resultMainArray = array(
                "{{siteUrl}}",
                "{{contactUsUrl}}",
                "{{privacyPolicyUrl}}",
                "{{termsUrl}}",
                "{{patientFindDoctorUrl}}",
                "{{providerSeoUrl}}",
                "{{reason}}",
                "{{patientFirstName}}",
                "{{patientShortLastName}}",
                "{{appointmentDayName}}",
                "{{appointmentMonthDate}}",
                "{{appointmentTime}}",
                "{{providerFirstName}}",
                "{{providerLastName}}",
                "{{providerSuffix}}",
                "{{providerSpecialty}}",
                "{{providerLocationName}}",
                "{{providerAddress}}",
                "{{providerPhoneNumberFormatted}}",
                "{{patientProfilePhoto}}",
                "{{providerProfilePhoto}}",
                "{{imgUrl}}",
                "{{currentYear}}",
            );

            $resultReplaceArray = array(
                $siteURL,
                $contactusURL,
                $privacyURL,
                $termsURL,
                $findDoctorURL,
                $findDoctorURL,
                $reason,
                $patientFirstName,
                $patientLastName,
                $day,
                $date,
                $time,
                $providerFirstName,
                $providerLastName,
                $suffix,
                $providerInfo->label ?? '',
                $location,
                $providerLocation->address ?? '',
                $providerLocation->office_phone_number ?? '',
                $patientImage,
                $doctorImage,
                $imgURL,
                $year,
            );

            $emailTemplateData = '';
            $emailMiddleBody = '';

            if ($appointment['consultant_type'] == 'in person') {
                $emailTemplateData = getEmailTemplateData('appointment_cancelled_by_patient');
                $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                $emailData['patient']['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                $emailData['patient']['content'] = $emailMiddleBody;
                $emailData['patient']['email'] = auth()->user()->email;
            }

            if ($appointment['consultant_type'] == 'video') {
                $emailTemplateData = getEmailTemplateData('appointment_cancelled_by_patient_video');
                $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                $emailData['patient']['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                $emailData['patient']['content'] = $emailMiddleBody;
                $emailData['patient']['email'] = auth()->user()->email;
            }

            $emailTemplateData = getEmailTemplateData('appointment_cancelled_by_patient_to_dr');
            $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
            $emailData['doctor']['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
            $emailData['doctor']['content'] = $emailMiddleBody;
            $emailData['doctor']['email'] = !empty($providerInfo) ? $providerInfo->email : "";

            $appointment = $this->appointment::where(['appointments.id' => $appointmentDetails['appointment_id']])->first();

            $doctorDetails = $this->doctorInformation->where('user_id', $appointment->doctor_id)->first();
            $appointmentPatientDetails = $this->appointmentPatientDetail->where('id', $appointment->appointment_patient_id)->first();
            $appointmentTiming = date("M d", strtotime($appointment->appointment_date)) . ' at ' . date("H A", strtotime($appointment->start_time));

            if ($doctorDetails)
                $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

            $notificationTitle = 'Appointment Cancelled';

            $notiDocDetails = $notiPatDetails = array();
    
            $notiDocDetails['templateName'] = 'appointment_update_doctor';
            $notiDocDetails['patientName'] = $appointmentPatientDetails->patient_name;

            $notiPatDetails['templateName'] = 'appointment_update_patient';
            $notiPatDetails['doctorName'] = $doctorName;

            $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_cancelled_title';
            $notiDocDetails['appointmentStatus'] = $notiPatDetails['appointmentStatus'] = $appointment->status;
            $notiDocDetails['appointmentTiming'] = $notiPatDetails['appointmentTiming'] = $appointmentTiming;

            if ($appointment->consultant_type == 'video') {
                $notiDocDetails['templateName'] = 'appointment_update_doctor_video';
                $notiPatDetails['templateName'] = 'appointment_update_patient_video';
            }

            $this->sendPushNotification($notificationTitle, $notiDocDetails, $appointment->doctor_id, $appointment->id, config('constants.appointment_status.3'));
            $this->sendPushNotification($notificationTitle, $notiPatDetails, $appointmentPatientDetails->patient_id, $appointment->id, config('constants.appointment_status.3'));
            $user = User::find($appointmentPatientDetails->patient_id);
            broadcast(new PushNotification($user))->toOthers();

            // SMS
            if ($appointment->allow_text_messages == 1) {
                $smsNotification = new SmsNotificationService();

                $smsNotification->sendSms(
                    $appointmentPatientDetails->patient_phone_number ?? NULL,
                    str_replace(
                        array("{{doctorName}}", "{{appointmentTiming}}"),
                        array($doctorName, $appointmentTiming),
                        getSmsTemplateData('appointment_cancelled_by_provider')
                    )
                );
            }

            return $emailData;
            // return $cancelBooking;
        }
    }

    /**
     * Book appoinment new
     *
     * @param  array $dataParams
     * @return Collection
     */
    public function bookAppoinmentNew(array $dataParams): mixed
    {
        return DB::transaction(
            function () use ($dataParams) {
                $startTime = $dataParams['start_time'];
                $endTime = $dataParams['end_time'];

                if (!empty($dataParams['location']) && empty($endTime)) {
                    $doctorLocationData = $this->doctorLocation::where('id', $dataParams['location'])->first();
                    $appointmentDuration = !empty($doctorLocationData) ? $doctorLocationData->appointment_duration : 0;
                    $endTime = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                }

                if (isset($dataParams['id']) && ($dataParams['id'] > 0)) {
                    $appointment = $this->appointmentPatientDetail::find($dataParams['id']);
                    $appointment->updated_by = $dataParams['patient_id'];
                    $appointment->updated_at = Carbon::now();
                } else {
                    $appointment = $this->appointmentPatientDetail;
                    $appointment->created_by = $dataParams['patient_id'];
                }
                if (!empty($dataParams['booking_details'])) {

                    $appointment->patient_id = $dataParams['patient_id'];
                    $appointment->patient_name = $dataParams['patient_name'];
                    $appointment->patient_phone_number = $dataParams['phone_number'];
                    $appointment->patient_address = $dataParams['address'];
                    $appointment->patient_age = $dataParams['age'];
                    $appointment->patient_state_id = $dataParams['state_id'];
                    $appointment->patient_city_id = $dataParams['city_id'];
                    $appointment->patient_country_id = $dataParams['country_id'];
                    $appointment->appointment_for = $dataParams['booking_for'];
                    $appointment->save();

                    $patientInfo = $this->patientInformation::where('user_id', $dataParams['patient_id'])->first();
                    $patientImage = !empty($patientInfo) && $patientInfo->profile_image != '' ? getImage($patientInfo->profile_image, $patientInfo->user_id) : asset('/assets/images/default_profile.jpg');
                    $patientFirstName = !empty(explode(' ', $dataParams['patient_name'])[0]) ? explode(' ', $dataParams['patient_name'])[0] : '';
                    $patientLastName = !empty(explode(' ', $dataParams['patient_name'])[1]) ? explode(' ', $dataParams['patient_name'])[1] : '';
                    $year = date("Y");
                    $day = date('D', strtotime($dataParams['appoinment_date']));
                    $date = date('jS M,Y', strtotime($dataParams['appoinment_date']));
                    $time = date("H:i", strtotime($dataParams['start_time']));

                    $siteURL = env('FRONT_APP_URL');
                    $privacyURL = env('FRONT_APP_URL') . 'patient/privacy-policy';
                    $contactusURL = env('FRONT_APP_URL') . 'patient/home';
                    $termsURL = env('FRONT_APP_URL') . 'patient/terms-condition';
                    $findDoctorURL = env('FRONT_APP_URL') . 'patient/doctor-list';
                    $imgURL = asset('/assets/images') . '/';
                    $providerConfirmAppointmentUrl = env('FRONT_APP_URL') . 'doctor/appointments';

                    $resultMainArray = array(
                        "{{patientSignInUrl}}",
                        "{{providerConfirmAppointmentUrl}}",
                        "{{providerCancelAppointmentUrl}}",
                        "{{siteUrl}}",
                        "{{contactUsUrl}}",
                        "{{privacyPolicyUrl}}",
                        "{{termsUrl}}",
                        "{{patientFindDoctorUrl}}",
                        "{{patientFirstName}}",
                        "{{patientShortLastName}}",
                        "{{appointmentDayName}}",
                        "{{appointmentMonthDate}}",
                        "{{appointmentTime}}",
                        "{{providerFirstName}}",
                        "{{providerLastName}}",
                        "{{providerSuffix}}",
                        "{{providerSpecialty}}",
                        "{{providerLocationName}}",
                        "{{providerAddress}}",
                        "{{providerPhoneNumberFormatted}}",
                        "{{patientProfilePhoto}}",
                        "{{providerProfilePhoto}}",
                        "{{imgUrl}}",
                        "{{currentYear}}",
                    );

                    // $receiverEmail = $this->user::find($dataParams['patient_id']);
                    $bookingId = $this->getBookingId(9);

                    if (!empty($dataParams['doctor_id']) && empty($dataParams['booking_details'])) {

                        foreach ($dataParams['doctor_id'] as $doctorId) {

                            $appointmentDetail = new Appointment();
                            $appointmentDetail->appointment_patient_id = $appointment->id;
                            $appointmentDetail->doctor_id = $doctorId;
                            $appointmentDetail->location_id = $dataParams['location'] ?? Null;
                            $appointmentDetail->booking_id = $bookingId;
                            $appointmentDetail->appointment_date = $dataParams['appoinment_date'];
                            $appointmentDetail->start_time = $startTime;
                            $appointmentDetail->end_time = $endTime;
                            $appointmentDetail->consultant_type = $dataParams['consultancy_type'];
                            $appointmentDetail->reason = $dataParams['consultancy_reason'];
                            $appointmentDetail->registration_form = $dataParams['registration_form'] ?? '';
                            $appointmentDetail->additional_note = $dataParams['additional_note'];
                            $appointmentDetail->status = config('constants.appointment_status.0');
                            $appointmentDetail->allow_text_messages = $dataParams['allow_text_messages'];
                            // email
                            if ($appointmentDetail->save()) {
                                $providerInfo = $this->doctorInformation::where('user_id', $doctorId)
                                    ->leftjoin('users', 'doctor_information.user_id', 'users.id')
                                    ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
                                    ->select('doctor_information.specialty_id', 'doctor_information.*', 'users.email', 'master_sub_categories.label')
                                    ->first();

                                $providerLocation = $this->doctorLocation::where('doctor_id', $doctorId)->first();
                                $location = !empty($providerLocation->city_id) ? $this->city::find(($providerLocation->city_id))->name : '';
                                $suffix = !empty($providerInfo) ? $providerInfo->suffix : '';
                                $doctorImage = !empty($providerInfo) && $providerInfo->profile_image != '' ? getImage($providerInfo->profile_image, $doctorId) : asset('/assets/images/default_profile.jpg');
                                $providerFirstName = !empty($providerInfo) ? 'Dr.' . $providerInfo->first_name : '';
                                $providerLastName = !empty($providerInfo) ?  $providerInfo->last_name : '';

                                $resultReplaceArray = array(
                                    $siteURL,
                                    $providerConfirmAppointmentUrl,
                                    $providerConfirmAppointmentUrl,
                                    $siteURL,
                                    $contactusURL,
                                    $privacyURL,
                                    $termsURL,
                                    $findDoctorURL,
                                    $patientFirstName,
                                    $patientLastName,
                                    $day,
                                    $date,
                                    $time,
                                    $providerFirstName,
                                    $providerLastName,
                                    $suffix,
                                    $providerInfo->label ?? '',
                                    $location,
                                    $providerLocation->address ?? '',
                                    $providerLocation->office_phone_number ?? '',
                                    $patientImage,
                                    $doctorImage,
                                    $imgURL,
                                    $year,
                                );

                                $emailTemplateData = '';
                                $emailMiddleBody = '';

                                if ($dataParams['consultancy_type'] == 'in person') {
                                    $emailTemplateData = getEmailTemplateData('appointment_pending');
                                    $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                                    $emailData['patient']['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                                    $emailData['patient']['content'] = $emailMiddleBody;
                                    $emailData['patient']['email'] = auth()->user()->email;
                                }

                                if ($dataParams['consultancy_type'] == 'video') {
                                    $emailTemplateData = getEmailTemplateData('appointment_pending_video');
                                    $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                                    $emailData['patient']['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                                    $emailData['patient']['content'] = $emailMiddleBody;
                                    $emailData['patient']['email'] = auth()->user()->email;
                                }

                                $emailTemplateData = getEmailTemplateData('appointment_pending_to_dr');
                                $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                                $emailData['doctor']['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                                $emailData['doctor']['content'] = $emailMiddleBody;
                                $emailData['doctor']['email'] = !empty($providerInfo) ? $providerInfo->email : '';

                                $appointment['email'] = $emailData;

                                if (!empty($appointmentDetail) && !empty($appointment)) {
                                    $doctorDetails = $this->doctorInformation->where('user_id', $doctorId)->first();
                                    $appointmentTiming = date("M d", strtotime($appointmentDetail->appointment_date)) . ' at ' . date("H A", strtotime($appointmentDetail->start_time));

                                    if ($doctorDetails)
                                        $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                                    $notificationTitle = 'Appointment Booking';

                                    $notiDocDetails = $notiPatDetails = array();
    
                                    $notiDocDetails['templateName'] = 'appointment_booking_doctor';
                                    $notiDocDetails['patientName'] = $appointment->patient_name;

                                    $notiPatDetails['templateName'] = 'appointment_booking_patient';
                                    $notiPatDetails['doctorName'] = $doctorName;

                                    $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_booking_title';
                                    $notiDocDetails['appointmentTiming'] = $notiPatDetails['appointmentTiming'] = $appointmentTiming;

                                    if ($appointmentDetail->consultant_type == 'video') {
                                        $notiDocDetails['templateName'] = 'appointment_booking_doctor_video';
                                        $notiPatDetails['templateName'] = 'appointment_booking_patient_video';
                                    }

                                    $notificationData = $this->sendPushNotification($notificationTitle, $notiDocDetails, $doctorId, $appointmentDetail->id, config('constants.appointment_status.0'));
                                    $notificationData = $this->sendPushNotification($notificationTitle, $notiPatDetails, $appointment->patient_id, $appointmentDetail->id, config('constants.appointment_status.0'));
                                    $user = User::find($appointment->patient_id);
                                    broadcast(new PushNotification($user))->toOthers();

                                    // SMS
                                    if ($appointmentDetail->allow_text_messages == 1) {
                                        $smsNotification = new SmsNotificationService();

                                        $smsNotification->sendSms(
                                            $appointment->patient_phone_number ?? NULL,
                                            str_replace(
                                                array("{{doctorName}}", "{{appointmentTiming}}"),
                                                array($doctorName, $appointmentTiming),
                                                getSmsTemplateData('appointment_pending')
                                            )
                                        );
                                    }
                                }
                            }
                        }
                    } else {
                        foreach ($dataParams['booking_details'] as $bookingDetails) {

                            if (!empty($bookingDetails)) {
                                $consultantType = $bookingDetails['consultancy_type'] == 'in_person' ? "in person" : 'video';
                                $appointmentDate = date("Y-m-d H:i:s", strtotime($bookingDetails['slot_date'] . " " . $bookingDetails['slot_time']));
                                $startTime = date("H:i", strtotime($bookingDetails['slot_time']));
                                $doctorLocationData = $this->doctorLocation::where('id', $bookingDetails['location'])->first();
                                $appointmentDuration = !empty($doctorLocationData) ? $doctorLocationData->appointment_duration : 0;
                                $endTime = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                            }

                            $appointmentDetail = new Appointment();
                            $appointmentDetail->appointment_patient_id = $appointment->id;
                            $appointmentDetail->doctor_id = $bookingDetails['doctor_id'] ?? $dataParams['doctor_id'];
                            $appointmentDetail->location_id = $bookingDetails['location'] ?? $dataParams['location'];
                            $appointmentDetail->booking_id = $bookingId;
                            $appointmentDetail->appointment_date = $appointmentDate ?? $dataParams['appoinment_date'];
                            $appointmentDetail->start_time = $startTime;
                            $appointmentDetail->end_time = $endTime;
                            $appointmentDetail->consultant_type = $consultantType ?? $dataParams['consultancy_type'];
                            $appointmentDetail->reason = $dataParams['consultancy_reason'];
                            $appointmentDetail->registration_form = '';
                            $appointmentDetail->additional_note = $dataParams['additional_note'];
                            $appointmentDetail->status = config('constants.appointment_status.0');
                            $appointmentDetail->allow_text_messages = $dataParams['allow_text_messages'];

                            if ($appointmentDetail->save()) {
                                $providerInfo = $this->doctorInformation::where('user_id', $bookingDetails['doctor_id'])
                                    ->leftjoin('users', 'doctor_information.user_id', 'users.id')
                                    ->leftjoin('master_sub_categories', 'doctor_information.specialty_id', 'master_sub_categories.id')
                                    ->select('doctor_information.specialty_id', 'doctor_information.*', 'users.email', 'master_sub_categories.label')
                                    ->first();

                                $providerLocation = $this->doctorLocation::where('doctor_id', $bookingDetails['doctor_id'])->first();
                                $location = !empty($providerLocation->city_id) ? $this->city::find($providerLocation->city_id)->name : '';
                                $suffix = !empty($providerInfo) ? $providerInfo->suffix : '';
                                $doctorImage = !empty($providerInfo) && $providerInfo->profile_image != '' ? getImage($providerInfo->profile_image, $bookingDetails['doctor_id']) : asset('/assets/images/default_profile.jpg');
                                $providerFirstName = !empty($providerInfo) ? 'Dr.' . $providerInfo->first_name : '';
                                $providerLastName = !empty($providerInfo) ?  $providerInfo->last_name : '';

                                $resultReplaceArray = array(
                                    $siteURL,
                                    $providerConfirmAppointmentUrl,
                                    $providerConfirmAppointmentUrl,
                                    $siteURL,
                                    $contactusURL,
                                    $privacyURL,
                                    $termsURL,
                                    $findDoctorURL,
                                    $patientFirstName,
                                    $patientLastName,
                                    $day,
                                    $date,
                                    $time,
                                    $providerFirstName,
                                    $providerLastName,
                                    $suffix,
                                    $providerInfo->label ?? '',
                                    $location,
                                    $providerLocation->address ?? '',
                                    $providerLocation->office_phone_number ?? '',
                                    $patientImage,
                                    $doctorImage,
                                    $imgURL,
                                    $year,
                                );

                                if (!empty($dataParams['registration_form'])) {
                                    foreach ($dataParams['registration_form'] as $registrationForm) {
                                        if ($registrationForm['doctor_id'] == $bookingDetails['doctor_id'])
                                            $appointmentDetail->registration_form = $registrationForm['file'];
                                        $appointmentDetail->save();
                                    }
                                }

                                $emailTemplateData = '';
                                $emailMiddleBody = '';

                                if ($dataParams['consultancy_type'] == 'in person') {
                                    $emailTemplateData = getEmailTemplateData('appointment_pending');
                                    $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                                    $emailData['patient']['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                                    $emailData['patient']['content'] = $emailMiddleBody;
                                    $emailData['patient']['email'] = auth()->user()->email;
                                }

                                if ($dataParams['consultancy_type'] == 'video') {
                                    $emailTemplateData = getEmailTemplateData('appointment_pending_video');
                                    $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                                    $emailData['patient']['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                                    $emailData['patient']['content'] = $emailMiddleBody;
                                    $emailData['patient']['email'] = auth()->user()->email;
                                }

                                $emailTemplateData = getEmailTemplateData('appointment_pending_to_dr');
                                $emailMiddleBody = !empty($emailTemplateData) ? str_replace($resultMainArray, $resultReplaceArray, $emailTemplateData->template_body) : '';
                                $emailData['doctor']['subject'] = !empty($emailTemplateData) ? $emailTemplateData->template_subject : '';
                                $emailData['doctor']['content'] = $emailMiddleBody;
                                $emailData['doctor']['email'] = !empty($providerInfo) ? $providerInfo->email : '';

                                $appointment['email'] = $emailData;

                                if (!empty($appointmentDetail) && !empty($appointment)) {
                                    $appoinmentPatientDetails = $this->appointmentPatientDetail::where('id', $appointment->appointment_patient_id)->first();

                                    $doctorDetails = $this->doctorInformation->where('user_id', $bookingDetails['doctor_id'])->first();
                                    $appointmentTiming = date("M d", strtotime($appointmentDetail->appointment_date)) . ' at ' . date("H A", strtotime($appointmentDetail->start_time));

                                    if ($doctorDetails)
                                        $doctorName = $doctorDetails->first_name . " " . $doctorDetails->last_name;

                                    $notificationTitle = 'Appointment Booking';

                                    $notiDocDetails = $notiPatDetails = array();
    
                                    $notiDocDetails['templateName'] = 'appointment_booking_doctor';
                                    $notiDocDetails['patientName'] = $appointment->patient_name;

                                    $notiPatDetails['templateName'] = 'appointment_booking_patient';
                                    $notiPatDetails['doctorName'] = $doctorName;

                                    $notiDocDetails['notificationTitle'] = $notiPatDetails['notificationTitle'] = 'appointment_booking_title';
                                    $notiDocDetails['appointmentTiming'] = $notiPatDetails['appointmentTiming'] = $appointmentTiming;

                                    if ($appointmentDetail->consultant_type == 'video') {
                                        $notiDocDetails['templateName'] = 'appointment_booking_doctor_video';
                                        $notiPatDetails['templateName'] = 'appointment_booking_patient_video';
                                    }

                                    $notificationData = $this->sendPushNotification($notificationTitle, $notiDocDetails, $bookingDetails['doctor_id'], $appointmentDetail->id, config('constants.appointment_status.0'));
                                    $notificationData = $this->sendPushNotification($notificationTitle, $notiPatDetails, $appointment->patient_id, $appointmentDetail->id, config('constants.appointment_status.0'));
                                    $user = User::find($appointment->patient_id);
                                    broadcast(new PushNotification($user))->toOthers();

                                    // SMS
                                    if ($appointmentDetail->allow_text_messages == 1) {
                                        $smsNotification = new SmsNotificationService();

                                        $smsNotification->sendSms(
                                            $appointment->patient_phone_number ?? NULL,
                                            str_replace(
                                                array("{{doctorName}}", "{{appointmentTiming}}"),
                                                array($doctorName, $appointmentTiming),
                                                getSmsTemplateData('appointment_pending')
                                            )
                                        );
                                    }
                                }
                            }
                        }
                    }

                    return $appointment;
                }

                return false;
            }
        );
    }

    /**
     * getFeedListData
     *
     * @param  mixed $feedData
     * @param  mixed $offset
     * @param  mixed $limit
     * @param  mixed $currentPage
     * @return void
     */
    public function getFeedListData($feedData, $offset = '', $limit = '', $currentPage = '')
    {
        $feedInformationData = [];
        $doctorIds = [];
        $feedInformationFields = [
            'news_feeds.id', 'news_feeds.title', 'news_feeds.description', 'news_feeds.file', 'news_feeds.poster_image',
            'news_feeds.created_at', 'news_feeds.status', 'news_feeds.created_by', 'di.profile_image', 'di.first_name', 'di.last_name', 'di.user_id'
        ];
        $feedInformation = $this->newsFeed::with('comments')->leftJoin('doctor_information as di', 'di.user_id', 'news_feeds.created_by');

        if (!empty(Auth::user()->id)) {

            $doctorIds = $this->newsFeed::leftJoin('doctor_follows', 'doctor_follows.doctor_id', 'news_feeds.created_by')->where('doctor_follows.follow_by', Auth::user()->id)
                ->pluck('doctor_follows.doctor_id')->unique('doctor_follows.doctor_id')->toArray();
        }

        $feedInformation = $feedInformation->where('news_feeds.status', 1)->select($feedInformationFields);

        if (!empty($doctorIds)) {
            $doctorIds = implode(",", $doctorIds);
            $feedInformation = $feedInformation->orderByRaw("CASE WHEN news_feeds.created_by IN (" . $doctorIds . ")then -1  Else news_feeds.created_by END ASC,news_feeds.created_by");
        }
        $feedInformationLatest = $feedInformation->latest()->first();

        if (isset($feedData['doctor_id']) && !empty($feedData['doctor_id'])) {
            $feedInformation = $feedInformation->where('news_feeds.created_by', $feedData['doctor_id']);
        }

        $totalCount = $feedInformation->count();
        $feedInformation = $feedInformation->skip($offset)->take($limit)->orderBy('news_feeds.created_at', 'DESC')->get();

        $feedInformationData['feed_information'] = (count($feedInformation) > 0) ? $feedInformation : [];
        $feedInformationData['latest_feed_data'] = $feedInformationLatest;
        $feedInformationData['totalRecord'] = $totalCount;

        return $feedInformationData;
    }
}
