<?php

namespace App\Repositories;

use App\Interfaces\CalendarInterface;
use App\Models\Appointment;
use App\Models\DoctorBlockSlot;
use App\Models\DoctorLocation;
use App\Models\DoctorTiming;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class CalendarRepository implements CalendarInterface
{
    /**
     * doctorLocation
     *
     * @var mixed
     */
    protected $doctorLocation;
    /**
     * appointment
     *
     * @var mixed
     */
    protected $appointment;
    /**
     * doctorBlockSlot
     *
     * @var mixed
     */
    protected $doctorBlockSlot;
    /**
     * doctorTiming
     *
     * @var mixed
     */
    protected $doctorTiming;

    public function __construct(Appointment $appointment, DoctorBlockSlot $doctorBlockSlot, DoctorLocation $doctorLocation, DoctorTiming $doctorTiming)
    {
        $this->appointment = $appointment;
        $this->doctorBlockSlot = $doctorBlockSlot;
        $this->doctorLocation = $doctorLocation;
        $this->doctorTiming = $doctorTiming;
    }
    /**
     * blockDoctorSlot
     *
     * @return $doctorSlots
     */
    public function blockDoctorSlot($bookingRequestData, $doctorId)
    {
        $doctorBlockSlot = $this->doctorBlockSlot;
        $doctorBlockSlot->doctor_id = $doctorId;
        $doctorBlockSlot->location_id = $bookingRequestData['location_id'];
        $doctorBlockSlot->date = date('Y-m-d', strtotime($bookingRequestData['date']));
        $doctorBlockSlot->start = date("H:i A", strtotime($bookingRequestData['start_time']));
        $doctorBlockSlot->end = date("H:i A", strtotime($bookingRequestData['end_time']));
        $doctorBlockSlot->created_by = $doctorId;
        $doctorBlockSlotData = $doctorBlockSlot->save();

        $doctorSlots = [];
        $doctorSlots['doctor_id'] = $doctorId;
        $doctorSlots['location_id'] = $bookingRequestData['location_id'];
        $doctorSlots['slot_data'] = $doctorBlockSlot->where('doctor_id', $doctorId)->get();

        return $doctorSlots;
    }

    /**
     * manageWorkingTime
     *
     * @return $doctorWorkingTime
     */
    public function manageWorkingTime($doctorWorkTimingRequestData, $doctorId)
    {
        return DB::transaction(
            function () use ($doctorWorkTimingRequestData, $doctorId) {
                $doctorWorkingTime = [];
                $appointmentDuration = !empty($doctorWorkTimingRequestData['duration']) ? explode(":", $doctorWorkTimingRequestData['duration']) : Null;
                $doctorLocation = $this->doctorLocation::where(['doctor_id' => $doctorId, 'id' => $doctorWorkTimingRequestData['location_id']]);
                $doctorLocation->update([
                    'appointment_duration' => $doctorWorkTimingRequestData['duration'] ?? Null,
                ]);
                $getDoctorInformation = $doctorLocation->select('appointment_duration')->first();

                if (!empty($doctorWorkTimingRequestData['work_timing'])) {
                    foreach ($doctorWorkTimingRequestData['work_timing'] as $day => $doctorWorkTimingRequest) {
                        if (!empty($doctorWorkTimingRequest)) {
                            foreach ($doctorWorkTimingRequest as $slotData) {

                                $doctorWorkTiming = $this->doctorTiming::updateOrCreate([
                                    'id' => $slotData['working_time_id'],
                                ], [
                                    'doctor_id' => $doctorId,
                                    'location_id' => !empty($doctorWorkTimingRequestData['location_id']) ? $doctorWorkTimingRequestData['location_id'] : Null,
                                    'day' => !empty($day) ? getDayName($day) : Null,
                                    'start_time' => !empty($slotData['start_time']) ? date("H:i", strtotime($slotData['start_time'])) : Null,
                                    'end_time' => !empty($slotData['end_time']) ? date("H:i", strtotime($slotData['end_time'])) : Null,
                                    'is_available' => 1,
                                    'created_by' => $doctorId,
                                ]);
                            }
                        }
                    }

                    $doctorWorkingTime['work_timing_data'] = $this->doctorTiming::where(['location_id' => $doctorWorkTimingRequestData['location_id'], 'doctor_timings.doctor_id' => $doctorId])
                        ->leftJoin('doctor_locations as dl', 'dl.id', 'doctor_timings.location_id')
                        ->select('doctor_timings.id', 'start_time', 'end_time', 'day', 'dl.appointment_duration')->orderBy('doctor_timings.id', 'DESC')
                        ->get()->groupBy('day');
                }

                $doctorWorkingTime['doctor_id'] = $doctorId;
                $doctorWorkingTime['location_id'] = $doctorWorkTimingRequestData['location_id'];
                $doctorWorkingTime['appointment_duration'] = $getDoctorInformation->appointment_duration;

                return $doctorWorkingTime;
            }
        );
    }

    /**
     * getWorkingTime
     *
     * @return $doctorWorkingTime
     */
    public function getWorkingTime($doctorSlotData, $doctorId)
    {
        $doctorWorkingTime = [];
        $getDoctorInformation = $this->getDoctorLocation($doctorId, $doctorSlotData['location_id']);

        $doctorWorkingTimeData = $this->doctorTiming::where(['location_id' => $doctorSlotData['location_id'], 'doctor_timings.doctor_id' => $doctorId])
            ->leftJoin('doctor_locations as dl', 'dl.id', 'doctor_timings.location_id')
            ->select('doctor_timings.id', 'start_time', 'end_time', 'day', 'dl.appointment_duration')->orderBy('doctor_timings.id', 'DESC')
            ->get()->groupBy('day');

        $doctorWorkingTime = [];

        $doctorWorkingTime['work_timing_data'] = $doctorWorkingTimeData;
        $doctorWorkingTime['doctor_id'] = $doctorId;
        $doctorWorkingTime['location_id'] = $doctorSlotData['location_id'];
        $doctorWorkingTime['appointment_duration'] = $getDoctorInformation->appointment_duration;

        return $doctorWorkingTime;
    }

    /**
     * getDoctorAppointment
     *
     * @return $doctorAppointmentArray
     */
    public function getDoctorAppointment($doctorAppointmentData, $doctorId)
    {
        $doctorAppointmentArray = [];

        $start_date = Carbon::parse($doctorAppointmentData['start_date'])->startOfDay();
        $end_date = Carbon::parse($doctorAppointmentData['end_date'])->endOfDay();

        $getDoctorAppointment = $this->appointment::where(function ($query) use ($doctorAppointmentData) {
            $query->where('appointments.location_id', $doctorAppointmentData['location_id'])
                ->orWhereNull('appointments.location_id');
        })
            ->leftJoin('appointment_patient_details as apd', 'apd.id', 'appointments.appointment_patient_id')
            ->whereBetween('appointment_date', [$start_date, $end_date])
            ->select('appointments.*', 'apd.patient_id', 'apd.patient_name')
            ->where('doctor_id', $doctorId)
            ->where('status', '!=', 'cancelled')
            ->get();

        $doctorBlockSlot = $this->doctorBlockSlot::where(['doctor_id' => $doctorId, 'location_id' => $doctorAppointmentData['location_id']])->get();

        $doctorAppointmentArray = [];
        $doctorAppointmentArray['doctor_id'] = $doctorId;
        $doctorAppointmentArray['location_id'] = $doctorAppointmentData['location_id'];
        $doctorAppointmentArray['appointment_data'] = $getDoctorAppointment;
        $doctorAppointmentArray['block_slot'] = $doctorBlockSlot;

        return $doctorAppointmentArray;
    }

    /**
     * getDoctorInformation
     *
     * @return $doctorInformationData
     */
    public function getDoctorLocation($doctorId, $locationId)
    {
        return $this->doctorLocation::select('appointment_duration')->where(['doctor_id' => $doctorId, 'id' => $locationId])->first();
    }

    /**
     * deleteWorkingTime
     *
     * @return $doctorInformationData
     */
    public function deleteWorkingTime($workingTimeId, $doctorId)
    {
        return $this->doctorTiming::where(['id' => $workingTimeId, 'doctor_id' => $doctorId])->delete();
    }
}
