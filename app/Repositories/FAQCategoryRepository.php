<?php

namespace App\Repositories;

use App\Interfaces\FAQCategoryInterface;
use App\Models\Language;
use App\Models\FaqCategory;

class FAQCategoryRepository implements FAQCategoryInterface
{
    /**
     * $language
     *
     * @var mixed
     */
    protected $language;

    /**
     * $faqCategory
     *
     * @var mixed
     */
    protected $faqCategory;

    /**
     * __construct
     *
     * @param Language $language
     * @param FaqCategory $faqCategory
     */
    public function __construct(Language $language, FaqCategory $faqCategory)
    {
        $this->language = $language;
        $this->faqCategory = $faqCategory;
    }

    /**
     * list
     *
     * @param int $languageId
     *
     * @return array
     */
    public function list(int $languageId): array
    {
        $category = $this->faqCategory->where('language_id', $languageId)->get();
        $language = $this->language->get();
        return compact('category', 'language');
    }

    /**
     * edit
     *
     * @param int $categoryId
     * @param int $languageId
     *
     * @return object
     */
    public function edit(int $categoryId, int $languageId): object
    {
        return $this->faqCategory->where(['id' => $categoryId, 'language_id' => $languageId])->with('language')->get();
    }

    /**
     * update
     *
     * @param array $input
     * @param int $categoryId
     * @param int $languageId
     *
     * @return int
     */
    public function update(array $input, int $categoryId, int $languageId): int
    {
        $faq = $this->faqCategory->where(['id' => $categoryId, 'language_id' => $languageId])
            ->update([
                'name' => $input['title'],
                'language_id' => $languageId,
                'updated_by' => auth()->user()->id,
            ]);

        return $faq;
    }

    /**
     * getAllCategories
     *
     * @return array
     */
    public function getAllCategories(): array
    {
        $languageId = getLanguageId(app()->getLocale());
        $uniqueTitle = $this->faqCategory->where('language_id', 1)->select('name')->get()->toArray();
        
        return $this->faqCategory->where('language_id', $languageId)
            ->get()
            ->map(function ($value, $key) use ($uniqueTitle) {
                $value['unique_title'] = $uniqueTitle[$key]['name'] ?? '';

                return $value;
            })
            ->toArray();
    }
}
