<?php

namespace App\Repositories;

use App\Models\City;
use App\Models\Country;
use App\Models\MasterSubCategory;
use App\Models\MasterSubCategoryChild;
use App\Models\State;
use App\Models\TopSpeciality;
use App\Interfaces\MasterDataInterface;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class MasterDataRepository implements MasterDataInterface
{
    /**
     * city
     *
     * @var mixed
     */
    protected $city;
    /**
     * country
     *
     * @var mixed
     */
    protected $country;
    /**
     * masterSubCategory
     *
     * @var mixed
     */
    protected $masterSubCategory;
    /**
     * state
     *
     * @var mixed
     */
    protected $state;
    protected $topSpeciality;

    public function __construct(
        City $city,
        Country $country,
        MasterSubCategory $masterSubCategory,
        MasterSubCategoryChild $masterSubCategoryChild,
        State $state,
        TopSpeciality $topSpeciality
    ) {
        $this->city = $city;
        $this->country = $country;
        $this->masterSubCategory = $masterSubCategory;
        $this->masterSubCategoryChild = $masterSubCategoryChild;
        $this->state = $state;
        $this->topSpeciality = $topSpeciality;
    }
    /**
     * getAllCountries
     *
     * @return Collection
     */
    public function getAllCountries(): Collection
    {
        return $this->country::select('id', 'name')->get();
    }

    /**
     * getCitiesFromState
     *
     * @param  mixed $stateId
     * @return Collection
     */
    public function getCitiesFromState($stateId): Collection
    {
        $cities = $this->city::select('id', 'name')->where('status', config('constants.status.active'));
        if ($stateId > 0) {
            $cities = $cities->where('state_id', $stateId);
        }
        return $cities->get();
    }

    public function getAllStates(): Collection
    {
        return $this->state::select('id', 'name')->get();
    }

    /**
     * getCityStateId
     *
     * @return Collection
     */
    public function getCityStateId($cityName, $stateName): array
    {
        $city = $this->city::where('name', $cityName)->first('id');
        $state = $this->state::where('name', $stateName)->first('id');

        $data = array(
            'city_id' => $city ? $city->id : '',
            'state_id' => $state ? $state->id : '',
        );

        return $data;
    }

    public function getCategoriesFromType($type)
    {
        $languageId = getLanguageId(app()->getLocale());
        if ($type == 'certification' || $type == 'language' || $type == 'distance') {
            return $this->masterSubCategory::join('master_categories as m', 'm.id', 'master_sub_categories.master_category_id')
                ->where('m.name', $type)
                ->where('status', config('constants.status.active'))
                ->where('is_approved', 1)
                ->orderBy('order_no')->get(['master_sub_categories.id', 'label']);
        } else if ($type == 'occupation' || $type == 'specialty' || $type == 'symptom' || $type == 'procedure') {
            if ($type == 'specialty' || $type == 'symptom') {
                return $this->masterSubCategory::join('master_categories as m', 'm.id', 'master_sub_categories.master_category_id')
                    ->join('master_sub_category_child', 'master_sub_categories.id', 'master_sub_category_child.master_sub_category_id')
                    ->where('m.name', $type)
                    ->where('status', config('constants.status.active'))
                    ->where('master_sub_category_child.language_id', $languageId)
                    ->where('is_approved', 1)
                    ->orderBy('label')->get(['master_sub_category_child.master_sub_category_id', 'master_sub_category_child.label']);
            }

            return $this->masterSubCategory::join('master_categories as m', 'm.id', 'master_sub_categories.master_category_id')
                ->join('master_sub_category_child', 'master_sub_categories.id', 'master_sub_category_child.master_sub_category_id')
                ->where('m.name', $type)
                ->where('status', config('constants.status.active'))
                ->where('master_sub_category_child.language_id', $languageId)
                ->where('is_approved', 1)
                ->orderBy('order_no')->get(['master_sub_category_child.master_sub_category_id', 'master_sub_category_child.label']);
        } else {
            return collect();
        }
    }

    public function getSymptoms($specialtyId)
    {
        $languageId = getLanguageId(app()->getLocale());

        return $this->masterSubCategoryChild::whereIn('master_sub_category_id', $this->masterSubCategory::where('parent_id', $specialtyId)->pluck('id'))->where('language_id', $languageId)->orderBy('label')->get(['master_sub_category_id', 'label']);
    }

    public function getFiltersWithCount($type)
    {
        $doctorInformationsCount = $this->masterSubCategory::leftJoin('master_categories as m', 'm.id', 'master_sub_categories.master_category_id')
            ->leftJoin('doctor_overviews as do', 'master_sub_categories.id', 'do.master_data_id')
            ->where([
                'name' => $type,
                'status' => config('constants.status.active'),
                'is_approved' => 1
            ])->orderBy('order_no')
            ->select(['master_sub_categories.id', 'label', 'value', DB::raw('(SELECT COUNT(*) FROM doctor_overviews WHERE doctor_overviews.master_data_id = master_sub_categories.id) as count')])
            ->get();

        return $doctorInformationsCount;
    }

    public function getLocation(array $locationData)
    {

        $client = new Client();

        if (!empty($locationData['latitude']) && !empty($locationData['longitude'])) {
            $coordinates = $locationData['latitude'] . ',' . $locationData['longitude'];
            $apiRequest = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $coordinates . '&sensor=true&key=AIzaSyAjsVIkECQdqciuzLaXfq1q6TEm6Ynozec');

            $googleApiResponse = !empty($apiRequest) ? json_decode($apiRequest->getBody()) : [];

            if (!empty($googleApiResponse)) {
                if ($googleApiResponse->status != 'ZERO_RESULTS') {
                    $countryName = !empty($googleApiResponse->results[1]->address_components[6]) ? $googleApiResponse->results[1]->address_components[6]->long_name : $googleApiResponse->results[1]->address_components[4]->long_name;

                    $cityName = ($countryName == 'United States') ? $googleApiResponse->results[1]->address_components[2]->long_name : 'New York';
                    $stateName = ($countryName == 'United States') ? $googleApiResponse->results[1]->address_components[3]->long_name : 'New York';
                    $latitude = $locationData['latitude'];
                    $longitude = $locationData['longitude'];
                }
            }
        } else if (!empty($locationData['zip_code'])) {
            $zipcode = $locationData['zip_code'];
            $apiRequest = $client->request('GET', 'https://maps.googleapis.com/maps/api/geocode/json?address=' . $zipcode . '&sensor=true&key=AIzaSyAjsVIkECQdqciuzLaXfq1q6TEm6Ynozec');

            $googleApiResponse = !empty($apiRequest) ? json_decode($apiRequest->getBody()) : [];

            if (!empty($googleApiResponse)) {
                if ($googleApiResponse->status != 'ZERO_RESULTS') {
                    $countryName = !empty($googleApiResponse->results[0]->address_components[5]) ? $googleApiResponse->results[0]->address_components[5]->long_name : $googleApiResponse->results[0]->address_components[4]->long_name;

                    $cityName = ($countryName == 'United States') ? $googleApiResponse->results[0]->address_components[1]->long_name : 'New York';
                    $stateName = ($countryName == 'United States') ? $googleApiResponse->results[0]->address_components[3]->long_name : 'New York';
                    $latitude = isset($googleApiResponse->results[0]->geometry->location->lat) ? $googleApiResponse->results[0]->geometry->location->lat : '';
                    $longitude = isset($googleApiResponse->results[0]->geometry->location->lng) ? $googleApiResponse->results[0]->geometry->location->lng : '';
                }
            }
        }

        $response = [
            'city' => $cityName ?? 'New York',
            'state' => $stateName ?? 'New York',
            'latitude' => $latitude ?? '',
            'longitude' => $longitude ?? ''
        ];

        return $response;
    }

    public function getTopSpeciality()
    {
        $languageId = getLanguageId(app()->getLocale());
        $topSpeciality = $this->topSpeciality::join('doctor_information as di', 'di.user_id', 'top_specialities.doctor_id')->join('master_sub_category_child', 'top_specialities.speciality_id', 'master_sub_category_child.master_sub_category_id')->where('master_sub_category_child.language_id', $languageId)->select(['top_specialities.id', 'top_specialities.speciality_id', 'top_specialities.doctor_id', 'di.profile_image as specialty_image', 'di.first_name', 'di.last_name', 'master_sub_category_child.label'])->get();

        $unregiDocs = $this->topSpeciality::join('doctors as d', 'd.id', 'top_specialities.unreg_doctor_id')
        ->join('master_sub_category_child', 'top_specialities.speciality_id', 'master_sub_category_child.master_sub_category_id')
        ->where('master_sub_category_child.language_id', $languageId)
        ->select(['top_specialities.id', 'top_specialities.speciality_id', 'top_specialities.unreg_doctor_id', 'd.profile_image as specialty_image', 'd.first_name', 'd.last_name', 'master_sub_category_child.label'])
        ->get();

        return $topSpeciality->concat($unregiDocs);
    }
}
