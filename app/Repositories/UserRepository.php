<?php

namespace App\Repositories;

use App\Interfaces\UserInterface;
use App\Models\User;
use App\Models\UserOtp;

class UserRepository implements UserInterface
{
    /**
     * user
     *
     * @var mixed
     */
    protected $user;
    /**
     * userOtp
     *
     * @var mixed
     */
    protected $userOtp;

    public function __construct(User $user, UserOtp $userOtp)
    {
        $this->user = $user;
        $this->userOtp = $userOtp;
    }
    public function findUser($userName, $roleId)
    {
        /*if (is_numeric($userName)) {
            $country_code = config('constants.country_code');
            $phoneNumber = preg_replace("/^\+?{$country_code}/", '', $userName);
            $userName = config('constants.country_code') . $phoneNumber;
        }*/

        // $roleIds = getDoctorAndPatientRoleIds();
        return $this->user::where('role_id', $roleId)
            ->where('email', $userName)
            ->orWhere('phone_number', $userName)
            ->first();
    }

    public function verifyUser($userId)
    {
        $isVerified = $this->user::where('id', $userId)->update(['is_verified' => 1, 'otp_count' => 0, 'updated_at' => now()]);
        if ($isVerified) {
            $this->userOtp::where('user_id', $userId)->delete();
        }
        return $isVerified;
    }
}
