<?php

namespace App\Transformations;

use Carbon\Carbon;

trait NotificationTransformable
{
    /**
     * transformNotificationData
     *
     * @param  mixed $notificationArray
     * @return void
     */
    protected function transformNotificationData(mixed $notificationArray): array
    {
        $responseArray = [];

        if ($notificationArray) {
            foreach ($notificationArray as $key => $notification) {
                
                $responseArray[$key] = !empty($notification->message) ? json_decode($notification->message) : Null;
                $responseArray[$key]->is_read = $notification->is_read ?? Null;
                $responseArray[$key]->notification_id = $notification->id ?? Null;
                $responseArray[$key]->created_at = Carbon::createFromTimeStamp(strtotime($notification->created_at))->diffForHumans() ?? Null;
            }
        }

        return $responseArray;
    }
}
