<?php

namespace App\Transformations;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

trait PatientTransformable
{
    /**
     * transformFavoriteDoctor
     *
     * @author Heli Patel
     * @param  mixed $doctorBlockSlotData
     * @return array
     */
    public function transformFavoriteDoctor($favoriteDoctorData)
    {
        $favoriteDoctorArray = [];
        $favoriteDoctorWithGroup = [];

        if (!empty($favoriteDoctorData)) {
            $data = [];
            foreach ($favoriteDoctorData['doctor_information'] as $groupName => $favoriteDoctorGroup) {
                foreach ($favoriteDoctorGroup as $favoriteDoctor) {

                    $data = [
                        'patient_id' => $favoriteDoctor->patient_id,
                        'doctor_favorite_group_id' => $favoriteDoctor->doctor_favorite_group_id,
                        'doctor_information' => $this->getDoctorInformationData($favoriteDoctorGroup),
                    ];
                }
                $favoriteDoctorArray[$groupName] = $data;
            }

            $favoriteGroupArray = [];

            if (!empty($favoriteDoctorData['other_favorite_group'])) {
                foreach ($favoriteDoctorData['other_favorite_group'] as $otherGroupDataArray) {
                    foreach ($otherGroupDataArray as $key => $otherGroupData) {
                        $otherGroupData['doctor_information'] = [];
                        $favoriteGroupArray[$otherGroupData['name']] = $otherGroupData;
                    }
                }

                $favoriteDoctorWithGroup = array_merge_recursive($favoriteDoctorArray, $favoriteGroupArray);
            } else {
                $favoriteDoctorWithGroup = $favoriteDoctorArray;
            }
        }

        return  $favoriteDoctorWithGroup;
    }

    /**
     * transformPatientAppointments
     *
     * @author Heli Patel
     * @param  array $doctorAppointmentData
     * @return array
     */
    protected function transformPatientAppointments(array $doctorAppointmentData, int $offset = null, int $limit = null, int $currentPage = null): mixed
    {
        $responseArray = [];
        $rescheduleData = null;

        $totalCount = isset($doctorAppointmentData['totalRecord']) ? $doctorAppointmentData['totalRecord'] : 0;
        unset($doctorAppointmentData['totalRecord']);

        if (!empty($doctorAppointmentData['appointment_data'])) {
            $responseArray['appointment'] = [];
            foreach ($doctorAppointmentData['appointment_data'] as $appointment) {
                // Set appointment type
                if (!empty($appointment->consultant_type)) {
                    if ($appointment->consultant_type == 'video') {
                        $consultancyType = "video_consultancy";
                    } elseif ($appointment->consultant_type) {
                        $consultancyType = "in_person";
                    }
                }

                $data = [
                    'appointment_id' => !empty($appointment->appointment_id) ? $appointment->appointment_id  : Null,
                    'booking_id' => !empty($appointment->booking_id) ? $appointment->booking_id  : Null,
                    'patient_id' => !empty($appointment->patient_id) ? $appointment->patient_id  : Null,
                    'patient_name' => !empty($appointment->patient_name) ? $appointment->patient_name  : Null,
                    'doctor_id' => !empty($appointment->doctor_id) ? $appointment->doctor_id  : Null,
                    'first_name' => !empty($appointment->first_name) ? $appointment->first_name  : Null,
                    'last_name' => !empty($appointment->last_name) ? $appointment->last_name  : Null,
                    'occupation_id' => $appointment->occupation_id,
                    'occupation_name' => $appointment->occupation_name,
                    'specialty_id' => $appointment->specialty_id,
                    'specialty_name' => $appointment->specialty_name,
                    'profile_image' => !empty($appointment->profile_image) ? getImage($appointment->profile_image, $appointment->user_id) : '',
                    'address' => $appointment->address . ", " . $appointment->zipcode,
                    'suite' => $appointment->suite,
                    'doctor_location_id' => $appointment->doctor_location_id,
                    'consultancy_type' => !empty($appointment->consultant_type) ? $consultancyType  : Null,
                    'consultancy_reason' => !empty($appointment->reason) ? $appointment->reason  : Null,
                    'date' => !empty($appointment->appointment_date) ? date('d-m-Y', strtotime($appointment->appointment_date)) : Null,
                    'start_time' => !empty($appointment->start_time) ? date('h:i A', strtotime($appointment->start_time)) : Null,
                    'end_time' => !empty($appointment->end_time) ? date('h:i A', strtotime($appointment->end_time)) : Null,
                    'appointment_status' => $appointment->status ? $appointment->status : Null,
                    'booked_on' => $appointment->created_at ? date('d-m-Y H:i A', strtotime($appointment->created_at)) : Null,
                    'consultation_url' => $appointment->consultation_url ? $appointment->consultation_url : Null,
                    'is_multiple_booking' => !empty($appointment->isMultipleBooking) ? ((count($appointment->isMultipleBooking) >= 2) ? true : false) : false,
                ];

                // if ($appointment->status == 'reschedule_requested') {
                $data['reschedule_appointment_by_patient'] = $appointment->rescheduleAppointmentByPatient;
                $data['reschedule_appointment_by_doctor'] = $appointment->rescheduleAppointmentByDoctor;

                if (!empty($appointment->rescheduleAppointmentByPatient) && !empty($appointment->rescheduleAppointmentByDoctor)) {

                    if ($appointment->rescheduleAppointmentByPatient->status == 'pending') {
                        $data['reschedule_appointment_by_doctor'] = null;
                        $data['reschedule_appointment_by_patient'] = $appointment->rescheduleAppointmentByPatient;
                    }

                    if ($appointment->rescheduleAppointmentByDoctor->status == 'pending') {
                        $data['reschedule_appointment_by_patient'] = null;
                        $data['reschedule_appointment_by_doctor'] = $appointment->rescheduleAppointmentByDoctor;
                    }
                }

                // }

                $data['review_submitted'] = false;

                if ($appointment->review) {
                    $data['review_submitted'] = true;
                    $data['review'] = $appointment->review;
                }

                array_push($responseArray['appointment'], $data);
            }

            // $responseArray['is_multiple_booking'] = !empty($doctorAppointmentData['is_multiple_booking']) ? $doctorAppointmentData['is_multiple_booking']  : Null;
            $responseArray['past_bookings'] = !empty($doctorAppointmentData['past_bookings']) ? $doctorAppointmentData['past_bookings']  : Null;
            $responseArray['upcoming_bookings'] = !empty($doctorAppointmentData['upcoming_bookings']) ? $doctorAppointmentData['upcoming_bookings']  : Null;
            $responseArray['no_appointment_schedule'] = !empty($doctorAppointmentData['no_appointment_schedule']) ? $doctorAppointmentData['no_appointment_schedule']  : Null;
        }

        if (!empty($limit) && ($currentPage >= 0)) {
            $paginator = new Paginator($responseArray, $totalCount, $limit, $currentPage);
            return $paginator;
        }

        return $responseArray;
    }

    /**
     * getDoctorInformationData
     *
     * @author Heli Patel
     * @param  array $favoriteDoctorData
     * @return array
     */
    public function getDoctorInformationData(Collection $favoriteDoctorData): array
    {
        $doctorData = [];
        $reviewCount = 0;

        foreach ($favoriteDoctorData as $groupName => $favoriteDoctor) {

            $reviewsData = $favoriteDoctor->review->toArray();
            $reviewCount = count($reviewsData);

            if ($reviewCount > 0) {
                $ratings = array_map(function ($review) {
                    return $review['rating'];
                }, $reviewsData);
                $average = array_sum($ratings) / count($ratings);
            }

            $doctorData[] = [
                'id' => $favoriteDoctor->id,
                'doctor_id' => $favoriteDoctor->doctor_id,
                'email' => $favoriteDoctor->email,
                'distance' => isset($favoriteDoctor->distance) ? round($favoriteDoctor->distance, 3) : 'Not Found',
                'first_name' => $favoriteDoctor->first_name,
                'last_name' => $favoriteDoctor->last_name,
                'occupation_id' => $favoriteDoctor->occupation_id,
                'occupation_name' => $favoriteDoctor->occupation_name,
                'specialty_id' => $favoriteDoctor->specialty_id,
                'specialty_name' => $favoriteDoctor->specialty_name,
                'profile_image' => !empty($favoriteDoctor->profile_image) ? getImage($favoriteDoctor->profile_image, $favoriteDoctor->user_id) : '',
                'address' => $favoriteDoctor->address . ", " . $favoriteDoctor->zipcode,
                'suite' => $favoriteDoctor->suite,
                'in_person' => $favoriteDoctor->in_person,
                'video_consultancy' => $favoriteDoctor->video_consultancy,
                'doctor_location_id' => $favoriteDoctor->doctor_location_id,
                'review_count' => $reviewCount,
                'avg_rating' => $average ?? 0
            ];
        }

        return $doctorData;
    }

    /**
     * transformAppointmentList
     **
     * @author Krishna Shah
     * @param  integer $offset
     * @param  integer $limit
     * @param  integer $currentPage
     * @param  array   $appointmentData
     * @return JsonResponse
     */
    protected function transformFeedList($feedData, $offset = '', $limit = '', $currentPage = '')
    {
        $responseArray = [];
        $totalCount = !empty($feedData['totalRecord']) ? $feedData['totalRecord'] : 0;
        unset($feedData['totalRecord']);
        if (count($feedData['feed_information']) > 0) {
            foreach ($feedData['feed_information'] as $key => $feedInfo) {

                $fileName = !empty($feedInfo['file']) ? getImage($feedInfo['file'], $feedInfo['id']) : NULL;
                // Allowed files -- jpeg,png,jpg,gif,svg,mp4,mov,ogg,qt
                $extension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
                $fileType = NULL;

                if (in_array($extension, ['jpeg', 'png', 'jpg', 'svg'])) {
                    $fileType = 'image';
                } else {
                    $fileType = 'video';
                }

                $responseArray['feed_information'][] = [
                    'id' => $feedInfo['id'] ?? NULL,
                    'doctor_id' => $feedInfo['user_id'] ?? NULL,
                    'title' => $feedInfo['title'] ?? NULL,
                    'description' => $feedInfo['description'] ?? NULL,
                    'status' => $feedInfo['status'] ?? NULL,
                    'file' => !empty($feedInfo['file']) ? getImage($feedInfo['file'], $feedInfo['user_id']) : NULL,
                    'poster_image' => !empty($feedInfo['poster_image']) ? getImage($feedInfo['poster_image'], $feedInfo['user_id']) : NULL,
                    'file_type' => !empty($feedInfo['file']) ? $fileType : NULL,
                    'profile_image' => !empty($feedInfo['profile_image']) ? getImage($feedInfo['profile_image'], $feedInfo['created_by']) : NULL,
                    'first_name' => $feedInfo['first_name'] ?? NULL,
                    'last_name' => $feedInfo['last_name'] ?? NULL,
                    'created_at' => !empty($feedInfo['created_at']) ? $feedInfo['created_at'] : NULL,
                    'is_like' => $feedInfo->isAuthUserLikedPost(),
                    'is_follow' => $feedInfo->isAuthUserFollowedPost(),
                    'like_count' => $feedInfo['likecount'] ?? 0,
                    'comment_count' => $feedInfo['commentcount'] ?? 0
                ];
            }

            // Allowed files -- jpeg,png,jpg,gif,svg,mp4,mov,ogg,qt
            $latestFeedFileExtension = strtolower(pathinfo($feedData['latest_feed_data']['file'], PATHINFO_EXTENSION));

            if (in_array($latestFeedFileExtension, ['jpeg', 'png', 'jpg', 'svg'])) {
                $latestFeedFiletype = 'image';
            } else {
                $latestFeedFiletype = 'video';
            }
            $feedData['latest_feed'] = [];

            if (!empty($feedData['latest_feed_data']->comments)) {
                foreach ($feedData['latest_feed_data']->comments as $key => $comment) {
                    $comment['patient_profile_image'] = !empty($comment['patient_profile_image']) ? getImage($comment['patient_profile_image'], $comment['created_by']) : NULL;
                    $comment['dr_profile_image'] = !empty($comment['dr_profile_image']) ? getImage($comment['dr_profile_image'], $comment['created_by']) : NULL;
                }
                $feedData['latest_feed']['comments'] = $feedData['latest_feed_data']->comments;
            }

            $feedData['latest_feed'] = [
                'id' => $feedData['latest_feed_data']['id'] ?? NULL,
                'doctor_id' => $feedData['latest_feed_data']['user_id'] ?? NULL,
                'title' => $feedData['latest_feed_data']['title'] ?? NULL,
                'description' => $feedData['latest_feed_data']['description'] ?? NULL,
                'status' => $feedData['latest_feed_data']['status'] ?? NULL,
                'first_name' => $feedData['latest_feed_data']['first_name'] ?? NULL,
                'last_name' => $feedData['latest_feed_data']['last_name'] ?? NULL,
                'created_at' => !empty($feedData['latest_feed_data']['created_at']) ? $feedData['latest_feed_data']['created_at'] : NULL,
                'is_like' => $feedData['latest_feed_data']->isAuthUserLikedPost(),
                'is_follow' => $feedData['latest_feed_data']->isAuthUserFollowedPost(),
                'like_count' => $feedData['latest_feed_data']['likecount'],
                'comment_count' => $feedData['latest_feed_data']['commentcount'],
                'file' => !empty($feedData['latest_feed_data']['file']) ? getImage($feedData['latest_feed_data']['file'], $feedData['latest_feed_data']['user_id']) : NULL,
                'file_type' => !empty($feedData['latest_feed_data']['file']) ? $latestFeedFiletype : NULL,
                'poster_image' => !empty($feedData['latest_feed_data']['poster_image']) ? getImage($feedData['latest_feed_data']['poster_image'], $feedData['latest_feed_data']['user_id']) : NULL,
                'profile_image' => !empty($feedData['latest_feed_data']['profile_image']) ? getImage($feedData['latest_feed_data']['profile_image'], $feedData['latest_feed_data']['user_id']) : NULL,
            ];

            $responseArray['latest_feed'] =  !empty($feedData['latest_feed_data']) ? $feedData['latest_feed'] : [];
        } else {
            $responseArray['feed_information'] = [];
        }
        if (!empty($limit) && ($currentPage >= 0)) {
            $paginator = new Paginator($responseArray, $totalCount, $limit, $currentPage);
            return $paginator;
        }
        return $responseArray;
    }
}
