<?php

namespace App\Transformations;

use Illuminate\Support\Carbon;

trait MessageTransformable
{
    /**
     * transformMessageData
     *
     * @param  array $messageData
     * @return void
     */
    protected function transformMessageData(array $messageData, int $fromUserId): array
    {
        $responseArray = [];

        if ($messageData) {
            foreach ($messageData as $val) {
                $temp = [
                    'id' => $val['id'],
                    'user_name' => $val['user'],
                    'profile_image' => $val['profile_image'] ? getImage($val['profile_image'], $fromUserId) : '',
                    "message" =>  $val['message'],
                    "file" => $val['file'],
                    "message_type" => $val['message_type'],
                    "send_at" => date('d-m-Y H:i:s', strtotime($val['created_at'])),
                    "send_by_me" => $val['sent_by_me'],
                    "original_file_name" => $val['original_file_name'], //$this->getMessageType($val['file'])
                    "send_by_me" => $val['sent_by_me'],
                ];
                array_push($responseArray, $temp);
            }
        }

        return $responseArray;
    }
    protected function transformChatRequestList(array $chatList): array
    {
        $responseArray = [];

        $unreadMessages = array_filter($chatList['messages'], function ($element) {
            return isset($element['is_read']) && $element['is_read'] == 0;
        });

        foreach ($chatList['chatRequestList'] as $key => $chatRequestList) {
            $count = 0;
            $chatList['chatRequestList'][$key]['most_recent_message'] = '';
            $chatList['chatRequestList'][$key]['most_recent_message_date'] = '';
            $chatList['chatRequestList'][$key]['unread_messages'] = $count;
            $chatList['chatRequestList'][$key]['message_type'] = '';


            $unreadMessage =  array_filter($unreadMessages, function ($element) use ($chatRequestList) {
                return isset($chatRequestList['patient_id']) ? $element['from_user_id'] == $chatRequestList['patient_id'] : $element['from_user_id'] == $chatRequestList['doctor_id'];
            });

            $allMessages = array_filter($chatList['messages'], function ($element) use ($chatRequestList) {
                return isset($chatRequestList['patient_id']) ? ($element['from_user_id'] == $chatRequestList['patient_id'] ||  $element['to_user_id'] == $chatRequestList['patient_id']) : ($element['from_user_id'] == $chatRequestList['doctor_id'] || $element['to_user_id'] == $chatRequestList['doctor_id']);
            });

            $lastMessage = end($allMessages);
            $chatList['chatRequestList'][$key]['unread_messages']  = count($unreadMessage);
            $chatList['chatRequestList'][$key]['message_type'] = !empty($lastMessage) ? $lastMessage['message_type'] : '';

            if (count($allMessages)) {
                if (isset($chatRequestList['patient_id'])) {
                    $chatList['chatRequestList'][$key]['most_recent_message']  = ($lastMessage['from_user_id'] == $chatRequestList['patient_id'] || $lastMessage['to_user_id'] == $chatRequestList['patient_id']) ? $lastMessage['message'] : '';
                    $chatList['chatRequestList'][$key]['most_recent_message_date']  = ($lastMessage['from_user_id'] == $chatRequestList['patient_id'] || $lastMessage['to_user_id'] == $chatRequestList['patient_id']) ?  Carbon::parse($lastMessage['created_at'])->format('d-m-Y H:i:s') : '';
                } else {
                    $chatList['chatRequestList'][$key]['most_recent_message']  = ($lastMessage['from_user_id'] == $chatRequestList['doctor_id'] || $lastMessage['to_user_id'] == $chatRequestList['doctor_id']) ? $lastMessage['message'] : '';
                    $chatList['chatRequestList'][$key]['most_recent_message_date']  = ($lastMessage['from_user_id'] == $chatRequestList['doctor_id'] || $lastMessage['to_user_id'] == $chatRequestList['doctor_id']) ?  Carbon::parse($lastMessage['created_at'])->format('d-m-Y H:i:s') : '';
                }
            }

            if (!empty($chatList['chatRequestList'][$key]['patient_id']))
                $chatList['chatRequestList'][$key]['profile_image'] = !empty($chatList['chatRequestList'][$key]['profile_image']) ? getImage($chatList['chatRequestList'][$key]['profile_image'], $chatList['chatRequestList'][$key]['patient_id']) : '';

            if (!empty($chatList['chatRequestList'][$key]['doctor_id']))
                $chatList['chatRequestList'][$key]['profile_image'] = !empty($chatList['chatRequestList'][$key]['profile_image']) ? getImage($chatList['chatRequestList'][$key]['profile_image'], $chatList['chatRequestList'][$key]['doctor_id']) : '';
        }

        $responseArray = $chatList['chatRequestList'];

        // Sort the array
        if (!empty($responseArray))
            usort($responseArray, 'date_compare');

        return $responseArray;
    }

    protected function getMessageType($file)
    {
        $fileType = "text";
        $video = ['mp4', 'mkv', 'webm'];
        $image = ['png', 'jpg', 'jpeg', 'png', 'svg'];
        $doc = ['doc', 'docx', 'pdf'];
        if (!empty($file)) {
            $fileExt = explode('.', $file);
            $ext = end($fileExt);
            if (in_array($ext, $image)) {
                $fileType = "image";
            } elseif (in_array($ext, $video)) {
                $fileType = "video";
            } elseif (in_array($ext, $doc)) {
                $fileType = "document";
            }
        }
        return $fileType;
    }
}
