<?php

namespace App\Transformations;

use App\Models\User;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use DateTime;

trait CalendarTransformable
{

    /**
     * transformBookingData
     *
     * @author Heli Patel
     * @param  mixed $doctorBlockSlotData
     * @return array
     */
    protected function transformBookingData($doctorBlockSlotData): array
    {
        $responseArray = [
            'user_id' => $doctorBlockSlotData['doctor_id'],
            'doctor_id' => $doctorBlockSlotData['doctor_id'],
            'location_id' => $doctorBlockSlotData['location_id'],
            'block_slot' => $this->transformSlotData($doctorBlockSlotData['slot_data'])
        ];

        return $responseArray;
    }

    /**
     * transformSlotData
     *
     * @author Heli Patel
     * @param  mixed $doctorBlockSlotData
     * @return array
     */
    public function transformSlotData($doctorBlockSlotData)
    {
        $slotDataArray = [];
        foreach ($doctorBlockSlotData as $slotData) {
            $data = [
                'date' => !empty($slotData->date) ? date('d-m-Y', strtotime($slotData->date)) : Null,
                'start_time' => !empty($slotData->start) ? date('h:i A', strtotime($slotData->start)) : Null,
                'end_time' => !empty($slotData->end) ? date('h:i A', strtotime($slotData->end)) : Null,
                'event_name' => '',
            ];
            array_push($slotDataArray, $data);
        }
        return  $slotDataArray;
    }

     /**
     * transformDoctorWorkTiming
     *
     * @author Heli Patel
     * @param  array $workItemData
     * @return array
     */
    protected function transformDoctorWorkTiming($workItemData): array
    {
        $responseArray = [];
        $responseArray['work_timing'] = [];
        $data = [];
        $responseArray['user_id'] = !empty($workItemData['doctor_id']) ? $workItemData['doctor_id'] : Null;
        $responseArray['location_id'] = !empty($workItemData['location_id']) ? $workItemData['location_id'] : Null;
        $responseArray['appointment_duration'] = !empty($workItemData['appointment_duration']) ? $workItemData['appointment_duration'] : Null;
        $days = config('constants.days');
        $daysArray = [];

        if (!empty($workItemData['work_timing_data'])) {
            foreach ($workItemData['work_timing_data'] as $day => $workTimingData) {
                $workTimeArray = [];
                foreach ($workTimingData as $key => $workTiming) {
                    $workTimeArray[$key] = [
                        'working_time_id' => $workTiming->id,
                        'start_time' => !empty($workTiming->start_time) ? date('h:i', strtotime($workTiming->start_time)) : Null,
                        'start_period' => !empty($workTiming->start_time) ? date('A', strtotime($workTiming->start_time)) : Null,
                        'end_time' => !empty($workTiming->end_time) ? date('h:i', strtotime($workTiming->end_time)) : Null,
                        'end_period' => !empty($workTiming->end_time) ? date('A', strtotime($workTiming->end_time)) : Null,
                    ];
                }
                array_push($daysArray, getFullDay($day));
                $data[getFullDay($day)] = $workTimeArray;
            }
        }

        $remainingDays = array_diff($days, $daysArray);
        if (!empty($remainingDays)) {
            foreach ($remainingDays as $daysData) {
                $data[$daysData] = [];
            }
        }
        $responseArray['work_timing'] = $data;

        return $responseArray;
    }

    /**
     * transformDoctorAppointments
     *
     * @author Heli Patel
     * @param  array $doctorAppointmentData
     * @return array
     */
    protected function transformDoctorAppointments($doctorAppointmentData): array
    {
        $responseArray = [];
        $responseArray['doctor_id'] = !empty($doctorAppointmentData['doctor_id']) ? $doctorAppointmentData['doctor_id'] : Null;
        $responseArray['location_id'] = !empty($doctorAppointmentData['location_id']) ? $doctorAppointmentData['location_id'] : Null;
        $responseArray['appointment'] = [];
        $responseArray['block_slot'] = [];

        foreach ($doctorAppointmentData['appointment_data'] as $appointment) {
            // Set appointment type
            if (!empty($appointment->consultant_type)) {
                if ($appointment->consultant_type == 'video') {
                    $consultancyType = "video_consultancy";
                } elseif ($appointment->consultant_type) {
                    $consultancyType = "in_person";
                }
            }

            $data = [
                'id' => !empty($appointment->id) ? $appointment->id  : Null,
                'patient_id' => !empty($appointment->patient_id) ? $appointment->patient_id  : Null,
                'location_id' => !empty($appointment->location_id) ? $appointment->location_id  : Null,
                'consultancy_type' => !empty($appointment->consultant_type) ? $consultancyType  : Null,
                'date' => !empty($appointment->appointment_date) ? date('d-m-Y', strtotime($appointment->appointment_date)) : Null,
                'start_time' => !empty($appointment->start_time) ? date('h:i A', strtotime($appointment->start_time)) : Null,
                'end_time' => !empty($appointment->end_time) ? date('h:i A', strtotime($appointment->end_time)) : Null,
                'event_name' => !empty($appointment->patient_name) ? $appointment->patient_name : Null,
                'appointment_status' => $appointment->status ? $appointment->status : Null,
            ];
            array_push($responseArray['appointment'], $data);
        }

        foreach ($doctorAppointmentData['block_slot'] as $blockSlot) {
            $blockSlot = [
                'date' => !empty($blockSlot->date) ? date('d-m-Y', strtotime($blockSlot->date)) : Null,
                'start_time' => !empty($blockSlot->start) ? date('h:i A', strtotime($blockSlot->start)) : Null,
                'end_time' => !empty($blockSlot->end) ? date('h:i A', strtotime($blockSlot->end)) : Null,
            ];
            array_push($responseArray['block_slot'], $blockSlot);
        }

        return $responseArray;
    }

    /**
     * transformWorkingTimeSlots
     *
     * @author Heli Patel
     * @param  string $interval, string $start_time,string $end_time,Object $workTimingData
     * @return array
     */
    function getTimeSlot(Object $workTimingDataArray): array
    {
        $daysArray = [];
        $start_time= Null;
        $end_time= Null;
        $data = [];

        foreach ($workTimingDataArray as $day => $workTimingData) {
            $workTimeArray = [];
            foreach ($workTimingData as $key => $workTiming) {

                $appointmentDuration = !empty($workTiming->appointment_duration) ? $workTiming->appointment_duration : Null;
                $start_time = !empty($workTiming->start_time) ? Carbon::parse($workTiming->start_time)->format('H:i') : Null;
                $end_time = !empty($workTiming->end_time) ? Carbon::parse($workTiming->end_time)->format('H:i') : Null;
                
                $start = new DateTime($start_time);
                $end = new DateTime($end_time);
                $startTime = $start->format('H:i');
                $endTime = $end->format('H:i');
                $time= [];
                $i = 0;
                
                while (strtotime($startTime) <= strtotime($endTime)) {
                    $start = $startTime;
                    $end = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                    $startTime = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                    $i++;

                    if (strtotime($startTime) <= strtotime($endTime)) {
                        $time[$i] = [
                            'working_time_id' => $workTiming->id,
                            'start_time' => $start,
                            'start_period' => !empty($start) ? date('A', strtotime($start)) : Null,
                            'end_time' => $end,
                            'end_period' => !empty($end) ? date('A', strtotime($end)) : Null,
                        ];

                    }
                }
                $workTimeArray[$key] = array_values($time);
                $merged = call_user_func_array('array_merge',$workTimeArray);
            }
            array_push($daysArray, getFullDay($day));
            $data[getFullDay($day)] = array_values($merged);
        }

        $data['days_array'] = $daysArray;
        return $data;
    }
}
