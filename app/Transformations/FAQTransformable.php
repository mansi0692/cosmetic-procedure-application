<?php

namespace App\Transformations;

trait FAQTransformable
{
    /**
     * transformFAQCategoryData
     *
     * @param  array $faqCategoryList
     * @return void
     */
    protected function transformFAQCategoryData(array $faqCategoryList): array
    {
        $responseArray = [];

        if ($faqCategoryList) {
            foreach ($faqCategoryList as $val) {

                $temp = [
                    'id' => $val['id'],
                    "title" =>  $val['name'],
                    "unique_title" =>  $val['unique_title'],
                    "parent_id" => $val['parent_id']
                ];

                array_push($responseArray, $temp);
            }
        }

        return $responseArray;
    }

    /**
     * transformFAQData
     *
     * @param  array $faqList
     * @return void
     */
    protected function transformFAQData(array $faqList): array
    {
        $responseArray = [];

        if ($faqList) {
            foreach ($faqList as $val) {

                $temp = [
                    'id' => $val['id'],
                    'category_id' => $val['faq_category_id'],
                    "title" =>  $val['title'],
                    'description' => $val['description'],
                ];

                array_push($responseArray, $temp);
            }
        }

        return $responseArray;
    }
}
