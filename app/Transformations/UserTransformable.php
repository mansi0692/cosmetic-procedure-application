<?php

namespace App\Transformations;

use App\Models\User;

trait UserTransformable
{

    /**
     * transformUser
     *
     * @param  mixed $userInfo
     * @param  mixed $token
     * @return array
     */
    protected function transformUser($userInfo, $token = ''): array
    {

        if ($userInfo->role_id == config('constants.roles.doctor')) {
            $responseArray = [
                'user_id' => $userInfo->id,
                'first_name' => $userInfo->first_name,
                'username' => $userInfo->username,
                'last_name' => $userInfo->last_name,
                'email' => !is_null($userInfo->email) ? $userInfo->email : '',
                'phone_number' => !is_null($userInfo->phone_number) ? $userInfo->phone_number : NULL,
                'profile_image' => !empty($userInfo->profile_image) ? getImage($userInfo->profile_image, $userInfo->id) : '',
                'role_id' => $userInfo->role_id
                // 'gender' => $userInfo->gender,
            ];
        } else {
            $responseArray = [
                'user_id' => $userInfo->id,
                'full_name' => $userInfo->full_name,
                'username' => $userInfo->username,
                'email' => !is_null($userInfo->email) ? $userInfo->email : '',
                'phone_number' => !is_null($userInfo->phone_number) ? $userInfo->phone_number : NULL,
                'profile_image' => !empty($userInfo->profile_image) ? getImage($userInfo->profile_image, $userInfo->id) : '',
                'role_id' => $userInfo->role_id
                // 'gender' => $userInfo->gender,
                // 'address' => !is_null($userInfo->address) ? $userInfo->address : '',
                // 'country_id' => !is_null($userInfo->country_id) ? $userInfo->country_id : '',
                // 'city_id' => !is_null($userInfo->city_id) ? $userInfo->city_id : '',
                // 'state_id' => !is_null($userInfo->state_id) ? $userInfo->state_id : '',
                // 'zipcode' => !is_null($userInfo->zipcode) ? $userInfo->zipcode : '',
            ];
        }
        if (!empty($token)) {
            $responseArray['token'] = $token;
        }

        return $responseArray;
    }
}
