<?php

namespace App\Transformations;

use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Carbon\CarbonPeriod;
use DateTime;
use Illuminate\Support\Facades\Storage;
use App\Models\NewsFeed;
use App\Models\MasterSubCategory;

trait DoctorTransformable
{
    protected function transformDoctorInformation($information)
    {
        $responseArray = [
            'user_id' => $information->user_id,
            'doctor_id' => $information->doctor_id,
            'name_prefix' => $information->name_prefix,
            'email' => $information->email,
            'country_code' => !empty($information->country_code) ? $information->country_code : '',
            'phone_number' => !empty($information->office_phone_number) ? $information->office_phone_number : NULL,
            'ext' => !empty($information->ext) ? $information->ext : null,
            'office_fax' => !empty($information->office_fax) ? $information->office_fax : NULL,
            'medical_school' => !empty($information->medical_school) ? $information->medical_school : '',
            'graduation_year' => !empty($information->grad_year) ? $information->grad_year : NULL,
            'date_of_birth' => !empty($information->date_of_birth) ? $information->date_of_birth : '',
            'state' => !empty($information->state_id) ? $information->state_id : NULL,
            'state_name' => !empty($information->state_name) ? $information->state_name : '',
            'is_favorite' => $information->is_favorite,
            'registration_form' => !empty($information->registration_form) ? getImage($information->registration_form, $information->user_id) : '',
            'profile_information' => [
                'first_name' => $information->first_name,
                'last_name' => $information->last_name,
                'occupation_id' => $information->occupation_id,
                'occupation_name' => $information->occupation_name,
                'specialty_id' => $information->specialty_id,
                'specialty_name' => $information->speciality_name,
                'symptom' => $information->symptom,
                'procedure' => $information->procedure,
                'is_verified' => $information->is_document_verified,
                'address' => (!empty($information->address)) ? $information->address . " " . $information->zipcode : '',
                'distance' => isset($information->distance) ? round($information->distance, 3) : 'Not Found',
                'profile_image' => !empty($information->profile_image) ? getImage($information->profile_image, $information->user_id) : '',
            ],
            'settings' => [
                'in_person' => $information->in_person,
                'video_consultancy' => $information->video_consultancy,
            ],
            'overview' =>  $this->transformOverview($information->about, $information->overview),
            'location' =>  $information->location,
            'document' =>  $this->transformDocument($information->media),
            'media' => $this->transformMedia($information->media),
            'review' =>  !empty($information->latest_review) ? $this->transformReview($information->latest_review) : [],
            'avg_rating' =>  !empty($information->review) ? $this->transformAvgRating($information->review) : '',
            'total_review_count' => !empty($information->reviewCount) ? $information->reviewCount : '',
            'feed_exists' => NewsFeed::where('created_by', $information->user_id)->first() ? true : false,
        ];

        // if (!empty($information->review)) {
        //     $review = $information->review->toArray();
        //     $responseArray['review'] = [
        //         'booking_id' => $review['booking_id'] ?? '',
        //         'doctor_id' => $review['doctor_id'] ?? '',
        //         'bedside_manner' => $review['bedside_manner'] ?? '',
        //         'answered_questions' => $review['answered_questions'] ?? '',
        //         'after_care' => $review['after_care'] ?? '',
        //         'time_spent' => $review['time_spent'] ?? '',
        //         'responsiveness' => $review['responsiveness'] ?? '',
        //         'courtesy' => $review['courtesy'] ?? '',
        //         'payment_process' => $review['payment_process'] ?? '',
        //         'wait_times' => $review['wait_times'] ?? '',
        //         'review' => $review['review'] ?? '',
        //         'created_at' => !empty($review['review_created_at']) ? Carbon::parse($review['review_created_at'])->format('d M Y, h:i A') : ''
        //     ];
        //     $responseArray['avg_rating'] = $review['average_rating'] ?? 0;
        // } else {
        //     $responseArray['review'] = [];
        //     $responseArray['avg_rating'] = 0;
        // }

        return $responseArray;
    }

    public function transformOverview($about, $overviewDetail)
    {
        $overviewArray = [];
        // $overviewArray['about'] = !empty($about) ? $about : '';
        if (!empty($about)) {
            $overviewArray['about'] = $about;
        }

        if (!empty($overviewDetail)) {
            $data = [];
            foreach ($overviewDetail as $key => $value) {

                $data = ['id' => $value->id];

                if (empty($value->master_data_id)) {
                    $data['value'] = !empty($value->other) ? $value->other : '';
                } else {
                    $data['category_id'] = $value->master_data_id;
                    $data['category_name'] = $value->other_category;
                }

                if (!empty($value->other_category) && !$value->is_approved) {
                    $data['category_id'] = 0;
                    $data['other'] =  $value->other_category;
                    $data['category_name'] = $value->other_category;
                }

                $overviewArray[$value->type][] = $data;
            }
        }

        return !empty($overviewArray) ? $overviewArray : Null;
    }

    public function transformMedia($medias)
    {
        $mediaArray = [];
        foreach ($medias as $media) {
            $fileName = getImage($media->file, $media->doctor_id);
            if ($media->upload_type == 'media') {
                $data = [
                    'id' => $media->id,
                    'doctor_id' => $media->doctor_id,
                    'file' => $fileName,
                    'type' => $media->type,
                    'order_no' => $media->order_no,
                    'description' => !empty($media->description) ? $media->description : '',
                ];
                array_push($mediaArray, $data);
            }
        }
        return  $mediaArray;
    }

    public function transformDocument($medias)
    {
        $documentArray = [];
        foreach ($medias as $media) {
            $fileName = getImage($media->file, $media->doctor_id);
            $extension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            if ($media->upload_type == 'proof') {
                $data = [
                    'id' => $media->id,
                    'doctor_id' => $media->doctor_id,
                    'file' => $fileName,
                    'type' =>  $extension,
                ];
                array_push($documentArray, $data);
            }
        }
        return  $documentArray;
    }

    protected function transformDoctorsList($doctorsData, $offset = '', $limit = '', $currentPage = '', $transform = 0)
    {
        $responseArray = [];

        $totalCount = isset($doctorsData['totalRecord']) ? $doctorsData['totalRecord'] : 0;
        unset($doctorsData['totalRecord']);

        foreach ($doctorsData as $key => $doctorInfo) {
            if ($doctorInfo->user_id) {
                $responseArray['doctor_information'][] = [
                    'user_id' => $doctorInfo->user_id,
                    'doctor_id' => $doctorInfo->user_id,
                    'email' => $doctorInfo->email,
                    'country_code' => !empty($doctorInfo->country_code) ? $doctorInfo->country_code : '',
                    'phone_number' => !empty($doctorInfo->office_phone_number) ? $doctorInfo->office_phone_number : NULL,
                    'distance' => isset($doctorInfo->distance) ? round($doctorInfo->distance, 3) : 'Not Found',
                    'is_favorite' => $doctorInfo->is_favorite,
                    'first_name' => $doctorInfo->first_name,
                    'last_name' => $doctorInfo->last_name,
                    'occupation_id' => $doctorInfo->occupation_id,
                    'specialty_id' => $doctorInfo->specialty_id,
                    'occupation_name' => $doctorInfo->occupation_name,
                    'specialty_id' => $doctorInfo->specialty_id,
                    'specialty_name' => $doctorInfo->specialty_name,
                    'profile_image' => !empty($doctorInfo->profile_image) ? getImage($doctorInfo->profile_image, $doctorInfo->user_id) : '',
                    'registration_form' => !empty($doctorInfo->registration_form) ? getImage($doctorInfo->registration_form, $doctorInfo->user_id) : '',
                    'location' =>  $doctorInfo->location,
                    'address' => $doctorInfo->address . ", " . $doctorInfo->zipcode,
                    'suite' => $doctorInfo->suite,
                    'in_person' => $doctorInfo->in_person,
                    'doctor_location_id' => $doctorInfo->doctor_location_id,
                    'video_consultancy' => $doctorInfo->video_consultancy,
                    'overview' =>  $this->transformOverview($doctorInfo->about, $doctorInfo->overview),
                    'review' =>  !empty($doctorInfo->latest_review) ? $this->transformReview($doctorInfo->latest_review) : [],
                    'is_read' =>  !empty($doctorInfo->is_read) ? 0 : 1,
                    'unread_notification_count' =>  !empty($doctorInfo->is_read) ? $doctorInfo->is_read : 0,
                    'avg_rating' =>  !empty($doctorInfo->review) ? $this->transformAvgRating($doctorInfo->review) : '',
                    'total_review_count' =>  !empty($doctorInfo->review) ? count($doctorInfo->review) : '',
                ];
            } elseif ($doctorInfo->npi_number) {
                $specialtyName = $doctorInfo['specialties'][0] ?? NULL;
                unset($doctorInfo['specialties']);

                $responseArray['doctor_information'][] = [
                    'id' => $doctorInfo->id,
                    'npi_number' => $doctorInfo->npi_number ? $doctorInfo->npi_number : NULL,
                    'first_name' => $doctorInfo->first_name ? $doctorInfo->first_name : NULL,
                    'last_name' => $doctorInfo->last_name ? $doctorInfo->last_name : NULL,
                    'address' => $doctorInfo->address ? $doctorInfo->address : NULL,
                    'profile_image' => !empty($doctorInfo->profile_image) ? config('constants.get_file_path') . 'top-doctors/' . $doctorInfo->profile_image : NULL,
                    'city' => $doctorInfo->city ? $doctorInfo->city : NULL,
                    'state' => $doctorInfo->state ? $doctorInfo->state : NULL,
                    'zipcode' => $doctorInfo->zipcode ? $doctorInfo->zipcode : NULL,
                    'specialty_id' => MasterSubCategory::where('label', $doctorInfo['specialty_name'])->first()->id ?? NULL,
                    'specialty_name' => $specialtyName
                ];
            }
        }

        if ($transform != 1 && isset($responseArray['doctor_information'])) {
            $responseArray =  $responseArray['doctor_information'];
        }

        if (!empty($limit) && ($currentPage >= 0)) {
            $paginator = new Paginator($responseArray, $totalCount, $limit, $currentPage);
            return $paginator;
        }
        return $responseArray;
    }

    /**
     * transformReview
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function transformReview($doctorInfo)
    {
        if (!empty($doctorInfo)) {
            $review = $doctorInfo->toArray();
            $responseArray = [
                'booking_id' => $review['booking_id'] ?? '',
                'doctor_id' => $review['doctor_id'] ?? '',
                'bedside_manner' => $review['bedside_manner'] ?? '',
                'answered_questions' => $review['answered_questions'] ?? '',
                'after_care' => $review['after_care'] ?? '',
                'time_spent' => $review['time_spent'] ?? '',
                'responsiveness' => $review['responsiveness'] ?? '',
                'courtesy' => $review['courtesy'] ?? '',
                'payment_process' => $review['payment_process'] ?? '',
                'wait_times' => $review['wait_times'] ?? '',
                'review' => $review['review'] ?? '',
                'created_at' => !empty($review['review_created_at']) ? Carbon::parse($review['review_created_at'])->format('d M Y, h:i A') : ''
            ];
        }
        return $responseArray;
    }

    /**
     * transformAvgRating
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    public function transformAvgRating($doctorInfo)
    {
        if (!empty($doctorInfo)) {
            $reviewsData = !is_array($doctorInfo) ? $doctorInfo->toArray() : $doctorInfo;
            $reviewCount = count($reviewsData);
            $average = 0;

            if ($reviewCount > 0) {
                $ratings = array_map(function ($review) {
                    return $review['rating'];
                }, $reviewsData);
                $average = array_sum($ratings) / count($ratings);
            }

            $responseArray = round($average, 2) ?? 0;
        } else {
            $responseArray['review'] = [];
            $responseArray = 0;
        }

        return $responseArray;
    }


    /**
     * transformDoctorCompareData
     **
     * @author Heli Patel
     * @param  Request $request
     * @return JsonResponse
     */
    protected function transformDoctorCompareData($doctorsData)
    {
        foreach ($doctorsData as $key => $information) {
            $experience = now()->year - $information->grad_year;
            $responseArray[] = [
                'doctor_id' => $information->user_id,
                'name_prefix' => $information->name_prefix,
                'name' => $information->first_name . " " . $information->last_name,
                'email' => $information->email,
                'specialty_id' => $information->specialty_id,
                'specialty_name' => $information->specialty_name,
                'suffix' => !empty($information->country_code) ? $information->country_code : '',
                'profile_image' => !empty($information->profile_image) ? getImage($information->profile_image, $information->user_id) : '',
                'yume_verified' => $information->is_document_verified,
                'experience' => $experience,
                'in_person' => $information->in_person,
                'video_consultancy' => $information->video_consultancy,
                'overview' =>  $this->transformOverview($information->about, $information->overview),
                'location' =>  $information->location,
                'media' => $this->transformMedia($information->media)
            ];
        }

        return $responseArray;
    }

    /**
     * transformAppointmentList
     **
     * @author Krishna Shah
     * @param  integer $offset
     * @param  integer $limit
     * @param  integer $currentPage
     * @param  array   $appointmentData
     * @return JsonResponse
     */
    protected function transformAppointmentList($appointmentData, $offset = '', $limit = '', $currentPage = '')
    {
        $responseArray = [];
        $totalCount = !empty($appointmentData['totalRecord']) ? $appointmentData['totalRecord'] : 0;
        unset($appointmentData['totalRecord']);
        if (count($appointmentData['appointment_information']) > 0) {
            foreach ($appointmentData['appointment_information'] as $key => $appointmentInfo) {
                $consult_type = '';
                if (!empty($appointmentInfo['consultant_type'])) {
                    if ($appointmentInfo['consultant_type'] == 'video') {
                        $consult_type = "video_consultancy";
                    } elseif ($appointmentInfo['consultant_type'] == 'in person') {
                        $consult_type = "in_person";
                    }
                }
                $responseArray['appointment_information'][] = [
                    'appointment_id' => $appointmentInfo['id'] ?? 0,
                    'booking_id' => $appointmentInfo['booking_id'] ?? '',
                    'consultancy_type' => $consult_type,
                    'appointment_date' => !empty($appointmentInfo['appointment_date']) ? $appointmentInfo['appointment_date'] : '',
                    'slot_time' => !empty($appointmentInfo['appointment_date']) ? Carbon::parse($appointmentInfo['appointment_date'])->format('d M, h:i A') : '',
                    'start_time' => (!empty($appointmentInfo['start_time'])) ? Carbon::parse($appointmentInfo['start_time'])->format('h:i A') : '',
                    'end_time' => (!empty($appointmentInfo['end_time'])) ? Carbon::parse($appointmentInfo['end_time'])->format('h:i A') : '',
                    'reason' => $appointmentInfo['reason'] ?? '',
                    'status' => $appointmentInfo['status'] ?? '',
                    'booked_on' =>  !empty($appointmentInfo['appointment_created_on']) ? Carbon::parse($appointmentInfo['appointment_created_on'])->format('d M, h:i A') : '',
                    'registration_form' => !empty($appointmentInfo['registration_form']) ? getImage($appointmentInfo['registration_form'], $appointmentInfo['doctor_id']) : '',
                    'consultation_url' => $appointmentInfo['consultation_url'] ?? '',
                    'location' => $appointmentInfo['location_address'] ?? '',
                    'joint_by_patient' => $appointmentInfo['joint_by_patient'] ?? 0,
                    'joint_by_doctor' => $appointmentInfo['joint_by_doctor'] ?? 0,
                    'declined_reason' => $appointmentInfo['declined_reason'] ?? '',
                    'location_id' => $appointmentInfo['location_id'] ?? null,
                    'appointment_reschedules_id' => $appointmentInfo['appointment_reschedules_id'] ?? 0,
                    // 'new_slot_time' => $appointmentInfo['new_slot_time'] ?? '',
                    // 'appointment_reschedules_location_id' => $appointmentInfo['appointment_reschedules_location_id'] ?? '',
                    // 'appointment_reschedules_consultant_type' => $appointmentInfo['appointment_reschedules_consultant_type'] ?? '',
                    // 'appointment_reschedules_requested_by' => $appointmentInfo['appointment_reschedules_requested_by'] ?? '',
                ];

                $responseArray['appointment_information'][$key]['reschedule_appointment_by_patient'] = $appointmentInfo['reschedule_appointment_by_patient'];
                $responseArray['appointment_information'][$key]['reschedule_appointment_by_doctor'] = $appointmentInfo['reschedule_appointment_by_doctor'];

                if (!empty($appointmentInfo['reschedule_appointment_by_patient']) && !empty($appointmentInfo['reschedule_appointment_by_doctor'])) {

                    if ($appointmentInfo['reschedule_appointment_by_patient']['status'] == 'pending' && $appointmentInfo['reschedule_appointment_by_patient']['status'] != 'rejected' || $appointmentInfo['reschedule_appointment_by_doctor']['status'] == 'rejected') {
                        $responseArray['appointment_information'][$key]['reschedule_appointment_by_doctor'] = null;
                        $responseArray['appointment_information'][$key]['reschedule_appointment_by_patient'] = $appointmentInfo['reschedule_appointment_by_patient'];
                    }

                    if ($appointmentInfo['reschedule_appointment_by_doctor']['status'] == 'pending' && $appointmentInfo['reschedule_appointment_by_doctor']['status'] != 'rejected' || $appointmentInfo['reschedule_appointment_by_patient']['status'] == 'rejected') {
                        $responseArray['appointment_information'][$key]['reschedule_appointment_by_patient'] = null;
                        $responseArray['appointment_information'][$key]['reschedule_appointment_by_doctor'] = $appointmentInfo['reschedule_appointment_by_doctor'];
                    }

                    if ($appointmentInfo['reschedule_appointment_by_patient']['status'] == 'rejected' && $appointmentInfo['reschedule_appointment_by_doctor']['status'] == 'rejected') {
                        $responseArray['appointment_information'][$key]['reschedule_appointment_by_doctor'] = null;
                        $responseArray['appointment_information'][$key]['reschedule_appointment_by_patient'] = null;
                    }
                }

                // $responseArray['appointment_information'][$key]['reschedule_appointment_by_patient'] = $appointmentInfo['reschedule_appointment_by_patient'] ?? '';
                // $responseArray['appointment_information'][$key]['reschedule_appointment_by_doctor'] = $appointmentInfo['reschedule_appointment_by_doctor'] ?? '';

                $responseArray['appointment_information'][$key]['patient'] = [
                    'patient_id' => $appointmentInfo['patient_id'] ?? '',
                    'full_name' => $appointmentInfo['patient_name'] ?? '',
                    'phone_number' => $appointmentInfo['patient_phone_number'] ?? '',
                    'age' => $appointmentInfo['patient_age'] ?? '',
                    'address' => $appointmentInfo['patient_address'] ?? '',
                    'city' => $appointmentInfo['patient_city_name'] ?? '',
                    'state' => $appointmentInfo['patient_state_name'] ?? '',
                    'country' => $appointmentInfo['patient_country_name'] ?? '',
                    'profile_image' => !empty($appointmentInfo['patient_profile_image']) ? getImage($appointmentInfo['patient_profile_image'], $appointmentInfo['patient_id']) : ''
                ];

                if (!empty($appointmentInfo['review'])) {
                    $review = $appointmentInfo['review'];
                    $getAllReview = $appointmentInfo['get_all_review'];
                    $responseArray['appointment_information'][$key]['review'] = [
                        'booking_id' => $review['booking_id'] ?? '',
                        'doctor_id' => $review['doctor_id'] ?? '',
                        'bedside_manner' => $review['bedside_manner'] ?? '',
                        'answered_questions' => $review['answered_questions'] ?? '',
                        'after_care' => $review['after_care'] ?? '',
                        'time_spent' => $review['time_spent'] ?? '',
                        'responsiveness' => $review['responsiveness'] ?? '',
                        'courtesy' => $review['courtesy'] ?? '',
                        'payment_process' => $review['payment_process'] ?? '',
                        'wait_times' => $review['wait_times'] ?? '',
                        'review' => $review['review'] ?? '',
                        'created_at' => !empty($review['review_created_at']) ? Carbon::parse($review['review_created_at'])->format('d M Y, h:i A') : ''
                    ];
                    $responseArray['appointment_information'][$key]['avg_rating'] = !empty($getAllReview) ? $this->transformAvgRating($getAllReview) : '';
                } else {
                    $responseArray['appointment_information'][$key]['review'] = [];
                    $responseArray['appointment_information'][$key]['avg_rating'] = 0;
                }
            }
        } else {
            $responseArray['appointment_information'] = [];
        }

        $responseArray['newCount'] = $appointmentData['statusCount']['newCount'] ?? 0;
        $responseArray['todayCount'] = $appointmentData['statusCount']['todayCount'] ?? 0;
        $responseArray['upcomingCount'] = $appointmentData['statusCount']['upcomingCount'] ?? 0;
        $responseArray['cancelledCount'] = $appointmentData['statusCount']['cancelledCount'] ?? 0;
        $responseArray['completedCount'] = $appointmentData['statusCount']['completedCount'] ?? 0;
        $responseArray['rescheduleCount'] = $appointmentData['statusCount']['rescheduleCount'] ?? 0;
        $responseArray['waitlistCount'] = $appointmentData['statusCount']['waitlistCount'] ?? 0;
        $responseArray['offlineCount'] = $appointmentData['statusCount']['offlineCount'] ?? 0;
        $responseArray['is_read'] = !empty($appointmentData['is_read']) ? 0 : 1;
        $responseArray['unread_notification_count'] = !empty($appointmentData['is_read']) ? $appointmentData['is_read'] : 0;

        if (!empty($limit) && ($currentPage >= 0)) {
            $paginator = new Paginator($responseArray, $totalCount, $limit, $currentPage);
            return $paginator;
        }
        return $responseArray;
    }

    /**
     * transformAppointmentDetails
     **
     * @author Krishna Shah
     * @param  $appointmentInfo
     * @return JsonResponse
     */
    protected function transformAppointmentDetails($appointmentInfo)
    {
        $responseArray = [];
        $chatRequestStatus = isset($appointmentInfo->chat_request_status) ? $appointmentInfo->chat_request_status : 2;

        if (!empty($appointmentInfo)) {
            $consult_type = '';
            if (!empty($appointmentInfo->consultant_type)) {
                if ($appointmentInfo->consultant_type == 'video') {
                    $consult_type = "video_consultancy";
                } elseif ($appointmentInfo->consultant_type) {
                    $consult_type = "in_person";
                }
            }
            $responseArray = [
                'appointment_id' => $appointmentInfo->id,
                'booking_id' => $appointmentInfo->booking_id ?? '',
                'doctor_id' => $appointmentInfo->doctor_id ?? '',
                'name_prefix' => $appointmentInfo->name_prefix,
                'consultancy_type' => $consult_type,
                'slot_date' => !empty($appointmentInfo->appointment_date) ? $appointmentInfo->appointment_date : '',
                'slot_time' => !empty($appointmentInfo->appointment_date) ? Carbon::parse($appointmentInfo->appointment_date)->format('d M, h:i A') : '',
                'start_time' => (!empty($appointmentInfo['start_time'])) ? Carbon::parse($appointmentInfo['start_time'])->format('h:i A') : '',
                'end_time' => (!empty($appointmentInfo['end_time'])) ? Carbon::parse($appointmentInfo['end_time'])->format('h:i A') : '',
                // 'new_slot_time' => (!empty($appointmentInfo->new_slot_time)) ? Carbon::parse($appointmentInfo->new_slot_time)->format('d M, h:m A') : '',
                'reason' => $appointmentInfo->reason ?? '',
                'status' => $appointmentInfo->status ?? '',
                'booked_on' =>  Carbon::createFromFormat('Y-m-d H:i:s', $appointmentInfo->appointment_created_on)->format('d M, h:i A'),
                'registration_form' => !empty($appointmentInfo['registration_form']) ? getImage($appointmentInfo['registration_form'], $appointmentInfo->patient_id) : '',
                'registration_form_name' => !empty($appointmentInfo['registration_form']) ? $appointmentInfo['registration_form'] : '',
                'registration_form_extension' => !empty($appointmentInfo['registration_form']) ? strtolower(pathinfo($appointmentInfo['registration_form'], PATHINFO_EXTENSION)) : '',
                'consultation_url' => $appointmentInfo->consultation_url ?? '',
                'location' => $appointmentInfo->location_address ?? '',
                'dr_name' => $appointmentInfo->first_name . " " . $appointmentInfo->last_name,
                'dr_profile_image' => !empty($appointmentInfo->profile_image) ? getImage($appointmentInfo->profile_image, $appointmentInfo->doctor_id) : '',
                'dr_specialty_type' => $appointmentInfo->specialty_name ?? '',
                'specialty_id' => $appointmentInfo->specialty_id ?? '',
                'joint_by_patient' => $appointmentInfo->joint_by_patient ?? 0,
                'joint_by_doctor' => $appointmentInfo->joint_by_doctor ?? 0,
                'declined_reason' => $appointmentInfo->declined_reason ?? '',
                'location_id' => $appointmentInfo->location_id ?? null,
                'additional_notes' => $appointmentInfo->additional_note ?? '',
                'chat_request_status' => $chatRequestStatus,
            ];

            $responseArray['reschedule_appointment_by_patient'] = $appointmentInfo->rescheduleAppointmentByPatient;
            $responseArray['reschedule_appointment_by_doctor'] = $appointmentInfo->rescheduleAppointmentByDoctor;

            if (!empty($appointmentInfo->rescheduleAppointmentByPatient) && !empty($appointmentInfo->rescheduleAppointmentByDoctor)) {

                if ($appointmentInfo->rescheduleAppointmentByPatient->status == 'pending' && $appointmentInfo->rescheduleAppointmentByPatient->status != 'rejected' || $appointmentInfo->rescheduleAppointmentByDoctor->status == 'rejected') {
                    $responseArray['reschedule_appointment_by_doctor'] = null;
                    $responseArray['reschedule_appointment_by_patient'] = $appointmentInfo->rescheduleAppointmentByPatient;
                }

                if ($appointmentInfo->rescheduleAppointmentByDoctor->status == 'pending' && $appointmentInfo->rescheduleAppointmentByDoctor->status != 'rejected' || $appointmentInfo->rescheduleAppointmentByPatient->status == 'rejected') {
                    $responseArray['reschedule_appointment_by_patient'] = null;
                    $responseArray['reschedule_appointment_by_doctor'] = $appointmentInfo->rescheduleAppointmentByDoctor;
                }

                if ($appointmentInfo->rescheduleAppointmentByPatient->status == 'rejected' && $appointmentInfo->rescheduleAppointmentByDoctor->status == 'rejected') {
                    $responseArray['reschedule_appointment_by_patient'] = null;
                    $responseArray['reschedule_appointment_by_doctor'] = null;
                }
            }

            $responseArray['patient'] = [
                'patient_id' => $appointmentInfo->patient_id ?? '',
                'full_name' => $appointmentInfo->patient_name ?? '',
                'phone_number' => $appointmentInfo->patient_phone_number ?? null,
                'age' => $appointmentInfo->patient_age ?? '',
                'address' => $appointmentInfo->patient_address ?? '',
                'city' => $appointmentInfo->patient_city_name ?? '',
                'state' => $appointmentInfo->patient_state_name ?? '',
                'country' => $appointmentInfo->patient_country_name ?? '',
                'profile_image' => !empty($appointmentInfo->profile_image) ? getImage($appointmentInfo->patient_profile_image, $appointmentInfo->patient_id) : ''
            ];
            if (!empty($appointmentInfo->review)) {
                $review = $appointmentInfo->review->toArray();
                $getAllReview = $appointmentInfo['getAllReview'];
                $rating = $review['rating'];

                $responseArray['review'] = [
                    "patient_name" => $review['fullname'] ?? '',
                    "user_name" => $review['username'] ?? '',
                    "profile_image" => $review['profile_image'] ? getImage($review['profile_image'], $review['patient_id']) : '',
                    'booking_id' => $review['booking_id'] ?? '',
                    'doctor_id' => $review['doctor_id'] ?? '',
                    'bedside_manner' => $review['bedside_manner'] ?? '',
                    'answered_questions' => $review['answered_questions'] ?? '',
                    'after_care' => $review['after_care'] ?? '',
                    'time_spent' => $review['time_spent'] ?? '',
                    'responsiveness' => $review['responsiveness'] ?? '',
                    'courtesy' => $review['courtesy'] ?? '',
                    'payment_process' => $review['payment_process'] ?? '',
                    'wait_times' => $review['wait_times'] ?? '',
                    "rating" => $rating ?? '',
                    'review' => $review['review'] ?? '',
                    'created_at' => !empty($review['review_created_at']) ? Carbon::parse($review['review_created_at'])->format('d M Y, h:i A') : ''
                ];
                $responseArray['avg_rating'] = !empty($getAllReview) ? $this->transformAvgRating($getAllReview) : '';
            } else {
                $responseArray['review'] = [];
                $responseArray['avg_rating'] = 0;
            }
        }
        return $responseArray;
    }

    /**
     * transformAppointmentDetails
     **
     * @author Heli Patel
     * @param  array $locationData
     * @return array
     */
    public function transformDoctorLocation($locationData): array
    {
        $locationArray = [];
        foreach ($locationData as $location) {
            $data = [
                'id' => $location->id ?? null,
                'city' => $location->city_name ?? null,
                'state' => $location->state_name ?? null,
                'address' => $location->address ?? null,
                'zipcode' => $location->zipcode ?? null,
                'latitude' => $location->latitude ?? null,
                'longitude' => $location->longitude ?? null,
                'appointment_duration' => $location->appointment_duration ?? null,
                'office_phone_number' => $location->office_phone_number ?? null,
                'office_fax' => $location->office_fax ?? null,
                'ext' => $location->ext ?? null,
            ];
            array_push($locationArray, $data);
        }
        return  $locationArray;
    }

    /**
     * transformDoctorWorkTiming
     *
     * @author Heli Patel
     * @param  array $doctorTiming
     * @return array
     */
    protected function transformDoctorWorkTiming($doctorTiming): array
    {
        $responseArray = [];
        $workTimeArray = [];
        $daysArray = [];
        $bookSlotArray = [];
        $data = [];
        $responseArray['work_timing'] = [];
        $responseArray['block_slot'] = [];
        $responseArray['user_id'] = !empty($doctorTiming['doctor_id']) ? $doctorTiming['doctor_id'] : Null;
        $responseArray['location_id'] = !empty($doctorTiming['location_id']) ? $doctorTiming['location_id'] : Null;
        $responseArray['registration_form'] = !empty($doctorTiming['registration_form']) ? getImage($doctorTiming['registration_form'], $doctorTiming['doctor_id']) : Null;
        $days = config('constants.days');

        if (!empty($doctorTiming['work_timing_data'])) {
            $data = $this->getTimeSlot($doctorTiming['work_timing_data']);
        }

        $daysArray = $data['days_array'];
        unset($data['days_array']);

        $remainingDays = array_diff($days, $daysArray);

        if (!empty($remainingDays)) {
            foreach ($remainingDays as $daysData) {
                $data[$daysData] = [];
            }
        }
        $responseArray['work_timing'] = $data;

        foreach ($doctorTiming['block_slot'] as $blockSlot) {
            $blockSlot = [
                'date' => !empty($blockSlot->date) ? date('d-m-Y', strtotime($blockSlot->date)) : Null,
                'start_time' => !empty($blockSlot->start) ? date('H:i A', strtotime($blockSlot->start)) : Null,
                'end_time' => !empty($blockSlot->end) ? date('H:i A', strtotime($blockSlot->end)) : Null,
            ];
            array_push($responseArray['block_slot'], $blockSlot);
        }

        foreach ($doctorTiming['book_slot'] as $book_slot) {
            $bookSlotArray = [
                'date' => !empty($book_slot->appointment_date) ? date('d-m-Y', strtotime($book_slot->appointment_date)) : Null,
                'start_time' => !empty($book_slot->start_time) ? date('H:i A', strtotime($book_slot->start_time)) : Null,
                'end_time' => !empty($book_slot->end_time) ? date('H:i A', strtotime($book_slot->end_time)) : Null,
                'is_appointment' => true,
                'status' => 0
            ];

            if ($book_slot->status != config('constants.appointment_status.1') && !empty($book_slot->status))
                $bookSlotArray['status'] = 1;

            // Check if reschedule appointment exists
            // if (!empty($book_slot->rescheduleAppointmentByDoctor) && $book_slot->rescheduleAppointmentByDoctor->status == 'accepted') {
            //     $bookSlotArray = [
            //         'date' => !empty($book_slot->rescheduleAppointmentByDoctor->appointment_date) ? date('d-m-Y', strtotime($book_slot->rescheduleAppointmentByDoctor->appointment_date)) : Null,
            //         'start_time' => !empty($book_slot->rescheduleAppointmentByDoctor->start_time) ? date('H:i A', strtotime($book_slot->rescheduleAppointmentByDoctor->start_time)) : Null,
            //         'end_time' => !empty($book_slot->rescheduleAppointmentByDoctor->end_time) ? date('H:i A', strtotime($book_slot->rescheduleAppointmentByDoctor->end_time)) : Null,
            //         'is_appointment' => true,
            //         'status' => 0
            //     ];

            //     if ($book_slot->status != config('constants.appointment_status.1') && !empty($book_slot->status))
            //         $bookSlotArray['status'] = 1;
            // }

            // if (!empty($book_slot->rescheduleAppointmentByPatient) && $book_slot->rescheduleAppointmentByPatient->status == 'accepted') {
            //     $bookSlotArray = [
            //         'date' => !empty($book_slot->rescheduleAppointmentByPatient->appointment_date) ? date('d-m-Y', strtotime($book_slot->rescheduleAppointmentByPatient->appointment_date)) : Null,
            //         'start_time' => !empty($book_slot->rescheduleAppointmentByPatient->start_time) ? date('H:i A', strtotime($book_slot->rescheduleAppointmentByPatient->start_time)) : Null,
            //         'end_time' => !empty($book_slot->rescheduleAppointmentByPatient->end_time) ? date('H:i A', strtotime($book_slot->rescheduleAppointmentByPatient->end_time)) : Null,
            //         'is_appointment' => true,
            //         'status' => 0
            //     ];

            //     if ($book_slot->status != config('constants.appointment_status.1') && !empty($book_slot->status))
            //         $bookSlotArray['status'] = 1;
            // }

            array_push($responseArray['block_slot'], $bookSlotArray);
        }

        $responseArray['settings'] = [
            'video_consultancy' => $doctorTiming['video_consultancy'],
            'in_person' => $doctorTiming['in_person'],
        ];

        foreach ($responseArray['work_timing'] as $day => $days) {
            if (!empty($days)) {
                $input = $responseArray['work_timing'][$day];
                $temp  = [];
                $keys  = [];

                foreach ($input as $key => $data) {
                    unset($data['working_time_id']);
                    if (!in_array($data, $temp)) {
                        $temp[]     = $data;
                        $keys[$key] = true;
                    }
                }

                $responseArray['work_timing'][$day] = array_values(array_intersect_key($input, $keys));
            }
        }
        return $responseArray;
    }

    /**
     * transformWorkingTimeSlots
     *
     * @author Heli Patel
     * @param  string $interval, string $start_time,string $end_time,Object $workTimingData
     * @return array
     */
    function getTimeSlot(Object $workTimingDataArray): array
    {
        $daysArray = [];
        $start_time = Null;
        $end_time = Null;
        $data = [];

        foreach ($workTimingDataArray as $day => $workTimingData) {
            $workTimeArray = [];
            foreach ($workTimingData as $key => $workTiming) {

                $appointmentDuration = !empty($workTiming->appointment_duration) ? $workTiming->appointment_duration : Null;
                $start_time = !empty($workTiming->start_time) ? Carbon::parse($workTiming->start_time)->format('H:i') : Null;
                $end_time = !empty($workTiming->end_time) ? Carbon::parse($workTiming->end_time)->format('H:i') : Null;

                $start = new DateTime($start_time);
                $end = new DateTime($end_time);
                $startTime = $start->format('H:i');
                $endTime = $end->format('H:i');
                $time = [];
                $i = 0;

                while (strtotime($startTime) <= strtotime($endTime)) {
                    $start = $startTime;
                    $end = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                    $startTime = date('H:i', strtotime('+' . $appointmentDuration . ' minutes', strtotime($startTime)));
                    $i++;

                    if (strtotime($startTime) <= strtotime($endTime)) {
                        $time[$i] = [
                            'working_time_id' => $workTiming->id,
                            'start_time' => $start,
                            'start_period' => !empty($start) ? date('A', strtotime($start)) : Null,
                            'end_time' => $end,
                            'end_period' => !empty($end) ? date('A', strtotime($end)) : Null,
                        ];
                    }
                }
                $workTimeArray[$key] = array_values($time);
                $merged = call_user_func_array('array_merge', $workTimeArray);
            }
            array_push($daysArray, getFullDay($day));
            $data[getFullDay($day)] = array_values($merged);
        }

        $data['days_array'] = $daysArray;
        return $data;
    }

    /**
     * transformAppointmentList
     **
     * @author Krishna Shah
     * @param  integer $offset
     * @param  integer $limit
     * @param  integer $currentPage
     * @param  array   $appointmentData
     * @return JsonResponse
     */
    protected function transformFeedList($feedData, $offset = '', $limit = '', $currentPage = '')
    {
        $responseArray = [];
        $totalCount = !empty($feedData['totalRecord']) ? $feedData['totalRecord'] : 0;
        unset($feedData['totalRecord']);



        if (count($feedData['feed_information']) > 0) {
            foreach ($feedData['feed_information'] as $key => $feedInfo) {

                $fileName = !empty($feedInfo['file']) ? getImage($feedInfo['file'], $feedInfo['id']) : '';
                // Allowed files -- jpeg,png,jpg,gif,svg,mp4,mov,ogg,qt
                $extension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
                $fileType = '';

                if (in_array($extension, ['jpeg', 'png', 'jpg', 'svg'])) {
                    $fileType = 'image';
                } else {
                    $fileType = 'video';
                }
                $responseArray['feed_information'][] = [
                    'id' => $feedInfo['id'] ?? '',
                    'doctor_id' => $feedInfo['user_id'] ?? '',
                    'patient_id' => $feedInfo['user_id'] ?? '',
                    'title' => $feedInfo['title'] ?? '',
                    'description' => (string) $feedInfo['description'] ?? '',
                    'status' => $feedInfo['status'] ?? '',
                    'file' => !empty($feedInfo['file']) ? getImage($feedInfo['file'], $feedInfo['created_by']) : '',
                    'poster_image' => !empty($feedInfo->poster_image) ? getImage($feedInfo->poster_image, $feedInfo['created_by']) : '',
                    'file_type' => !empty($feedInfo['file']) ? $fileType : '',
                    'profile_image' => !empty($feedInfo['profile_image']) ? getImage($feedInfo['profile_image'], $feedInfo['user_id']) : '',
                    'first_name' => $feedInfo['first_name'] ?? '',
                    'last_name' => $feedInfo['last_name'] ?? '',
                    'created_at' => !empty($feedInfo['created_at']) ? $feedInfo['created_at'] : '',
                    'like_count' => $feedInfo['likecount'] ?? 0,
                    'comment_count' => $feedInfo['commentcount'] ?? 0,
                    'is_like' => $feedInfo->isAuthUserLikedPost(),
                    'is_follow' => $feedInfo->isAuthUserFollowedPost(),
                ];
            }
        } else {
            $responseArray['feed_information'] = [];
        }
        if (!empty($limit) && ($currentPage >= 0)) {
            $paginator = new Paginator($responseArray, $totalCount, $limit, $currentPage);
            return $paginator;
        }
        return $responseArray;
    }

    protected function transformFeedDetails($feedInfo)
    {
        $responseArray = [];
        if (!empty($feedInfo)) {

            $fileName = !empty($feedInfo['file']) ? getImage($feedInfo['file'], $feedInfo['id']) : '';
            // Allowed files -- jpeg,png,jpg,gif,svg,mp4,mov,ogg,qt
            $extension = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            $fileType = '';

            if (in_array($extension, ['jpeg', 'png', 'jpg', 'svg'])) {
                $fileType = 'image';
            } else {
                $fileType = 'video';
            }

            $responseArray = [
                'id' => $feedInfo->id ?? '',
                'doctor_id' => $feedInfo->user_id ?? '',
                'title' => $feedInfo->title ?? '',
                'description' => $feedInfo->description ?? '',
                'status' => $feedInfo->status ?? '',
                'file' => !empty($feedInfo->file) ? getImage($feedInfo->file, $feedInfo['created_by']) : '',
                'poster_image' => !empty($feedInfo->poster_image) ? getImage($feedInfo->poster_image, $feedInfo->user_id) : '',
                'file_type' => !empty($feedInfo['file']) ? $fileType : '',
                'profile_image' => !empty($feedInfo['profile_image']) ? getImage($feedInfo['profile_image'], $feedInfo['created_by']) : '',
                'first_name' => $feedInfo['first_name'] ?? '',
                'last_name' => $feedInfo['last_name'] ?? '',
                'created_at' => !empty($feedInfo['created_at']) ? $feedInfo['created_at'] : '',
                'like_count' => $feedInfo['likecount'] ?? 0,
                'comment_count' => $feedInfo['commentcount'] ?? 0,
                'is_like' => $feedInfo->isAuthUserLikedPost(),
                'is_follow' => $feedInfo->isAuthUserFollowedPost(),
            ];
            if (!$feedInfo->comments->isEmpty()) {
                $comments = $feedInfo->comments->toArray();
                foreach ($comments as $key => $comment) {
                    $responseArray['comments'][$key] = [
                        'id' => $comment['comment_id'] ?? '',
                        'description' => $comment['description'] ?? '',
                        'created_at' => !empty($comment['comment_created_at']) ? (new Carbon($comment['comment_created_at']))->diffForHumans() : ''
                    ];
                    if (!empty($comment['role_id'])) {
                        if ($comment['role_id'] == 2) {
                            $responseArray['comments'][$key]['full_name'] = $comment['dr_first_name'] . " " . $comment['dr_last_name'];
                            $responseArray['comments'][$key]['profile_image'] = !empty($comment['dr_profile_image']) ? getImage($comment['dr_profile_image'], $comment['comment_by']) : '';
                        } elseif ($comment['role_id'] == 3) {
                            $responseArray['comments'][$key]['full_name'] = $comment['patient_full_name'] ?? '';
                            $responseArray['comments'][$key]['profile_image'] = !empty($comment['patient_profile_image']) ? getImage($comment['patient_profile_image'], $comment['comment_by']) : '';
                        }
                    }
                }
            } else {
                $responseArray['comments'] = [];
            }
        }
        return $responseArray;
    }
    /**
     * transformReviewList
     *
     * @param  array $reviewData
     * @param  string $offset
     * @param  string $limit
     * @param  string $currentPage
     * @return array
     */
    public function transformReviewList(array $reviewData, string $offset = '', string $limit = '', string $currentPage)
    {
        $responseArray = [];
        $responseArray['ratings_and_reviews'] = [];
        $responseArray['over_all_rating'] = [];
        $totalCount = $reviewData['totaldata'] ? $reviewData['totaldata'] : 0;
        $excellentReviewCount = 0;
        $very_good_review_count = 0;
        $average_review_count = 0;
        $poor_review_count = 0;
        $terrible = 0;
        unset($reviewData['totaldata']);
        if (!empty($reviewData)) {
            foreach ($reviewData['reviewInformation'] as $data) {
                $rating = $data['rating'];
                $temp = [
                    "patient_name" => $data['fullname'] ?? '',
                    "user_name" => $data['username'] ?? '',
                    "profile_image" => $data['profile_image'] ? getImage($data['profile_image'], $data['patient_id']) : '',
                    "booking_id" => $data['booking_id'] ?? '',
                    "doctor_id" => $data['doctor_id'] ?? '',
                    "bedside_manner" => $data['bedside_manner'] ?? '',
                    "answered_questions" => $data['answered_questions'] ?? '',
                    "after_care" => $data['after_care'] ?? '',
                    "time_spent" => $data['time_spent'] ?? '',
                    "responsiveness" => $data['responsiveness'] ?? '',
                    "courtesy" => $data['courtesy'] ?? '',
                    "payment_process" => $data['payment_process'] ?? '',
                    "wait_times" => $data['wait_times'] ?? '',
                    "rating" => $rating ?? '',
                    "review" => $data['review'] ?? '',
                    "created_at" => date('Y-m-d H:i:s', strtotime($data['created_at'])) ?? ''
                ];

                array_push($responseArray['ratings_and_reviews'], $temp);
            }

            foreach ($reviewData['allReviews'] as $allreviewData) {
                $rating = $allreviewData['rating'];

                $excellentReviewCount = $rating > 4.1 && $rating <= 5 ? $excellentReviewCount + 1 : $excellentReviewCount;
                $very_good_review_count = $rating > 3.1 && $rating <= 4 ? $very_good_review_count + 1 : $very_good_review_count;
                $average_review_count = $rating > 2.1 && $rating <= 3 ? $average_review_count + 1 : $average_review_count;
                $poor_review_count = $rating > 1.1 && $rating <= 2 ? $poor_review_count + 1 : $poor_review_count;
                $terrible = $rating > 0.1 && $rating <= 1 ? $terrible + 1 : $terrible;
            }

            $responseArray['over_all_rating']['total_review_count'] = count($reviewData['allReviews']);
            $responseArray['over_all_rating']['avg_rating'] = round($reviewData['rating'], 2);
            $responseArray['over_all_rating']['excellentReviewCount'] = $excellentReviewCount;
            $responseArray['over_all_rating']['very_good_review_count'] = $very_good_review_count;
            $responseArray['over_all_rating']['average_review_count'] = $average_review_count;
            $responseArray['over_all_rating']['poor_review_count'] = $poor_review_count;
            $responseArray['over_all_rating']['terrible'] = $terrible;
        }

        if (!empty($limit) && ($currentPage >= 0)) {
            $paginator = new Paginator($responseArray, $totalCount, $limit, $currentPage);
            return $paginator;
        }

        return $responseArray;
    }
}
