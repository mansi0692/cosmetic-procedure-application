<?php

return [
    'confirmation_pending' => 'ожидается подтверждение',
    'confirmed' => 'подтвержденный',
    'completed' => 'завершенный',
    'cancelled' => 'отменен',
    'reschedule_requested' => 'запрошен перенос',
    'waiting' => 'ожидающий',
    'no_appointment_schedule' => 'нет расписания встреч',
    'accepted' => 'принято',
    'rejected' => 'отклоненный',
    'expired' => 'истекший',
    'pending' => 'в ожидании',
    'cancelled_as_booked_by_another_doctor' => 'отменен по назначению другого врача'
];