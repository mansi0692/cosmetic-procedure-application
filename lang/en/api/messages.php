<?php

return [
    'success' => [
        'MESSAGE_NO_DATA_FOUND' => 'No data found',
        'MESSAGE_CITY_LISTING' => 'City listed successfully',
        'MESSAGE_NO_CITY_FOUND' => 'City not found',
        'MESSAGE_COUNTRY_LISTING' => 'Country listed successfully',
        'MESSAGE_NO_COUNTRY_FOUND' => 'Country not found',
        'LOGIN_SUCCESS' => 'Login Successful.',
        'REGISTRATION_SUCCESS' => ':user_type registered successfully.',
        'OTP_SENT' => 'OTP has been sent successfully.',
        'OTP_VERIFIED' => 'OTP verified successfully.',
        'PASSWORD_RESET' => 'Password has been reset successfully.',
        'LOGOUT_SUCCESS' => 'User successfully signed out',
        'MESSAGE_DOCTOR_LISTING' => 'Doctor listed successfully',
        'MESSAGE_APPOINTMENT_LISTING' => 'Appointment listed successfully',
        'MESSAGE_FEED_LISTING' => 'Feed listed successfully',
        'MESSAGE_NO_DOCTOR_FOUND' => 'Doctor not found',
        'MESSAGE_STATE_LISTING' => 'State listed successfully',
        'MESSAGE_NO_STATE_FOUND' => 'State not found',
        'MESSAGE_DOCTOR_PROFILE_FOUND' => 'Doctor profile found successfully',
        'MESSAGE_NO_DOCTOR_PROFILE_FOUND' => 'Doctor profile not found',
        'MESSAGE_DOCTOR_REGISTERED_SUCCESSFULLY' => 'Doctor registered successfully',
        'MESSAGE_OCCUPATION_LISTING' => 'Occupation listed successfully',
        'MESSAGE_NO_OCCUPATION_FOUND' => 'Occupation not found',
        'SAVE_PROFILE_SUCCESS' => 'Profile has been saved successfully',
        'SAVE_OVERVIEW_SUCCESS' => 'Overview has been saved successfully',
        'MESSAGE_DOCTOR_PROFILE_SETTINGS_UPDATED_SUCCESSFULLY' => 'Doctor profile settings updated successfully',
        'MESSAGE_DOCTOR_LOCATION_DATA_SAVE' => 'Doctor location saved successfully',
        'MESSAGE_DOCTOR_LOCATION_DATA_UPDATE' => 'Doctor location updated successfully',
        'MESSAGE_DATA_FOUND' => 'Data found successfully',
        'MESSAGE_PROFILE_INFORMATION' => 'Profile Information fetched successfully.',
        'MESSAGE_OCCUPATION_LISTING' => 'Occupation listed successfully',
        'MESSAGE_NO_OCCUPATION_FOUND' => 'Occupation not found',
        'MESSAGE_CATEGORY_LISTING' => 'Categories listed successfully.',
        'MESSAGE_NO_CATEGORY_FOUND' => 'Categories are not available for this type.',
        'PROFILE_DATA_DELETE_SUCCESS' => 'Record has been deleted successfully.',
        'MESSAGE_SPECIALITY_FOUND' => 'Specialty listed successfully',
        'MESSAGE_SPECIALITY_NOT_FOUND' => 'Specialty not found',
        'PASSWORD_CHANGED' => 'Password has been changed successfully.',
        'MESSAGE_DOCTOR_MEDIA_UPLOADED_SUCCESSFULLY' => 'Doctor media uploaded successfully',
        'MESSAGE_DOCTOR_MEDIA_UPDATED_SUCCESSFULLY' => 'Doctor media updated successfully',
        'MESSAGE_DOCTOR_PROOF_UPLOADED_SUCCESSFULLY' => 'Doctor proof uploaded successfully',
        'MESSAGE_MAIL_SENT_SUCCESSFULLY' => 'Admin has been notified about it. We will get back to you as soon as possible.',
        'MESSAGE_PATIENT_ACCOUNT_CANCELLED' => 'Admin has been notified about cancellation. Once admin approves it, your account will be suspended permanently.',
        'MESSAGE_PATIENT_UPDATE' => 'Patient profile updated successfully',
        'MESSAGE_PATIENT_GET_PROFILE' => 'Patient profile found successfully',
        'MESSAGE_CONTACT_US_PATIENT' => 'Admin has been notified about it. We will get back to you as soon as possible',
        'MESSAGE_CMS_CONTENT' => 'CMS content found successfully',
        'MESSAGE_SUCCESSFULLY_RECEIVED_YOU_JOIN_REQUEST' => 'Successfully received your join waitlist request',
        'MESSAGE_CALENDAR_BLOCK_SLOT' => 'Slot updated successfully',
        'MESSAGE_DOCTOR_WORKING_TIME_UPDATED' => 'Working time updated successfully',
        'MESSAGE_DOCTOR_WORKING_TIME_FETCHED' => 'Working time fetched successfully',
        'MESSAGE_DOCTOR_WORKING_TIME_DELETED' => 'Working time deleted successfully',
        'MESSAGE_DOCTOR_APPOINTMENT_FETCHED' => 'Doctor appointment fetched successfully',
        'MESSAGE_SUCCESSFULLY_RECEIVED_YOU_APPOINMENT_REQUEST' => 'Successfully received your appointment request',
        'MESSAGE_ADD_FAVORITE_GROUP'   => 'Doctor favorite group created successfully',
        'MESSAGE_UPDATE_FAVORITE_GROUP' => 'Doctor favorite group updated successfully',
        'MESSAGE_DELETE_FAVORITE_GROUP' => 'Doctor favorite group deleted successfully',
        'MESSAGE_FAVORITE_GROUP_LIST' => 'Doctor favorite group fetched successfully',
        'MESSAGE_DOCTOR_ADD_TO_FAVORITE'   => 'Doctor successfully added to favorites',
        'MESSAGE_DOCTOR_REMOVE_FROM_FAVORITE_LIST' => 'Doctor successfully removed from favorites',
        'MESSAGE_DOCTOR_FAVORITE_LIST' => 'Doctor favorite list fetched successfully',
        'MESSAGE_DOCTOR_REGISTRATION_FORM_UPLOAD' => "Registration form upload successfully",
        'MESSAGE_APPOINTMENT_DETAILS' => 'Appointment details fetched successfully',
        'MESSAGE_FEED_DETAILS' => 'Feed details fetched successfully',
        'MESSAGE_FEED_LIKE_DETAILS' => 'Feed like details updated successfully',
        'MESSAGE_DOCTOR_FOLLOW' => 'The doctor has been :type successfully',
        'MESSAGE_FEED_COMMENT_DETAILS' => 'Feed comment details updated successfully',
        'MESSAGE_DOCTOR_TIME_SLOT_LIST' => 'Doctor time slot fetched successfully',
        'MESSAGE_PATIENT_LIST' => 'Patient list fetched successfully',
        'MESSAGE_APPOINMENT_BOOK' => 'Appoinment booked successfully',
        'MESSAGE_PATIENT_BOOKING_LIST' => 'Patient bookings list fetched successfully',
        'MESSAGE_RESCHEDULE_BOOKING' => 'Appointment rescheduled successfully',
        'MESSAGE_DOCTOR_BOOKING_FORM' => 'Doctor appointment booking form fetched successfully',
        'MESSAGE_PATIENT_RESCHEDULE_ACCEPT' => 'Appointment details updated successfully',
        'MESSAGE_DOCTOR_RATING_ADDED' => 'Doctor review added successfully',
        'MESSAGE_DOCTOR_RATING_FETCH' => "Doctor review fetched successfully",
        'MESSAGE_CANCEL_BOOKING' => "Appointment has been cancelled successfully",
        'MESSAGE_DOCTOR_LOCATION_DATA_FETCH' => 'Doctor location fetched successfully',
        'MESSAGE_FEED_ACTION_SUCCESSFULLY' => 'Feed has been :type successfully',
        'MESSAGE_PATIENT_DETAILS_ADD' => "Patient details added successfully",
        'MESSAGE_PATIENT_DETAILS_UPDATE' => "Patient details updated successfully",
        'MESSAGE_REVIEW_LIST' => "Review list fetch successfully",
        'MESSAGE_SENT' => 'Message sent successfully',
        'MESSAGE_CHAT_MESSAGE_FETCH' => 'Message fetched successfully',
        'MESSAGE_CHAT_REQUEST_UPDATE' => 'Chat request has been sent to doctor. Once doctor accepts it, you shall be able to start chatting !!',
        'MESSAGE_CHAT_REQUEST_LIST_FETCH' => 'Chat request list fetched successfully',
        'MESSAGE_CHAT_REQUEST_DELETE' => 'Chat deleted successfully',
        'NO_MESSAGE_FOUND' => 'No message available',
        'MESSAGE_FAQ_CATEGORY_LISTING' => 'FAQ Categories listed successfully',
        'MESSAGE_FAQ_LISTING' => 'FAQs listed successfully',
        'NOTIFICATIONS_DATA_FETCH'=> 'Notifications fetched successfully',
        'MESSAGE_CHAT_MESSAGE_READ' => 'Messages are read successfully',
        'MESSAGE_CITY_STATE_ID' => 'City/State ID found successfully',
        'MESSAGE_CITY_STATE_ID_NOT_FOUND' => 'City/State ID not found',
        'MESSAGE_NOTIFICATION_STATUS_UPDATE' => 'Notification status updated successfully',
        'MESSAGE_LOCATION_FOUND' => 'Location Data fetched successfully',
        'MESSAGE_INVITATION_SENT' => 'Email send succcessfully',
        'MESSAGE_DOCTOR_INVITATION_EMAIL_EXIST' => 'This email has already been invited',
        'MESSAGE_DOCTOR_REGISTRATION_EMAIL_EXIST' => 'This email is already registered',
        'MESSAGE_TOP_SPECIALITY_FOUND' => 'Top Speciality Data fetched successfully',
        'MESSAGE_ADMIN_NOTIFIED' => 'Admin has been notified successfully',
    ],

    'error' => [
        'MESSAGE_INVALID_CREDENTIAL' => 'Invalid credentials, Please try again.',
        'NO_DATA_FOUND' => ':module record not found for given data. Please try again.',
        'OTP_LIMIT_EXCEED' => 'You have exceeds  3 times. try after one hour',
        'OTP_NOT_MATCH' => 'Please enter valid OTP.',
        'EMAIL_FORMAT' => 'Please enter valid email address.',
        'PHONE_FORMAT' => 'Please enter valid mobile number.',
        'INVALID_OTP' => 'Invalid OTP, Please try again.',
        'USER_NOT_VERIFIED' => 'Your account is not verified yet.',
        'TRY_AFTER_FEW_MINUTE' => 'Please try after 5 minutes.',
        'USER_NOT_ACTIVATED' => 'User is not active. Please contact administrator.',
        'PASSWORD_NOT_MATCHED' => 'The old password does not match. Please try again.',
        'PROFILE_DATA_NOT_FOUND' => 'Record not found for given data. Please try again.',
        'USER_NOT_ACTIVATED' => 'User is not active. Please contact administrator.',
        'THERE_IS_SOME_ERROR' => 'There is some error, Please try again',
        'MESSAGE_CALENDAR_BLOCK_SLOT_ERROR' => 'You can not block this slot as there is already a booked appointment for the slot',
    ]

];
