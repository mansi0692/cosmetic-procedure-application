<?php

return [

    'label' => [
        'EMAIL_OR_PHONE' => 'Email Address/Phone Number',
        'PASSWORD' => 'Password',
        'FORGOT_PASSWORD' => 'Forgot Password',
        'LOGIN' => 'Login',
        'UPDATE' => 'Update',
        'CHANGE_PASSWORD' => 'Change password',
        'CURRENT_PASSWORD' => 'Current password',
        'NEW_PASSWORD' => 'New password',
        'CONFIRM_PASSWORD' => 'Confirm password',
        'ARE_YOU_SURE_TO_DELETE_THIS_RECORD' => 'Are you sure, you want to delete this record?',
        'CATEGORY_SAVED_SUCCESSFULLY' => 'Category saved successfully',
        'CATEGORY_UPDATED_SUCCESSFULLY' => 'Category updated successfully',
    ],

    'success' => [
        'PASSWORD_CHANGED' => 'Password has been changed successfully.',
        'DOCTOR_ADMIN_SAVED_SUCCESSFULLY' => 'Doctor admin saved successfully',
        'PATIENT_ADMIN_SAVED_SUCCESSFULLY' => 'Patient admin saved successfully',
        'DOCTOR_ADMIN_UPDATED_SUCCESSFULLY' => 'Doctor admin updated successfully',
        'PATIENT_ADMIN_UPDATED_SUCCESSFULLY' => 'Patient admin updated successfully',
    ],

    'errors' => [
        'INVALID_EMAIL_PASSWORD' => 'Invalid email or password',
        'NOT_ALLOWED_TO_ACCESS_PAGE' => 'You are not allowed to access',
        'THERE_IS_SOME_ERROR' => 'There is some error',
    ]

];
