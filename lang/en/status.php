<?php

return [
    'confirmation_pending' => 'confirmation pending',
    'confirmed' => 'confirmed',
    'completed' => 'completed',
    'cancelled' => 'cancelled',
    'reschedule_requested' => 'reschedule requested',
    'waiting' => 'waiting',
    'no_appointment_schedule' => 'no appointment schedule',
    'accepted' => 'accepted',
    'rejected' => 'rejected',
    'expired' => 'expired',
    'pending' => 'pending',
    'cancelled_as_booked_by_another_doctor' => 'cancelled as booked by another doctor'
];