<?php

return [
    'confirmation_pending' => 'التأكيد معلق',
    'confirmed' => 'تم تأكيد',
    'completed' => 'منجز',
    'cancelled' => 'ألغيت',
    'reschedule_requested' => 'طلبت إعادة الجدولة',
    'waiting' => 'انتظار',
    'no_appointment_schedule' => 'لا يوجد جدول مواعيد',
    'accepted' => 'وافقت',
    'rejected' => 'مرفوض',
    'expired' => 'منتهية الصلاحية',
    'pending' => 'ريثما',
    'cancelled_as_booked_by_another_doctor' => 'تم إلغاؤه حسب حجز طبيب آخر'
];