<?php

return [

    'label' => [
        'EMAIL_OR_PHONE' => 'عنوان البريد الإلكتروني/رقم الهاتف',
        'PASSWORD' => 'كلمة المرور',
        'FORGOT_PASSWORD' => 'هل نسيت كلمة السر',
        'LOGIN' => 'تسجيل الدخول',
        'UPDATE' => 'تحديث',
        'CHANGE_PASSWORD' => 'غير كلمة السر',
        'CURRENT_PASSWORD' => 'كلمة المرور الحالية',
        'NEW_PASSWORD' => 'كلمة السر الجديدة',
        'CONFIRM_PASSWORD' => 'تأكيد كلمة المرور',
        'ARE_YOU_SURE_TO_DELETE_THIS_RECORD' => 'هل أنت متأكد، أنك تريد حذف هذا السجل؟',
        'CATEGORY_SAVED_SUCCESSFULLY' => 'تم حفظ الفئة بنجاح',
        'CATEGORY_UPDATED_SUCCESSFULLY' => 'تم تحديث الفئة بنجاح',
    ],

    'success' => [
        'PASSWORD_CHANGED' => 'تم تغيير كلمة المرور بنجاح.',
        'DOCTOR_ADMIN_SAVED_SUCCESSFULLY' => 'تم حفظ مشرف الطبيب بنجاح',
        'PATIENT_ADMIN_SAVED_SUCCESSFULLY' => 'تم حفظ مسؤول المريض بنجاح',
        'DOCTOR_ADMIN_UPDATED_SUCCESSFULLY' => 'تم تحديث مشرف الطبيب بنجاح',
        'PATIENT_ADMIN_UPDATED_SUCCESSFULLY' => 'تم تحديث مشرف المريض بنجاح',
    ],

    'errors' => [
        'INVALID_EMAIL_PASSWORD' => 'البريد الإلكتروني أو كلمة السر خاطئة',
        'NOT_ALLOWED_TO_ACCESS_PAGE' => 'لا يسمح لك بالوصول',
        'THERE_IS_SOME_ERROR' => 'هناك خطأ ما',
    ]

];
