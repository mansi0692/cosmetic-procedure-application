<?php

return [
    'success' => [
        'MESSAGE_NO_DATA_FOUND' => 'Datos no encontrados',
        'MESSAGE_CITY_LISTING' => 'Ciudad listada con éxito',
        'MESSAGE_NO_CITY_FOUND' => 'Ciudad no encontrada',
        'MESSAGE_COUNTRY_LISTING' => 'País listado con éxito',
        'MESSAGE_NO_COUNTRY_FOUND' => 'País no encontrado',
        'LOGIN_SUCCESS' => 'Inicio de sesión exitosa.',
        'REGISTRATION_SUCCESS' => ':user_type registrado correctamente.',
        'OTP_SENT' => 'OTP ha sido enviado con éxito.',
        'OTP_VERIFIED' => 'OTP verificado con éxito.',
        'PASSWORD_RESET' => 'La contraseña se ha restablecido con éxito.',
        'LOGOUT_SUCCESS' => 'El usuario cerró la sesión con éxito',
        'MESSAGE_DOCTOR_LISTING' => 'El doctor aparece con éxito',
        'MESSAGE_APPOINTMENT_LISTING' => 'Cita listada exitosamente',
        'MESSAGE_FEED_LISTING' => 'Feed enumerado con éxito',
        'MESSAGE_NO_DOCTOR_FOUND' => 'Doctor no encontrado',
        'MESSAGE_STATE_LISTING' => 'Estado enumerado con éxito',
        'MESSAGE_NO_STATE_FOUND' => 'Estado no encontrado',
        'MESSAGE_DOCTOR_PROFILE_FOUND' => 'Perfil médico encontrado con éxito',
        'MESSAGE_NO_DOCTOR_PROFILE_FOUND' => 'Perfil médico no encontrado',
        'MESSAGE_DOCTOR_REGISTERED_SUCCESSFULLY' => 'Doctor registrado exitosamente',
        'MESSAGE_OCCUPATION_LISTING' => 'Ocupación listada con éxito',
        'MESSAGE_NO_OCCUPATION_FOUND' => 'Ocupación no encontrada',
        'SAVE_PROFILE_SUCCESS' => 'El perfil se ha guardado con éxito',
        'SAVE_OVERVIEW_SUCCESS' => 'La descripción general se ha guardado correctamente',
        'MESSAGE_DOCTOR_PROFILE_SETTINGS_UPDATED_SUCCESSFULLY' => 'La configuración del perfil del médico se actualizó correctamente',
        'MESSAGE_DOCTOR_LOCATION_DATA_SAVE' => 'La ubicación del médico se guardó correctamente',
        'MESSAGE_DOCTOR_LOCATION_DATA_UPDATE' => 'Ubicación del médico actualizada con éxito',
        'MESSAGE_DATA_FOUND' => 'Datos encontrados con éxito',
        'MESSAGE_PROFILE_INFORMATION' => 'Información de perfil obtenida con éxito.',
        'MESSAGE_OCCUPATION_LISTING' => 'Ocupación listada con éxito',
        'MESSAGE_NO_OCCUPATION_FOUND' => 'Ocupación no encontrada',
        'MESSAGE_CATEGORY_LISTING' => 'Categorías listadas con éxito.',
        'MESSAGE_NO_CATEGORY_FOUND' => 'Las categorías no están disponibles para este tipo.',
        'PROFILE_DATA_DELETE_SUCCESS' => 'El registro ha sido eliminado con éxito.',
        'MESSAGE_SPECIALITY_FOUND' => 'Especialidad listada con éxito',
        'MESSAGE_SPECIALITY_NOT_FOUND' => 'Especialidad no encontrada',
        'PASSWORD_CHANGED' => 'La contraseña ha sido cambiada con éxito.',
        'MESSAGE_DOCTOR_MEDIA_UPLOADED_SUCCESSFULLY' => 'Los medios del médico se cargaron con éxito',
        'MESSAGE_DOCTOR_MEDIA_UPDATED_SUCCESSFULLY' => 'Doctor media actualizado con éxito',
        'MESSAGE_DOCTOR_PROOF_UPLOADED_SUCCESSFULLY' => 'Prueba médica cargada con éxito',
        'MESSAGE_MAIL_SENT_SUCCESSFULLY' => 'El administrador ha sido notificado al respecto. Nos pondremos en contacto con usted lo antes posible.',
        'MESSAGE_PATIENT_ACCOUNT_CANCELLED' => 'El administrador ha sido notificado sobre la cancelación. Una vez que el administrador lo apruebe, su cuenta se suspenderá de forma permanente.',
        'MESSAGE_PATIENT_UPDATE' => 'Perfil del paciente actualizado con éxito',
        'MESSAGE_PATIENT_GET_PROFILE' => 'Perfil de paciente encontrado con éxito',
        'MESSAGE_CONTACT_US_PATIENT' => 'El administrador ha sido notificado al respecto. Nos pondremos en contacto con usted lo antes posible.',
        'MESSAGE_CMS_CONTENT' => 'Contenido CMS encontrado con éxito',
        'MESSAGE_SUCCESSFULLY_RECEIVED_YOU_JOIN_REQUEST' => 'Recibida con éxito su solicitud de unirse a la lista de espera',
        'MESSAGE_CALENDAR_BLOCK_SLOT' => 'Ranura actualizada con éxito',
        'MESSAGE_DOCTOR_WORKING_TIME_UPDATED' => 'Horario de trabajo actualizado con éxito',
        'MESSAGE_DOCTOR_WORKING_TIME_FETCHED' => 'Tiempo de trabajo obtenido con éxito',
        'MESSAGE_DOCTOR_WORKING_TIME_DELETED' => 'El tiempo de trabajo se eliminó con éxito',
        'MESSAGE_DOCTOR_APPOINTMENT_FETCHED' => 'Cita con el doctor obtenido con éxito',
        'MESSAGE_SUCCESSFULLY_RECEIVED_YOU_APPOINMENT_REQUEST' => 'Recibida con éxito su solicitud de cita',
        'MESSAGE_ADD_FAVORITE_GROUP'   => 'Grupo favorito de médicos creado con éxito',
        'MESSAGE_UPDATE_FAVORITE_GROUP' => 'Grupo favorito de médicos actualizado con éxito',
        'MESSAGE_DELETE_FAVORITE_GROUP' => 'El grupo favorito del doctor se eliminó con éxito',
        'MESSAGE_FAVORITE_GROUP_LIST' => 'El grupo favorito del doctor se obtuvo con éxito',
        'MESSAGE_DOCTOR_ADD_TO_FAVORITE'   => 'Doctor agregado exitosamente a favoritos',
        'MESSAGE_DOCTOR_REMOVE_FROM_FAVORITE_LIST' => 'Doctor eliminado con éxito de favoritos',
        'MESSAGE_DOCTOR_FAVORITE_LIST' => 'La lista de favoritos de los médicos se obtuvo con éxito',
        'MESSAGE_DOCTOR_REGISTRATION_FORM_UPLOAD' => "Formulario de registro subido con éxito",
        'MESSAGE_APPOINTMENT_DETAILS' => 'Detalles de la cita obtenidos con éxito',
        'MESSAGE_FEED_DETAILS' => 'Detalles del feed obtenidos con éxito',
        'MESSAGE_FEED_LIKE_DETAILS' => 'Feed como detalles actualizados con éxito',
        'MESSAGE_DOCTOR_FOLLOW' => 'El médico ha sido :type con éxito.',
        'MESSAGE_FEED_COMMENT_DETAILS' => 'Detalles de comentario de feed actualizados con éxito',
        'MESSAGE_DOCTOR_TIME_SLOT_LIST' => 'La franja horaria del médico se ha obtenido correctamente',
        'MESSAGE_PATIENT_LIST' => 'Lista de pacientes obtenida con éxito',
        'MESSAGE_APPOINMENT_BOOK' => 'Cita reservada con éxito',
        'MESSAGE_PATIENT_BOOKING_LIST' => 'Lista de reservas de pacientes recuperada con éxito',
        'MESSAGE_RESCHEDULE_BOOKING' => 'Cita reprogramada con éxito',
        'MESSAGE_DOCTOR_BOOKING_FORM' => 'Formulario de reserva de cita médica obtenido con éxito',
        'MESSAGE_PATIENT_RESCHEDULE_ACCEPT' => 'Detalles de la cita actualizados con éxito',
        'MESSAGE_DOCTOR_RATING_ADDED' => 'Revisión del médico agregada con éxito',
        'MESSAGE_DOCTOR_RATING_FETCH' => "Revisión del médico obtenida con éxito",
        'MESSAGE_CANCEL_BOOKING' => "La cita ha sido cancelada con éxito",
        'MESSAGE_DOCTOR_LOCATION_DATA_FETCH' => 'Ubicación del médico obtenida con éxito',
        'MESSAGE_FEED_ACTION_SUCCESSFULLY' => 'El feed se ha :type correctamente',
        'MESSAGE_PATIENT_DETAILS_ADD' => "Detalles del paciente agregados con éxito",
        'MESSAGE_PATIENT_DETAILS_UPDATE' => "Datos del paciente actualizados con éxito",
        'MESSAGE_REVIEW_LIST' => "Lista de revisión recuperada con éxito",
        'MESSAGE_SENT' => 'Mensaje enviado con éxito',
        'MESSAGE_CHAT_MESSAGE_FETCH' => 'Mensaje obtenido con éxito',
        'MESSAGE_CHAT_REQUEST_UPDATE' => 'La solicitud de chat se ha enviado al médico.. Una vez que la doctora lo acepte, podrás comenzar a chatear !!',
        'MESSAGE_CHAT_REQUEST_LIST_FETCH' => 'Lista de solicitudes de chat obtenida con éxito',
        'MESSAGE_CHAT_REQUEST_DELETE' => 'Chat eliminado con éxito',
        'NO_MESSAGE_FOUND' => 'No hay mensajes disponibles',
        'MESSAGE_FAQ_CATEGORY_LISTING' => 'FAQ categorías listadas con éxito',
        'MESSAGE_FAQ_LISTING' => 'FAQs enumerado con éxito',
        'MESSAGE_CHAT_MESSAGE_READ' => 'Los mensajes se leen correctamente',
        'MESSAGE_CITY_STATE_ID' => 'ID de ciudad/estado encontrado con éxito',
        'MESSAGE_CITY_STATE_ID_NOT_FOUND' => 'ID de ciudad/estado no encontrado',
        'MESSAGE_NOTIFICATION_STATUS_UPDATE' => 'Estado de notificación actualizado con éxito',
        'MESSAGE_LOCATION_FOUND' => 'Datos de ubicación obtenidos con éxito',
        'MESSAGE_INVITATION_SENT' => 'Correo electrónico enviado con éxito',
        'MESSAGE_DOCTOR_INVITATION_EMAIL_EXIST' => 'Este correo electrónico ya ha sido invitado.',
        'MESSAGE_DOCTOR_REGISTRATION_EMAIL_EXIST' => 'Este correo electrónico ya está registrado',
        'MESSAGE_TOP_SPECIALITY_FOUND' => 'Principales datos de especialidad obtenidos con éxito',
        'MESSAGE_ADMIN_NOTIFIED' => 'El administrador ha sido notificado con éxito',
    ],

    'error' => [
        'MESSAGE_INVALID_CREDENTIAL' => 'Credenciales no válidas, inténtalo de nuevo.',
        'NO_DATA_FOUND' => ':module registro no encontrado para datos dados. Inténtalo de nuevo.',
        'OTP_LIMIT_EXCEED' => 'Tienes excede 3 veces. intente despues de una hora',
        'OTP_NOT_MATCH' => 'Ingrese una OTP válida.',
        'EMAIL_FORMAT' => 'Por favor ingrese una dirección de correo electrónico válida.',
        'PHONE_FORMAT' => 'Por favor, introduzca un número de móvil válido.',
        'INVALID_OTP' => 'OTP no válido, inténtalo de nuevo.',
        'USER_NOT_VERIFIED' => 'Tu cuenta no se ha verificado aún.',
        'TRY_AFTER_FEW_MINUTE' => 'Intente después de 5 minutos.',
        'USER_NOT_ACTIVATED' => 'El usuario no está activo. Póngase en contacto con el administrador.',
        'PASSWORD_NOT_MATCHED' => 'La contraseña anterior no coincide. Inténtalo de nuevo.',
        'PROFILE_DATA_NOT_FOUND' => 'Registro no encontrado para datos dados. Inténtalo de nuevo.',
        'USER_NOT_ACTIVATED' => 'El usuario no está activo. Póngase en contacto con el administrador.',
        'THERE_IS_SOME_ERROR' => 'There is some error, Please try again',
        'MESSAGE_CALENDAR_BLOCK_SLOT_ERROR' => 'No puede bloquear este espacio porque ya hay una cita reservada para el espacio',
    ]

];
