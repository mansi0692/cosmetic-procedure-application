<?php

return [
    'confirmation_pending' => 'pendiente de confirmación',
    'confirmed' => 'confirmada',
    'completed' => 'terminada',
    'cancelled' => 'cancelada',
    'reschedule_requested' => 'reprogramación solicitada',
    'waiting' => 'esperando',
    'no_appointment_schedule' => 'sin horario de citas',
    'accepted' => 'aceptada',
    'rejected' => 'rechazada',
    'expired' => 'caducada',
    'pending' => 'pendiente',
    'cancelled_as_booked_by_another_doctor' => 'cancelado como reservado por otro médico'
];