<?php

return [
    'confirmation_pending' => 'confirmation en attente',
    'confirmed' => 'confirmé',
    'completed' => 'complété',
    'cancelled' => 'annulé',
    'reschedule_requested' => 'report demandé',
    'waiting' => 'attendre',
    'no_appointment_schedule' => 'pas de rendez-vous',
    'accepted' => 'accepté',
    'rejected' => 'rejetée',
    'expired' => 'expiré',
    'pending' => 'en attendant',
    'cancelled_as_booked_by_another_doctor' => 'annulé car réservé par un autre médecin'
];