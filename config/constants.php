<?php

return [

    'roles' => [
        'super_admin' => 1,
        'doctor' => 2,
        'patient' => 3,
        'doctor_admin' => 4,
        'patient_admin' => 5
    ],

    'status' => [
        'active' => 1,
        'inactive' => 0
    ],
    'otp_limit' => 3,
    'country_code' => '+1',
    's3' => [
        'uploadUrl' => 'uploads/',
    ],
    // 'get_file_path' => 'https://d1p3522o1cemir.cloudfront.net/uploads/',
    'get_file_path' => 'https://d3n504zwkllrix.cloudfront.net/uploads/',
    'patient_cancel_request' => [
        'PENDING' => 0,
        'APPROVED' => 1
    ],
    'doctor_feature_limit' => 12,

    'main_categories' => [
        1 => 'certification',
        2 => 'occupation',
        3 => 'specialty',
        4 => 'symptom',
        5 => 'language',
        6 => 'distance',
        7 => 'procedure',
    ],

    'faq_categories' => [
        1 => 'About YUME',
        2 => 'Data & Privacy',
        3 => 'Appointments',
        4 => 'Account',
        5 => 'Review',
    ],

    'days' => [
        'Mon' => 'monday',
        'Tue' => 'tuesday',
        'Wed' => 'wednesday',
        'Thu' => 'thursday',
        'Fri' => 'friday',
        'Sat' => 'saturday',
        'Sun' => 'sunday',
    ],

    'appointment_status' => [
        0 => 'confirmation_pending',
        1 => 'confirmed',
        2 => 'completed',
        3 => 'cancelled',
        4 => 'reschedule_requested',
        5 => 'waiting',
        6 => 'no_appointment_schedule',
        7 => 'accepted',
        8 => 'rejected',
        9 => 'expired',
        10 => 'pending',
        11 => 'cancelled_as_booked_by_another_doctor'
    ],

    'appointment_status_notification' => [
        'confirmation_pending' => 'confirmation pending',
        'confirmed' => 'confirmed',
        'completed' => 'completed',
        'cancelled' => 'cancelled',
        'reschedule_requested' => 'reschedule requested',
        'waiting' => 'waiting',
        'no_appointment_schedule' => 'no appointment schedule',
        'accepted' => 'accepted',
        'rejected' => 'rejected',
        'expired' => 'expired',
        'pending' => 'pending',
        'cancelled_as_booked_by_another_doctor' => 'cancelled as booked by another doctor'
    ]
];
